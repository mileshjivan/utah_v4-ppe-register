/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author Smabe
 */
public class S5_2_PPE_Register_TestSuite extends BaseClass {

    static TestMarshall instance;

    public S5_2_PPE_Register_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.coreBeta;

        //*******************************************
    }

       @Test
    public void S5_2_PPE_Type_Library_QA01S5_2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\S5_2_PPE_Type_Library_QA01S5_2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR1_Capture_PPE_Register_Main_Scenario
    @Test
    public void FR1_Capture_PPE_Register_Main_Scenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\PPE Register v5.2\\FR1-Capture PPE Register - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_Capture_PPE_Register_Alternate_Scenario_1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\PPE Register v5.2\\FR1-Capture PPE Register - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_Capture_Permit_To_Work_Alternate_Scenario_2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\PPE Register v5.2\\FR1-Capture PPE Register - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_Capture_Permit_To_Work_Optional_Scenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\PPE Register v5.2\\FR1-Capture PPE Register - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR2-Capture Items Issued - Main Scenario
    @Test
    public void FR2_Capture_Items_Issued_Main_Scenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\PPE Register v5.2\\FR2-Capture Items Issued - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR3-Sign off PPE Register - Main Scenario
    @Test
    public void FR3_Sign_off_PPE_Register_Main_Scenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\PPE Register v5.2\\FR3-Sign off PPE Register - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR3_Sign_off_PPE_Register_Alternate_Scenario_1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\PPE Register v5.2\\FR3-Sign off PPE Register - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR3_Sign_off_PPE_Register_Alternative_Scenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\PPE Register v5.2\\FR3-Sign off PPE Register - Alternative Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR4-View Reports - Main Scenario
    @Test
    public void FR4_View_Reports_Main_Scenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\PPE Register v5.2\\FR4-View Reports - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

}
