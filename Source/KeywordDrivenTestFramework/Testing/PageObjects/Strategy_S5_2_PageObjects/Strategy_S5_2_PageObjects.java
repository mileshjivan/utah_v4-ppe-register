/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Strategy_S5_2_PageObjects;

/**
 *
 * @author smabe
 */
public class Strategy_S5_2_PageObjects
{

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String navigate_EHS()
    {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }

    public static String strategic_objectives()
    {
        return "//label[text()='Strategic Objectives']";
    }

    public static String strategy_Add_Button()
    {
        return "//div[@id='btnActAddNew']";
    }

    public static String process_flow()
    {
        return "//div[@id='btnProcessFlow_form_953FABAA-1574-418D-9D83-2CEFB1598C56']";
    }

    public static String process_flow_objectives()
    {
        return "//div[@id='btnProcessFlow_form_87BE3A0E-E088-4D81-A4D8-6DC7CE9A3DA2']";
    }

    public static String Strategy()
    {
        return "//div[@id='control_9C61FC9D-7CA2-4ADE-8874-BB94676F8EC2']//input";
    }

    public static String Purpose()
    {
        return "//div[@id='control_899465AC-1BD4-4A1C-A00E-AF0AD5BAEEE0']//textarea";
    }

    public static String Mission_statement()
    {
        return "//div[@id='control_B9A90B3A-564A-4677-B6F8-B18AA677CF49']//textarea";
    }

    public static String validateSave()
    {
        return "//div[@class='ui floating icon message transition visible']";
    }

    public static String saveWait2()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }

    public static String SaveBtn()
    {
        return "//div[@id='btnSave_form_953FABAA-1574-418D-9D83-2CEFB1598C56']";
    }

    public static String Strategy_owner_option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    
      public static String Responsible_person_option_targets(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')])[2]";
    }

    public static String Strategy_owner_drop_down()
    {
        return "//div[@id='control_3EA10D6E-C008-4E88-BD1D-95D5E120F08F']//ul";
    }

    public static String Global_Company_Drop_Down()
    {
        return "(//li[@title='Global Company']//a)[1]";
    }
    
    public static String Visible_Global_Company_Drop_Down()
    {
        return "//div[contains(@class, 'transition visible')]//li[@title='Global Company']//a";
    }

    public static String business_UnitSelect(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String business_unit_drop_down()
    {
        return "//div[@id='control_433CDAF3-4AA4-4996-BEEA-9043E9D7E1AD']//ul";
    }

    public static String Impact_type_drop_down()
    {
        return "//div[@id='control_7D3DE117-588A-4F36-80E1-F61013AACC3C']//ul";
    }

    public static String Impact_type_select_all()
    {
        return "//div[@id='control_7D3DE117-588A-4F36-80E1-F61013AACC3C']//span//b[@class='select3-all']";
    }

    public static String Project_link()
    {
        return "//div[@id='control_E22A7BA3-7A73-45BE-AB2A-DF6A82A41E81']";
    }

    public static String Project_drop_down()
    {
        return "//div[@id='control_905BF122-090A-4CB8-A9CA-7A5E7AF9FD10']//ul";
    }

    public static String supporting_tab()
    {

        return "//div[text()='Supporting Documents']";
    }

    public static String supporting_tab_2()
    {
        return "(//div[text()='Supporting Documents'])[2]";
    }

    public static String linkbox()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String linkbox_2()
    {
        return "(//b[@original-title='Link to a document'])[2]";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlTitle']";
    }

    public static String urlAddButton()
    {
        return "//div[@class='confirm-popup popup']//div[contains(text(),'Add')]";
    }

    public static String SaveSupportingDocuments_SaveBtn()
    {
        return "//div[@id='control_8DBCD46A-2680-4E12-AC1D-F344D5EF34D6']";
    }

    public static String SaveSupportingDocuments_SaveBtn_Objectives()
    {
        return "//div[@id='control_910E8C90-8E4A-4BFD-A5CB-6EEF64B9111A']";
    }

    public static String Strategic_Objectives_Add()
    {
        return "//div[@id='control_0F1BA18A-EEFA-4975-AE3B-77C2B6D28091']//div[@id='btnAddNew']";
    }

    public static String Strategic_Objectives_Targets_Add()
    {
        return "//div[@id='control_5E272474-4A14-4E26-B008-A6E09B2412E8']//div[@id='btnAddNew']";
    }

    public static String Objectives()
    {
        return "//div[@id='control_816B3C96-66CF-42B7-A12D-7D62EA5E0846']//textarea";
    }

    public static String Objectives_category_drop_down()
    {
        return "//div[@id='control_C456CA31-0061-4121-8D19-1A8264719288']//ul";
    }

    public static String Objectives_owner_drop_down()
    {
        return "//div[@id='control_2BFEE57E-670E-45F3-B6C9-CACB585E5117']//ul";
    }

    public static String Link_Process_Tab()
    {
        return "//div[@id='control_8C2EABDC-85E0-43FD-B183-11A00D59419E']//div[text()='Linked Process/Activity']";
    }

    public static String Link_Performance_Tab()
    {
        return "//div[@id='control_8C2EABDC-85E0-43FD-B183-11A00D59419E']//div[text()='Linked Performance Indicators']";
    }

    public static String Link_Process_Select_All()
    {
        return "//div[@id='control_37937E90-1C78-4FE4-859D-D4C5B9E0D0D5']//span//b[@class='select3-all']";
    }

    public static String SaveBtnProcess()
    {
        return "//div[@id='btnSave_form_87BE3A0E-E088-4D81-A4D8-6DC7CE9A3DA2']";
    }

    public static String Link_Performance_Select_All()
    {
        return "//div[@id='control_5DB6D462-C101-4FCA-834F-7BB24E0CE3B6']//span//b[@class='select3-all']";
    }

    public static String process_flow_Targets()
    {
        return "//div[@id='btnProcessFlow_form_40BF8BBF-8157-4A5C-835E-1BCC3C5AD875']";
    }

    public static String Action_description()
    {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String Action_due_date()
    {
       return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }

    public static String SaveBtnTargets()
    {
        return"//div[@id='btnSave_form_40BF8BBF-8157-4A5C-835E-1BCC3C5AD875']";
    }

    public static String Responsible_person_drop_down()
    {
        return"//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//ul";
    }

    public static String Department_responsible_drop_down()
    {
       return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//ul";
    }

    public static String close_window()
    {
       return "(//div[contains(@class, 'transition visible')]//i[@class='close icon cross'])[4]";
    }
    
     public static String close_window_record()
    {
       return "(//div[contains(@class, 'transition visible')]//i[@class='close icon cross'])[3]";
    }

    public static String reportsBtn()
    {
        return"//div[@id='btnReports']";
    }
    
      public static String viewReportsIcon() {
        return "//span[@title='View report ']";
    }

       public static String continueBtn() {
        return "//div[@id='btnConfirmYes']";
    }

    public static String StrategyReportHeader()
    {
        return"//div[text()='Strategy Report']";
    }
    
     public static String viewFullReportsIcon() {
        return "//span[@title='Full report ']";
    }
         public static String BusinessUnitHeader() {
        return "//div[text()='Business unit']";
    }
}
