/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Communication_Groups_V5_2_PageObjects;

/**
 *
 * @author skhumalo
 */
public class Communication_Groups_PageObjects {
    public static String failed(){
        return "(//div[@id='txtAlert'])[2]";
    }
    public static String inspection_Record_Saved_popup(){
        return "//div[@id='divForms']//div[contains(text(),'Record saved')]";
    }
    public static String CommunicationGroup_ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_7AE45E04-0972-4D78-B41B-E0DD4ED626A1']";
    }
    public static String CommunicationGroups_Save() {
        return "//div[@id='btnSave_form_7AE45E04-0972-4D78-B41B-E0DD4ED626A1']";
    }
    public static String Communication_Groups_Add() {
        return "//div[@id='btnActAddNew']";
    }
    public static String CustomEmails() {
        return "//div[@id='control_445BFA9F-9C42-4D96-8B6B-008391DBE37F']//textarea";
    }
    public static String Environmen_Health_Safety_Module(){
        return "//label[text()='Environment, Health & Safety']";
    }
    public static String Communication_Module(){
        return "//label[text()='Communications']";
    }
    public static String CommunicationGroup_Module(){
        return "//label[text()='Communication Groups']";
    }
    public static String GroupName() {
        return "(//div[@id='control_B61273B1-3490-4A4F-B16D-62EEEFAAD872']//input)[1]";
    }
    public static String JobProfiles_Dropdown() {
        return "//div[@id='control_80FB7989-C47F-492F-9DCB-8F00781390DE']";
    }
    public static String JobProfiles_SelectAll() {
        return "//div[@id='control_80FB7989-C47F-492F-9DCB-8F00781390DE']//b[@class='select3-all']";
    }
    public static String SupportMaintenance_Module() {
        return "//div[@id='group_7d4d24eb-e775-4c99-a049-1c12ec7e1d75']";
    }
    public static String saveWait() {
        return "//div[@class='ui inverted dimmer active']";
    }
    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }
    public static String validateSave(){
        return "//div[@class='ui floating icon message transition visible']";
    }
    
}
