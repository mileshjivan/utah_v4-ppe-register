/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Emergency_Response_Team_S5_2_PageObject;

/**
 *
 * @author SKhumalo
 */
public class Emergency_Response_Team_PageObject {
    public static String ContactNumber() {
        return "(//div[@id='control_79F95CF9-F0EB-447A-8878-B91B111163B3']//input)[1]";
    }
    public static String ERT_Register() {
        return "//div[text()='Emergency Response Team']";
    }
    public static String EmailAddress() {
        return "(//div[@id='control_B6F2BD1D-781B-4FB3-B6B9-E9C00DBB00E9']//input)[1]";
    }
    public static String Emergency_Response_Teams() {
        return "//label[text()='Emergency Response Teams']";
    }
    public static String IPCC_Codes() {
        return "//label[text()='IPCC Codes']";
    }
    public static String OperateMaintenance() {
        return "//label[text()='Operate Maintenance']//..//i";
    }
    public static String MonitoringMaintenance() {
        return "//label[text()='Monitoring Maintenance']//..//i";
    }
    public static String RTM_CloseButton() {
        return "(//div[@id='form_33DFD268-A46C-4D7F-80C7-14D5F5530C34']//i[@class='close icon cross'])[1]";
    }
    public static String TeamName_Header() {
        return "//div[text()='Team name']";
    }

    public static String navigate_Carbon_Footprint() {
        return "//label[text()='Carbon Footprint']";
    }
    public static String searchBtn() {
        return "//div[@id='searchOptions']//div[text()='Search']";
    }
    public static String viewFullReportsIcon() {
        return "//span[@title='Full report ']";
    }
    public static String reportsBtn() {
        return "//div[@id='btnReports']";
    }
    public static String viewReportsIcon() {
        return "//span[@title='View report ']";
    }
    public static String continueBtn() {
        return "//div[@id='btnConfirmYes']";
    }
    public static String RTM_Record(String data) {
        return "(//div[@id='control_5E46125E-0C20-451B-8019-9174EB0D51A7']//div//table)[3]//div[text()='" + data + "']";
    }
    public static String RTM_process_flow() {
        return "//div[@id='btnProcessFlow_form_33DFD268-A46C-4D7F-80C7-14D5F5530C34']";
    }
    public static String Response_Team_Add_Button() {
        return "//div[@id='control_5E46125E-0C20-451B-8019-9174EB0D51A7']//div[text()='Add']";
    }
    public static String Role() {
        return "(//div[@id='control_32F8C182-5D50-4B2B-93C4-F1D20848D03D']//input)[1]";
    }
    public static String SaveButton() {
        return "//div[@id='btnSave_form_FB5D671F-0F91-4CC2-96B4-A1DA0C94297D']";
    }
    public static String RTM_SaveButton() {
        return "//div[@id='btnSave_form_33DFD268-A46C-4D7F-80C7-14D5F5530C34']";
    }
    public static String SaveSupportingDocuments_SaveBtn() {
        return "//div[text()='Save supporting documents']";
    }
    public static String TeamName() {
        return "(//div[@id='control_FED97497-13D2-4446-A649-FAF35715B218']//input)[1]";
    }
    public static String Team_Tab() {
        return "//div[text()='Team']";
    }
    public static String businessUnit_Option1(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String businessUnit_Option(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[1]";
    }
    public static String businessUnit_dropdown() {
        return "//div[@id='control_367581CE-32CB-4923-9B24-1FDF8DC5747C']";
    }
    public static String fullName_Dropdown() {
        return "//div[@id='control_6EE2438D-2B28-4A11-8E8E-B81E57A86B27']";
    }
    public static String impactType_Option(String data){
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String impactType_Dropdown() {
        return "//div[@id='control_8DA377C4-295A-4FCC-B7B3-B2269151B655']";
    }
    public static String navigate_EHS() {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }
    public static String Add_Button() {
        return "//div[text()='Add']";
    }
    public static String process_flow() {
        return "//div[@id='btnProcessFlow_form_FB5D671F-0F91-4CC2-96B4-A1DA0C94297D']";
    }
    public static String validateSave() {
        return "//div[@class='ui floating icon message transition visible']";
    }
    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }
    public static String supporting_tab() {
        return "//div[text()='Supporting Documents']";
    }
    public static String linkbox() {
        return "//b[@original-title='Link to a document']";
    }
    public static String LinkURL() {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }
    public static String urlTitle() {
        return "//div[@class='confirm-popup popup']//input[@id='urlTitle']";
    }
    public static String urlAddButton() {
        return "//div[@class='confirm-popup popup']//div[contains(text(),'Add')]";
    }
    public static String iframeXpath() {
        return "//iframe[@id='ifrMain']";
    }
}
