/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects;
/**
 *
 * @author SKhumalo
 */
public class Incident_Management_PageObjects {

    public static String Action_DueDate() {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }

    public static String Actions_desc() {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String CommunicationDetails() {
        return "(//div[@id='control_5A57A682-9BB6-49F6-B58A-C7866621BB9E']//textarea)[1]";
    }

    public static String CommunicationGroup_Option(String data) {
        return "//div[@id='control_3B21FF22-08C4-4B27-9AD2-C116F0E94FB1'] //div[contains(@class, 'transition visible')]//a[contains(text(),'Company wide')]//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String CommunicationTopic() {
        return "(//div[@id='control_2E0071ED-1481-4707-ABE0-C0802831772B']//input)[1]";
    }

    public static String CreateCommunication_ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_C6B3A628-2B52-4445-842F-7A9BD7D3DCAC']";
    }

    public static String CreateEngagements_ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_693F051B-BD01-4FE3-B457-E1E13354A955']";
    }

    public static String Department_Resp_Option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String Department_Resp_Option1(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[1]";
    }

    public static String Department_Resp_dropdown() {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']";
    }

    public static String EngagementDescription() {
        return "//div[@id='control_447A0C59-9860-47BB-B91E-3BFC76CFAD44']//textarea";
    }

    public static String EngagementTitle() {
        return "(//div[@id='control_367E40D2-1918-43A6-900E-BD392E677916']//input)[1]";
    }

    public static String Findings_Actions_saveButton() {
        return "//div[@id='btnSave_form_CCC7B08A-9084-4470-922B-C10D6150CEE8']";
    }

    public static String IMFA_processFlow() {
        return "//div[@id='btnProcessFlow_form_CCC7B08A-9084-4470-922B-C10D6150CEE8']";
    }

    public static String IMF_processFlow() {
        return "//div[@id='btnProcessFlow_form_0B2C6ED4-CC58-477E-931E-7949371422D0']";
    }

    public static String IncidentManagement_CreateCommunication_CloseButton() {
        return "(//div[@id='form_C6B3A628-2B52-4445-842F-7A9BD7D3DCAC']//i[@class='close icon cross'])[1]";
    }

    public static String IncidentManagement_CreateCommunication_Gridview() {
        return "(//div[@id='control_56842E83-010F-467F-A82E-D5F959961AC6']//div//table)[3]";
    }

    public static String IncidentManagement_CreateEngagement_CloseButton() {
        return "(//div[@id='form_693F051B-BD01-4FE3-B457-E1E13354A955']//i[@class='close icon cross'])[1]";
    }

    public static String IncidentManagement_CreateEngagement_Gridview() {
        return "(//div[@id='control_1C42A19A-2F54-485B-8EEA-D4B7A14D13E4']//div//table)[3]";
    }

    public static String IncidentManagement_IncidentActions_CloseButton() {
        return "(//div[@id='form_EE164B84-29D5-424F-A584-A66FC3FE9BCA']//i[@class='close icon cross'])[1]";
    }

    public static String IncidentManagement_IncidentActions_Gridview() {
        return "(//div[@id='control_45F45C6F-AA39-494A-A64D-5EA19914C37C']//div//table)[3]";
    }

    public static String IncidentManagement_IncidentInvestigationFindings_CloseButton() {
        return "(//div[@id='form_0B2C6ED4-CC58-477E-931E-7949371422D0']//i[@class='close icon cross'])[1]";
    }

    public static String IncidentManagement_IncidentInvestigationFindings_Gridview() {
        return "(//div[@id='control_781A7295-A3B9-49BC-9CBA-5C18491C8649']//div//table)[3]";
    }

    public static String Incident_Investigation_Findings_Actions_add() {
        return "//div[@id='control_C313CB91-30D1-4C52-A657-8A23FCE29C35']//div[text()='Add']";
    }
    
    public static String Incident_Job_Safety_Analysis_ProcessFlow(){
        return "//div[@id='btnProcessFlow_form_EBF4B94D-9E08-49BD-9899-CD64DC78AE1D']";
    }

    public static String Incident_Related_Bowties_ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_E5D05DD0-7E8F-4A41-9077-0A1F8DB472E7']";
    }

    public static String Incident_SignOff_Tab() {
        return "//div[text()='4.Incident Sign Off']";
    }

    public static String InternalCommunicationTab() {
        return "//span[text()='Internal Communication']";
    }
    
    public static String CreateCommunition_Panel(){
        return "//div[text()='Create Communication']";
    }
    
    public static String CreateCommunition_AddButton(){
        return "//div[text()='Create Communication']//..//div[text()='Add']";
    }

    public static String InvestigationComments() {
        return "//div[@id='control_8D1AE953-65A5-4E95-A6D6-3237EA4A48B1']//textarea";
    }

    public static String InvestigationCompleted_Checkbox() {
        return "(//div[@id='control_1F2CA953-B448-4A17-B1D0-383EB46D1349']//div)[2]";
    }
    
    public static String RelatedCommunition_Panel(){
        return "//div[text()='Related Communication']";
    }
    
    public static String ExternalCommunicationTab() {
        return "//span[text()='External Communication']";
    }

    public static String CreateEngagements_Panel(){
        return "//div[text()='Create Engagements']";
    }
    
    public static String CreateEngagements_AddButton(){
        return "//div[text()='Create Engagements']//..//div[text()='Add']";
    }
    
    public static String RelatedEngagements_Panel(){
        return "//div[text()='Related Engagements']";
    }

    public static String RequireSignOff() {
        return "(//div[@id='control_EBAA15EE-C16D-4632-ABDC-B1ED9C19C934']//div)[2]";
    }

    public static String ResponsiblePerson_Option(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')])[2]";
    }

    public static String ResponsiblePerson_dropdown() {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']";
    }
    
    public static String RiskAssessment_ProcessFlow(){
        return "//div[@id='btnProcessFlow_form_3A80F1F4-AE90-47B2-B9BB-0C920589C04D']";
    }
    public static String CauseAnalysis_Dropdown() {
        return "//div[@id='control_14265454-B46A-46E9-8864-615AF38545F8']";
    }

    public static String CauseAnalysis_Option(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')])[2]";
    }

    public static String CauseSelection_Dropdown() {
        return "//div[@id='control_64F83C0A-F990-4A81-806D-04E0C8AF4DB1']";
    }

    public static String CauseSelection_Option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String Description() {
        return "//div[@id='control_4189060F-9B25-4FD2-BE5C-5EEA2A6A1F50']//textarea";
    }

    public static String ImmediateCause_Comments() {
        return "(//div[@id='control_3B5C13F3-4B60-4D50-AEC9-3A99DACDE35A']//input)[1]";
    }

    public static String InappropriateBehaviour_Dropdown() {
        return "//div[@id='control_02805E6D-4F50-4365-A0AE-1315B8734A26']";
    }

    public static String InappropriateBehaviour_Option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String IndividualTeamActionsDetails() {
        return "//div[@id='control_7FBCB2BE-8B9C-4F36-A8E8-BEE9000740BF']//textarea";
    }

    public static String IndividualTeamActions_Option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String IndividualTeamActions_Option1(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[text()='" + data + "']/i)[1]";
    }
    public static String OrganisationalFactors_AddButton() {
        return "//div[@id='control_4A381D1C-BC8B-4FEC-A44E-4D1F4EFCA22B']//div[text()='Add']";
    }

    public static String OrganisationalFactors_CauseSelection_Dropdown() {
        return "//div[@id='control_9CAD4B7D-621C-4E16-9FDA-3CD221BBE540']";
    }

    public static String OrganisationalFactors_Description() {
        return "//div[@id='control_9D30A0EF-4FC3-459C-BE53-909CC0C12D3D']//textarea";
    }

    public static String OrganisationalFactors_SaveButton() {
        return "//div[@id='control_4A381D1C-BC8B-4FEC-A44E-4D1F4EFCA22B']//div[text()='Save']";
    }

    public static String Organisational_CauseAnalysis_Dropdown() {
        return "//div[@id='control_3C994AF6-C22E-4553-9112-811A170CC12E']";
    }

    public static String OrganizationalFactorsDetails() {
        return "//div[@id='control_A2E8FDDA-F1FB-4534-8866-23F173C6991A']//textarea";
    }

    public static String OrganizationalFactors_Option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String OrganizationalFactors_Option1(String data) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']/i[@class='jstree-icon jstree-checkbox']";
    }

    public static String Organizational_Factors_Panel() {
        return "//div[@id='control_746C2FBF-1654-48F9-9CFF-C1FA3BDF067A']//span[text()='Organizational Factors']";
    }

    public static String RCATImmediateCauseActions_CloseButton() {
        return "(//div[@id='form_ED153320-EAFE-4FBC-BFEF-7EC183292F93']//i[@class='close icon cross'])[1]";
    }

    public static String RCATImmediateCauseActions_Panel() {
        return "//div[@id='control_B9C9D9A4-48CF-4116-9F0C-5899B3A20DDD']//div[text()='RCAT Immediate Cause Actions']";
    }

    public static String RCATImmediateCause_Gridview() {
        return "(//div[@id='control_63B4B7E1-9778-46C5-8FBF-6307658BE9BE']//div//table)[3]";
    }

    public static String RCAT_Immediate_Causes_AddButton() {
        return "//div[@id='control_63B4B7E1-9778-46C5-8FBF-6307658BE9BE']//div[text()='Add']";
    }

    public static String RCAT_Immediate_Causes_Dropdown() {
        return "//div[@id='control_705495C5-49E8-45D8-ADED-B14323F1158C']";
    }

    public static String RCAT_Immediate_Causes_Option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }
    public static String RCAT_Immediate_Causes_Option_Dropdown(String data){
        return "(//div[contains(@class, 'transition visible')]//a[text()='" + data + "']//..//i)[1]";
    }

    public static String RCAT_Immediate_Causes_ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_ED153320-EAFE-4FBC-BFEF-7EC183292F93']";
    }

    public static String RootCauses_Option(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[text()='" + data + "'])[2]";
    }

    public static String RootCauses_Option1(String data) {
        return " //div[contains(@class, 'transition visible')]//a[text()='" + data + "']/i[@class='jstree-icon jstree-checkbox']";
    }

    public static String SaveAndContinue_Step_4() {
        return "//div[@id='control_2CF4FD6E-B9AA-44D4-801C-2940E5495E06']//div[text()='Save and continue to Step 4']";
    }

    public static String Submit_Step_3() {
        return "//div[@id='control_851B10A6-EC82-47C0-8F18-8C8CB97D7963']//div[text()='Submit Step 3']";
    }

    public static String TaskEnvironmentFactors_Description() {
        return "//div[@id='control_C3AEA36E-2D01-4844-93F5-AC21CC521C19']//textarea";
    }
    public static String OrganisationalFactors_Option(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]/i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String Individual_Environment_SaveButton() {
        return "//div[@id='control_C601D54F-5A69-4BD9-A784-C04B7E95C01C']//div[text()='Save']";
    }

    public static String OrganisationalFactors_Dropdown() {
        return "//div[@id='control_17AD36D0-A261-44B9-94C5-5DD3B5369A9B']";
    }
    public static String SearchFilter(){
        return "//div[@id='control_FFC3A55F-7020-4548-929F-DAB50544E408']//div[@id='btnFilter']//div[text()='Search']";
    }

    public static String Absent_And_Failed_Defenses_Panel() {
        return "//span[text()='Absent and Failed Defenses']";
    }

    public static String AdditionalInformation() {
        return "//div[text()='Additional Information']//..//..//div[@id='control_3ED76037-967D-4A52-BA61-BD97B3936B39']//div[@class='c-chk']";
    }
    
    public static String Individual_Team_Actions_Panel(){
        return "//span[text()='Individual / Team actions']";
    }

    public static String Individual_Team_Panel() {
        return "//span[text()='Individual / Team']";
    }

    public static String Organisational_Factors_Panel() {
        return "//span[text()='Organisational Factors']";
    }

    public static String RCAT_Analysis_Panel() {
        return "//span[text()='RCAT Analysis']";
    }

    public static String RCAT_Immediate_Causes_Panel() {
        return "//div[text()='RCAT Immediate Causes']";
    }

    public static String RCAT_Root_Causes_Panel() {
        return "//div[text()='RCAT Root Causes']";
    }
    
    public static String ReasonForNoInvestigation() {
        return "//div[@id='control_77086D14-A262-4C61-A207-0CFACEBEBB36']//textarea";
    }

    public static String Refresh_Button() {
        return "//div[@id='control_E8AC341B-3D7B-4A87-8C02-91EDF4A9EB34']";
    }

    public static String SearchFilter_Button() {
        return "(//div[@class='actionBar filter']//div[text()='Search'])[5]";
    }

    public static String TaskEnvironmentFactors_SaveButton() {
        return "//div[@id='control_1551ED48-393A-4376-A0AB-783EA22A1833']//div[text()='Save']";
    }

    public static String TaskEnvironmentalConditions_Dropdown() {
        return "//div[@id='control_BE32787F-3A2E-41D0-A501-1984FDB865D9']";
    }

    public static String TaskEnvironmentalConditions_Option(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]/i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String TaskEnvironmentalFactorsDetails() {
        return "//div[@id='control_EFAE1279-7357-4BB0-BD0C-FC039FFAE35C']//textarea";
    }

    public static String TaskEnvironmentalFactors_AddButton() {
        return "//div[@id='control_1551ED48-393A-4376-A0AB-783EA22A1833']//div[text()='Add']";
    }

    public static String TaskEnvironmentalFactors_CauseAnalysis_Dropdown() {
        return "//div[@id='control_875F6E0B-08AC-43A2-9D7C-3781CC5D6A7B']";
    }

    public static String TaskEnvironmentalFactors_CauseSelection_Dropdown() {
        return "//div[@id='control_E6C95D17-4F24-4710-95FE-13DD645D586C']";
    }

    public static String Task_Environmental_Factors_Panel() {
        return "(//span[text()='Task / Environmental Factors'])[2]";
    }
    
    public static String Task_Environmental_Factors_Panel_1() {
        return "(//span[text()='Task / Environmental Factors'])[1]";
    }

    public static String TypeOfCommunication_Dropdown() {
        return "//div[@id='control_1B9053A3-BA1C-435B-BAE9-E2BD3F14E5EB']";
    }

    public static String TypeOfCommunication_Option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String bodyPartsSelectInjuredLocationsXPath1(String data) {
        return "//div[contains(@class, 'select3-drop select3-drop-ddl select3_')][contains(@class, 'transition visible')]//a[text()='" + data + "']/..//i[@class='jstree-icon jstree-ocl']";
    }

    public static String controlAnalysisAction_CloseButton() {
        return "(//div[@id='form_C8F39E88-B969-4F49-9E2C-89FB6942CB29']//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String controlAnalysisActions_GridView() {
        return "(//div[@id='control_12B38196-7C92-4C19-8017-FA7B768135AD']//div//table)[3]";
    }

    public static String controlAnalysisAddButon() {
        return "//div[text()='Control Analysis']//..//div[@title='Add']";
    }

    public static String controlAnalysis_CloseButton() {
        return "(//div[@id='form_821BC9A7-867F-410A-9837-A4A8D3AFD781']//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String controlAnalysis_GridView() {
        return "(//div[@id='control_FFC3A55F-7020-4548-929F-DAB50544E408']//div//table)[3]";
    }

    public static String controlReference() {
        return "(//div[@id='control_8651F55E-97E5-4E7B-9066-BDDCE6BE0C67']//input)[1]";
    }

    public static String criticalControl() {
        return "//div[@id='control_9318BB65-2947-4FC6-9853-A2E44803CC5F']";
    }

    public static String expandGlobalCompany_IncidentActions() {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'Global Company')]//..//i[@class='jstree-icon jstree-ocl']";
    }

    public static String expandSouthAfrica_IncidentActions() {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'South Africa')]//..//i[@class='jstree-icon jstree-ocl']";
    }

    public static String incidentCommunication_SaveButton() {
        return "//div[@id='btnSave_form_C6B3A628-2B52-4445-842F-7A9BD7D3DCAC']";
    }

    public static String incidentCommunication_SaveToContinue_Button() {
        return "//div[@id='control_5845B8BB-15A3-4662-8FD9-DF0BB0B9616B']";
    }

    public static String incidentEngagement_SaveButton() {
        return "//div[@id='btnSave_form_693F051B-BD01-4FE3-B457-E1E13354A955']";
    }

    public static String incidentEngagement_SaveToContinue_Button() {
        return "//div[@id='control_7505352B-EFA7-4024-A69D-B205355148AD']";
    }

    public static String incidentInvestigation_FindingdAndApproval_tabxpath() {
        return "//div[text()='Findings & Approval ']";
    }

    public static String incidentInvestigation_ManagementFindingsActions_Close_Selectxpath() {
        return "(//div[@id='form_CCC7B08A-9084-4470-922B-C10D6150CEE8']//i[@class='close icon cross'])[1]";
    }

    public static String incidentInvestigation_RelatedJSA_Openxpath() {
        return "((//div[@id='control_01B1F475-DC2E-4EEF-B0B4-6272EC0C05FC']//div//table)[3]//div[contains(text(),'Global Company -> South Africa -> Victory Site')])[1]";
    }

    public static String incidentInvestigation_RelatedRJSA_OpenedRecordxpath() {
        return "(//div[@id='form_EBF4B94D-9E08-49BD-9899-CD64DC78AE1D']//div[contains(text(),'- Record #')])[1]";
    }

    public static String incidentInvestigation_Related_Bowties_OpenedRecordxpath() {
        return "(//div[@id='form_E5D05DD0-7E8F-4A41-9077-0A1F8DB472E7']//div[contains(text(),'- Record #')])[1]";
    }

    public static String incidentInvestigation_Related_Bowties_Openxpath() {
        return "((//div[@id='control_44411406-1DA6-493A-9A86-695A82FE16F9']//div//table)[3]//div[contains(text(),'Global Company -> South Africa -> Victory Site')])[1]";
    }

    public static String incidentInvestigation_Related_Bowties_Panelxpath() {
        return "//div[@id='control_BE8F6FE0-ABD8-4DCD-9E83-960D64347B9E']//span[contains(text(),'Related Bowties')]";
    }

    public static String individualTeam_AddButton() {
        return "//span[text()='Individual / Team']/../..//div[@title='Add']";
    }

    public static String navigate_EHS(){
        return "//label[text()='Environment, Health & Safety']";
    }

    public static String navigate_incidentManagement()
    {
        return "//label[text()='Incident Management']";
    }

    public static String IM_Add()
    {
        return "//div[@class='actionBar']//div[text()='Add']";
    }

    public static String IM_processFlow()
    {
        return "//div[@id='btnProcessFlow_form_B6196CB4-4610-463D-9D54-7B18E614025F']";
    }
    public static String environmentalHealthSafetyXpath()
    {
        return "//label[contains(text(),'Environmental, Health')]";
    }

    public static String allRecordID()
    {
        return "//td[@role='gridcell'][@class='no-wrap sortColumn']";
    }

    public static String environmentalHealth()
    {
        return "//div[@id='section_711036e0-fe8c-478f-a260-87beae7432fc']";
    }

    public static String save_To_Continue_Button() {
        return "//div[@id='control_E663CE5B-C738-4B9D-A3BD-CAA85B656074']";
    }

    public static String seacrhButton()
    {
        return "//div[text()='Search']";
    }

    public static String IncidentManagmentXpath()
    {
        return "//label[text()='Incidents & Near Misses']";
        //label[contains(text(),'Incidents & Near Misses')]
    }

    public static String IncidentsManagment()
    {
        return "//label[contains(text(),'Incidents Management')]";
    }

    public static String ViewFilterBtn()
    {
        return "//div[@id='btnActFilter']";
    }

    public static String map()
    {
        return "//div[@id='control_29629F13-0707-446F-BD1B-5EFC503555C4']//span[text()='Map']";
    }

    public static String addNewBtn()
    {
        return "//div[@id='btnActAddNew']";
    }

    public static String IncidentManagemetLoadingXpath()
    {
        return "//div[@id='divSearch']//div[@class='ui active inverted dimmer']";
    }

    public static String IncidentManagemetLoadingDoneXpath()
    {
        return "//div[@class='ui inverted dimmer hidden']//div[@title='Loading...']";
    }

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String IncidentDescription()
    {
        return "//div[@id='control_E887FEB2-18C9-444E-A023-B05D5F08BC28']//textarea";
    }

    public static String incidentOccured()
    {
        return "//div[@id='control_8FC01D3C-93D7-4570-BAEA-765533CDCB75']";
    }

    public static String Mining()
    {
        return "//a[text()='Mining ']//../i[@class='jstree-icon jstree-ocl']";
    }

    public static String SouthAfrica()
    {
        return "//a[text()='South Africa']//../i[@class='jstree-icon jstree-ocl']";
    }

    public static String VictoryMine()
    {
        return "//a[text()='Victory Mine']//../i[@class='jstree-icon jstree-ocl']";
    }

    public static String Ore()
    {
        return "//a[text()='Ore Processing']";
    }

    public static String Panel()
    {
        return "//li[@id='4cee9a75-7667-44e9-a0c1-77ad5092e86c']";
    }

    public static String projectLink()
    {
        return "//div[@id='control_26DC542D-8D63-4388-B320-03B9C9ED7C88']//div[@class='c-chk']";
    }

    public static String location()
    {
        return "//div[@id='control_0E9F8B1D-C36C-4999-9185-A5658E72A9DB']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String PinToMap()
    {
        return "//div[@id='control_82B50C4C-22B2-46DB-8F8D-677EC2CA7D33']//div[@class='c-chk']";
    }

    public static String project()
    {
        return "//div[@id='control_1D125078-AF69-492D-887E-0BE99AF4D528']";
    }

    public static String searchRecordNo() {
        return "(//input[@class='txt border'])[46]";
        //return "//input[@class='txt border dirty-filter']";
    }

    public static String selectProject(String text)
    {
        return "//a[contains(text(),'" + text + "')]";
    }

    public static String Simon()
    {
        return "//a[@id='91520b60-bfa6-4392-bb92-a9ebf8abcdd6_anchor']";
    }

    public static String SelectMap()
    {
        return "//span[text()='Map']";
    }

    public static String RiskDisciple()
    {
        return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']";
    }

    public static String impactTypeWait()
    {
        return "//a[contains(text(),'Business Risk')]";
    }

    public static String incedentTypeWait()
    {
        return "//a[contains(text(),'Business Loss')]";
    }

    public static String incidenttypeAll()
    {
        return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']//b[@original-title='Select all']";
    }

    public static String impacTypeAll()
    {
        return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']//b[@original-title='Select all']";
    }

    public static String Compliance()
    {
        return "//a[@id='b38c4a28-05f9-423d-a53e-3d2940d29f6f_anchor']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String SelectAll()
    {
        return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']//b[@class='select3-all']";
    }

    public static String IncidentClassification()
    {
        return "//div[@id='control_F6FDC1A4-AAE5-4C3E-B12B-EFD7A35FCB51']";
    }

    public static String Accident()
    {
        return "//a[@id='e11fcae9-3784-4677-8a89-5ea95443688b_anchor']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String Environmental()
    {
        return "//a[@id='dfa1dbcf-81f5-49b7-a6ba-a070cfbbff19_anchor']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String Injury()
    {
        return "//a[@id='75c54c6e-e0f2-49a1-bba3-575131962f3c_anchor']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String DatePicker()
    {
        return "//span[@class='k-widget k-datepicker k-header']";
    }

    public static String Date()
    {
        return "//a[@title='12 September 2018']";
    }

    public static String ClickUp()
    {
        return "//b[@class='select3-down drop_click']";
    }

    public static String IncidentTime()
    {
        return "//div[@id='control_2CE711B1-4A48-4381-916B-17F5FD255123']//input[@class='is-timeEntry']";
    }

    public static String Shift()
    {
        return "//div[@id='control_D47426BB-60CA-49A3-93BA-433B1E8BC4BD']";
    }

    public static String DayShift()
    {
        return "//a[text()='Day Shift']";
    }

    public static String ActionTaken()
    {
        return "//div[@id='control_58E573F1-1421-4B47-B9F9-68668A0AA36A']//textarea[@class='txt']";
    }

    public static String reportedBy()
    {
        return "//div[@id='control_0F48D804-ECD1-4700-B480-72338CD49E6F']";
    }

    public static String Save()
    {
        return "//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@i18n='save']";
    }

    public static String SuperVisor()
    {
        return "//div[@id='control_F2A12EB0-344C-47E6-BB85-B74B9B53CEFB']//ul[@class='select3-tagbox']";
    }

    public static String SelectAdmin()
    {
        return "//a[@id='b8ad5f90-d582-46c4-b186-d99649824acd_anchor']";
    }

    public static String ViewFilter_Button()
    {
        return "//div[@id='btnActFilter']";
    }

    public static String ViewFilter__RecordNumber_textfield()
    {
        return "//div[@id='ddlWhereType_7748DD6A-031D-48C0-8915-779360C3A7EF']//..//input[@class='txt border']";
    }

    public static String ViewFilter_Search_Button()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String home()
    {
        return "//i[@class='icon health safety and environment link']";
    }

    public static String incidentTitle(){
        return "(//div[@id='control_E9C32F4B-AB3C-4ABB-B8E6-E5D5E34F139B']//input)[1]";
    }

    public static String incidentDescription()
    {
        return "//div[@id='control_E887FEB2-18C9-444E-A023-B05D5F08BC28']//textarea";
    }

    public static String selectGlobalCompany(String text)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + text + "')]";
    }

    public static String impactDropDown()
    {
        return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']";
    }

    public static String selectImpactType(String text)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + text + "')]//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String upArrow(){
        return "//b[@class='select3-down drop_click']";
    }

    public static String incidentTypeDropDown(){
        return "//div[@id='control_AE90A60A-6ABF-4F14-B977-EEC2AA060764']";
    }

    public static String selectIncidentType(String text){
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + text + "')]";
    }

    public static String selectAllIncidents(){
        return "//div[@id='control_F6FDC1A4-AAE5-4C3E-B12B-EFD7A35FCB51']//span[@class='select3-arrow']/.//b[@class='select3-all']";
    }

    public static String select_All_Impact_Type(){
        return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']//span[@class='select3-arrow']/.//b[@class='select3-all']";
    }
    public static String supplierDropDown(){
        return "(//a[contains(text(),'Supplier')]/../i[@class='jstree-icon jstree-ocl'])[2]";
    }

    public static String selectIncidentOption(String text){
        return "//a[contains(text()," + text + "')]//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String selectAllIncidentOptions()
    {
        return "(//b[@class='select3-all'])[16]";
    }

    public static String secondArrow()
    {
        return "//b[@class='select3-down drop_click']";
    }

    public static String dateOfOccurrence()
    {
        return "//div[@id='control_A68454D1-B0FB-4EB7-B861-2AF37ACAC8DF']//input[@type='text']";
    }

    public static String timeOfOccurrence()
    {
        return "//div[@id='control_2CE711B1-4A48-4381-916B-17F5FD255123']//input[@type='text']";
    }

    public static String reportedDate()
    {
        return "//div[@id='control_991FEA22-9C36-4EC7-B385-744259EB6599']//input[@type='text']";
    }

    public static String reportedTime()
    {
        return "//div[@id='control_B623B7E6-DB94-4A72-933F-4847D48066D5']//input[@type='text']";
    }

    public static String supervisorDropDown()
    {
        return "//div[@id='control_AF57F968-9366-4935-91F9-9360687E8D9F']//li";
    }

    public static String selectSupervisor(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[2]";
    }

    public static String externalPartiesInvolved()
    {
        return "//div[@id='control_D086CF05-958D-4916-ADC3-BD45703467A5']";
    }

    public static String incidentOccurredDropDown()
    {
        return "//div[@id='control_8FC01D3C-93D7-4570-BAEA-765533CDCB75']";
    }

    public static String shiftDropDown()
    {
        return "//div[@id='control_D47426BB-60CA-49A3-93BA-433B1E8BC4BD']";
    }

    public static String selectShift(String text)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + text + "')]";
    }

    public static String externalParties()
    {
        return "(//i[@class='jstree-icon jstree-checkbox'])[1]";
    }

    public static String immediateActionTaken()
    {
        return "//div[@id='control_58E573F1-1421-4B47-B9F9-68668A0AA36A']//textarea[@type='text']";
    }

    public static String scrolUp()
    {
        return "//div[@id='control_9CB1940D-0C7D-4D56-BB83-F3B74034681C']";
    }

    public static String incidentOwnerDropDown()
    {
        return "//div[@id='control_4DC8AFD7-836E-4681-9FB8-99FAF564054C']";
    }

    public static String incidentOwner(String text)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + text + "')]";
    }

    public static String saveAndContinue()
    {
        return "//div[@id='control_790CDCA6-7FC9-4BF7-B523-F5DEABB20652']//div[@class='c-btn imgButton']";
    }

    public static String processFlow()
    {
        return "//div[@id='btnProcessFlow_form_B6196CB4-4610-463D-9D54-7B18E614025F']";
    }

    public static String secondScroll()
    {
        return "(//div[@class='c-pnl-heading'])[1]";
    }

    public static String verificationAndAdditionalDetail()
    {
        return "//li[@id='tab_90D1C7C9-45EE-402F-B76C-A61222A110E1']";
    }

    public static String personsInvolved()
    {
        return "//div[@id='control_8FC0CB8C-0D87-4E41-9090-6EE580CB32CB']";
    }

    public static String addButton()
    {
        return "(//div[@id='btnAddNew'])[1]";
    }

    public static String enterFullName()
    {
        return "(//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4'])[4]";
    }

    public static String enterDecriptionOfInvolvement()
    {
        return "(//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4'])[3]";
    }

    public static String processFlow2()
    {
        return "//div[@id='btnProcessFlow_form_B6196CB4-4610-463D-9D54-7B18E614025F']";
    }

    public static String savePersonsInvolved()
    {
        return "//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']";
    }

    public static String witnessStatement()
    {
        return "//div[@id='control_BAEC7B73-C141-4EA8-84FC-EFA360DC48D1']";
    }

    public static String addWitnessStatementButton()
    {
        return "//div[@id='control_5E4C6D37-0D4E-42D1-AC3D-B1E210B05DD9']//div[@id='btnAddNew']";
    }

    public static String witnessProcessFlow()
    {
        return "(//span[@class='img icon flow-tree icon-button'])[2]";
    }

    public static String witnessName()
    {
        return "//div[@id='control_4E6AE5EC-D5F8-40FD-88E5-7FEF114D2EC9']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String witnessSurname()
    {
        return "//div[@id='control_E01BF26E-A948-4BED-9935-AAEE045C3521']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String witnessSummary()
    {
        return "//div[@id='control_F53C69CD-96A0-465E-9CBE-244FAE1B4780']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String saveWitness()
    {
        return "//div[@id='btnSave_form_FCE0307F-50FE-403C-BDFB-ED293325F9DA']";
    }

    public static String equipmentInvolved()
    {
        return "(//div[@class='c-pnl c-pnl-border'])[1]";
    }

    public static String addEquipment()
    {
        return "(//div[@id='btnAddNew'])[3]";
    }

    public static String equipmentProcessFlow()
    {
        return "(//span[@class='img icon flow-tree icon-button'])[2]";
    }

    public static String assetTypeDropDown()
    {
        return "//div[@id='control_C5608592-40BB-43E3-B988-AD520AB81BF1']";
    }

    public static String selectAssetType(String text)
    {
        return "//a[contains(text(),'" + text + "')]";
    }

    public static String assetDropDown()
    {
        return "//div[@id='control_0A75D091-94E4-4A68-A9D6-6D67562F2F4C']";
    }

    public static String selectFirstArrow()
    {
        return "(//ul[@class='jstree-container-ul jstree-children'])[22]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String selectSecondtArrow()
    {
        return "//a[contains(text(),'Bin')]/..//../i[@class='jstree-icon jstree-ocl']";
    }

    public static String selectAsset(String text)
    {
        return "//a[contains(text(),'" + text + "')]";
    }

    public static String equipmentDescription()
    {
        return "//div[@id='control_57465454-B90F-4638-97AB-CD04E42635E1']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String descriptionOfDamage()
    {
        return "//div[@id='control_A8336A99-8A31-491D-8776-C2F5A76EA3F9']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String damageCost()
    {
        return "//div[@id='control_AE6023AB-8BA0-4AC4-BFCE-E64EE9943D5B']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String operatorEmploymentTypeDropDown()
    {
        return "//div[@id='control_80696A19-7EFC-4DC9-87EF-87DFDBBB5B15']";
    }

    public static String selectOperatorEmploymentType(String text)
    {
        return "//a[contains(text(),'" + text + "')]";
    }

    public static String operatorDropDown()
    {
        return "//div[@id='control_F17E41FB-2A2D-410F-8219-6FD01F15E039']";
    }

    public static String selectOperator(String text)
    {
        return "//a[contains(text(),'" + text + "')]";
    }

    public static String saveEquipmentTools()
    {
        return "//div[@id='btnSave_form_CEAD7953-CD9C-4E8A-ADB8-15C8AA6A092F']";
    }

    public static String assetsTb()
    {
        return "//div[@id='control_0F686E95-8AB8-4A68-85B3-7670D78BA16A']";
    }

    public static String addAssets()
    {
        return "(//div[@id='btnAddNew'])[4]";
    }

    public static String assetsProcessFlow()
    {
        return "(//span[@class='img icon flow-tree icon-button'])[2]";
    }

    public static String processDropDown()
    {
        return "//div[@id='control_28914C65-06E3-4C73-91DB-D0461DB0C410']";
    }

    public static String financialManagementFirstDropDownArrow()
    {
        return "//a[contains(text(),'Financial Management')]/..//i[@class='jstree-icon jstree-ocl']";
    }

    public static String selectTheProcessOption(String text)
    {
        return "//a[contains(text(),'" + text + "')]";
    }

    public static String processDescription()
    {
        return "//div[@id='control_22D64E6B-38AB-4D0E-9AA1-6D8659B956A7']//div[@class='border c-dtl']";
    }

    public static String requiredRTODropDown()
    {
        return "//div[@id='control_C29B7061-6896-4086-8BDE-1AEEE2F7ED4F']";
    }

    public static String selectRTO(String text)
    {
        return "//a[contains(text(),'" + text + "')]";
    }

    public static String quantity()
    {
        return "//div[@id='control_13675B86-8CC6-46D6-B54D-52838975DA99']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String mandotoryDropDown()
    {
        return "//div[@id='control_985E4D2A-EE5B-4A61-A173-E1F1027864C8']";
    }

    public static String selectMandotory(String text)
    {
        return "//a[contains(text(),'" + text + "')]";
    }

    public static String enterComments()
    {
        return "//div[@id='control_022B7F82-688D-4D84-9F2D-C21B02B429BD']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String backUpDropDown()
    {
        return "//div[@id='control_E452C970-B034-44C2-B4EF-B20849E8B03F']";
    }

    public static String selectBackUp(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[2]";
    }

    public static String saveAssets()
    {
        return "//div[@id='btnSave_form_2000EE74-35E5-4218-90FC-47E8A0660483']";
    }

    public static String checkBox()
    {
        return "//div[@id='control_DFDE9A4A-7D55-4007-8F7E-BE834816BE3D']//div[@class='c-chk']";
    }

    public static String regulatoryDropDown()
    {
        return "//div[@id='control_429F4B53-D8C3-48D1-B0F8-594996A78872']//div[@class='c-pnl-heading']";
    }

    public static String addRegulatoryAuthority()
    {
        return "(//div[@id='btnAddNew'])[4]";
    }

    public static String regulatoryProcessFlow()
    {
        return "(//span[@class='img icon flow-tree icon-button'])[2]";
    }

    public static String regulatoryAuthorityDropDown()
    {
        return "//div[@id='control_B58E20B0-261B-481C-982B-0CFAD75B5997']";
    }

    public static String regulatoryAuthorityOption(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[2]";
    }

    public static String contactPerson()
    {
        return "//div[@id='control_9E07C773-D45B-43AA-B8D8-E37F2949A6C6']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String contactNumber()
    {
        return "//div[@id='control_01757065-A495-4AEB-BEB5-3DD4910CA67E']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String contactEmail()
    {
        return "//div[@id='control_FB3DCA41-4C12-4A60-B97A-2762D7966770']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String dateField()
    {
        return "//div[@id='control_ECB2B00F-CA0B-42DD-8014-638125746A46']//input[@type='text']";
    }

    public static String time()
    {
        return "(//span[@class='timeEntry-control'])[3]";
    }

    public static String communicationSummary()
    {
        return "//div[@id='control_6DF5113D-B3F1-4B24-AC01-2F1F1A8B3AA5']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String saveRegulatoryAuthority()
    {
        return "//div[@id='btnSave_form_727412E0-FBAE-44FA-BBFA-2E8CBBBFBECA']";
    }

    public static String saveAndCloseRegulatoryAuthority()
    {
        return "//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']";
    }

    public static String validateSave()
    {
        return "//div[@class='ui floating icon message transition visible']";
    }

    public static String incidentStatus()
    {
        return "//div[@id='control_B1667955-9616-471C-BBEC-14F334A90E65']";
    }

    public static String addNewExternalParty()
    {
        return "//div[@id='control_896D931B-C39C-47BD-8FA8-A1B95B700DDC']//div[text()='Add new external party']";
    }

    public static String externalPartiesSelect(String data)
    {
        return "//a[text()='"+data+"']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String refreshBtn()
    {
        return "//div[@id='control_F3EBE4A9-5055-414F-8D34-1DD0E968BD92']//b[@class='select3-redo']";
    }

    public static String AddNewBtn()
    {
        return "//div[@id='btnActAddNew']";
    }

    public static String linkbox()
    {
        return "//div[@id='control_8FAF5512-5015-4A5E-8E65-14626A22BE9D']//b[@original-title='Link to a document']";
    }

    public static String verificationAndAddDetTab()
    {
        return "//div[text()='2.Verification & Additional Detail']";
    }

    public static String persons_Involved()
    {
        return "//span[text()='Persons Involved']";
    }
    
    public static String personsInvolvedAdd()
    {
        return "//div[@id='control_099C8373-B255-4407-B983-91ED6318453A']//div[text()='Add']";
    }

    public static String fullName()
    {
        return "//div[@id='control_F476AA51-55D8-4537-8C78-5B2EEC764093']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String descriptionOfInvolvement()
    {
        return "//div[@id='control_5C18C41F-812C-4224-AEF1-79A07DEE0676']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String impactTypeSelectAll()
    {
        return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']//b[@class='select3-all']";
    }

    public static String reportedByDD()
    {
        return "//div[@id='control_F2A12EB0-344C-47E6-BB85-B74B9B53CEFB']";
    }

    public static String reportedBySelect(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String submit1EventReport()
    {
        return "//div[@id='control_3CEB80A1-3AFF-4ABA-B53B-E73C65FF0D40']";
    }

   

    public static String highLightRecord(String id) {
        return "//span[text()='" + id + "']/../..";
    }

    public static String recordXpath(String id) {
        return "//span[text()='" + id + "']";
    }

    public static String getRecordIdXpath() {
        return "//div[@id='divPager'][@class='recnum']//div[@class='record']";
    }

    public static String getAssetRecord() {
        return "//div[@id='form_CEAD7953-CD9C-4E8A-ADB8-15C8AA6A092F']//div[@id='divPager'][@class='recnum']//div[@class='record']";
    }

    public static String clickClose() {
        return "//div[@id='form_B6196CB4-4610-463D-9D54-7B18E614025F']//i[@class='close icon cross']";
    }

    public static String incidentRefresh() {
        return "//a[@class='k-pager-refresh k-link']";
    }
    
    public static String riskRegisterform(){
        return "//div[@id='form_3AB45051-222E-416C-AAD3-86B2EDA98BED'][@class='form active transition visible']";
    }
    
    public static String tablexpath(){
        return "//table[@data-role]";
    }

    public static String dateOcXpath() {
        return "//div[@id='control_A68454D1-B0FB-4EB7-B861-2AF37ACAC8DF']//input";
    }

    public static String timeXpath() {
        return "//div[@id='control_2CE711B1-4A48-4381-916B-17F5FD255123']//input";
    }

    public static String reportedDateXpath() {
        return "//div[@id='control_991FEA22-9C36-4EC7-B385-744259EB6599']//input";
    }

    public static String reportedTimeXpath() {
        return "//div[@id='control_B623B7E6-DB94-4A72-933F-4847D48066D5']//input";
    }

    public static String shiftXpath() {
        return "//div[@id='control_D47426BB-60CA-49A3-93BA-433B1E8BC4BD']//li";
    }

    public static String partInvolvedXpath() {
        return "//div[@id='control_D086CF05-958D-4916-ADC3-BD45703467A5']/div[@class='c-chk']";
    }

    public static String occuredText() {
        return "//div[@id='control_8FC01D3C-93D7-4570-BAEA-765533CDCB75']//li";
    }

    public static String projectText() {
        return "//div[@id='control_8FC01D3C-93D7-4570-BAEA-765533CDCB75']//li";
    }

    public static String impactTypeText() {
        return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']//li";
    }

   

    public static String incidentText() {
        return "//div[@id='control_C54832FB-66ED-4070-9F04-6FC6C48D2F1E']//li";
    }

    public static String mapZoominPic() {
        return "ZoomIn.PNG";
    }

    public static String mapZoomOutPic() {
        return "ZoomOut.PNG";
    }

    public static String mapPic() {
        return "map1.PNG";
    }

    public static String imagePic1() {
        return "image1.PNG";
    }

    public static String fileNamePic1() 
    {
        return "FileName.PNG";
    }
    
     public static String fullReportPic1() {
        return "entertext.PNG";
    }


    public static String dvtPic() {
        return "dvt.PNG";
    }

    public static String dvt2Pic() {
        return "dvt2.PNG";
    }

    public static String dvt3Pic() {
        return "dvt3.PNG";
    }

    public static String image2Pic() {
        return "image2.PNG";
    }

    public static String image3Pic() {
        return "image3.PNG";
    }

    public static String openPic() {
        return "open.PNG";
    }
    
    public static String savePic() {
        return "Save.PNG";
    }


    public static String africaPic() {
        return "africa.PNG";
    }

    public static String joziPic() {
        return "jozi.PNG";
    }

    public static String joziPic2() {
        return "jozi2.PNG";
    }

    public static String markPic() {
        return "mark.PNG";
    }

    public static String markIconPic() {
        return "markIcon.PNG";
    }
    
    public static String downloadPic() {
        return "Downloading1.PNG";
    }
    
    public static String downloadPic2() {
        return "Downloading2.PNG";
    }
    public static String IncidentOccured() {
        return "//div[@id='control_8FC01D3C-93D7-4570-BAEA-765533CDCB75']";
    }

    public static String globalCompanyXpath() {
        return "//a[text()='Global Company']//../i[@class='jstree-icon jstree-ocl']";
    }

    public static String clickGlobalCompanyXpath() {
        return "//a[text()='Global Company']";
    }

    public static String anyDropDownXpath(String option) {
        return "//a[text()='" + option + "']//../i[@class='jstree-icon jstree-ocl']";
    }

    public static String anyClickOptionXpath(String option) {
        return "//a[text()='" + option + "']";
    }

    public static String anyClickOptionCheckXpath(String option) {
        return "//a[text()='" + option + "']//../i[@class='jstree-icon jstree-checkbox']";
    }

    public static String southAfricaXpath() {
        return "//a[text()='South Africa']//../i[@class='jstree-icon jstree-ocl']";
    }

    public static String victorySiteXpath() {
        return "//a[text()='Victory Site']";
    }

    public static String specificLocationXpath() {
        return "//div[@id='control_0E9F8B1D-C36C-4999-9185-A5658E72A9DB']//input[@language]";
    }

    public static String ProjectLink() {
        return "//div[@id='control_26DC542D-8D63-4388-B320-03B9C9ED7C88']//div[@class='c-chk']";
    }

    public static String Location() {
        return "//div[@id='control_0E9F8B1D-C36C-4999-9185-A5658E72A9DB']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String Project() {
        return "//div[@id='control_1D125078-AF69-492D-887E-0BE99AF4D528']";
    }

    public static String Safety() {
        return "//a[@id='f86e0bdd-b06d-4122-a92a-2b8dcb4ba17d_anchor']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String incidentTypeSelectAllXpath() {
        return "//div[@id='control_F6FDC1A4-AAE5-4C3E-B12B-EFD7A35FCB51']//b[@class='select3-all']";
    }

    public static String incidentTypeTextXpath() {
        return "//div[@id='control_F6FDC1A4-AAE5-4C3E-B12B-EFD7A35FCB51']//b[@class='select3-all']";
    }

    public static String incidentTypeXpath() {
        return "//div[@id='control_F6FDC1A4-AAE5-4C3E-B12B-EFD7A35FCB51']//li";
    }

    public static String mapPicXpath() {
        return "//div[@id='control_6618F574-B672-4450-93CF-365F64F8CC02']";
    }


    public static String Reported() {
        return "//div[@id='control_0F48D804-ECD1-4700-B480-72338CD49E6F']";
    }

    public static String sighOffActionXpath(String name) {
//        return "//div[contains(@class, 'transition visible')]//a[text()='" + name + "']";
        return "(//a[contains(text(),'"+name+"')])[2]";
    }
    
    public static String incidentOwner_select(String name){
        return "(//a[contains(text(),'"+name+"')])[1]";
    }

    public static String addMoreImagesRow1() {
        return "//div[@id='control_814FC712-ED13-4082-BD9B-CE2AE74F015B']";
    }

    public static String addMoreImagesRow2() {
        return "//div[@id='control_6A0ACBBC-54E4-423F-B8F3-5F6A0C3D1E89']";
    }

    public static String saveWait() {
        return "//div[@class='ui inverted dimmer active']";
    }
    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }
    public static String submitWait(){
        return "//div[@class='ui inverted dimmer active']/div[text()='Saving...']";
    }
    public static String saveWait3() {
        return "//div[@class='ui inverted dimmer']";

    }
    
    public static String saveIncidents(){
        return "//div[@class='form transition visible active']";
    }

   

    //New 
    public static String incidenttitleXpath() {
        return "//div[@id='control_E9C32F4B-AB3C-4ABB-B8E6-E5D5E34F139B']//input[@language]";
    }

    public static String markButtonXpath() {
        return "//div[@id='mapbox_point_b2ee88d5']";
    }

    public static String IncidentOwnerXpath() {
        return "//div[@id='control_4DC8AFD7-836E-4681-9FB8-99FAF564054C']//li";
    }

    public static String scrollXpath() {
        return "//div[@id='control_A5DA1B86-239A-4A5C-8C9C-873C38C0E605']";
    }

    public static String saveStep2() {
        return "//div[text()='Save and continue to Step 2']";
    }

    public static String submitXpath()
    {
        return "//div[text()='Submit 1.Event Report']";
    }

    public static String incident_Select(String text){
        return "(//a[contains(text(),'"+text+"')])[1]";
    }
    public static String loadingData() {
        return "//div[@class='ui inverted dimmer']/div[text()='Loading data...']";
    }
    public static String griedView_Mask(){
        return "(//div[@class='k-loading-mask'])[2]";
    }
    public static String griedView_Mask2(){
        return "(//div[@class='k-loading-mask']/div[@class='k-loading-image'])[2]";
    }
    
    public static String expand_global(){
        return "//a[text()='Global Company']/../i";
    }
    public static String expand_sa(){
        return "//a[text()='South Africa']/../i";
    }
    public static String expand_victory(){
        return "//a[text()='Victory Site']";
    }
    public static String external_part(String text){
        return "(//a[text()='"+text+"']/i[1])[1]";
    }
    public static String save(){
        return "(//div[text()='Save'])[2]";
    }

    public static String image()
    {
        return "//div[@id='control_E39283CD-AF7A-4775-B1D7-8CE676E49D65']/div[1]/div[3]";
    }

    public static String image2()
    {
        return "//div[@id='control_1048F27D-23CF-49AA-8388-E156D2BEA560']/div[1]/div[3]";
    }

    public static String image3()
    {
        return "//div[@id='control_5907A4DA-20C8-4BE8-BE76-39564081C6EC']/div[1]/div[3]";
    }

    public static String GlobalCompanyXpath() {
        return "//a[text()='Global Company']//../i[@class='jstree-icon jstree-ocl']";
    }

    public static String Incident_Classification() {
        return "//div[@id='control_F6FDC1A4-AAE5-4C3E-B12B-EFD7A35FCB51']";
    }

    public static String Incident_Classification_All() {
        return "//div[@id='control_F6FDC1A4-AAE5-4C3E-B12B-EFD7A35FCB51']//b[@original-title='Select all']";
    }
    public static String link_buttonxpath() {
        return "(//b[@class='linkbox-link'])[2]";
    }
  
    public static String EmployeeTypeXPath()
    {
        return "//div[@id='control_21267B5E-E90B-4719-A15D-CF40EE711ADB']//li";
    }
  
    public static String addDetailsXpath()
    {
        return "//div[text()='2.Verification and Additional Detail']";
    }


    public static String uploadPic()
    {
        return "upload.PNG";
    }

    public static String uploadbutton()
    {
        return "(//b[@class='linkbox-upload'])[2]";
    }

    public static String anySupervisorXpath(String option)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']";
    }

    public static String anyActiveDropdownXpath(String option)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + option + "')]//../i[@class='jstree-icon jstree-ocl']";
    }

    public static String anyActiveOptionDropdownXpath(String option)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + option + "')]";
    }

    public static String EnvironmentXpath()
    {
        return "//li[@id='tab_1ACBA560-AF30-4AC3-B7B0-EC5D264E1BD7']";
    }

    public static String saveSupportingdocument()
    {
        return "//div[@id='control_F1395E4D-7A99-4032-A127-20F4D7E31DE2']";
    }

    public static String EnvironmentDropDownXpath()
    {
        return "//div[@id='control_BE858B00-B8FF-4614-A61C-3F91A5373908']";
    }

    public static String QualitytXpath()
    {
        return "//li[@id='tab_83609D85-9F2F-4BAF-8CB2-887CA05FC034']";
    }

    public static String QualityDropDownXpath()
    {
        return "//div[@id='control_AF57F968-9366-4935-91F9-9360687E8D9F']";
    }

    public static String railwaySafetyXpath()
    {
        return "//li[@id='tab_C765F87F-EEE5-40F5-AF0F-C52C3C964384']";
    }

    public static String railwaySafetyDropDownXpath()
    {
        return "//div[@id='control_07D5114B-C2C7-4526-971E-3316526C95BC']";
    }

    public static String socialSusXpath()
    {
        return "//li[@id='tab_718C3400-E9C8-4C9A-98D2-87C0E4D43DB6']";
    }

    public static String socialSusDropDownXpath()
    {
        return "//div[@id='control_01699214-099D-41EF-9C3D-7E2C83510000']";
    }

    public static String occHygieneXpath()
    {
        return "//li[@id='tab_22127382-55B8-49C4-BBF2-89F59794D122']";
    }

    public static String occHygieneDropDownXpath()
    {
        return "//div[@id='control_8D7C561A-C604-484C-91E3-A5A029C75B51']";
    }

    public static String complianceXpath()
    {
        return "//li[@id='tab_A41DFD6D-7804-41EA-AB3D-9ABF5EDE0BE7']";
    }

    public static String complianceDropDownXpath()
    {
        return "//div[@id='control_5ACB10F8-13FF-42DF-873C-D79241E16399']";
    }

    public static String personInvolvedXpath()
    {
        return "//span[text()='Persons Involved']";
    }

    public static String personInvolvedAddXpath()
    {
        return "//div[@id='control_099C8373-B255-4407-B983-91ED6318453A']//div[@i18n='add_new']";
    }

    public static String personInvolvedNameXpath()
    {
        return "//div[@id='control_F476AA51-55D8-4537-8C78-5B2EEC764093']//input[@language]";
    }

    public static String personInvolvedDecXpath()
    {
        return "//div[@id='control_5C18C41F-812C-4224-AEF1-79A07DEE0676']//textarea";
    }

    public static String witnessStatementsXpath()
    {
        return "//span[text()='Witness Statements']";
    }

    public static String waitTableWitness()
    {
        return "//div[@instanceid='FCE0307F-50FE-403C-BDFB-ED293325F9DA'][@data-role]//table[@data-role]";
    }

    public static String injuredPersonswait()
    {
        return "//div[@instanceid='510481BC-5774-4957-920F-C8323A843BF9'][@data-role]//table[@data-role]";
    }

    public static String waitRegulatoryTable()
    {
        return "//div[@instanceid='70D0CF9E-D264-4EAA-8A27-C6D30650438D'][@data-role]//table[@data-role]";
    }

    public static String waitTableEquipAndTool()
    {
        return "//div[@instanceid='CEAD7953-CD9C-4E8A-ADB8-15C8AA6A092F'][@data-role]//table[@data-role]";
    }

    public static String euipmentInvolvedTabXpath()
    {
        return "//span[text()='Equipment Involved']";
    }

    public static String euipmentAddXpath()
    {
        return "//div[text()='Equipment and Tools']//..//div[@i18n='add_new']";
    }

    public static String euipmentTabXpath()
    {

        return "//div[text()='Equipment and Tools']//..//div[@i18n='add_new']";
    }

    public static String saveVerAndAddXpath()
    {
        return "//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@i18n='save']";
    }

    public static String assetTypeDropDownXpath()
    {
        return "//div[@id='control_C5608592-40BB-43E3-B988-AD520AB81BF1']";
    }

    public static String saveEquipToolsXpath()
    {
        return "//div[@id='btnSave_form_CEAD7953-CD9C-4E8A-ADB8-15C8AA6A092F']//div[@i18n='save']";
    }

    public static String closeEquipToolXpath()
    {
        return "//div[@id='form_CEAD7953-CD9C-4E8A-ADB8-15C8AA6A092F']//i[@class='home icon search']/../i[@class='close icon cross']";
    }

    

    public static String anyClickOptionContainsXpath(String option)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'"+option+"')]";
    }

   
    public static String anyDropDownCheckXpath(String option)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']//../i[@class='jstree-icon jstree-checkbox']";
    }

    public static String saveIncidentManagementFullInvestigation()
    {
        return "//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']";
    }

    public static String anyContainsDropDownCheckXpath(String option)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + option + "')]//../i[@class='jstree-icon jstree-checkbox']";
    }

    public static String assetDropDownXpath()
    {

        return "//div[@id='control_0A75D091-94E4-4A68-A9D6-6D67562F2F4C']";

    }

    public static String equipmentDescXpath()
    {
        return "//div[@id='control_57465454-B90F-4638-97AB-CD04E42635E1']//input[@language]";
    }

    public static String operatorEmTypeDropdownpath()
    {
        return "//div[@id='control_80696A19-7EFC-4DC9-87EF-87DFDBBB5B15']";
    }

    public static String operatorDropdownpath()
    {
        return "//div[@id='control_F17E41FB-2A2D-410F-8219-6FD01F15E039']";
    }

    public static String timeOfDayDropdownpath()
    {
        return "//div[@id='control_8C6FBB79-AE8F-4A16-A835-7EBE7D6CEC26']";
    }

    public static String weatherCondDropdownpath()
    {
        return "//div[@id='control_96006618-3C3A-4573-961C-D249943B7266']";
    }

    public static String roadConditionsCondDropdownpath()
    {
        return "//div[@id='control_17554023-4D73-4833-8C46-30D23FC6FCAA']";
    }

    public static String roadTypeDropdownpath()
    {
        return "//div[@id='control_FBF71D96-5A7F-4B41-A3BD-93A64AA12285']";
    }

    public static String InsuranceCoverDropdownpath()
    {
        return "//div[@id='control_662EF4A6-19C6-44FF-B645-3250C2A3A908']";
    }

    public static String motorVehiTabXpath()
    {
        return "//span[text()='Motor Vehicle Accident Information']";
    }

    public static String descrOfDamageXpath()
    {
        return "//div[@id='control_A8336A99-8A31-491D-8776-C2F5A76EA3F9']//textarea";
    }

    public static String damageCostXpath()
    {
        return "//div[@id='control_AE6023AB-8BA0-4AC4-BFCE-E64EE9943D5B']//input[@language]";
    }

    public static String deductibleAmountXpath()
    {
        return "//div[@id='control_FEA0FD6D-ECC3-4CBF-9237-CCF0BB7D1181']//input[@language]";

    }

    public static String witnessStatementsAddXpath()
    {
        return "//div[text()='Witness Statements']//..//div[@i18n='add_new']";
    }

    public static String witnessNameXpath()
    {
        return "//div[@id='control_4E6AE5EC-D5F8-40FD-88E5-7FEF114D2EC9']//input[@language]";
    }

    public static String witnessSurnameXpath()
    {
        return "//div[@id='control_E01BF26E-A948-4BED-9935-AAEE045C3521']//input[@language]";
    }

   

    public static String EnvironmentSaveWait()
    {
        return "//div[@id='form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@class='ui inverted dimmer active']";
    }

    public static String EnvironmentNotActiveSaveWait()
    {
        return "//div[@id='form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@class='ui inverted dimmer']";
    }

    public static String saveInjuredPersonXpath()
    {
        return "//div[@id='form_510481BC-5774-4957-920F-C8323A843BF9']//div[@class='ui inverted dimmer']";
    }

    public static String saveActiveInjuredPersonXpath()
    {
        return "//div[@id='form_510481BC-5774-4957-920F-C8323A843BF9']//div[@class='ui inverted dimmer active']";
    }

    public static String assestWait(String name)
    {
        return "//div[text()='" + name + "']";
    }

    

    public static String loadingDataIncidenrSearch()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Loading data...']";

    }

    public static String SaveRegulatoryWait()
    {
        return "//div[@id='formWrapper_70D0CF9E-D264-4EAA-8A27-C6D30650438D']/..//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }

    public static String loadingPermissions()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Loading permissions']";

    }

    public static String loadingPermission()
    {
        return "//div[contains(text(),'Loading permissions')]";
    }

    public static String loadingPermissionActive()
    {
        return "//div[@class='ui inverted dimmer active']/div[text()='Loading permissions']";

    }

    public static String loadingActive()
    {
        return "";

    }

    public static String loadingForms()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Loading form...']";

    }

    public static String environmentalSpillLoadingFormActive()
    {
        return "//div[@class='form active transition hidden']//div[text()='Loading form...']";
    }

    public static String environmentalSpillLoadingFormNotActive()
    {
        return "//div[@class='form active transition visible']//div[text()='Loading form...']";
    }

    public static String loadingFormsActive()
    {
        return "//div[@class='ui inverted dimmer active']/div[text()='Loading form...']";

    }

    public static String loadingFormsActiveSearch()
    {
        return "//div[@id='divFormLoading'][@class='form active transition visible']";
    }

    public static String loadingFormsActiveSearchDone()
    {
        return "//div[@id='divFormLoading'][@class='form active transition hidden']";
    }

    public static String witnessSummaryXpath()
    {
        return "//div[@id='control_F53C69CD-96A0-465E-9CBE-244FAE1B4780']//textarea";
    }

    public static String witnessStatementFlow()
    {
        return "//div[@id='btnProcessFlow_form_FCE0307F-50FE-403C-BDFB-ED293325F9DA']";
    }

    public static String regularyAuthorityFlow()
    {
        return "//div[@id='btnProcessFlow_form_BE4AD883-61A6-44BE-A6D4-ED24A897D71C']";
    }

    public static String editPhaseActive()
    {
        return "//div[@id='divFlow_form_FCE0307F-50FE-403C-BDFB-ED293325F9DA'][not(contains(@style, 'display: none;'))]//div[@class='step active']";
    }

    public static String regularyAuthorityeditPhaseActive()
    {
        return "//div[@id='divProcess_70D0CF9E-D264-4EAA-8A27-C6D30650438D'][not(contains(@style, 'display: none;'))]//div[@class='step active']";
    }

    public static String regularyAuthority_SaveAndClose_Button(){
        return "//div[@id='control_9C53896F-CD4D-4B1F-9CFE-7E8FE48244FA']";
    }
    public static String regularyAuthoritySave()
    {
        return "//div[@id='btnSave_form_BE4AD883-61A6-44BE-A6D4-ED24A897D71C']";
    }

    public static String witnessSaveCloseButtonXpath()
    {
        return "//div[@id='control_85085CF3-A567-4BB4-AA88-785C6564C3C8']//div[text()='Save and close']";
    }

    public static String witnessSaveButtonXpath()
    {
        return "//div[@id='btnSave_form_FCE0307F-50FE-403C-BDFB-ED293325F9DA']//div[text()='Save']";
    }

    public static String isReportableToRegulatoryAuthorityCheckboxXpath()
    {
        return "//input[@id='chk101']/..";
    }

    public static String safetyTab()
    {
        return "//div[text()='Safety']";
    }

    public static String safetyRegulatoryAuthorityHeadingXpath()
    {
        return "//span[text()='Safety Regulatory Authority']";
    }

    public static String regulatoryAuthorityAddButtonXpath()
    {
        return "//div[@id='control_8C4372CA-39BA-4187-877C-9E2F02C32DF7']//div[text()='Regulatory Authority ']/..//div[@title='Add']";
    }

    public static String regulatoryAuthority()
    {
        return "//div[@id='control_B58E20B0-261B-481C-982B-0CFAD75B5997']";
    }

    public static String regulatoryAuthorityInput(String authority)
    {
        return "//a[text()='" + authority + "'][not(contains(@class,'jstree-anchor jstree-clicked'))]";
    }

    public static String regulatoryAuthorityInput1(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'"+data+"')]";
    }

    public static String regulatoryAuthorityItem()
    {
        return "//div[@class='select3-drop select3-drop-ddl select3_6d25d9cb transition visible']//*[@id='e2a52df9-cd89-48b3-9ad6-16ceff1c4693_anchor']";
    }

    public static String contactPersonXpath()
    {
        return "//div[@id='control_9E07C773-D45B-43AA-B8D8-E37F2949A6C6']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String contactNumberXpath()
    {
        return "//div[@id='control_01757065-A495-4AEB-BEB5-3DD4910CA67E']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String contactEmailXpath()
    {
        return "//div[@id='control_FB3DCA41-4C12-4A60-B97A-2762D7966770']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String dateXpath()
    {
        return "//div[@id='control_ECB2B00F-CA0B-42DD-8014-638125746A46']//input[@data-role='datepicker']";
    }

    

    public static String communicationSummaryXpath()
    {
        return "//div[@id='control_6DF5113D-B3F1-4B24-AC01-2F1F1A8B3AA5']//textarea";
    }

    public static String saveAndCloseXpath()
    {
        return "//div[@id='control_9C53896F-CD4D-4B1F-9CFE-7E8FE48244FA']//div[text()='Save and close']";
    }

    public static String EquipAndTools_Save_Xpath()
    {
        return "//div[@id='btnSave_form_CEAD7953-CD9C-4E8A-ADB8-15C8AA6A092F']//div[text()='Save']";
    }

    public static String fr6link()
    {
        return "//div[@id='control_2AB69419-7475-49CE-AF0E-CE0E6119B9B7']//b[@class='linkbox-link']";
    }

    public static String linkop()
    {
        return "//div[@id='control_8FAF5512-5015-4A5E-8E65-14626A22BE9D']//b[@class='linkbox-link']";
    }

    public static String linkalt()
    {
        return "//div[@id='control_4E4BC8C7-0144-47D5-B972-5A7B29E557BB']//b[@class='linkbox-link']";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    

    public static String witnessStatementsProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_FCE0307F-50FE-403C-BDFB-ED293325F9DA']";
    }

    public static String anyClickOptionArrowXpath(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]/../i";
    }

    public static String anyActiveOptionsDropdownXpath(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'"+data+"')]";
    }

    public static String anyClickOptionContainXpath(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'"+data+"')]";
    }

    public static String anyOptionXpath(String data)
    {

        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";

    }

    public static String equipAndToolsProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_CEAD7953-CD9C-4E8A-ADB8-15C8AA6A092F']";
    }

    public static String detailsTab()
    {
        return "//div[text()='Details']";
    }

    public static String signOffRequiredChckBox()
    {
        return "//div[@id='control_9CDCBA85-8A1F-44D0-9488-481FBAB9C9A9']//div//div";
    }

    public static String signOff_DD()
    {
        return "//div[@id='control_CFC88ED0-299D-48D1-B904-5B765F6C1821']";
    }

    public static String signOffSelect(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'"+data+"')]";
    }

    public static String ifReportableChckBox()
    {
        return "//div[@id='control_E7B42CEC-A97A-4637-BA2A-E6569513671B']//div//div";
    }

    public static String regulatoryAdd()
    {
        return "//div[@id='control_5F3CDBFF-2101-4E6C-AA2D-60A1A830673F']//div[text()='Add']";
    }

    public static String regulatoryAuthorityPanel()
    {
        return "//div[@id='control_B46DEC6A-70BB-4667-892B-5E66FE4F4703']//span[contains(text(),'Regulatory Authority')]";
    }

    public static String saveAndClose()
    {
        return "//div[@id='control_9C53896F-CD4D-4B1F-9CFE-7E8FE48244FA']";
    }
    public static String inspection_RecordSaved_popup() {
        return "//div[@id='divForms']//div[contains(text(),'Record saved')]";
    }
    public static String recordSaved_popup() {
        return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])[2]";
    }
    public static String failed(){
       return "(//div[@id='txtAlert'])[2]";
   }
    
    public static String SupportingDocumentsTab() {
        return "//div[text()='Supporting Documents']";
    }

    public static String VerificationAndAdditionalTab() {
        return "//li[@id='tab_90D1C7C9-45EE-402F-B76C-A61222A110E1']//div[text()='2.Verification and Additional Detail']";
    }

    public static String activeUnderInvesgationXpath() {
        return "//div[text()='3.Under Investigation']//../..//div[@class='step active']";
    }

    public static String SaveButtonXPath() {
        return "//div[text()='Save supporting documents']";
    }

    public static String PersonsInvolvedXpath() {
        return "//div[@class='nreqbox control pnl transition visible']//span[text()='Persons Involved']";
    }

    public static String WitnessStatementsXpath() {
        return "//div[@class='nreqbox control pnl transition visible']//span[text()='Witness Statements']";
    }

    public static String EquipmentInvolvedTabXpath() {
        return "//div[@class='nreqbox control pnl transition visible']//span[text()='Equipment Involved']";
    }

    public static String ValidateGridIsEditableXPath() {
        return "//div[@id='control_099C8373-B255-4407-B983-91ED6318453A']//div[@class='grid k-grid k-widget k-reorderable editable']";
    }

    public static String ValidateGridIsNotEditableXPath() {
        return "//div[@id='control_5E4C6D37-0D4E-42D1-AC3D-B1E210B05DD9']//div[@class='grid k-grid k-widget k-reorderable']";
    }

    public static String EuipmentValidateGridIsNotEditableXPath() {
        return "//div[@id='control_45349B8B-5312-4623-BF54-BE4D75D4FB8F']//div[@class='grid k-grid k-widget k-reorderable']";
    }

    public static String ANewRowIsDisplayedXPath() {
        return "//div[@id='control_099C8373-B255-4407-B983-91ED6318453A']//tbody//tr[@role='row']";
    }

    public static String SaveXPath() {
        return "//div[@id='control_099C8373-B255-4407-B983-91ED6318453A']//div[@title='Save']";
    }

    public static String CloseWitnessStatementsXPath() {
        return "//div[@id='form_FCE0307F-50FE-403C-BDFB-ED293325F9DA']//i[@class='home icon search']/../i[@class='close icon cross']";
    }

    public static String WaitForItemXPath(String name) {
        return "//div[@id='control_D0351C3F-0C62-47C4-83CE-5DDB30BBD5DF']//div[text()='" + name + "']";
    }

    public static String RestrictionsTextAreaXPath() {
        return "//div[@id='control_53B19CFD-7FE1-40A3-B77D-D01693699C63']//textarea";
    }

    public static String RestrictionsTextAreaHiddenXPath() {
        return "//div[@id='control_53B19CFD-7FE1-40A3-B77D-D01693699C63'][@class='nreqbox control pnl transition hidden']//textarea";
    }

    public static String DrugAndAlcoholLoadingXPath() {
        return "//div[@class='ui active inverted dimmer transition visible']//div[id='txtWait']";
    }

    public static String DrugAndAlcoholNoResultsXPath() {
        return "//div[@id='control_DDED67AF-D58A-419D-9696-B26C394C0133']//div[text()='No results returned']";
    }

    public static String DrugAndAlcoholTableXPath() {
        return "//div[@id='control_DDED67AF-D58A-419D-9696-B26C394C0133']";
    }

    public static String ConductDrugAndAlcoholTestButtonXPath() {
        return "//div[@id='control_882404B6-DD63-4169-AAAB-1A71C3CCE79E']//div[text()='Conduct Drug and Alcohol test  ']";
    }

    public static String SaveAndCloseXPath() {
        return "//div[@id='control_C17F80E0-588B-4A71-814B-A2FEEE9D4B3B']//div[text()='Save and close']";
    }

    public static String LoadingDataActiveXPath() {
        return "//div[@id='form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@class='ui inverted dimmer']//div[@class='ui text loader'][text()='Loading data...']";
    }

    public static String WaitForTableToLoadXPath() {
        return "//div[@class='ui inverted dimmer hidden']//div[@title='Loading...']";
    }

    public static String LoadingFormXPath() {//div[@id='form_510481BC-5774-4957-920F-C8323A843BF9']//div[@class='ui inverted dimmer']//div[text()='Loading data...']
        return "//div[@id='form_510481BC-5774-4957-920F-C8323A843BF9']//div[@class='ui inverted dimmer']//div[text()='Loading data...']";
    }//div[@class='ui active inverted dimmer']//div[text()='Loading form...']

    public static String MedicalTestDateXPath() {
        return "//div[@id='control_5735F7DF-7695-4096-9ADB-9C21AA9D3FD7']//input";
    }

    public static String AppointmentDateXPath() {
        return "//div[@id='control_4E1D0588-3512-41B8-BAF8-DC8517754FEF']//input";
    }

    public static String PersonSubmitToATestXPath() {
        return "//div[@id='control_711BFF97-3ECC-4FEC-8F3B-F6A3C080684C']//li";
    }

    public static String DrugTestResultXPath() {
        return "//div[@id='control_E47B8DA0-0088-4009-9B6F-4EE8AA8F96C1']//li";
    }

    public static String AlcoholTestResultXPath() {
        return "//div[@id='control_C50181BC-31EF-44B6-AD76-D2BE7DFF4844']//li";
    }

    public static String SaveDrugAndAlcoholTestXPath() {
        return "//div[@id='form_CBE44FB8-CECF-4F77-8BC2-2A82882035E4']//div[text()='Save']";
    }

    public static String RefreshAlcoholAndDrugsXPath() {
        return "//div[@id='control_DDED67AF-D58A-419D-9696-B26C394C0133']//a[@title='Refresh']";
    }

    public static String PersonBeingTestedTableXPath(String record) {
        return "//span[@title='" + record + "']";
    }

    public static String concessions_QualityTab_xpath() {
        return "//li[@id='tab_83609D85-9F2F-4BAF-8CB2-887CA05FC034']//div[contains(text(),'Quality')]";
    }

    public static String concessions_QualityConcession_Expandxpath() {
        return "//span[contains(text(),'Quality Concessions ')]";
    }

    public static String concessions_Add_Buttonxpath() {
        return "//div[contains(text(),'Concession')]//..//div[@title='Add']";
    }
    
    public static String concessions_RailwaySafetySupervisor_DropdownXpath(){
        return "//div[contains(text(),'Railway safety supervisor')]/../../div[2]//li";
    }

    public static String concessions_Description_TextAreaxpath() {
        return "//div[@id='control_04842A6A-3005-4D07-9131-6B25C1373134']//textarea";
//        return "//div[@class='select3-drop select3-drop-ddl select3_e6f1606e transition visible']//input[@placeholder='Type to search']";    
    }

    public static String concessions_Impact_TextAreaxpath() {
        return "//div[@id='control_9ECE066A-3D4C-4209-B2A3-54ED4D4DF4E8']//textarea";
    }

    public static String concessions_CorrectiveAction_TextAreaxpath() {
        return "//div[@id='control_83CBB14B-6E16-490E-B408-EF19607B33AD']//textarea";
    }

    public static String concessions_AllPartiesAffected_CheckBoxxpath() {
        return "//div[@id='control_667887E0-CE52-4221-BB30-216FE9041A02']//b[@class='select3-all']";
    }

    public static String concessions_ResponsiblePerson_SelectFieldxpath() {
        return "//div[@id='control_97EE57FD-6D77-47A7-94E4-7726E6C129C5']//li[contains(text(),'Please select')]";
    }

    public static String concessions_ResponsiblePerson_TypeSearchxpath() {
//        return "//ul[@class='select3-results jstree jstree-29 jstree-default jstree-loading']//..//input[@type='text']";
        return"//ul[@class='select3-results jstree jstree-30 jstree-default jstree-loading']//..//input[@type='text']";
    }

    public static String concessions_ResponsiblePerson_xpath(String person) {
//        return "//ul[@class='select3-results jstree jstree-30 jstree-default jstree-loading']//..//a[contains(text(),'" + person + "')]";
        return "//div[@id='divForms']/div[33]//a[contains(text(),'"+person+"')]";
    }

    public static String flowProcessXpath() {
        return "//div[@id='btnProcessFlow_form_92519649-BEF5-4E75-9F01-FEF7B6B6243E']";
    }
    
     public static String responsiblePerson(String option) {
        return "//div[contains(@class, 'transition visible')][contains(@class, 'select3-drop select3-drop-ddl')]//a[text()='" + option + "']";
    }

    public static String concessions_StartDate_InputAreaxpath() {
        return "//div[@id='control_F7F730D0-04B1-4408-B1C2-8B7977A9EEA5']//input";
    }

    public static String concessions_EndDate_InputAreaxpath() {
        return "//div[@id='control_271024EF-4AA9-406F-A3E0-9A688EDCCA44']//input";
    }

    public static String concessions_Priority_SelectFieldxpath() {
        return "//div[@id='control_5C97E0A8-1CEE-4C48-A5A8-241D19A335BF']//li";
    }
       
    public static String concessions_Priority_TypeSearchxpath() {
//        return "//ul[@class='select3-results jstree jstree-30 jstree-default jstree-loading']//..//input";
        return"//ul[@class='select3-results jstree jstree-31 jstree-default jstree-loading']//..//input";
    }

    public static String concessions_Priority_xpath(String priority) {
//        return "//ul[@class='select3-results jstree jstree-30 jstree-default jstree-loading']//..//a[contains(text(),'" + priority + "')]";
//        return"//ul[@class='select3-results jstree jstree-31 jstree-default jstree-loading']//..//a[contains(text(),'"+ priority +"')]";
        return "//div[@id='divForms']/div[34]//a[text()='"+priority+"']";
    }

    public static String concessions_SaveToContinue_Buttonxpath() {
        return "//div[@id='control_99941FE9-6B06-4946-A5EA-446872CDDDE6']//div[contains(text(),'Save to continue')]";
    }

    public static String RecordNumberXPath() {
        return "//div[contains(text(),'Record #')]";
    }

    public static String concessionsAction_Add_Buttonxpath() {
        return "//div[contains(text(),'Concession Action')]//..//div[@title='Add']";
    }

    public static String concessionActionActive() {
        return "//div[@id='form_92519649-BEF5-4E75-9F01-FEF7B6B6243E'][@class='form active transition visible']";

    }

    public static String Concession_Status_XPath() {
        return "//div[@id='control_DECFB794-FDE7-4085-BBAE-06738217604F']//..//li[@title='Logged']";
    }

    public static String concessionsAction_Description_Textfieldxpath() {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String concessionsAction_DepartmentResponsible_Buttonxpath() {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//li";
//        return "//div[contains(text(),'Action Detail')]/../../../../../..//div[@class='tabpanel']/div[2]/div[1]/div[1]/div[4]//li";
    }

    public static String concessionsAction_DepartmentResponsible_xpath(String department) {
//        return"//ul[@class='select3-results jstree jstree-33 jstree-default jstree-loading']//a[contains(text(),'"+ department +"')]";
//        return "//ul[@class='select3-results jstree jstree-32 jstree-default jstree-loading']//a[contains(text(),'" + department + "')]";
        return "//div[@id='divForms']/div[38]//a[text()='"+department+"']";

    }

    public static String concessionsAction_ResponsiblePerson_SelectFieldxpath() {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//li[contains(text(),'Please select')]";
    }

    public static String concessionsAction_ResponsiblePerson_TypeSearchxpath() {
        return"//ul[@class='select3-results jstree jstree-34 jstree-default jstree-loading']//..//input";
//        return "//ul[@class='select3-results jstree jstree-33 jstree-default jstree-loading']//..//input";
    }

    public static String concessionsAction_ResponsiblePerson_xpath(String person) {
//        return"//ul[@class='select3-results jstree jstree-34 jstree-default jstree-loading']//..//a[contains(text(),'"+ person +"')]";
//        return "//ul[@class='select3-results jstree jstree-33 jstree-default jstree-loading']//..//a[contains(text(),'" + person + "')]";
        return "//div[@id='divForms']/div[39]//a[contains(text(),'"+person+"')]";
    }

    public static String concessionsAction_ActionDueDate_Textfieldxpath() {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }

    public static String concessionsAction_Save_DropDownxpath() {
        return "//div[@id='btnSave_form_92519649-BEF5-4E75-9F01-FEF7B6B6243E']//div[@class='more options icon chevron down']";
    }

    public static String concessions_Save_DropDownxpath() {
        return "//div[@id='btnSave_form_B3205F35-ED12-44E1-BD2F-995318C72096']//div[@class='more options icon chevron down']";
    }

    public static String concessionsAction_Save_DropDownMorexpath() {
        return "//div[@id='btnSave_form_92519649-BEF5-4E75-9F01-FEF7B6B6243E']//div[@class='options icon chevron down more']";
    }

    public static String concessionsAction_SaveAndClose_Buttonxpath() {
        return "//div[@id='btnSave_form_92519649-BEF5-4E75-9F01-FEF7B6B6243E']//div[@title='Save and close']";
    }

    public static String toBeInitiatedXpath() {
        return "//div[text()='To be initiated']//../..//div[@class='step active']";
    }

    public static String concessionActionsCloseButton() {
        return "//div[@id='form_92519649-BEF5-4E75-9F01-FEF7B6B6243E']//i[@class='home icon search']/..//i[@class='close icon cross']";
    }

    public static String getConcessionActionRecordID() {
        return "//div[@id='form_92519649-BEF5-4E75-9F01-FEF7B6B6243E']//div[@class='recnum']//div[@class='record']";
    }

    public static String inProgress() {
        return "//div[@id='control_DECFB794-FDE7-4085-BBAE-06738217604F']//li[@title='In Progress']";
    }

    public static String concessions_SaveAndClose_On_More_Buttonxpath() {
        return "//div[@id='btnSave_form_B3205F35-ED12-44E1-BD2F-995318C72096']//div[@title='Save and close']";
    }

    public static String concessionsAction_Save_Buttonxpath() {
        return "//div[@id='btnSave_form_92519649-BEF5-4E75-9F01-FEF7B6B6243E']//..//div[@title='Save']";
    }

    public static String retrievedConcessionActionRecordNumberXPath() {
        return "//div[@id='form_92519649-BEF5-4E75-9F01-FEF7B6B6243E']//div[contains(text(),'- Record #')]";
    }

    public static String retrieveConcessionRecordNumberXPath() {
        return "//div[@id='form_B3205F35-ED12-44E1-BD2F-995318C72096']//div[contains(text(),'- Record #')]";
    }

    public static String concessionsAction_OpenSsavedConcessionRecord_SelectField(String concessionActionRecordNumber) {
        return "//span[contains(text(),'" + concessionActionRecordNumber + "')]";
    }

    public static String concessionsAction_RefreshRecords_Buttonxpath() {
        return "//div[contains(text(),'Concession Actions')]//..//..//a[@title='Refresh']";
    }

    public static String concessions_Save_DropDownMorexpath() {
        return "//div[@id='btnSave_form_B3205F35-ED12-44E1-BD2F-995318C72096']//div[@class='more options icon chevron down']";
    }

    public static String concessions_SaveAndClose_Buttonxpath() {
        return "//div[@id='btnSave_form_B3205F35-ED12-44E1-BD2F-995318C72096']//div[@title='Save and close']";
    }

    public static String incidentManagement_VerifAndAddiDetails_Tabxpath() {
        return "//div[@class='tabpanel_move_content tabpanel_move_content_scroll']//div[contains(text(),'2.Verification and Additional Detail')]";
    }

    public static String concessions_OpenRecord_Selectxpath(String recordNumber2) {
        return "//span[contains(text(),'" + recordNumber2 + "')]";
    }

    //Risk And Impact Assessment xpaths
    public static String RiskAndImpactAssessment_Tab_ButtonXpath() {
        return "//div[contains(text(),'Risk and Impact Assessment')]";
    }

    public static String flowProcess_ButtonXpath() {
        return "//div[@id='btnProcessFlow_form_B6196CB4-4610-463D-9D54-7B18E614025F']";
    }

    public static String RiskAndImpactAssessment_DeclarationCheckBox_CheckBoxXpath() {
        return "//div[@id='control_8AC963E8-5EB7-492E-8371-DFAB7FE3B5AD']//div[@class='c-chk']";
        //return"//div[@id='control_8AC963E8-5EB7-492E-8371-DFAB7FE3B5AD']//div[@class='icheckbox icheck-item icheck[o7d9e]']";
    }

    public static String RiskAndImpactAssessment_DeclarationComment_InputXpath() {
        return "//div[@id='control_280FC061-EC2C-4A95-916F-6D47A94C4758']//textarea";
    }

    public static String RiskAndImpactAssessment_LeadInvestigator_SelectFieldxpath() {
        return "//div[@id='control_97D0FB6B-ABA3-47F8-A4C6-8E0DB2C5E71A']//li";
    }

    public static String RiskAndImpactAssessment_LeadInvestigator_TypeSearchxpath() {
        return "//ul[@class='select3-results jstree jstree-29 jstree-default jstree-loading']//..//input";
    }

    public static String RiskAndImpactAssessment_LeadInvestigator_xpath(String lead) {
        return "(//a[contains(text(),'"+lead+"')])[9]";
    }

    public static String RiskAndImpactAssessment_SaveAndContinueToStep3_ButtonXpath() {
        return "//div[@id='control_9E3EFB23-6979-4773-8B4C-E8B0D6F49952']//div[contains(text(),'Save and continue to Step 3')]";
    }

    public static String investigationTab() {
        return "//div[text()='3.Incident Investigation']";
    }

    public static String RiskAndImpactAssessment_Save_ButtonXpath() {
        return "//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@class='more options icon chevron down']";
        //return"//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@title='Save']";
    }

    public static String RiskAndImpactAssessment_SaveAndClose_ButtonXpath() {
        return "//div[@id='btnSaveClose_form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@title='Save and close']";
    }

    public static String incidentManagement_InvestigationStatus_TitleXpath() {
        return "//div[@id='control_861FFF0E-9C05-40A9-8DB5-86FD61929844']";
    }

    public static String incidentManagement_InvestigationStatus_RetrieveXpath() {
        return "//div[@id='control_61A17BC2-21A9-463E-ADB9-536DCCAFCF4B']";
    }

    public static String RiskAndImpactAssessment_SubmitStep2_ButtonXpath() {
        return "//div[contains(text(),'Submit Step 2')]";
    }

    public static String RiskAndImpactAssessment_Refresh_ButtonXpath() {
        return "//a[@title='Refresh']";
    }

    //Capture Investigation team
    public static String investigationTeam_DueDate_textFieldXpath() {
        return "//div[@id='control_7E7E7BF6-08EF-4008-BEDA-5B7E422D739F']//input";
    }

    public static String investigationTeam_Scope_textFieldXpath() {
        return "//div[@id='control_669476AC-06EA-4FAE-9872-D47FB2469048']//textarea";
    }

    public static String investigationTeam_InvestigationTeam_DropDownXpath() {
        return "//span[contains(text(),'Investigation Team')]";
    }

    public static String investigationTeam_InvestigationTeam_ADD_ButtonXpath() {
        return "//div[@id='control_3006D33A-81F0-4C4F-A7F7-3289CCEEA80E']//div[@title='Add']";
    }

    public static String investigationTeam_Fullname_SelectFieldXpath() {
        return "//div[@id='control_74F3ABB3-D328-4733-BFC4-9D151C8478D3']//li";
    }

    public static String investigationTeam_Fullname_Xpath(String person) {
        return "//a[contains(text(),'" + person + "')]";
    }

    public static String investigationTeam_Role_TextFieldXpath() {
        return "//div[@id='control_9D9BAC87-D7A1-4552-A9B0-08F2ACCC4157']//input";
    }

    public static String investigationTeam_StartDate_TextFieldXpath() {
        return "//div[@id='control_B1A6F86B-56C1-4100-81F6-F9DE01644413']//input";
    }

    public static String investigationTeam_EndDate_TextFieldXpath() {
        return "//div[@id='control_0957E548-2540-4095-98A0-A8C30DB9FB72']//input";
    }

    public static String investigationTeam_Hours_TextFieldXpath() {
        return "//div[@id='control_CEE2D645-02D7-4931-9474-A2D4706B148B']//input";
    }

    public static String investigationTeam_InvestigationTeam_TitleXpath() {
        return "//div[contains(text(),'Investigation Team')]";
    }

    public static String investigationTeam_Save_textFieldXpath() {
        return "//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']//..//div[@title='Save']";
    }

    public static String investigationTeam_Save_ButtonXpath() {
        return "//div[@id='control_3006D33A-81F0-4C4F-A7F7-3289CCEEA80E']//div[@title='Save']";
    }

    public static String incidentInvestigation_InvestigationDetail_Tabxpath() {
        return "//div[contains(text(),'Investigation Detail')]";
    }

    public static String IncidentMangementStatusAfterSavingXPath() {
        return "//div[@id='control_B1667955-9616-471C-BBEC-14F334A90E65']//li";
    }

    public static String incidentInvestigation_ProcessOrActivity_Layoutxpath() {
        return "//div[@id='control_CC6900A0-1399-4340-AB58-7D532D79BEC8']//li";
    }

    public static String incidentInvestigation_ProcessOrActivity_TickAllxpath() {
        return "//div[@id='control_CC6900A0-1399-4340-AB58-7D532D79BEC8']//b[@original-title='Select all']";
    }

    public static String incidentInvestigation_RiskSource_Layoutxpath() {
        return "//div[@id='control_00829F4C-092C-4F33-8824-2661C7BCFA77']//li";
    }

    public static String incidentInvestigation_RiskSource_TickAllxpath() {
        return "//div[@id='control_00829F4C-092C-4F33-8824-2661C7BCFA77']//b[@original-title='Select all']";
    }

    public static String incidentInvestigation_Risk_Layoutxpath() {
        return "//div[@id='control_F1868BAD-518A-47C2-B452-81FA4EF109A9']//li";
    }

    public static String incidentInvestigation_Risk_TickAllxpath() {
        return "//div[@id='control_F1868BAD-518A-47C2-B452-81FA4EF109A9']//b[@original-title='Select all']";
    }

    public static String incidentInvestigation_InvestigationType_Layoutxpath() {
        return "//div[@id='control_9033FCFD-1EC6-4C92-AFF0-0AAF4B37E191']//li";
    }

    public static String incidentInvestigation_InvestigationType_TickAllxpath(String type) {
        return "//ul[@class='jstree-container-ul jstree-children']//a[contains(text(),'" + type + "')]";
    }

    public static String incidentInvestigation_Refresh_Buttonxpath() {
        return "//div[@id='control_E8AC341B-3D7B-4A87-8C02-91EDF4A9EB34']//div[contains(text(),'Refresh')]";
    }

    public static String incidentInvestigation_RelatedRisk_Panelxpath() {
        return "//span[contains(text(),'Related Risks')]";
    }
    
    public static String incidentInvestigation_RelatedJSA_Panelxpath(){
        return "//div[@id='control_01B1F475-DC2E-4EEF-B0B4-6272EC0C05FC']//span[contains(text(),'Related JSA')]";
    }

    public static String incidentInvestigation_RelatedRisk_Openxpath() {
        return "((//div[@id='control_FF23B09D-0EB5-4815-B5E0-3653C4BE017F']//div//table)[3]//div[contains(text(),'Access control hazards -> Unauthorised access to hazardous areas')])[1]";
    }

    public static String incidentInvestigation_RelatedRisk_OpenedRecordxpath() {
        return "(//div[@id='form_3A80F1F4-AE90-47B2-B9BB-0C920589C04D']//div[contains(text(),'- Record #')])[1]";
    }

    public static String incidentInvestigation_RelatedJSAs_Panelxpath() {
        return "//span[contains(text(),'Related JSA')]";
    }

    public static String incidentInvestigation_BowtieRiskAssement_Panelxpath() {
        return "//span[contains(text(),'Related Bowties')]";
    }

    public static String incidentInvestigation_LearningAndApproval_tabxpath() {
        return "//li[@id='tab_4E355F73-BD7A-4459-A420-D5A01E939D21']//div[contains(text(),'Learnings & Approval ')]";
    }

    public static String incidentInvestigation_ManagementFindings_Add_Buttonxpath() {
        return "//div[@id='control_781A7295-A3B9-49BC-9CBA-5C18491C8649']//div[@title='Add']";
    }

    public static String incidentInvestigation_ManagementFindings_Description_TextFieldxpath() {
        return "//div[@id='control_40ECC722-B08B-48F3-9906-3CFCE527C5CD']//textarea";
    }

    public static String incidentInvestigation_ManagementFindings_FindingOwner_Selectxpath() {
        return "//div[@id='control_434D9128-5EBC-4E25-9836-72A2C4451733']//li";
    }

    public static String incidentInvestigation_ManagementFindings_FindingOwner_xpath(String owner) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + owner + "')]";
    }

    public static String incidentInvestigation_ManagementFindings_RiskSource_Selectxpath() {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']//li";
    }

    public static String incidentInvestigation_ManagementFindings_RiskSource_TickAllxpath() {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']//b[@class='select3-all']";
    }

    public static String incidentInvestigation_ManagementFindings_Classification_Selectxpath() {
        return "//div[@id='control_7689E71C-DC7E-41C1-9124-C6312596E956']//li";
    }

    public static String incidentInvestigation_ManagementFindings_Classification_xpath(String classification) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + classification + "')]";
    }

    public static String incidentInvestigation_ManagementFindings_Risk_Selectxpath() {
        return "//div[@id='control_1D7F3DE4-F9C4-4626-80C9-0E600A0A8FF8']//li";
    }

    public static String incidentInvestigation_ManagementFindings_Risk_xpath(String risk) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + risk + "')]";
    }

    public static String incidentInvestigation_ManagementFindings_Save_Selectxpath() {
        return "//div[@id='btnSave_form_0B2C6ED4-CC58-477E-931E-7949371422D0']//div[@title='Save']";
    }

    public static String incidentInvestigation_ManagementFindings_RecordNumber_Selectxpath() {
        return "//div[@id='form_0B2C6ED4-CC58-477E-931E-7949371422D0']//div[contains(text(),'- Record #')]";
    }

    public static String incidentInvestigation_ManagementFindings_Close_Selectxpath() {
        return "//div[@id='form_0B2C6ED4-CC58-477E-931E-7949371422D0']//i[2]";
    }

    public static String incidentInvestigation_ManagementFindings_ListedRecord_Listxpath(String retrieveRecordNumber) {
        return "//div[@id='grid']//span[@title='" + retrieveRecordNumber + "']";
    }

    public static String incidentInvestigation_RelatedJSAs_Refresh_Buttonxpath() {
        return "//div[@id='control_3CD0FC6D-FBE1-474A-9921-494B05186C6B']//span[@class='k-icon k-i-reload']";
    }

    public static String incidentInvestigation_BowtieRiskAssement_Refresh_Buttonxpath() {
        return "//div[@id='control_44411406-1DA6-493A-9A86-695A82FE16F9']//span[@class='k-icon k-i-reload']";
    }

    public static String incidentInvestigation_BowtieRiskAssement_Message_Buttonxpath() {
        return "//div[@id='control_44411406-1DA6-493A-9A86-695A82FE16F9']//span[@class='k-pager-info k-label']";
    }

    public static String incidentInvestigation_RelatedJSAs_Message_Buttonxpath() {
        return "//div[@id='control_3CD0FC6D-FBE1-474A-9921-494B05186C6B']//span[@class='k-pager-info k-label']";
    }

    public static String concessions_StatusDropDown_Buttonxpath() {
        return "//div[@id='control_DECFB794-FDE7-4085-BBAE-06738217604F']//span[@class='select3-arrow']";
    }

    public static String concessions_Status_InProgress_Buttonxpath() {
        return "//ul[@class='select3-results jstree jstree-34 jstree-default jstree-loading']//a[contains(text(),'In Progress')]";
        //return"//ul[@class='select3-results jstree jstree-32 jstree-default jstree-loading']//a[contains(text(),'In Progress')]";
    }

    public static String concessions_AffectedParties_CheckBoxXpath() {
        return "//div[@id='control_569C771D-5F69-4957-BF21-E480351D417E']//div[@class='c-chk']";
    }

    public static String concessions_SignOff_CheckBoxXpath() {
        return "//div[@id='control_9A96ECD0-D256-4A86-8F4B-A903EAB67FA2']//div[@class='c-chk']";
        //return"//div[@id='control_57BBCC92-234A-429D-810D-7A33F29DD0CF']//div[@class='c-lbl']";
    }

    public static String incidentActions_SaveMoreOptions_Buttonxpath() {
        return "//div[@id='btnSave_form_EE164B84-29D5-424F-A584-A66FC3FE9BCA']//div[@class='more options icon chevron down']";
    }

    public static String incidentActions_SaveAndClose_Buttonxpath() {
        return "//div[@id='btnSave_form_EE164B84-29D5-424F-A584-A66FC3FE9BCA']//div[@title='Save and close']";
    }

    public static String incidentInvestigation_TabXpath() {
        return "//li[@id='tab_3A331B31-BF4C-4C42-ADB3-936999FEE515']//div[contains(text(),'3.Incident Investigation')]";
    }

    public static String LearningAndApproval_Findings_Panelxpath() {
        return "//span[contains(text(),'Investigation Findings')]";
    }

    public static String incidentInvestigation_learnings_TextFieldxpath() {
        return "//div[@id='control_0711AFDC-7A24-45F4-9A7D-4B27F4104D49']//textarea";
    }

    public static String incidentInvestigation_Conclusion_TextFieldxpath() {
        return "//div[@id='control_46CC5397-E89F-406E-ACC0-6AC270D0E371']//textarea";
    }

    public static String LearningAndApproval_declaration_CheckBoxXpath() {
        return "//div[@id='control_1F2CA953-B448-4A17-B1D0-383EB46D1349']//div[@class='c-chk']";
    }

    public static String incidentInvestigation_Comments_TextFieldxpath() {
        return "//div[@id='control_8D1AE953-65A5-4E95-A6D6-3237EA4A48B1']//textarea";
    }

    public static String incidentInvestigation_SaveAndContinue_Buttonxpath() {
        return "//div[@id='control_2CF4FD6E-B9AA-44D4-801C-2940E5495E06']//div[contains(text(),'Save and continue to Step 4')]";
    }

    public static String incidentInvestigation_Submit_Buttonxpath() {
        return "//div[@id='control_851B10A6-EC82-47C0-8F18-8C8CB97D7963']//div[contains(text(),'Submit Step 3')]";
    }

    public static String IncidentManagementId() {
        return "//div[@id='form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@class='recnum']//div[@class='record']";
    }

    public static String incidentmanagement_Refresh_Buttonxpath() {
        return "//span[@class='k-icon k-i-reload']";
    }

    public static String incident_xpath(String incidentRec) {
        return "//span[@title='" + incidentRec + "']";
    }

    public static String EquipAndTools_Assets_PanelArrowXPath() {
        return "//span[contains(text(),'Assets')]";
    }

    public static String EquipAndTools_AssetsAdd_ButtonXPath() {
        return "//div[@id='control_BC26F4F4-386E-42B1-A767-33E77CA09F6E']//div[@title='Add']";
    }

    public static String assets_Process_SelectXPath() {
        return "//div[@id='control_28914C65-06E3-4C73-91DB-D0461DB0C410']//li";
    }

    public static String process_Production_ExpandXPath(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'"+data+"')]";
    }

    public static String process_ProductionXPath() {
        return "//div//a[contains(text(),'Financial controls')]";
    }

    public static String assets_RTORequired_SelectXPath() {
        return "//div[@id='control_C29B7061-6896-4086-8BDE-1AEEE2F7ED4F']//li";
    }

    public static String assets_RTORequired_XPath() {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'1:00')])[1]";
    }

    public static String assets_Quantity_XPath() {
        return "//div[@id='control_13675B86-8CC6-46D6-B54D-52838975DA99']//input[@type='number']";
    }

    public static String assets_Mandatory_SelectXPath() {
        return "//div[@id='control_985E4D2A-EE5B-4A61-A173-E1F1027864C8']//li";
    }

    public static String process_Mandatory_XPath(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'"+data+"')]";
    }

    public static String assets_Backup_SelectXPath() {
        return "//div[@id='control_E452C970-B034-44C2-B4EF-B20849E8B03F']//li";
    }

    public static String process_Backup_XPath(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'"+data+"')]";
    }

    public static String assets_Comments_XPath() {
        return "//div[@id='control_022B7F82-688D-4D84-9F2D-C21B02B429BD']//textarea";
    }

    public static String assets_Save_XPath() {
        return "//div[@id='btnSave_form_2000EE74-35E5-4218-90FC-47E8A0660483']//div[@title='Save']";
    }

    public static String assetId() {
        return "//div[@id='form_2000EE74-35E5-4218-90FC-47E8A0660483']//div[contains(text(),'- Record #')]";
    }

    public static String incidentActions_Close_Buttonxpath() {
        return "//i[@class='home icon search']/..//i[@class='close icon cross']";
    }

    public static String incidentManagement_Refresh_Buttonxpath() {
        return "//a[@title='Refresh']";
    }

    public static String incidentManagement_xpath(String rec) {
        return "//span[@title='" + rec + "']";
    }

    public static String IncidentSignOff_Tabxpath() {
        return "//div[contains(text(),'4.Incident Sign Off')]";
    }

    public static String IncidentSignOff_ActionSummary_Panelxpath() {
        return "//div[@id='control_13FDC30A-C7B7-46B8-9C9B-B32DFFE1E2D4']//i[@class='toggle']";
    }

    public static String IncidentSignOff_ActionSummaryRecord_xpath() {
        return "//div[@id='control_78D0FDEF-5BB7-47F5-855E-269AC2CAACE4']//td[6]";
    }

    public static String IncidentSignOff_ActionSummaryRecordNumber_xpath() {
        return "//div[@id='form_EE164B84-29D5-424F-A584-A66FC3FE9BCA']//div[contains(text(),'- Record #')]";
    }

    public static String equipAndToolsId() {
        return "//div[@id='form_CEAD7953-CD9C-4E8A-ADB8-15C8AA6A092F']//div[@class='recnum']//div[@class='record']";
    }

    public static String assets_ProcessFlow_xpath() {
        return "//div[@id='btnProcessFlow_form_2000EE74-35E5-4218-90FC-47E8A0660483']//span";
    }

    public static String LearningAndApproval_Process_ButtonXpath() {
        return "//div[@id='btnProcessFlow_form_B6196CB4-4610-463D-9D54-7B18E614025F']//span";
    }

    public static String IncidentSignOff_Process_buttonxpath() {
        return "//div[@id='btnProcessFlow_form_EE164B84-29D5-424F-A584-A66FC3FE9BCA']//span";
    }

    public static String concessions_Process_Buttonxpath() {
        return "//div[@id='btnProcessFlow_form_B3205F35-ED12-44E1-BD2F-995318C72096']//span";
    }

    public static String actionFeedbackTab() {
        return "//div[text()='Action Feedback'][@class='title']";
    }

    public static String concessionActionFeedbackAdd() {
        return "//div[text()='Action Feedback'][@class='link']/..//div[@id='btnAddNew']";
    }

    public static String actionFeedbackActiveForm() {
        return "//div[@id='form_B012440A-7BDD-4C0B-A2EB-80A0DA4E1D83'][@class='form transition visible active']";
    }

    public static String actionFeedbackFlowProcessButton() {
        return "//div[@id='btnProcessFlow_form_B012440A-7BDD-4C0B-A2EB-80A0DA4E1D83']";
    }
    
    public static String pinProcessFlowConcessionActions(){
        return "//div[@id='divFlow_form_92519649-BEF5-4E75-9F01-FEF7B6B6243E']//span[@original-title='Pin process flow']";
        
    }

    public static String actionFeedbackTextArea() {
        return "//div[@id='control_B9A4BEF3-B4D1-491D-881A-6021DDE86169']//textarea";
    }

    public static String actionFeedbackActionCompleteDropdown() {
        return "//div[@id='control_40EFC614-1BC1-4F14-934E-928BF0980FF2']";
    }

    public static String actionFeedbackSave() {
        return "//div[@id='btnSave_form_B012440A-7BDD-4C0B-A2EB-80A0DA4E1D83']";
    }

    public static String concessionActionFeedbackCloseButton() {
        return "//div[@id='form_B012440A-7BDD-4C0B-A2EB-80A0DA4E1D83']//i[@class='home icon search']/..//i[@class='close icon cross']";
    }

    public static String getConcessionActionFeedbackRecordID() {
        return "//div[@id='form_B012440A-7BDD-4C0B-A2EB-80A0DA4E1D83']//div[@class='recnum']//div[@class='record']";
    }

    public static String activeAddPhaseXpath() {
        return "//div[text()='Add phase']//../..//div[@class='step active']";
    }

    public static String activeEditPhaseXpath() {
        return "//div[text()='Edit phase']/..//..//div[@class='step active']";
    }
    
    public static String activeClosedPhaseXpath() {
        return "//div[text()='Closed']/..//..//div[@class='step active']";
    }


    public static String signOffTab() {
        return "//div[text()='Sign Off'][@class='title']";
    }

    public static String concessionSighOffAdd() {
        return "//div[text()='Action Sign Off'][@class='link']/..//div[@id='btnAddNew']";
    }

    public static String actionSignOffActiveForm() {
        return "//div[@id='form_1C4C3224-4FB3-4A94-8ED1-EA085B325C05'][@class='form active transition visible']";
    }

    public static String actionSignOffFlowProcessButton() {
        return "//div[@id='btnProcessFlow_form_1C4C3224-4FB3-4A94-8ED1-EA085B325C05']";
    }

    public static String actionSignOffDropdown() {
        return "//div[@id='control_7EB577FB-3A6B-4CAF-B5E5-1D9E6D8040F0']";
    }

    public static String actionSignOffCommentTextArea() {
        return "//div[@id='control_55A6635D-1D32-4DB9-953C-3329A76E4F05']//textarea";
    }

    public static String actionOffSave() {
        return "//div[@id='btnSave_form_1C4C3224-4FB3-4A94-8ED1-EA085B325C05']";
    }

    public static String concessionActionsSighOffCloseButton() {
        return "//div[@id='form_1C4C3224-4FB3-4A94-8ED1-EA085B325C05']//i[@class='home icon search']/..//i[@class='close icon cross']";
    }

    public static String SignOffactiveEditPhaseXpath() {
        return "//div[text()='Edit phase']/..//..//div[@class='step active']";
    }

    public static String getConcessionActionSighOffRecordID() {
        return "//div[@id='form_1C4C3224-4FB3-4A94-8ED1-EA085B325C05']//div[@class='recnum']//div[@class='record']";
    }
    public static String getConcessionRecordID(){
        return "//div[@id='form_B3205F35-ED12-44E1-BD2F-995318C72096']//div[@class='recnum']//div[@class='record']";
    }

    public static String activeConcessionActions() {
        return "//div[@id='form_92519649-BEF5-4E75-9F01-FEF7B6B6243E'][@class='form transition visible active']";
    }

    public static String SignOffactiveClosePhaseXpath() {
        return "//div[text()='Closed']/..//..//div[@class='step active']";
    }
    public static String allAffectedPartiesCheckbox(){
        return "//div[@id='control_569C771D-5F69-4957-BF21-E480351D417E']";
    }

    public static String IncidentSignOff_Admin1_Optionxpath() {
        return"//tr[6]//div[@id='control_778E6B65-48C8-4B96-89E1-0807EF28B247']//span[2]";
    }

    public static String IncidentSignOff_Admin1_Yesxpath() {
        return"//ul[@class='select3-results jstree jstree-45 jstree-default jstree-loading']//a[contains(text(),'Yes')]";
    }
    
    public static String IncidentSignOff_Admin1_Noxpath(){
        return "//ul[@class='select3-results jstree jstree-45 jstree-default jstree-loading']//a[contains(text(),'No')]";
    }
    
    public static String IncidentSignOff_Admin1_Detailsxpath(){
        return "//tr[6]//div[@name='txb_n1']//input[@language]";
    }

    public static String IncidentSignOff_Admin2_Optionxpath() {
        return"//tr[7]//div[@id='control_778E6B65-48C8-4B96-89E1-0807EF28B247']//span[2]";
    }

    public static String IncidentSignOff_Admin2_Yesxpath() {
        return"//ul[@class='select3-results jstree jstree-46 jstree-default jstree-loading']//a[contains(text(),'Yes')]";
    }
    
    public static String IncidentSignOff_Admin2_Noxpath(){
        return "//ul[@class='select3-results jstree jstree-46 jstree-default jstree-loading']//a[contains(text(),'No')]";
    }
    
    public static String IncidentSignOff_Admin2_Detailsxpath(){
        return "//tr[7]//div[@name='txb_n1']//input[@language]";
    }

    public static String IncidentSignOff_Admin3_Optionxpath() {
        return"//tr[8]//div[@id='control_778E6B65-48C8-4B96-89E1-0807EF28B247']//span[2]";
    }

    public static String IncidentSignOff_Admin3_Yesxpath() {
        return"//ul[@class='select3-results jstree jstree-47 jstree-default jstree-loading']//a[contains(text(),'Yes')]";
    }
    
    public static String IncidentSignOff_Admin3_Noxpath(){
        return "//ul[@class='select3-results jstree jstree-47 jstree-default jstree-loading']//a[contains(text(),'No')]";
    }
    
    public static String IncidentSignOff_Admin3_Detailsxpath(){
        return "//tr[8]//div[@name='txb_n1']//input[@language]";
    }

    public static String IncidentManagement_Save_Buttonxpath() {
        return"//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@title='Save']";
    }

    public static String IncidentInvestigationStatus() {
        return"//div[@id='control_61A17BC2-21A9-463E-ADB9-536DCCAFCF4B']//li";
    }

    public static String IncidentSignOff_Admin4_Optionxpath() {
        return"//tr[9]//div[@id='control_778E6B65-48C8-4B96-89E1-0807EF28B247']//span[2]";
    }

    public static String IncidentSignOff_Admin4_Yesxpath() {
        return"//ul[@class='select3-results jstree jstree-48 jstree-default jstree-loading']//a[contains(text(),'Yes')]";
    }
    
    public static String IncidentSignOff_Admin4_Noxpath(){
        return "//ul[@class='select3-results jstree jstree-48 jstree-default jstree-loading']//a[contains(text(),'No')]";
    }
    
    public static String IncidentSignOff_Admin4_Detailsxpath(){
        return "//tr[9]//div[@name='txb_n1']//input[@language]";
    }

    public static String IncidentSignOff_Admin5_Optionxpath() {
        return"//tr[10]//div[@id='control_778E6B65-48C8-4B96-89E1-0807EF28B247']//span[2]";
    }

    public static String IncidentSignOff_Admin5_Yesxpath() {
        return"//ul[@class='select3-results jstree jstree-49 jstree-default jstree-loading']//a[contains(text(),'Yes')]";
    }
    
    public static String IncidentSignOff_Admin5_Noxpath(){
        return "//ul[@class='select3-results jstree jstree-49 jstree-default jstree-loading']//a[contains(text(),'No')]";
    }
    
    public static String IncidentSignOff_Admin5_Detailsxpath(){
        return "//tr[10]//div[@name='txb_n1']//input[@language]";
    }

    public static String IncidentSignOff_Admin6_Optionxpath() {
        return"//tr[11]//div[@id='control_778E6B65-48C8-4B96-89E1-0807EF28B247']//span[2]";
    }

    public static String IncidentSignOff_Admin6_Yesxpath() {
        return"//ul[@class='select3-results jstree jstree-50 jstree-default jstree-loading']//a[contains(text(),'Yes')]";
    }
    
    public static String IncidentSignOff_Admin6_Noxpath(){
        return "//ul[@class='select3-results jstree jstree-50 jstree-default jstree-loading']//a[contains(text(),'No')]";
    }
    
    public static String IncidentSignOff_Admin6_Detailsxpath(){
        return "//tr[11]//div[@name='txb_n1']//input[@language]";
    }

    public static String MaskBlock() {
        return"//div[@class='ui inverted dimmer active']";
    }

    public static String MaskNone() {
        return"//div[@class='ui inverted dimmer']";
    }

    public static String incidentManagement_Reports_Iconxpath() {
        return"//div[@id='btnReports_form_B6196CB4-4610-463D-9D54-7B18E614025F']//span";
    }

    public static String incidentManagement_IncidentSummary_Buttonxpath() {
        return"//div[@id='divReport_form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[contains(text(),'Reports')]//..//..//..//span[contains(text(),'Incident Summary')]";
    }
    
    public static String incidentManagement_IncidentFullSummary_Buttonxpath() {
        return"//div[@id='divReport_form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[contains(text(),'Reports')]//..//..//..//span[contains(text(),'Incident Full Report')]";
    }
    
     public static String sighOffConcessionCheckbox(){
        return "//div[@id='control_9A96ECD0-D256-4A86-8F4B-A903EAB67FA2']";
    }
     
     public static String activeConcessionPage() {
        return "//div[@id='form_B3205F35-ED12-44E1-BD2F-995318C72096'][@class='form active transition visible']";
    }
     
     public static String statusCompleted(){
         return "//li[text()='Completed']";
     }
     
     public static String savebutton(){
         return "//div[@id='btnSave_form_B3205F35-ED12-44E1-BD2F-995318C72096']";
     }

    public static String EquipAndToolsSupportingDocumentsTab()
    {
        return "//div[@class='tabpanel_tab_content']//div[text()='Supporting Documents']";
    }

    public static String assets_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_2000EE74-35E5-4218-90FC-47E8A0660483']";
    }

    public static String witnessStatement_ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_FCE0307F-50FE-403C-BDFB-ED293325F9DA']";
    }

    public static String witness_Save_and_Close() {
        return "//div[@id='control_85085CF3-A567-4BB4-AA88-785C6564C3C8']";
    }

    public static String RegulatoryAuthority_TimeXpath() {
        return "//div[@id='control_313DF594-0537-4584-BDC6-ED41CCD32957']//input";
    }

    public static String regulatoryAuthority_Record() {
        return "(//div[@id='control_5F3CDBFF-2101-4E6C-AA2D-60A1A830673F']//div//table)[3] ";
    }

    public static String InLine() {
        return "(//div[@id='control_5F3CDBFF-2101-4E6C-AA2D-60A1A830673F']//div//div//div)[58]";
    }
    public static String whyAnalysisPanel() {
        return "//span[text()='Why Analysis']";
    }
    
    public static String whyAnalysisPanelAdd() {
        return "//div[text()='Why Analysis']/../..//div[@id='btnAddNew']";
    }

    public static String injuredPersonsSubTabXPath() {
        return "//span[text()='Injured Persons']";
    }
    
    public static String whyAnalysisOrderNumber(){
        return "//div[@id='control_7831FA38-FB43-46A4-893D-42D365D66A05']";
    }
    
    public static String whyAnalysisWhy(){
        return "//div[@id='control_520640B0-0730-4F01-9D34-D185B8CCDA27']";
    }
    public static String whyAnalysisAnswer(){
        return "//div[@id='control_5A999D23-9FE7-4E9D-90E5-8A467467AB6A']";
    }


    public static String saveAndColse() {
        return "//div[@id='control_C2C40A28-FCC3-4B4D-BCB2-77F4467A3920']//div[text()='Save and close']";
    }

    public static String saveXpath() {
        return "//div[@id='btnSave_form_D6F2F0CD-C202-4238-92BF-3A91A2B306D7']//div[text()='Save']";
    }

    public static String injuryEditphaseXpath() {
        return "//div[@id='divProcess_D6F2F0CD-C202-4238-92BF-3A91A2B306D7']//div[@class='step active']//div[text()='Edit phase']";
    }

    public static String injuredPersonProcessButtonXpath() {
        return "//div[@id='btnProcessFlow_form_D6F2F0CD-C202-4238-92BF-3A91A2B306D7']";
    }

    public static String injuredPersonseditPhaseActive() {
        return "//div[@id='divProcess_510481BC-5774-4957-920F-C8323A843BF9'][not(contains(@style, 'display: none;'))]//div[@class='step active']";
    }

    public static String supportingDocuments() {
        return "//div[@id='form_510481BC-5774-4957-920F-C8323A843BF9']//li[@id='tab_FAF9F96B-39CB-42EB-B50C-78DA19D365C0'][@class='active']";
    }

    public static String drugAlcohol() {
        return "//div[@id='form_510481BC-5774-4957-920F-C8323A843BF9']//li[@id='tab_F45C2DD5-844A-40CA-BE41-7086A3E5ABCE'][@class='active']";
    }

    public static String drugAlcoholTab() {
        return "//div[@id='form_510481BC-5774-4957-920F-C8323A843BF9']//li[@id='tab_F45C2DD5-844A-40CA-BE41-7086A3E5ABCE']";
    }

    public static String injuredPersonsFlow() {
        return "//div[@id='btnProcessFlow_form_510481BC-5774-4957-920F-C8323A843BF9']";
    }

    public static String ClickdrugAlcohol() {
        return "//div[text()='Drug and Alcohol Tests']";
    }

    public static String addInjuredPersonsButtonXPath() {
        return "//div[text()='Injured Persons']/..//div[@title='Add']";
    }

    public static String employeeTypeDropdownXPath() {
        return "//div[@id='control_C33A033C-530C-48D8-B6B3-41A96C4C614B']//li";
    }

    public static String employeeTypeSearchFieldXPath() {
        return "//*[@id='select3_0fe39394']/div[1]/input";
    }

    public static String employeeTypeDropdownElementXPath(String employee) {
        return "//div[contains(@class, 'select3-drop select3-drop-ddl select3_')]//a[text()='" + employee + "']";
    }

    public static String fullNameDropdownXPath() {
        return "//div[@id='control_02385642-D56E-4A37-8DB6-4DA2FFC79EED']//li";
    }

    public static String fullNameElementXPath(String name) {
        return "//div[contains(@class, 'select3-drop select3-drop-ddl select3_')]//a[text()='" + name + "']";
    }

    public static String injuryOrIlnessDropdownXPath() {
        return "//div[@id='control_6FED9E3D-6F13-4BD2-B693-36DB2E988E54']//li";
    }

    public static String injuryOrIlnessElementXPath(String element) {
        return "//div[contains(@class, 'select3-drop select3-drop-ddl select3_')]//a[text()='" + element + "']";
    }

    public static String drugAlcoholTestRequiredDropdownXPath() {
        return "//div[@id='control_D4C09E82-A49E-405D-B157-C05EAB13D89C']//li";
    }

    public static String dropdownElementXPath(String element) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'"+element+"')]";
    }

    public static String treeElementXPath(String element) {
        return "//div[contains(@class, 'select3-drop select3-drop-ddl select3_')][contains(@class, 'transition visible')]//a[text()='" + element + "']/..//i";
    }

    public static String jobProfileTextFieldXPath() {
        return "//div[@id='control_75EFF518-42FE-4C27-AFD0-7638706EDD66']//div//div//input";
    }

    public static String positionStartDatePickerXPath() {
        return "//div[@id='control_0C3F7E32-244A-4B7C-AB80-408EDDA4C2AC']//input";
    }

    public static String usualJobTasksXPath() {
        return "//div[@id='control_2DEC4C59-4121-4E55-A8A9-F26080CFDD28']//textarea";
    }

    public static String injuryOrIllnessClassificationXPath() {
        return "//div[@id='control_E059B2A9-5762-47DF-8C50-04ECC0F7CE14']//li";
    }

    public static String descriptionXPath() {
        return "//div[@id='control_1FD85823-9331-4C4D-8254-DEEE2722AD5A']//textarea";
    }

    public static String activityAtTheTimeDropdownXPath() {
        return "//div[@id='control_F9E96FD7-8789-4703-A315-FA4343063B1B']//li";
    }

    public static String bodyPartsDropdownXPath() {
        return "//div[@id='control_BB36171E-B07E-4A75-80C6-8932D4A6DD3A']//li";
    }

    public static String specificTreeItemXPath(String element) {//div[contains(@class, 'select3-drop select3-drop-ddl select3_')][contains(@class, 'transition visible')]//a[text()='Injury']/..//i[@class='jstree-icon jstree-ocl']
        return "//div[contains(@class, 'select3-drop select3-drop-ddl select3_')][contains(@class, 'transition visible')]//a[text()='" + element + "']/..//i[@class='jstree-icon jstree-ocl']";
    }

    public static String specificTreeItemCheckXPath(String element) {//div[contains(@class, 'select3-drop select3-drop-ddl select3_')][contains(@class, 'transition visible')]//a[text()='Injury']/..//i[@class='jstree-icon jstree-ocl']
        return "//div[contains(@class, 'select3-drop select3-drop-ddl select3_')][contains(@class, 'transition visible')]//a[text()='" + element + "']/..//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String bodyPartsSelectInjuredLocationsXPath(String element) {
        return "//div[contains(@class, 'select3-drop select3-drop-ddl select3_')][contains(@class, 'transition visible')]//a[text()='" + element + "']/..//i[@class='jstree-icon jstree-checkbox']";
    }
    
    public static String bodyPartsSelectInjuredLocationsXPath2(String element) {
        return "(//div[contains(@class, 'select3-drop select3-drop-ddl select3_')][contains(@class, 'transition visible')]//a[text()='" + element + "']/..//i[@class='jstree-icon jstree-checkbox'])[1]";
    }

    public static String natureOfInjuryDropdownXPath() {
        return "//div[@id='control_B1770CDD-D005-43DC-8833-9CC55230D7AC']//li";
    }

    public static String mechanismDropdownXPath() {
        return "//div[@id='control_F68E8A27-7027-44DA-9804-6AD5524C3773']//li";
    }

    public static String natureOfInjuryLabelXPath() {
        return "//div[@id='control_2514FF32-01A9-423C-9F4F-FAAC50BC48ED']//div[text()='Nature of injury']";
    }

    public static String treatmentProvidedDescriptionXPath() {
        return "//div[@id='control_08613967-E400-4BC2-A972-EAC7FACD7F6F']//textarea";
    }

    public static String FollowUpRequiredCheckBoxXPath() {
        return "//div[@id='control_9D1737E8-037B-4722-9C13-DA1EB50A3022']//div[@class='c-chk']";
    }

    public static String followUpDetailsXPath() {
        return "//div[@id='control_0D9F346A-C77C-4E88-8F34-02E3225F3873']//textarea";
    }

    public static String AdditionalTreatmentRequiredCheckBoxXPath() {
        return "//div[@id='control_4E053A81-D6C1-46BE-B9A7-49862A87C970']//div[@class='c-chk']";
    }

    public static String TreatmentAwayFromWorkpaceCheckBoxXPath() {
        return "//div[@id='control_AC472479-B079-4B30-BC42-4522CB2D946B']//div[@class='c-chk']";
    }

    public static String ifTreatmentAwayFromWorkpaceCheckBoxXPath() {
        return "//div[@id='control_AC472479-B079-4B30-BC42-4522CB2D946B']//div[contains(@class, 'checked')]";
    }

    public static String TreatedInEmergencyRoomCheckBoxXPath() {
        return "//div[@id='control_A91CEA2B-C7C8-4A3B-8227-9653EF88826B']//div[@class='c-chk']";
    }

    public static String TreatedInpatientOverNightCheckBoxXPath() {
        return "//div[@id='control_27B303CB-E72B-4490-A100-F606240D3B4F']//div[@class='c-chk']";
    }

    public static String TreatmentFacililtyDetailsXPath() {
        return "//div[@id='control_8AFA35F7-2086-4E08-8FBD-6E0018C02286']//textarea";
    }

    public static String IsThisInjuryRecordableCheckBoxXPath() {
        return "//div[@id='control_70BC1E8B-CD32-44FE-B032-1C3464180DFD']//div[@class='c-chk']";
    }

    public static String IsItReportableCheckBoxXPath() {
        return "//div[@id='control_928AD9F0-9873-4242-8131-DEE2287A6AD6']//div[@class='c-chk']";
    }

    public static String ReportableToDropdownXPath() {
        return "//div[@id='control_A6CF2677-30BA-473B-8A86-F1756ED41EC8']//li";
    }

    public static String InjuryClaimCheckBoxXPath() {
        return "//div[@id='control_BE9780F4-5745-4FCF-BE94-6E798FA0C040']//div[@class='c-chk']";
    }

    public static String SaveAndContinueButtonXPath() {
        return "//div[@id='control_B08D329A-BA16-4EC9-AF30-22EEAD2E76AC']//div[text()='Save and continue ']";
    }

    public static String RecordSavedMessageXPath() {
        return "//div[@id='divMessage'][@class='ui floating icon message transition visible']//div[text()='Record saved']";
    }

    public static String ReturnToWorkManagementXPath() {
        return "//div[text()='Return to Work Management']";
    }

    public static String ReturnToWorkManagementAddButtonXPath() {
        return "//div[@id='control_0BBDE35F-D829-4440-B956-88FBBD23FE93']//div[text()='Add']";
    }

    public static String StatusDropdownXPath() {
        return "//div[@id='control_815C7F7D-D292-475B-B8CC-7BB62DB13B63']//li";
    }

    public static String OriginOfCaseDropdownXPath() {
        return "//div[@id='control_2C6A193C-1372-4651-AFBD-BF9328D375ED']//li";
    }

    public static String OriginOfCaseDeselectButtonOnDropdownXPath() {
        return "//div[@id='control_2C6A193C-1372-4651-AFBD-BF9328D375ED']//b[@original-title='De-select all']";
    }

    public static String OriginOfCaseValidateItemDropdownXPath(String text) {
        return "//div[@id='control_2C6A193C-1372-4651-AFBD-BF9328D375ED']//li[contains(text(),'" + text + "')]";
    }

    public static String ExternalCaseDropdownXPath() {
        return "//div[@id='control_4716CA6E-BF4F-4759-8C39-7017CB3963A3']//li";
    }

    public static String LinkToIncidentRecordDropdownXPath() {
        return "//div[@id='control_CB013909-8250-4437-9CD8-EB72696989D9']//li";
    }

    public static String DoctorsNoteDetailsHeadingXPath() {
        return "//div[@id='control_2786DFF9-407F-4592-85F3-717A78AE5C38']//span[text()]";
    }

    public static String MedicalPractionerDropdownXPath() {
        return "//div[@id='control_FEF66BA4-006C-4306-95FE-9C30F696EE00']//li";
    }

    public static String DoctorsNotesTextAreaXPath() {
        return "//div[@id='control_5A7D4F52-EB44-47FE-873B-91F487A402A8']//textarea";
    }

    public static String DateIssuedXPath() {
        return "//div[@id='control_0A4FBD9F-D7BE-40A4-B687-8F5AF269DC31']//input";
    }

    public static String FitnessForWorkDropdownXPath() {
        return "//div[@id='control_0F18182F-3C2B-4536-9ED8-3B762D2804BC']//li";
    }

    public static String DateFromXPath() {
        return "//div[@id='control_170034EC-65D8-43B5-95EC-757C9AA846E6']//input";
    }

    public static String DateToXPath() {
        return "//div[@id='control_6C837782-8216-4415-A790-550F66B561C1']//input";
    }

    public static String DateToHiddenXPath() {
        return "//div[@id='control_6C837782-8216-4415-A790-550F66B561C1'][contains(@style,'display: none;')]";
    }

    public static String UploadDocumentXPath() {
        return "//div[@id='control_DC169931-9139-4D33-AAA1-2C7E576EC010']//b[@original-title='Upload a document']";
    }

    public static String SuitableDuitiesHeadingXPath() {
        return "//div[@id='control_6B304FC8-1184-4DAF-9879-2761008826B4']//span[text()='Suitable Duties']";
    }

    public static String ReturnToWorkArrangementHeadingXPath() {
        return "//div[@id='control_5681BD07-84CC-4890-926F-A62443E914F2']//span[text()='Return to Work Arrangement']";
    }

    public static String ActionsAndSupportinhInformationHeadingXPath() {
        return "//div[@id='control_CCC6CB10-1851-4714-BD98-BA090D24C2A5']//span[text()='Actions & Supporting Information']";
    }

    public static String RehabilitationsProviderHeadingXPath() {
        return "//div[@id='control_A931C1C4-16AC-4922-BF1C-EFFBDC526190']//span[text()='Rehabilitation Provider']";
    }

    public static String AgreementsHeadingXPath() {
        return "//div[@id='control_2B017F25-7FB4-49F5-BB52-5C6071FEEA29']//span[text()='Agreements']";
    }

    public static String IsASuitableOfferOfEmploymentAttachedDropdownXPath() {
        return "//div[@id='control_B0570A51-0038-4201-8359-13C51128DA8F']//li";
    }

    public static String SecondUploadButton() {
        return "UploadSecond.PNG";
    }

    public static String DutieOrTasksToBeUndertakenTextAreaXPath() {
        return "//div[@id='control_4145C712-AFCB-42A1-A220-5955DC4BDA66']//textarea";
    }

    public static String WorkplaceSupportsTextAreaXPath() {
        return "//div[@id='control_3A6540C8-A597-4EFC-B291-88ABB428A9D1']//textarea";
    }

    public static String SpecificDutiesOrTasksTextAreaXPath() {
        return "//div[@id='control_523EB7C2-C6EB-433D-BCC1-4A1F1B09E95C']//textarea";
    }

    public static String ReturnToWorkArrangementTextAreaXPath() {
        return "//div[@id='control_6C94DBCC-DE8B-4B08-A170-3F311D9859FC']//textarea";
    }

    public static String ProviderNameTextFieldXPath() {
        return "//div[@id='control_E29D39AA-8DD7-4D2A-B6EE-8C89A6E6B041']//div//div//input";
    }

    public static String ContactNumberTextFieldXPath() {
        return "//div[@id='control_FE673810-F542-42C0-B971-DBB707A191E0']//div//div//input";
    }

    public static String EmailAddressTextFieldXPath() {
        return "//div[@id='control_4626A2B4-01D3-4470-8984-CEF0FB361D2A']//div//div//input";
    }

    public static String WorkersAgreementNameXPath() {
        return "//div[@id='control_AD63CBB3-6C69-4C31-8599-5131FC6C59EF']//div//div//input";
    }

    public static String WorkersAgreementDateOfAgreementXPath() {
        return "//div[@id='control_8BA22B58-2883-486E-821F-423BC126F91C']//input";
    }

    public static String SupervisorsAgreementNameXPath() {
        return "//div[@id='control_23E4FD0A-8181-46EA-AA45-303CF172545E']//div//div//input";
    }

    public static String SupervisorsAgreementDateOfAgreementXPath() {
        return "//div[@id='control_16C5568F-BB57-463D-B0D3-8CE35D55ACCA']//input";
    }

    public static String ReturnToWorkCoordinatorsAgreementNameXPath() {
        return "//div[@id='control_06F1838F-3A15-4B62-B5C6-AE33E8235B83']//div//div//input";
    }

    public static String ReturnToWorkCoordinatorsDateOfAgreementXPath() {
        return "//div[@id='control_EF7D2DC0-A90C-4E9F-958B-302A697B7B13']//input";
    }

    public static String TreatingMedicalPractionersAgreemenrNameXPath() {
        return "//div[@id='control_73C00A79-60B6-4567-81CE-9061838B9E5C']//div//div//input";
    }

    public static String TreatingMedicalPractionersAgreemenrDateOfAgreementXPath() {
        return "//div[@id='control_50504557-0CA4-4D81-89F6-FF089E004C36']//input";
    }

    public static String SaveUploadedReportButtonXPath() {
        return "//div[@id='control_54C06F49-468E-444A-940F-F4A0CC6C636D']//div[text()='Save Uploaded Report']";
    }

    public static String CloseFormButtonXPath() {
        return "//*[@id='form_F943DA67-2DF2-49E9-99CC-9B6FCA496AED']/div[1]/i[2]";
    }

    public static String CloseForm2ButtonXPath() {
        return "//*[@id='form_510481BC-5774-4957-920F-C8323A843BF9']/div[1]/i[2]";
    }

    public static String SaveFormButtonXPath() {
        return "//div[@id='btnSave_form_F943DA67-2DF2-49E9-99CC-9B6FCA496AED']";
    }

    public static String SelectTestToBeConductedXPath(String value) {
        return "//div[@id='control_0E38919C-6050-4473-9DFC-C1BE2F438C06']//a[text()='" + value + "']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String TestToBeConductedXPath() {
        return "//div[@class='nreqbox control pnl transition hidden']//div[@id='control_0E38919C-6050-4473-9DFC-C1BE2F438C06']";
    }

    public static String illnessDetailsTab() {
        return "//div[text()='Injury/Illness Detail']";
    }

    public static String ManagementWhoReferredThePersonXPath() {
        return "//div[@class='nreqbox control pnl transition hidden']//div[text()='Management who referred the person?']";
    }

    public static String ManagementWhoReferredThePersonLabelXPath() {
        return "//div[text()='Management who referred the person?']";
    }

    public static String ManagementWhoReferredThePersonDropdownXPath() {
        return "//div[@id='control_6E6CB335-293D-4FC4-8BED-AD70E9573E09']//li";
    }

    public static String ChronicIllnessPanelXPath() {
        return "//div[@label='Chronic illness management']";
    }

    public static String LinkToMedicalTestRecordFieldXPath() {
        return "//div[@id='control_59A61A40-9D8B-4920-AA48-7897FE7C2F17']/..//div[text()='Link to medical test record']";
    }

    public static String PersonBeingTestedXPath() {
        return "//div[@id='control_5D58831B-7752-493F-8EE5-A245C603B428']//li";
    }

    public static String LinkedIncidentXPath() {
        return "//div[@id='control_4342AE2C-BC1E-442B-AB70-4E0BF93F2515']//div[@class='c-chk']";
    }

    public static String LinkedIncidentDropdownXPath() {
        return "//div[@id='control_BE9FE046-7143-4819-8988-0E53681D3AF9']//li";
    }

    public static String GetIncidentRecordNumberXPath() {
        return "//div[@class='navbar']//div[contains(text(), '- Record #')]";
    }

    public static String LinkedIncidentTextFieldXPath() {
        return "//div[@id='select3_0e117628']//input";
    }

    public static String OpenedRecordNumberXPath(String record) {
        return "//div[contains(text(),'Record #" + record + "')]";
    }

    public static String SaveAndContinueButton_DropDownXPath() {
        return "//div[@id='btnSave_form_CBE44FB8-CECF-4F77-8BC2-2A82882035E4']//div[@class='more options icon chevron down']";
    }

    public static String SaveAndCloseButtonXPath() {
        return "//div[@id='btnSaveClose_form_CBE44FB8-CECF-4F77-8BC2-2A82882035E4']//div[contains(text(),'Save and close')]";
    }
    
    public static String illnessonduty_dropown(){
        return "//div[@id='control_6FED9E3D-6F13-4BD2-B693-36DB2E988E54']";
    }
    
    public static String illnessonduty_select(String text){
        return "//a[text()='"+text+"']";
    }

    public static String injuredPersons_non_editableGrid()
    {
        return "//span[text()='Injured Persons']";
    }

    public static String injuredPersons_AddButton()
    {
        return "//div[@id='control_D0351C3F-0C62-47C4-83CE-5DDB30BBD5DF']//div[@id='btnAddNew']";
    }

    public static String employeeType()
    {
        return "//div[@id='control_C33A033C-530C-48D8-B6B3-41A96C4C614B']";
    }

    public static String employeeType_Select(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String fullname()
    {
        return "//div[@id='control_02385642-D56E-4A37-8DB6-4DA2FFC79EED']";
    }

    public static String fullname_Select(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String Injury_Select(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String InjuryTab()
    {
        return "//div[text()='Injury/Illness Detail']";
    }
    public static String Injury_Claim_Tab()
    {
        return "//div[text()='Injury Claim']";
    }

    public static String drugAndAlcohol()
    {
        return "//div[@id='control_D4C09E82-A49E-405D-B157-C05EAB13D89C']";
    }
    

    public static String drugAndAlcohol_Select(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String drugAndAlcohol_Tab()
    {
        return "//div[text()='Drug & Alcohol Tests']";
    }

    public static String classification()
    {
        return "//div[@id='control_E059B2A9-5762-47DF-8C50-04ECC0F7CE14']";
    }

    public static String classification_Select(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String Activity()
    {
        return "//div[@id='control_F9E96FD7-8789-4703-A315-FA4343063B1B']";
    }

    public static String Activity_Select(String data)
    {
         return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String bodyPart()
    {
        return "//div[@id='control_BB36171E-B07E-4A75-80C6-8932D4A6DD3A']";
    }

    public static String bodyPart_Select(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String description()
    {
        return "//div[@id='control_1FD85823-9331-4C4D-8254-DEEE2722AD5A']//textarea";
    }

    public static String NatureOfInjury()
    {
        return "//div[@id='control_B1770CDD-D005-43DC-8833-9CC55230D7AC']";
    }

    public static String NatureOfInjury_Select(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String Mechanism()
    {
        return "//div[@id='control_F68E8A27-7027-44DA-9804-6AD5524C3773']";
    }

    public static String Mechanism_Select(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + " ']";
    }

    public static String FollowUpRequired()
    {
        return "//div[@id='control_9D1737E8-037B-4722-9C13-DA1EB50A3022']";
    }

    public static String AdditionalTreatmentRequired()
    {
        return "(//div[@id='control_4E053A81-D6C1-46BE-B9A7-49862A87C970']//div)[1]";
    }

    public static String Treatment()
    {
        return "//div[@id='control_AC472479-B079-4B30-BC42-4522CB2D946B']";
    }

    public static String recordable()
    {
        return "//div[@id='control_70BC1E8B-CD32-44FE-B032-1C3464180DFD']";
    }

    public static String InjuryClaim()
    {
        return "//div[@id='control_BE9780F4-5745-4FCF-BE94-6E798FA0C040']";
    }

    public static String save_Button()
    {
        return "//div[@id='btnSave_form_510481BC-5774-4957-920F-C8323A843BF9']";
    }

    public static String dropdown_Close()
    {
        return "//div[@id='control_BB36171E-B07E-4A75-80C6-8932D4A6DD3A']//b[@class='select3-down drop_click']";
    }

    public static String reportable()
    {
        return "//div[@id='control_928AD9F0-9873-4242-8131-DEE2287A6AD6']";
    }

    public static String saveAndClose_Button()
    {
        return "//div[@id='control_C17F80E0-588B-4A71-814B-A2FEEE9D4B3B']";
    }

    public static String clickInjuredPersonsRecord()
    {
        return "//div[@id='control_D0351C3F-0C62-47C4-83CE-5DDB30BBD5DF']//div[text()='Drag a column header and drop it here to group by that column']/../div[3]//tr";
    }

    public static String returnToWork_processFlow()
    {
        return "//div[@id='btnProcessFlow_form_F943DA67-2DF2-49E9-99CC-9B6FCA496AED']";
    }

    public static String testToBeConductedAllXPath()
    {
        return "//div[@id='control_0E38919C-6050-4473-9DFC-C1BE2F438C06']//b[@class='select3-all']";
    }

    public static String whoRefferedThePersonDropdownXPath()
    {
        return "//div[@id='control_6E6CB335-293D-4FC4-8BED-AD70E9573E09']";
    }

    public static String drugAndAlcoholTest_Tab()
    {
        return "//div[text()='Drug & Alcohol Tests']";
    }

    public static String conductDrugAndAlcoholTest_Btn()
    {
        return "//div[@id='control_882404B6-DD63-4169-AAAB-1A71C3CCE79E']";
    }

    public static String drugAndAlcoholTest_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_CBE44FB8-CECF-4F77-8BC2-2A82882035E4']";
    }

    public static String seachBtn1()
    {
        return "//div[@id='control_DDED67AF-D58A-419D-9696-B26C394C0133']//div[@id='btnFilter']";
    }

    public static String seachBtn2()
    {
        return "(//div[@id='control_DDED67AF-D58A-419D-9696-B26C394C0133']//div[text()='Search'])[2]";
    }

    public static String drugAndAlcoholTest_record(String date)
    {
        return "(//div[@id='control_DDED67AF-D58A-419D-9696-B26C394C0133']//div//table//div[contains(text(),'" + date + "')])";
    }

    public static String safetyHealth_Tab() {
        return "//div[text()='Safety/Health']";
    }

    public static String Injury_And_Illness_Tab() {
        return "//div[text()='Injury/Illness Detail']";
    }
    public static String Drug_Alcohol_Tab(){
        return "//div[text()='Drug & Alcohol Tests']";
    }
    public static String SupportingDocuments(){
        return "(//div[text()='Supporting Documents'])[3]";
    }

    public static String injuredPerson_Close() {
        return "(//div[@id='form_510481BC-5774-4957-920F-C8323A843BF9']//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String injuredPersonPanel_Record() {
        return "(//div[@id='control_FB63DB0F-EDE7-43CC-B116-14E583CD15C8']//div//table)[3]";
    }

    public static String injuryDetails_Panel() {
        return "//div[text()='Injury Details']";
    }

    public static String injuryDetails_AddButton() {
        return "//div[@id='control_4B6BDA86-0243-4F5A-8D07-A7E91B1F0231']//div[contains(text(),'Add')]";
    }

    public static String InjuryDetails_BodyPart() {
        return "//div[@id='control_1B635A55-E358-4EEF-B08F-694A5F3C0411']";
    }

    public static String InjuryDetails_NatureOfInjury() {
        return "//div[@id='control_816081F2-B79C-4E70-96A2-26D45E86C1DA']";
    }

    public static String InjuryDetails_Mechanism() {
        return "//div[@id='control_F0AD81AC-DC7F-44DE-8E65-7D304C715228']";
    }

    public static String ReturnedWorkManagement_Close() {
        return "(//div[@id='form_F943DA67-2DF2-49E9-99CC-9B6FCA496AED']//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String returnedWorkManagement_GridView() {
        return "(//div[@id='control_0BBDE35F-D829-4440-B956-88FBBD23FE93']//div//table)[3]";
    }
    public static String injuryClaimTab() {
        return "//div[text()='Injury Claim']";
    }
      public static String prevoiusRelatedInjuriesTable() {
        return "//div[@id='control_5FDE9B1C-06E4-48E4-9DF6-31DC6425C7FB']//i[@class='toggle']";
    }
    public static String thirdPartyDiplayedXpath(){
        return "//div[text()='Third party']";
    }

    public static String injuredPersonsSaveXpath() {
        return "//div[@id='btnSave_form_510481BC-5774-4957-920F-C8323A843BF9']";
    }

    public static String addDetailsVXpath() {
        return "//div[text()='2.Verification and Additional Detail']";
    }

    public static String injuryClaimAdd() {
        return "//div[@id='control_2EBAD31C-8852-480F-A8FA-D7E591BC4C01']//div[@i18n='add_new']";
    }

    public static String specificLoca() {
        return "//div[@id='control_2CC94640-D7B5-465D-8299-AD0DE2683B9C']//textarea";
    }

    public static String workersTabXpath() {
        return "//span[contains(text(),'Personal Details ')]";
    }

    public static String workersTabCheckOpenXpath() {
        return "//div[@class='c-pnl open']//span[contains(text(),'Personal Details')]";
    }

    public static String workersTabCloseXpath() {
        return "//div[@class='c-pnl']//span[contains(text(),'Personal Details')]";
    }

    public static String employeeDropdownXpath() {
        return "//div[@id='control_427765C1-D6C0-464A-95D9-B1772FD1F73E']";
    }

    public static String anyOptionCheckXpath(String option) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']//i[@class='jstree-icon jstree-checkbox']";
    }
    

    public static String compensationClaimDropdownXpath() {
        return "//div[@id='control_D40A7BA0-995F-4E3B-87FE-F687E7505DAE']";
    }

    public static String specialCommunicationXpath() {
        return "//div[@id='control_00BB6CD7-2583-47F6-AAD6-21F4F69F1073']//textarea";
    }

    public static String languageDropdownXpath() {
        return "//div[@id='control_C0C63DF8-B710-4DD6-8952-8DB8CCF92CC3']";
    }

    public static String partnerDropDownXpath() {
        return "//div[@id='control_3592E91C-9306-4993-B508-7C334963F070']";
    }

    public static String fullTimeStudentsXpath() {
        return "//div[@id='control_B7013667-7290-4363-9D47-F971C6E50A64']";
    }
    public static String pleaseProvideTheDateOfBirthXpath() {
        return "//div[@id='control_A81EF86A-0DED-4056-A602-DDCE98E0D1CB']//input";
    }

    public static String averageGrossXpath() {
        return "//div[@id='control_22129204-6ABE-4D07-8380-57A892FC61D4']//input[@language]";
    }

    public static String IncidentTabXpath() {
        return "//div[@class='c-pnl open']//span[contains(text(),'Incident and ')]";
    }

    public static String IncidentTabOpneXpath() {
        return "//span[contains(text(),'Incident and ')]";
    }

    public static String whatHappenedXpath() {
        return "//div[@id='control_A4D11B61-B241-4960-A3B1-1BCD83803593']//textarea";
    }

    public static String youWereInjuredXpath() {
        return "//div[@id='control_A259DE1C-1AB0-414A-8C0F-7BA41BF103CA']//textarea";
    }

    public static String responsibleEmployerXpath() {
        return "//div[@id='control_93394F16-15BC-40C0-A47F-BEFB476EB19E']//input[@language]";
    }

    public static String accidentWasReportedXpath() {
        return "//div[@id='control_260C1626-D20A-47F9-BE9E-41F90C034CE3']//input[@language]";
    }

    public static String involvedVehiclesXpath() {
        return "//div[@id='control_DECA5907-FDC5-4F1C-BEAC-9448515BB06C']//input[@language]";
    }

    public static String StateXpath() {
        return "//div[@id='control_A3D725C3-EBD4-4F97-B210-9ED78B9DBA6E']//input[@language]";
    }

    public static String injuryCausesDropdownXpath() {
        return "//div[@id='control_26B71E34-9805-47AA-9148-AA093BEE3265']";
    }

    public static String additionalDescriptionXpath() {
        return "//div[@id='control_BBAF50B9-B3F0-43C0-9FCA-E8D138C97E88']//textarea";
    }

    public static String injuryDateXpath() {
        return "//div[@id='control_15783961-124B-45B9-A141-4AEBFC09AD65']//input";
    }

    public static String injuryTimeXpath() {
        return "//div[@id='control_85E24D88-0ADA-4474-B34D-8A960BF89D9D']//input";
    }

    public static String injuryConditionsXpath() {
        return "//div[@id='control_4C2CA712-17E8-420E-8E91-32DE889E8D5B']//input[@language]";
    }

    public static String stoppedWorkingDateXpath() {
        return "//div[@id='control_30C70E42-071D-47DF-A1C7-EF37D67919E5']//input";
    }

    public static String stoppedWorkingTimeXpath() {
        return "//div[@id='control_575F2C35-CF19-4D3A-9E71-22B6E75431D8']//input";
    }

    public static String reportDateXpath() {
        return "//div[@id='control_6C059C7D-83CB-4BDD-A7D9-684CD55E15AB']//input";
    }

    public static String prevoiusInjuryDropdownXpath() {
        return "//div[@id='control_DD187060-9792-40D3-980E-A151FE29E8CA']";
    }

    public static String workerEmploymentDetailsTab() {
        return "//div[@id='control_019E24C6-592C-48A2-A3EE-5E330FC90C63']//i[@class='toggle']";
    }
    
     public static String workerEmploymentDetailsOpenTab() {
        return "//div[@id='control_3AE185E0-483D-4FD7-A3D2-3C6AA5CF1CCE']//span[contains(text(),'Worker')]/../../../div[@class='c-pnl open']";
    }
     public static String workerEmploymentDetailsClosedTab() {
        return "//div[@id='control_019E24C6-592C-48A2-A3EE-5E330FC90C63']//span[contains(text(),' Employment Details ')]/../../..//div[@class='c-pnl']";
    }


    public static String nameOrgPayingInjuredXpath() {
        return "//div[@id='control_958411C5-A30B-4B5F-A6AF-462BA0E390EC']//input[@language]";
    }

    public static String workPlaceAddressXpath() {
        return "//div[@id='control_A77D3E4F-7257-482C-9B59-511BC08178E9']//textarea";
    }

    public static String suburbXpath() {
        return "//div[@id='control_84491A7F-2C1A-4424-BC34-CBA180927BD1']//input[@language]";
    }

    public static String stateXpath() {
        return "//div[@id='control_E5753694-2F80-4F47-A368-34059C508827']//input[@language]";
    }

    public static String postCodeXpath() {
        return "//div[@id='control_3D492023-2214-4633-82F4-63E23A2EA45D']//input[@language]";
    }

    public static String nameofEmployerXpath() {
        return "//div[@id='control_442D04B7-12E3-47AA-BB21-62CCBD81D39D']//input[@language]";
    }

    public static String NumOfEmployerXpath() {
        return "//div[@id='control_268B8A0F-281B-4A58-88A2-89782CAB0970']//input[@language]";
    }

    public static String usualOccupationXpath() {
        return "//div[@id='control_1E5531A2-2DEB-4A9A-A9D5-1373D395E38C']//input[@language]";
    }

    public static String startedWorkDateXpath() {
        return "//div[@id='control_77208834-CFE0-4F7D-A349-FDCB8511F83D']//input";
    }

    public static String employmentTimInjuredDropdownXpath() {
        return "//div[@id='control_6624AFDA-BFAD-4F31-A7E3-1E8A33AA3B42']";
    }

    public static String workerEarningTab() {
        return "//div[@id='control_79B87E14-C858-48CA-BC90-E2161C65B3C4']//div[@class='c-pnl-heading']";
    }

    public static String hoursWorkedXpath() {
        return "//div[@id='control_424AEDE8-8F93-4225-865E-D17061BB3930']//input[@language]";
    }

    public static String workingTimeFromXpath() {
        return "//div[@id='control_F2331164-D27F-4560-A25A-6F25192E2363']//input";
    }

    public static String workingTimeToXpath() {
        return "//div[@id='control_88B529BB-CDD3-4357-9D70-ADAB2EDAD6E3']//input";
    }

    public static String weeklyShiftXpath() {
        return "//div[@id='control_775AB4AB-AE44-4E05-8C62-4601A58BB57D']//input[@language]";
    }

    public static String preTaxRateXpath() {
        return "//div[@id='control_B558E3E5-987E-46A0-870E-9EA826AEF168']//input[@language]";
    }

    public static String overtimeAlloXpath() {
        return "//div[@id='control_52BD610B-3C60-4686-BC76-49BAF1D756DA']//input[@language]";
    }

    public static String preTaxRateWeeklyXpath() {
        return "//div[@id='control_64554F81-C93F-4CBF-919B-A9886E6265F0']//input[@language]";
    }

    public static String overtimeWorkedXpath() {
        return "//div[@id='control_16813406-C04A-4372-8948-35348DF97B6B']//input[@language]";
    }

    public static String returnWorkDetailsTab() {
        return "//span[text()='Treatment and Return To Work Details ']";
    }
    
      public static String returnWorkDetailsOpenTab() {
        return "//div[@class='c-pnl open']//span[text()='Treatment and Return To Work Details ']";
    }

    public static String doctorTreatmentXpath() {
        return "//div[@id='control_C250CEB7-3E21-490F-8D24-697B28321B10']//input[@language]";
    }

    public static String numOfTreatmentXpath() {
        return "//div[@id='control_A5563576-1F72-474D-89B4-77629268EFC0']//input[@language]";
    }

    public static String nameOfClinicXpath() {
        return "//div[@id='control_E559E8D8-5C42-4AB4-87A6-0CA5BC9FB80C']//textarea";
    }

    public static String contactDetailsXpath() {
        return "//div[@id='control_2354211E-71A2-4354-8E85-16273E1A544A']//input[@language]";
    }

    public static String sameEmployerDropdownXpath() {
        return "//div[@id='control_C92060F3-6CCF-43C4-89A0-B409DC1CB6E7']";
    }

    public static String returnDateXpath() {
        return "//div[@id='control_EC7D9DE7-5F93-4BE7-B13A-B237A8A1A564']//input";
    }
    
     public static String hourWorked() {
        return "//div[@id='control_A3CD547F-2C9D-4DCD-857B-7A2AF0C7C8FA']//input";
    }
    
    public static String newEmployerNameXpath() {
        return "//div[@id='control_69A0C044-EDC4-42AB-9F2F-06AA033E668B']//input[@language]";
    }
    
    public static String newEmployerContactXpath() {
        return "//div[@id='control_275E3BF2-9C0C-4484-A730-78F60698FCD4']//input[@language]";
    }

    public static String howHoursWrkedXpath() {
        return "//div[@id='control_508DD233-9746-4126-BBF4-212645186225']//input";
    }

    public static String dutiesdropdownXpath() {
        return "//div[@id='control_C7C234C4-7E8E-46ED-95E3-55727D8A5670']";
    }
    
     public static String returnNewEmployersdropdownXpath() {
        return "//div[@id='control_94CCD94B-FDAA-4911-8E77-49BDF8FF9DC4']";
    }

    public static String formToYouremployerddropdownXpath() {
        return "//div[@id='control_4311A037-5F9A-425F-B981-8AEA23353F57']";
    }

    public static String returnClaimsDateXpath() {
        return "//div[@id='control_818C6AD7-6E86-427C-9227-E75BFE0B049C']//input";
    }

    public static String DateOfMedCertDateXpath() {
        return "//div[@id='control_65BEE57D-01FD-4066-A8C4-F94AB14BEFA1']//input";
    }

    public static String employerLodgementDetailsTab() {
        return "//span[text()='Employer Lodgement Details ']";
    }

    public static String firstDateCompletedCliamsFormXpath() {
        return "//div[@id='control_D900DDBF-5DCB-47C9-978D-4FA49838A098']//input";
    }

    public static String firstDateMedCertXpath() {
        return "//div[@id='control_43BFA093-C65F-4B8B-B2E3-7E0D406F1382']//input";
    }

    public static String dateClaimAgentXpath() {
        return "//div[@id='control_92058FCF-3B95-43E4-985C-6E78D50ADC2F']//input";
    }

    public static String claimCostXpath() {
        return "//div[@id='control_0C29DECD-5760-41E3-8952-0CE411F1D8E5']//input[@language]";
    }

    public static String lostTimeXpath() {
        return "//div[@id='control_ECAB367F-EC0F-4296-9CA2-AF011C9057BC']//input";
    }

    public static String employerDropdownXpath() {
        return "//div[@id='control_1B0973DF-6798-419F-8D51-EADC2FB6284D']";
    }

    public static String positionXpath() {
        return "//div[@id='control_EA313F9F-252F-4FC5-A0B2-4D44905DD062']//input[@language]";
    }

    public static String schemeNumberXpath() {
        return "//div[@id='control_F5AFF548-675B-4310-A268-891D3F1DF0B5']//input[@language]";
    }

    public static String injuryCliamFormSave() {
        return "//div[@id='btnSave_form_D6F2F0CD-C202-4238-92BF-3A91A2B306D7']";
    }

    public static String closeInjuryClaimForm() {
        return "//div[@id='form_D6F2F0CD-C202-4238-92BF-3A91A2B306D7']//i[@class='home icon search']/..//i[@class='close icon cross']";
    }

    public static String dutiesOption(String option) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']";
    }

    public static String waitTableInjuryClaim() {
        return "//div[@instanceid='D6F2F0CD-C202-4238-92BF-3A91A2B306D7'][@data-role]//table[@data-role]";
    }

    public static String closeInjuredPersonsForm() {
        return "//div[@id='form_510481BC-5774-4957-920F-C8323A843BF9']//i[@class='home icon search']/..//i[@class='close icon cross']";
    }
    
    public static String thirdPartyDropdown(){
        return "//div[@id='control_26B71E34-9805-47AA-9148-AA093BEE3265']";
    }
    
    public static String employessDropdown(){
        return "//div[@id='control_427765C1-D6C0-464A-95D9-B1772FD1F73E']";
    }
    
    public static String languageDropDown(){
        return "//div[@id='control_C0C63DF8-B710-4DD6-8952-8DB8CCF92CC3']";
    }
    
    public static String communicationDisability(){
        return "//div[@id='control_00BB6CD7-2583-47F6-AAD6-21F4F69F1073']//textarea";
    }

    public static String injuryClaimForm_processFlow()
    {
        return "//div[@id='btnProcessFlow_form_D6F2F0CD-C202-4238-92BF-3A91A2B306D7']";
    }

    public static String injuryClaimForm_Save()
    {
        return "//div[@id='btnSave_form_D6F2F0CD-C202-4238-92BF-3A91A2B306D7']";
    }

    public static String languageSelect(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='"+data+"']";
    }

    public static String addDetailsFieldXpath()
    {
        return "//div[@id='control_E4899E89-D6F3-4836-A3B0-63C67E10DF32']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String EquipAndTools_Close_Button() {
       return "(//div[@id='form_CEAD7953-CD9C-4E8A-ADB8-15C8AA6A092F']//i[@class='close icon cross'])[1]";
    }

    public static String Equipment_Tools_Record() {
        return "(//div[@id='control_45349B8B-5312-4623-BF54-BE4D75D4FB8F']//div//table)[3]";
    }

    public static String EquipAndTools_Asset_Close_Button() {
        return "(//div[@id='form_2000EE74-35E5-4218-90FC-47E8A0660483']//i[@class='close icon cross'])[1]";
    }

    public static String EquipmentTools_Asset_Record() {
        return "(//div[@id='control_BC26F4F4-386E-42B1-A767-33E77CA09F6E']//div//table)[3]";
    }

    public static String PreviousRelatedInjuries_Panel() {
        return "//span[contains(text(),'Previous related injures')]";
    }

    public static String previousRelatedInjuries_Record(String data) {
        return "(//div[@id='control_3782619E-08A8-4A99-A0F0-64FEE6FFD72C']//div//table//tr//div[contains(text(),'" + data + "')])";
    }

    public static String viewFilter_IncidentManagement() {
        return "(//div[contains(text(),'View filter')])[2]";
    }
    
    public static String record_No(){
        return "//input[@class='txt border']";
    }
    
    public static String recordNo(){
        return "(//input[@class='txt border'])[1]";
    }

    public static String serachBtn(){
        return "//div[@class='actionBar filter']//div[text()='Search']";
    }

    public static String selectRecord(String data){
        return "//span[text()='"+data+"']";
    }
    
    public static String selectActionRecord(){
        return "//div[@id='control_731174EE-AD50-4449-AF3F-253162F405CB']//div[text()='Drag a column header and drop it here to group by that column']/../div[3]//tr[1]";
    }

    public static String QualityConcession_Status() {
        return "(//div[@id='control_0486C9F0-B156-4A42-B0E0-32AD4A1DF53A']//div//table//div)[27]";
    }
    
     public static String EnvironmentTabXPath() {
        return "//div[@id='control_BC138B8D-FA48-4352-A20A-7FB5AF468D6C']//div[text()='Environment']";
    }
    
    public static String envirSpillRecordNumXpath(){
        return "//div[@id='form_225A3932-61B8-4E84-97AD-535E23855E13']//div[@id='divPager'][@class='recnum']/div[@class='record']";
    }

    public static String MonitoringRequiredCheckboxXPath() {
        return "//div[@id='control_9AD21B46-7462-4311-9D26-146E3DC51234']//div[@class='c-chk']";
    }

    public static String UncontrolledReleaseOrSpillXPath() {
        return "//div[@id='control_CEB22C22-C683-43D8-90E7-7EE38805727C']//span[text()='Uncontrolled release / spill']";
    }

    public static String environmentalPillGrip() {
        return "//div[@sourceid='225A3932-61B8-4E84-97AD-535E23855E13']";
    }

    public static String UncontrolledReleaseOrSpillAddButtonXPath() {
        return "//div[@id='control_CD5D42EA-CA49-4D1F-A1AB-6E0C42C3AF80']//div[@title='Add']";
    }

    public static String ProductDropdownXPath() {
        return "//div[@id='control_4D0FAC22-8D64-4C4A-85B7-21D05D42043E']//li";
    }

    public static String QualityReleasedTextFieldXPath() {
        return "//div[@id='control_A2598D8A-BBDF-4A8B-A667-E709942B8DE3']//div//div/input";
    }

    public static String evironmentSpillFlowProcessButton() {
        return "//div[@id='btnProcessFlow_form_225A3932-61B8-4E84-97AD-535E23855E13']";
    }

    public static String evironSpillAddPhaseXpath() {
        return "//div[text()='Add phase']//../..//div[@class='step active']";
    }

    public static String evironSpillEditPhaseXpath() {
        return "//div[text()='Edit phase']//../..//div[@class='step active']";
    }

    public static String QualityContainedTextFieldXPath() {
        return "//div[@id='control_11728820-4C02-4C13-B2BF-216C069EECB4']//div//div/input";
    }

    public static String UnitOFMeasureDropdownXPath() {
        return "//div[@id='control_DBC9A52C-ACE5-43D2-A8EB-BA8243A6E73A']//li";
    }

    public static String DetailedDescriptionTextAreaXPath() {
        return "//div[@id='control_F84B27C2-67B3-4226-95E5-3D3C09FCB91A']//textarea";
    }

    public static String CleanUpCostTextFieldXPath() {
        return "//div[@id='control_822E9E45-A2A6-4136-91F5-DEBCC42C42CC']//div//div//input";
    }

    public static String SubmitButtonXPath() {
        return "//div[@id='control_D25F3D20-8E33-419C-B724-EE84BEAC65CA']//div[text()='Submit']";
    }
    
    public static String Save_Close_ButtonXPath() {
        return "//div[@id='control_D25F3D20-8E33-419C-B724-EE84BEAC65CA']//div[text()='Save and close']";
    }
    
    public static String validateEnvirSpillgridSavedRecord(String record){
        return "//span[text()='"+record+"']";
    }

    public static String evironSpillSaveButtonXpath() {
        return "//div[@id='btnSave_form_225A3932-61B8-4E84-97AD-535E23855E13']";
    }

    public static String ValidateRecordWasCreatedXPath() {
        return "//div[@id='control_CD5D42EA-CA49-4D1F-A1AB-6E0C42C3AF80']//tr//div[text()='Acetone']";
    }

    public static String MonitoringHeadingXPath() {
        return "//span[text()='Monitoring']";
    }

    public static String SelectAllButtonMonitoringAreasXPath() {
        return "//div[@id='control_0FA48973-4517-449F-88FE-5484E372B9FE']//b[@class='select3-all']";
    }

    public static String WaterTabXPath() {
        return "//div[text()='Water']";
    }

    public static String ValidateWaterValuesXPath() {
        return "//div[@id='control_00278E07-DE4B-4ED8-8A29-41A4557D137C']//table[@data-role='selectable']";
    }

    public static String AirTabXPath() {
        return "//div[text()='Air']";
    }

    public static String ValidateAirValuesXPath() {
        return "//div[@id='control_942C9A46-36E7-4872-A014-1661890599BF']//table[@data-role='selectable']";
    }

    public static String QualitySubTab() {
        return "//div[text()='Quality']";
    }

    public static String QualityConcessionsXPath() {
        return "//span[text()='Quality Concessions ']";
    }

    public static String ConcessionsAddButtonXPath() {
        return "//div[text()='Concession']/..//div[text()='Add']";
    }

    public static String ConcessionDescriptionTextAreaXPath() {
        return "//div[@id='control_04842A6A-3005-4D07-9131-6B25C1373134']//textarea";
    }

    public static String ImpactTextAreaXPath() {
        return "//div[@id='control_9ECE066A-3D4C-4209-B2A3-54ED4D4DF4E8']//textarea";
    }

    public static String PartiesAffectedDropdownXPath() {
        return "";
    }

    public static String CorrectiveActionTextAreaXPath() {
        return "//div[@id='control_83CBB14B-6E16-490E-B408-EF19607B33AD']//textarea";
    }

    public static String qualityReleased_Input() {
        return "(//div[@id='control_A2598D8A-BBDF-4A8B-A667-E709942B8DE3']//input)[1]";
    }

    public static String quantityContained_Input() {
        return "(//div[@id='control_11728820-4C02-4C13-B2BF-216C069EECB4']//input)[1]";
    }
    
    public static String clean_up_Cost(){
        return "(//div[@id='control_822E9E45-A2A6-4136-91F5-DEBCC42C42CC']//input)[1]";
    }

    public static String EnvironmentSpillXPath_Record() {
        return "(//div[@id='control_CD5D42EA-CA49-4D1F-A1AB-6E0C42C3AF80']//div//table)[3]";
    }

    public static String SocialSustainability_TabXPath() {
        return "//div[@id='control_BC138B8D-FA48-4352-A20A-7FB5AF468D6C']//div[text()='Social Sustainability']";
    }

    public static String SocialSustabilityGrievances_Panel() {
        return "//div[@id='control_CF4D24F2-9D87-4071-8F95-926EE9CA39FE']//span[text()='Social Sustainability Grievances']";
    }

    public static String SocialSustabilityGrievances_AddButtonXPath() {
        return "//div[@id='control_BB3AC28C-4C9B-4B24-8654-E626051C059A']";
    }

    public static String Grievance_FlowProcessButton() {
        return "//div[@id='btnProcessFlow_form_A38BD6B3-FAD6-47B5-8124-8F7DBEC01739']";
    }
    
    public static String iframe_Xpath()
    {
        return "//iframe[@id='ifrMain']";
    }
    
    public static String Grievances_Reception_Channel;

    public static String Social_Sustainability()
    {
        return"//label[text()='Social Sustainability']";
    }
    
     public static String Grievances_Title_Option(String text)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + text + "')]";
    }
       public static String Grievances_Relationship_Owner_Option(String text)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + text + "')]";
    }
       
           public static String Grievances_Status_Option(String text)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + text + "')]";
    }

    public static String Social_Sustainability_Heading()
    {
      return "//h1[text()='Social Sustainability']";
    }

    public static String Complaints_Grievances_Tab()
    {
        return"//label[text()='Complaints & Grievances']";
    }
    
     public static String Process_Flow()
    {
        return "//span[@class='img icon flow-tree icon-button']";
    }
     
       public static String Button_Add()
    {
        return "//div[text()='Add']";
    }

    public static String Grievance_title()
    {
        return"(//div[@id='control_2640F649-7C15-4A4A-A49D-46D00B350FC0']//input)[1]";
    }

    public static String Grievance_summary()
    {
     return"(//div[@id='control_051ED5F3-EF25-4BED-BA5A-12FE10DD3877']//textarea)[1]";
    }

    public static String Grievance_reference_code()
    {
       return"(//div[@id='control_C3FF2F54-1F11-440F-877C-E179E880EB5F']//input)[1]";
    }

  

    public static String Latitude()
    {
       return"(//div[@id='control_600F3FE7-F1C0-4081-8523-4CE27BB854FB']//input)[1]";
    }

    public static String Longitude()
    {
        return"(//div[@id='control_3952FD06-E9F5-4DB0-92C1-CCA7C1181454']//input)[1]";
    }

    public static String Grievances_Reception_Date()
    {
       return"(//div[@id='control_61CCCAF9-261B-421E-9682-A1EACFB89ACB']//input)[1]";
    }

    public static String Business_Unit()
    {
      return "(//div[@id='control_6A2CB420-D98E-4181-9519-1658C80C4EE5']//ul//li)";
    }

    public static String Business_Unit_Option(String string)
    {
        return"//a[text()='"+string+"']";
    }

    public static String Grievances_Received_by()
    {
        return"//div[@id='control_7198DB52-AF78-41DB-ADE2-D8C9044868A6']//ul//li";
    }

    public static String Grievances_Received_by_option(String data)
    {
        return"//a[text()='"+data+"']";
    }

    public static String Individual_Entity_option(String data)
    {
     return"//a[text()='"+data+"']";
    }

    public static String Individual_Entity()
    {
            return "//div[@id='control_51BF378D-98C2-47AD-AF1A-22AFCB271228']//ul//li";

    }
    
     public static String Grievances_Name_3()
    {
        return"//div[@id='control_D64C196F-C0A3-4554-B859-61E42F789D3B']//ul//li";
    }

    public static String Grievances_Name()
    {
        return"(//li[text()='Please select'])[8]";
    }
    
     public static String Grievances_Name_1()
    {
        return"//div[@id='control_D64C196F-C0A3-4554-B859-61E42F789D3B']//ul//li";
    }

    public static String Grievances_Name_option(String data)
    {
        return"//a[text()='"+data+"']";
    }

    public static String Grievances_Location()
    {
        return"//div[@id='control_E6FA4852-121D-41FE-BC5A-D20212EC2190']//ul//li";
    }

    public static String Grievances_Location_option(String data)
    {
        return"//a[text()='"+data+"']";
    }

    public static String Grievances_Responsibility()
    {
       return "//div[@id='control_F9C1A84D-4E33-45BC-9C36-2BF4E8BDBC38']//ul//li";
    }

    public static String Grievances_Responsibility_option(String data)
    {
        return"//a[text()='"+data+"']";
    }

    public static String Grievances_Reception_Channel_option(String data)
    {
        return"//a[text()='"+data+"']";
    }

    public static String Grievances_Reception_Channel()
    {
        return"//div[@id='control_36ACD371-D11A-4D20-A89D-02CE2AB66EB9']//ul//li";
    }
    
    public static String inspection_Record_Saved_popup()
    {
        return "//div[@id='divForms']//div[contains(text(),'Record saved')]";
    }
      
        public static String Button_Save()
    {
        return "(//div[text()='Save'])[2]";
    }

    public static String Grievances_Name_Search()
    {
     return "(//ul[@class='jstree-container-ul jstree-children'])[4]//li[1]";
    }

    public static String Grievances_Location_Search(String text)
    {
        return"//div[contains(@class, 'transition visible')]//a[contains(text(),'" + text +"')]";
    }

    public static String Responsibility_Search(String text)
    {
         return"//div[contains(@class, 'transition visible')]//a[contains(text(),'"+text + "')]";
    }

    public static String Reception_Search(String text)
    {
           
       return"//div[contains(@class, 'transition visible')]//a[contains(text(),'" + text + "')]";
    }

    public static String Registration_label()
    {
      return"//div[text()='Registration has to be completed to move to STEP 2']";
    }

    public static String Registration_Complete()
    {
      return "//div[@id='control_93DE0394-7F2C-4178-9EA4-5DEF4EDA5922']//ul//li";
    }

    public static String Registration_Complete_Option(String text)
    {
       return"//div[contains(@class, 'transition visible')]//a[contains(text(),'" +text+"')]";
    }

    public static String Grievances_Name_Search_2()
    {
        return"(//div[contains(@class, 'transition visible')]//a[contains(text(),'aa')]/..//i)[1]";
    }

    public static String Grievances_Name_Search_Item()
    {
       return "(//a[text()=',m ,'])[1]";
    }

    public static String Search_Text_Box()
    {
       return "(//i[@class='icon search'])[8]/..//input";
    }

    public static String Search_Text_Box_Results(String data)
    {
        return"return//a[text()='"+data+"']";
    }

    public static String Grievances_Name_Drop_Down(String data)
    {
       return"//div[@id='control_D64C196F-C0A3-4554-B859-61E42F789D3B']//ul//li";
    }

    public static String Button_Grievant_Not_Registered()
    {
        return"//div[@id='control_751D50A1-55CC-48B7-A987-DC6F27997E38']";
    }
    
    
    
    
    

    public static String Stakeholder_First_Name()
    {
        return"(//div[@id='control_E186B9E5-4102-409D-8F57-7355938C09D9']//input)[1]";
    }
    
     public static String Stakeholder_First_Name_()
    {
        return"(//div[@id='control_E186B9E5-4102-409D-8F57-7355938C09D9']//input)[1]";
    }

    public static String Stakeholder_Last_Name()
    {
        return"(//div[@id='control_A9D1A3E8-C561-452A-A1F4-7BCB496B365F']//input)[1]";
    }

    public static String Stakeholder_Known_as()
    {
       return"(//div[@id='control_BED0557B-BBC2-46C1-B571-BE60A267F0EA']//input)[1]";
    }
    
    public static String Stakeholder_Known_as_2()
    {
       return"//div[@id='control_5126FC78-97E6-49AA-A6D0-327CD4FD2CC5']//input";
    }

    public static String Title()
    {
        return"(//div[@id='control_28C03054-D663-431B-9F65-38BE54617019']//ul//li)";
    }

    public static String Title_Options(String data)
    {
        return"return//a[text()='"+data+"']";
    }

    public static String Status()
    {
        return"(//div[@id='control_170F62D3-43C4-4548-A7C3-2E856B137AC8']//ul//li)";
    }

    public static String Status_Options(String data)
    {
        return"return//a[text()='"+data+"']";
    }

    public static String Stakeholder_Categories()
    {
       return "//div[@id='control_F1357856-7A84-4716-9B1D-077C87CC8591']//b[@original-title='Select all']";
    }
    
    public static String Stakeholder_Entity_Categories()
    {
       return "//div[@id='control_BDB3E74D-818E-4A51-8443-3F30BA7A472A']//b[@original-title='Select all']";
    }

    public static String Stakeholder_Business_Units()
    {
        return"//div[@id='control_4CFB2165-708B-4D55-9988-9CDCB5487291']//b[@original-title='Select all']";
    }

    public static String Stakeholder_Impact_Types()
    {
       return"//div[@id='control_65F1B5F4-17B9-48FE-817D-B27F54AB360E']//b[@original-title='Select all']";
    }

    public static String Grievances_Relationship_Owner()
    {
        return"//div[@id='control_4BC61A3A-EC52-4BEA-807E-B70C75D5B421']//li";
    }
    
     public static String Stakeholder_Relationship_Owner()
    {
        return"//div[@id='control_A01F1D1A-45BF-4A6B-B2C2-88046BDAFDA1']//li";
    }

    public static String Grievances_Name_Enter()
    {
        return"(//div[contains(@class, 'transition visible')]//input[@class])[130]";
    }

    public static String Button_Grievant_Not_Registered_1()
    {
       return"//div[@id='control_B076A3D3-95D4-4A7C-A1BB-F2394441CF1B']//div[text()='If grievant not registered ADD new']";
    }
    
      public static String Button_Grievant_Not_Registered_2()
    {
        return"//div[text()='If entity not registered ADD new']";
    }

    public static String Grievances_Name_2()
    {
        return"(//li[text()='Please select'])[7]";
    }

    public static String Stakeholder_Entity_type()
    {
        return"(//li[text()='Please select'])[4]";
    }

    public static String Stakeholder_Entity_type_Options()
    {
          return"//a[text()='test']";
    }

    public static String Stakeholder_Entity_Name()
    {
        return"//div[@id='control_6B36E56B-4BD2-4A16-AD58-94FE1883EFE2']//input";
    }

    public static String Industry()
    {
      return "//div[@id='control_425BD26A-5DDB-485A-BA7F-E8D2E51C4BEA']//ul//li";
    }

    public static String Industry_Option(String data)
    {
          return"//a[text()='"+data+"']";
    }

    public static String Stakeholder_Entity_Description()
    {
        return"//div[@id='control_4AEFCBE1-7C06-4528-BB6B-CFD298C47AA1']//textarea";
    }

    public static String Stakeholder_Relationship_Owner_Option(String data)
    {
         return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String Stakeholder_Entity_Business_Units()
    {
       return "//div[@id='control_931D1181-0EA6-4EBF-BAA9-B497F5793EC0']//b[@original-title='Select all']";
    }

    public static String Stakeholder_Entity_Impact_Types()
    {
       return"//div[@id='control_962A59FE-78C1-4FCE-B40E-CAC2A8124522']//b[@original-title='Select all']";
    }

    public static String selectAll_externalParties() {
        return "//div[@id='control_F3EBE4A9-5055-414F-8D34-1DD0E968BD92']//b[@class='select3-all']";
    }
    
    public static String tailingManagementTab(){
       return "(//label[contains(text(),'Tailings Management')])[1]";
   }
   public static String tailingManagementMonitoringTab(){
       return "//label[contains(text(),'Tailings Management Monitoring')]";
   }
   public static String airQualityMonitoringTab(){
       return "//label[contains(text(),'Air Quality Monitoring')]";
   }
 
   public static String businessUnitTab(){
       return "//div[@id='control_739C430A-AAD9-47C4-80E3-30FE13AB4D33']";
   }
   public static String businessUnitSelect(String text) {
        return "//div[@id='divForms']/div[4]//a[contains(text(),'" + text + "')]";
   }
   public static String airQualityType(){
       return "//div[@id='control_164E9ECF-98B7-44BE-810E-9E6D002BE0EB']";
   }
   public static String airQualityTypeSelect(String text){
       return "//div[@id='divForms']/div[6]//a[contains(text(),'" + text + "')]";
   }
   public static String monthTab(){
       return "//div[@id='control_4E1D4CE9-349A-491A-8D18-EFD53A2076C7']";
   }
   public static String monthOption(String text){
       return "//div[@id='divForms']/div[7]//a[contains(text(),'" + text + "')]";
   }
   public static String yearTab(){
       return "//div[@id='control_228918B1-E384-4DE9-9910-2D881B26DA23']";
   }        
    public static String yearOption(String text){
       return "//div[@id='divForms']/div[8]//a[contains(text(),'" + text + "')]";
   }
    public static String monitoringPointTab(){
        return "//div[@id='control_E96E3D7B-B465-4EEE-B615-F90459343BE6']";
    }
    public static String monitoringPointOption(String text){
        return "(//div[@id='divForms']//a[contains(text(),'" + text + "')])[1]";
    }
    public static String uploadLinkbox() {
        return "//div[@class='linkbox-options']//b[@class='linkbox-link']";
    }
    public static String saveToContinueButton(){
        return "//div[@id='control_D4D50C09-26C5-410D-9549-2CC4BE86A134']//div[contains(text(),'Save to continue')]";
    }
    public static String saveSupportingDocuments(){
        return "//div[@id='control_F1395E4D-7A99-4032-A127-20F4D7E31DE2']";
    }


    //measurements Xpaths
    public static String measurementTab() {
         return "//div[@id='control_B85384C4-EF31-486E-BAB1-D66480457717']";
    }
    public static String measurement(){
        return "//div[@id='control_B85384C4-EF31-486E-BAB1-D66480457717']//span[contains(text(),'Measurements')]";
    }

    public static String measurementsAddButton() {
        return "//div[@id='control_4117C574-64DE-4D2D-810F-0C8EF1E4E1F1']//div[contains(text(),'Add')]";
    }
    public static String measurementProcessFlow(){
        return "//div[@id='btnProcessFlow_form_F8D39CA4-85E3-4C8D-93BD-8B0F9981CF85']";
    }
    public static String measurementMonitoringTab(){
        return "//div[@id='control_A6D81C10-F563-40CE-83BF-19B9B5F432F4']";
    }

    public static String measurementMonitoringOption(String data) {
        return "(//a[contains(text(),'" + data + "')])[1]";
    }

    public static String measurementFieldTab() {
        return "//div[@id='control_0F289C2C-8DDB-4551-A052-E90F299940E2']";
    }
    public static String measurementField(){
        return "(//div[@id='control_0F289C2C-8DDB-4551-A052-E90F299940E2']//input)[1]";
    }

    public static String unitTab() {
        return "//div[@id='control_E743CAC3-3341-4CB7-9EF3-4863E15E63DA']";
    }

    public static String unitOption(String data) {
        return "//a[contains(text(),'" + data + "')]";
    }

    public static String measurementSaveButton() {
        return "//div[@id='btnSave_form_F8D39CA4-85E3-4C8D-93BD-8B0F9981CF85']/div[contains(text(),'Save')]";
    }

    public static String CEM_Parameter_panel(){
        return "//div[@id='control_3CB28695-7F32-4A0B-9B5E-A7399E51A9B2']//span[text()='Parameter Readings']";
    }
    
    public static String CEM_Add(){
        return "//div[@id='control_3CB28695-7F32-4A0B-9B5E-A7399E51A9B2']//div[text()='Add']";
    }
    
    public static String CEM_Parameters_dropdown(){
        return "//div[@id='control_F11D69C1-F470-4A98-AA57-C7DA50E56DA4']//ul";
    }
    
    public static String CEM_Parameters_select(String data){
        return "(//div[@id='divForms']//a[contains(text(),'" + data + "')])[1]";
    }
    
    public static String CEM_Measurement(){
        return "//div[@id='control_BC956D1B-8B55-469B-8956-895C33B5E897']/div/div/input";
    }
    
    public static String CEM_Sdate(){
        return "//div[@id='control_661EDBD4-2302-4210-A42C-FBF1F7E2F674']//input";
    }
    
    public static String CEM_stakenby(){
        return "//div[@id='control_293D62D6-7F8B-4F4C-935C-8D9E57CB23B0']/div/div//input";
    }
    
    public static String CEM_comments(){
        return "//div[@id='control_E9887CF7-47E3-45E3-88CB-B23729525ECC']/div/div//textarea";
    }
    
    public static String CEM_Save(){
        return "(//div[@id='divForms']//div[text()='Save'])[5]";
    }
    
    public static String AQMF_panel(){
        return "//div[@id='control_F0FAA022-87BA-48E1-9A52-D5856F210E3E']//span[text()='Air Quality Monitoring Findings']";
    }
    
    public static String AQMF_Add(){
        return "//div[@id='control_F0FAA022-87BA-48E1-9A52-D5856F210E3E']//div[text()='Add']";
    }
    
    public static String AQMF_processflow(){
        return "//div[@id='btnProcessFlow_form_A4BB87E5-A0CA-4A12-A57D-06244726EDAA']";
    }
    
    public static String AQMF_desc(){
        return "//div[@id='control_40ECC722-B08B-48F3-9906-3CFCE527C5CD']//textarea";
    }
    
    public static String AQMF_owner_dropdown(){
        return "//div[@id='control_434D9128-5EBC-4E25-9836-72A2C4451733']";
    }
    
    public static String AQMF_owner_select(String text){
        return "//a[contains(text(),'"+text+"')]";
    }
    
    public static String AQMF_risksource_dropdown(){
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']";
    }
    
    public static String AQMF_risksource_select(String text){
        return "//a[text()='"+text+"']/../i";
    }
    
    public static String AQMF_risksource_select2(String text1, String text2){
        return "//a[text()='"+text1+"']/..//a[text()='"+text2+"']/i[1]";
    }
    
    public static String AQMF_class_dropdown(){
        return "//div[@id='control_7689E71C-DC7E-41C1-9124-C6312596E956']";
    }
    
    public static String AQMF_class_select(String text){
        return "//a[text()='"+text+"']";
    }
    
    public static String AQMF_risk_dropdown(){
        return "//div[@id='control_1D7F3DE4-F9C4-4626-80C9-0E600A0A8FF8']";
    }
    
    public static String AQMF_risk_select(String text){
        return "//a[text()='"+text+"']";
    }
    
    public static String AQMF_arrow(){
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']/div/a/span[2]/b[1]";
    }
    
    public static String AQMF_save(){
        return "//div[@id='control_E0B5417B-8F24-4E1D-A4DB-8C68192F6F7B']//div[text()='Save to continue']";
    }
    
    public static String AQMF_savewait(){
        return "//div[text()='Air Quality Findings Actions']";
    }
    public static String expandGlobalCompany() {
        return "(//div[@id='divForms']/div[4]//a[contains(text(),'Global Company')]//..//i)[1]";
    }
    public static String expandSouthAfrica(){
        return "(//div[@id='divForms']/div[4]//a[contains(text(),'South Africa')]//..//i)[1]";
    }
    public static String CEM_processFlow() {
        return "//div[@id='btnProcessFlow_form_BCE8C74D-00E6-4B6A-AAEF-C92A9F7FDC22']";
    }

    public static String CEM_uploadLinkbox() {
        return "//div[@id='control_F60F4398-06EE-403F-891A-BFEA6314167E']//b[@class='linkbox-link']";
    }

    public static String closeConcession() {
        return "(//div[@id='form_B3205F35-ED12-44E1-BD2F-995318C72096']//i[@class='close icon cross'])[1]";
    }

    public static String concessionsRecord_Gridview(String concession_RecordNo) {
        return "//div[@id='control_0486C9F0-B156-4A42-B0E0-32AD4A1DF53A']//span[contains(text(),'" + concession_RecordNo + "')]";
    }

    public static String monitoringRequired_Checkbox() {
        return "//div[@id='control_9AD21B46-7462-4311-9D26-146E3DC51234']";
    }

    public static String monitoring_Panel() {
        return "//div[@id='control_FF4ADAB1-5F5D-43A0-A379-F619340F446C']//span[contains(text(),'Monitoring')]";
    }

    public static String monitoringAir_Panel() {
        return "//div[@id='control_9A9C1508-89BC-41A0-B08F-CCBB2EDF2A24']//div[contains(text(),'Monitoring - Air')]";
    }

    public static String monitoringAir_AddButton() {
        return "//div[@id='control_9A9C1508-89BC-41A0-B08F-CCBB2EDF2A24']//div[contains(text(),'Add Air Monitoring Record')]";
    }
  
    public static String refButton() {
        return "//div[@id='control_942C9A46-36E7-4872-A014-1661890599BF']//a[@class='k-pager-refresh k-link']";
    }
      public static String refButtonWater() {
        return "//div[@id='control_00278E07-DE4B-4ED8-8A29-41A4557D137C']//a[@class='k-pager-refresh k-link']";
    }


    public static String monitorReqquiredCheckbox() {
        return "//div[@id='control_9AD21B46-7462-4311-9D26-146E3DC51234']";
    }

    public static String TypeOfMonitoringAreaCheckbox(String monitor) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + monitor + "')]//../i[@class='jstree-icon jstree-checkbox']";
    }

    public static String monitoringAreasTapXpath(String monitor) {
        return "//div[text()='" + monitor + "']";
    }

    public static String monitorPanelXpath() {
        return "//span[text()='Monitoring']";
    }

    public static String businessUnitDropdownXpath() {
        return "//div[@id='control_739C430A-AAD9-47C4-80E3-30FE13AB4D33']";
    }

    public static String waterBusinessUnitDropdownXpath() {
        return "//div[@id='control_2DA9BFE1-0DFB-49D8-B8FA-91C41588F17F']";
    }
    
    public static String waterType_DropdownXpath() {
        return "(//div[@id='control_ECCB6A13-45D6-4733-B5BA-19D53E478387']//b)[1]";
    }
     public static String waterMeasurementTypeDropdownXpath() {
        return "//div[@id='control_500DF708-222A-4E1F-ADE3-03595ED3CF8C']";
    }
     
   public static String waterMonthDropdownXpath() {
        return "//div[@id='control_249D04EA-CF17-4906-B04B-85B8918F5250']";
    }
   
   public static String waterYearDropdownXpath() {
        return "//div[@id='control_AA23CE3F-66BA-4D1C-A149-564C368B1452']";
    }
   
   public static String waterMonitorPointDropdownXpath() {
        return "//div[@id='control_C14FC30B-429E-464A-91BF-32E867450C35']";
   }
   
    public static String waterFlowProcessXpath() {
        return "//div[@id='btnProcessFlow_form_F517B4C9-16D1-4734-9DAB-8AEDA8426F53']";
    }
    
    public static String waterSaveXpath() {
        return "//div[@id='btnSave_form_F517B4C9-16D1-4734-9DAB-8AEDA8426F53']";
    }


    public static String TypeOfMonitoringAreaOption(String monitor) {
        return "//a[contains(text(),'" + monitor + "')]";
    }

    public static String airQualityTypeDropdownXpath() {
        return "//div[@id='control_164E9ECF-98B7-44BE-810E-9E6D002BE0EB']";
    }

    public static String monthDropdownXpath() {
        return "//div[@id='control_4E1D4CE9-349A-491A-8D18-EFD53A2076C7']";
    }

    public static String yearDropdownXpath() {
        return "//div[@id='control_228918B1-E384-4DE9-9910-2D881B26DA23']";
    }

    public static String monitorPointDropdownXpath() {
        return "//div[@id='control_E96E3D7B-B465-4EEE-B615-F90459343BE6']";
    }

    public static String airMonitoringButtonXpath() {
        return "//div[@id='btnSave_form_74DBD0CA-CB81-485B-AFD9-8F5F59BEFD9D']";
    }

    public static String getAirMonitorRecord() {
        return "//div[@class='recnum']//div[@class='record']";
    }
    
    public static String airMonitorFlowProcessButton() {
        return "//div[@id='btnProcessFlow_form_74DBD0CA-CB81-485B-AFD9-8F5F59BEFD9D']";
    }

    public static String airSaveButton() {
        return "//div[@id='btnSave_form_74DBD0CA-CB81-485B-AFD9-8F5F59BEFD9D']";
    }

    public static String AirSaveWait() {
        return "//div[@id='form_74DBD0CA-CB81-485B-AFD9-8F5F59BEFD9D']//div[@class='ui inverted dimmer active']";
    }

    public static String AirNotActiveSaveWait() {
        return "//div[@id='form_74DBD0CA-CB81-485B-AFD9-8F5F59BEFD9D']//div[@class='ui inverted dimmer']";
    }
    
    public static String discharge_to(){
        return "//div[@id='control_D053C5AA-410A-4A27-9437-08B17E171652']/div/div/input";
    }

    public static String monitoringAreas_selectAll() {
        return "//div[@id='control_0FA48973-4517-449F-88FE-5484E372B9FE']//b[@class='select3-all']";
    }

    public static String businessUnit_DropDown() {
         return "//div[@id='control_739C430A-AAD9-47C4-80E3-30FE13AB4D33']";
    }

    public static String water_Tab() {
        return "//div[text()='Water']";
    }
    public static String Air_Tab() {
        return "//div[text()='Air']";
    }

    public static String monitoringWater_AddButton() {
        return "//div[@id='control_4DAD1791-E70E-4FE0-9E9E-F16FC7EBF9EC']";
    }

    public static String waterTransaction_FlowProcessButton() {
        return "//div[@id='btnProcessFlow_form_F517B4C9-16D1-4734-9DAB-8AEDA8426F53']";
    }

    public static String WT_businessUnit_DropDown() {
        return "//div[@id='control_2DA9BFE1-0DFB-49D8-B8FA-91C41588F17F']";
    }
    
    public static String threeIncidentsInvesgationXpath() {
        return "//div[text()='3.Incident Investigation']";
    }

    public static String InvestigationDueDateXPath() {
        return "//div[@id='control_7E7E7BF6-08EF-4008-BEDA-5B7E422D739F']//input";
    }
    
    public static String investigationScope() {
        return "//div[@id='control_669476AC-06EA-4FAE-9872-D47FB2469048']//textarea";
    }
    
    public static String ConsequenceImpact(){
        return "//div[text()='Consequence/ Impact Assessment']";
    }

    public static String Incident_rightarrow() {
        return "(//div[@class='tabpanel_tab_content']/div)[6]";
    }
    
    public static String assistAdd() {
        return "//div[@sourceid='30D4E41B-74D7-48DE-BBCB-EFEAD1F5C86B']//div[@title='Add']";
    }
    
    public static String records(){
        return "//div[@id='form_B3205F35-ED12-44E1-BD2F-995318C72096']//div[@id='divPager'][@class='recnum']/div[@class='record']";
    }

    public static String assetsTabXpath() {
        return "//div[@id='control_0F686E95-8AB8-4A68-85B3-7670D78BA16A']//span[text()='Assets']";//div[@id='control_0F686E95-8AB8-4A68-85B3-7670D78BA16A']//span[text()='Assets']
    }

    public static String processDropdownXpath() {
        return "//div[@id='control_3CD8F578-F09A-4506-BCC1-2E78DEB8775A']";
    }

    public static String processDescriptionXpath() {
        return "//div[@id='control_3CD8F578-F09A-4506-BCC1-2E78DEB8775A']//input";
    }

    public static String rtoRequiredDropdownXpath() {
        return "//div[@id='control_4DC86A43-C638-4786-9011-64C5F786E56F']";
    }

    public static String mandatoryDropdownXpath() {
        return "//div[@id='control_4548477B-C54F-4BF7-94E6-1501F86953C9']";
    }

    public static String backUpDropdown() {
        return "control_984AFB85-44EF-4D76-9317-A057CEC8C400";
    }

    public static String chevron_Down_ButtonXPath() {
        return "//div[@id='btnSave_form_B3205F35-ED12-44E1-BD2F-995318C72096']//div[@class='more options icon chevron down']";
    }

    public static String save_And_Close_XPath() {
        return "//div[@id='btnSave_form_B3205F35-ED12-44E1-BD2F-995318C72096']//div[text()='Save and close']";
    }

    public static String save_XPath() {
        return "//div[@id='btnSave_form_B3205F35-ED12-44E1-BD2F-995318C72096']//div[text()='Save']";
    }

    public static String concession_Status_XPath() {//div[@id='control_0486C9F0-B156-4A42-B0E0-32AD4A1DF53A']//div[text()='']
        return "//div[@id='control_0486C9F0-B156-4A42-B0E0-32AD4A1DF53A']//a[text()='Status']/../../../../../../..//td[@role='gridcell'][9]//div";
    }

    public static String data_Item_Concessions_XPath(String name) {
        return "//div[text()='" + name + "']";
    }

    public static String concession_Status_Dropdown_XPath() {
        return "//div[@id='control_DECFB794-FDE7-4085-BBAE-06738217604F']//b[@class='select3-down']";
    }

    public static String concessions_Loading_HiddenXPath() {
        return "//div[@id='divFormLoading'][@class='form active transition hidden']";
    }

    public static String concessions_Loading_VisibleXPath() {
        return "//div[@id='divFormLoading'][@class='form active transition visible']";
    }

    public static String verification_And_Additional_Loading_VisibleXPath() {
        return "//div[@id='form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@class='ui inverted dimmer active']//div[@class='ui text loader'][text()='Loading data...']";
    }

    public static String non_Editable_Grid_XPath() {
        return "//div[@id='control_0486C9F0-B156-4A42-B0E0-32AD4A1DF53A']//div[@class='grid k-grid k-widget k-reorderable']";
    }

    public static String search_Button_ConcessionXPath() {
        return "//div[@id='control_0486C9F0-B156-4A42-B0E0-32AD4A1DF53A']//span[@class='icon filter']/..//div[@title='Search']";
    }

    public static String search_Button_On_Filter_XPath() {
        return "//div[@id='control_0486C9F0-B156-4A42-B0E0-32AD4A1DF53A']//div[@id='act_filter_right']//div[@title='Search']";
    }

    public static String status_Filter_XPath() {
        return "//div[@id='control_0486C9F0-B156-4A42-B0E0-32AD4A1DF53A']//td[text()='Status']/..//a[@class='select3-choice']";
    }

    public static String table_Loading_XPath() {
        return "//div[@id='control_0486C9F0-B156-4A42-B0E0-32AD4A1DF53A']//div[@class='k-loading-image']";
    }

    public static String incident_Investigation_Tab_Xpath() {
        return "//li[@id='tab_3A331B31-BF4C-4C42-ADB3-936999FEE515']//div[text()='3.Incident Investigation']";
    }

    public static String process_Flow_Button_XPath() {
        return "//div[@id='form_B6196CB4-4610-463D-9D54-7B18E614025F']//span[@original-title='Process flow']";
    }

    public static String investigation_Planning_Tab_XPath() {
        return "//div[@id='control_4A4AC927-B046-4637-B3BF-E289C923C98F']//div[text()='Investigation Planning']";
    }

    public static String supporting_Information_XPath() {
        return "//div[@id='control_9A5398B3-88BB-4636-923E-C08D32F88C92']//span[text()='Supporting information to guide the investigation team during the investigation']";
    }

    public static String data_Collection_Editable_Grid_XPath() {
        return "//div[@id='control_643A1426-B1AC-487E-983B-29CB33C5E342']//div[@class='grid k-grid k-widget k-reorderable editable']";
    }

    public static String add_Data_Collection_Button_XPath() {
        return "//div[@id='control_643A1426-B1AC-487E-983B-29CB33C5E342']//div[@title='Add']";
    }

    public static String file_Name_Data_Collection_Field_XPath() {
        return "//div[@id='control_96582EB8-B3CF-49C2-A0B2-86E1502C7918']//input[not(@style='display: none;')]";
    }

    public static String description_Data_Collection_Field_XPath() {
        return "//div[@id='control_F6FD8C0D-ECBA-4546-80C0-19E86BE85307']//input[not(@style='display: none;')]";
    }

    public static String save_New_Data_Collection_Item_XPath() {
        return "//div[@id='control_643A1426-B1AC-487E-983B-29CB33C5E342']//div[@title='Save']";
    }

    public static String InvestigationScopeTextAreaXPath() {
        return "//div[@id='control_669476AC-06EA-4FAE-9872-D47FB2469048']//textarea";
    }

    public static String InvestigationDetailTabXPath() {
        return "//li[@id='tab_6F0A773F-7860-4EF0-BD5F-7E77A654B69B']//div[text()='Investigation Detail']";
    }

    public static String investigationDueDate() {
        return "//div[@id='control_7E7E7BF6-08EF-4008-BEDA-5B7E422D739F']//input";
    }

    public static String ProcessActivityDropdownXPath() {
        return "//div[@id='control_CC6900A0-1399-4340-AB58-7D532D79BEC8']//li";
    }

    public static String RiskSourceDropdownXPath() {
        return "//div[@id='control_00829F4C-092C-4F33-8824-2661C7BCFA77']//li";
    }

    public static String RiskDropdownXPath() {
        return "//div[@id='control_F1868BAD-518A-47C2-B452-81FA4EF109A9']//li";
    }

    public static String RiskDropdownXPath1() {
        return "//div[@id='control_00829F4C-092C-4F33-8824-2661C7BCFA77']//b[@class='select3-down drop_click']";
    }

    public static String InvestigationTypeDropdownXPath() {
        return "//div[@id='control_9033FCFD-1EC6-4C92-AFF0-0AAF4B37E191']//li";
    }

    public static String InvestigationTypeDropdownXPath1() {
        return "//div[@id='control_F1868BAD-518A-47C2-B452-81FA4EF109A9']//b[@class='select3-down drop_click']";
    }

    public static String MechanicalSelectionXPath() {
        return "//*[@id='777c5cc0-1af2-499d-96a5-794cfe6293da']/i";
    }

    public static String FullInvestigationSubTabXPath() {
        return "//li[@id='tab_4B20A9A5-81C9-4639-BD19-06BA7E49D204']//div[text()='Full Investigation']";
    }

    public static String CriticalControlsLinkedToThisBusinessProcessXPath() {
        return "//div[@id='control_EC8ABFCB-A4A6-4A97-B272-4A9FB36E18C5']//span[text()='Critical controls linked to this Process/Activity']";
    }

    public static String WhyAnalysisPanelXPath() {
        return "//div[@id='control_0894E664-8086-41C8-BE16-C302D23A6582']//span[text()='Why Analysis']";
    }

    public static String WhyAnalysisAddButton() {
        return "//span[text()='Why Analysis']/../..//div[@title='Add']";
    }

    public static String noResults() {
        return "//div[@instanceid='FA8CDFB6-9132-4F68-91C2-C9E35BD1FEA9']//div[text()='No results returned']";
    }

    public static String whyAnalysisOrder() {
        return "//div[@id='control_7831FA38-FB43-46A4-893D-42D365D66A05']//input[@type='number']";
    }

    public static String whyAnalysisText() {
        return "//div[@id='control_520640B0-0730-4F01-9D34-D185B8CCDA27']//textarea";
    }

    public static String whyAnalysisAnswerText() {
        return "//div[@id='control_5A999D23-9FE7-4E9D-90E5-8A467467AB6A']//textarea";
    }

    public static String saveWhyAnalysisXpath() {
        return "//div[@sourceid='FA8CDFB6-9132-4F68-91C2-C9E35BD1FEA9']//td//div[@onclick]";
    }

    public static String RelatedUnwantedEventsXPath() {
        return "";
    }

    public static String RelatedRisksXPath() {
        return "//div[@id='control_1A82D518-EC5B-490F-A7C9-C1A94331EADC']//span[text()='Related Risks']";
    }

    public static String RelatedBowtiesXPath() {
        return "//div[@id='control_BE8F6FE0-ABD8-4DCD-9E83-960D64347B9E']//span[text()='Related Bowties']";
    }

    public static String RelatedJSAPanelXPath() {
        return "//div[@id='control_01B1F475-DC2E-4EEF-B0B4-6272EC0C05FC']//span[contains(text(),'Related JSA')]";
    }

    public static String RelatedObligationsPanelXPath() {
        return "//div[@id='control_235A4359-3C38-430A-96DA-28C5BD0FC7F0']//span[text()='Related Obligations']";
    }

    public static String RelatedPoliciesAndProceduresPanelXPath() {
        return "//div[@id='control_A70C27AC-8845-43CB-A2A9-060982C79937']//span[text()='Related Policies & Procedures']";
    }

    public static String controlAnalisisTab() {
        return "//span[text()='Critical controls linked to this Process/Activity']";
    }

    public static String controlAnalisisAddButon() {
        return "//span[text()='Critical controls linked to this Process/Activity']/../..//div[@title='Add']";
    }

    public static String controlDropdown() {
        return "//div[@id='control_BE70E991-0197-4A0A-970F-3457B2970BE0']";
    }

    public static String controlDescription() {
        return "//div[@id='control_599A83BE-64A4-4FAD-B12E-E8EC37E1F20F']//textarea";
    }

    public static String controlAnalysisDropdown() {
        return "//div[@id='control_3DC7FD7E-2E4E-4AAE-9374-AD5434B5DAB8']";
    }

    public static String causeSelectionDropdown() {
        return "//div[@id='control_EACB40D1-4D8E-4D6E-9137-20FBE2151CC0']";
    }

    public static String controlFollowProDropdown() {
        return "//div[@id='control_A8FE6042-A8DE-49BD-9118-EE45F5E2AD3A']";
    }

    public static String controlFlowProcess() {
        return "//div[@id='btnProcessFlow_form_821BC9A7-867F-410A-9837-A4A8D3AFD781']";
    }

    public static String relatedLinkXpath() {
        return "//span[text()='Related Link']";
    }

    public static String anyDropdwonOptionXpath(String option) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + option + "']";
    }

    public static String controlAnalysisSaveButoon() {
        return "//div[@id='btnSave_form_821BC9A7-867F-410A-9837-A4A8D3AFD781']";
    }

    public static String supportDocumentsPanel() {
        return "//span[text()='Supporting Documents']";
    }

    public static String controlAnalysisId() {
        return "//div[@id='form_821BC9A7-867F-410A-9837-A4A8D3AFD781']//div[@class='recnum']//div[@class='record']";
    }

    public static String controlAnalysisActionId() {
        return "//div[@id='form_C8F39E88-B969-4F49-9E2C-89FB6942CB29']//div[@class='recnum']//div[@class='record']";
    }
    public static String incidentActionId() {
        return "//div[@id='form_EE164B84-29D5-424F-A584-A66FC3FE9BCA']//div[@class='recnum']//div[@class='record']";
    }
    
    public static String incidentCommunicationID(){
        return "//div[@id='form_C6B3A628-2B52-4445-842F-7A9BD7D3DCAC']//div[@class='recnum']//div[@class='record']";
    }

    public static String absentDetails() {
        return "//div[@id='control_A90D4EB2-E421-4641-8079-B55D41F4DEE1']//textarea";
    }

    public static String individualDetails() {
        return "//div[@id='control_7FBCB2BE-8B9C-4F36-A8E8-BEE9000740BF']//textarea";
    }

     public static String taskDetails() {
        return "//div[@id='control_EFAE1279-7357-4BB0-BD0C-FC039FFAE35C']//textarea";
    } 
    public static String teamDetails() {
        return "//div[@id='control_7FBCB2BE-8B9C-4F36-A8E8-BEE9000740BF']//textarea";
    }
    
      public static String organisationalDetails() {
        return "//div[@id='control_A2E8FDDA-F1FB-4534-8866-23F173C6991A']//textarea";
    }
      
    public static String actionDescription() {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String departmentResponsibleDropdown() {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']";
    }

    public static String responsiblePersonDropdown() {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']";
    }

    public static String actionDueDate() {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }

    public static String controlAnalysisActionTab() {
        return "//span[text()='Actions']";
    }

    public static String controlAnalysisActionAddButton() {
        return "//span[text()='Actions']/../..//div[@title='Add']";
    }

    public static String controlAnalysisActionProcessButton() {
        return "//div[@id='btnProcessFlow_form_C8F39E88-B969-4F49-9E2C-89FB6942CB29']";
    }
    
      public static String incidentActionProcessButton() {
        return "//div[@id='btnProcessFlow_form_EE164B84-29D5-424F-A584-A66FC3FE9BCA']";
    }

    public static String controlAnalysisActionSaveButton() {
        return "//div[@id='btnSave_form_C8F39E88-B969-4F49-9E2C-89FB6942CB29']";
    }
    
     public static String incidentActionsSaveButton() {
        return "//div[@id='btnSave_form_EE164B84-29D5-424F-A584-A66FC3FE9BCA']";
    }

    public static String activeToBeInitiatedXpath() {
        return "//div[text()='To be initiated']//../..//div[@class='step active']";
    }

    public static String ReasonForNoInvestigationFieldXPath() {
        return "//div[@id='control_EA250AFB-33A8-49BF-A022-51FBB6200AB5']//textarea";
    }

    public static String fullInvestigationTab() {
        return "//div[text()='Full Investigation']";
    }
    
    public static String actionTab(){
        return "//div[text()='Actions']";
    }
    
    public static String CommunicationTab(){
        return "//div[text()='Communication']";
    }
    
    public static String incidentActionAdd(){
        return "//div[text()='Incident Actions']/..//div[@title='Add']";
    }

    public static String saveButton() {
        return "//div[@id='btnSave_form_B6196CB4-4610-463D-9D54-7B18E614025F']";
    }

    public static String CFM_tab(){
        return "//label[text()='Central Findings Manager']";
    }
    
    public static String CFM_RootCause_dropdown(){
        return "//div[@id='control_E4B7948E-2728-48B0-9981-05057C548333']";
    }
    
    public static String CFM_RootCause_select(String text){
        return "(//a[text()='"+text+"']/i)[1]";
    }
    
    public static String CFM_RootCause_arrow(){
        return "//div[@id='control_E4B7948E-2728-48B0-9981-05057C548333']//b[@class='select3-down drop_click']";
    }
    
    public static String CFM_RCC_textarea(){
        return "//div[@id='control_B0C4CB46-6EF6-43B0-B739-6AD8110C1A1A']/div/div/textarea";
    }
    
   
    
    public static String supporting_tab(){
        return "(//div[text()='Supporting Documents'])[2]";
    }
    
     public static String linkADoc_buttonxpath() {
        return "(//b[@class='linkbox-link'])[2]";
    }

    public static String urlInput_TextAreaxpath() {
        return "//input[@id='urlValue']";
    }

    public static String tile_TextAreaxpath() {
        return "//input[@id='urlTitle']";
    }

    public static String linkADoc_Add_buttonxpath() {
        return "//div[text()='Add']";
    }
    
    public static String close_X() {
        return "(//i[@class='close icon cross'])[11]";
    }
    
    public static String actionSignOff_Close(){
        return "(//div[@id='form_99C739EB-8FEF-4871-B955-7A27A210A75E']//i[@class='close icon cross'])[1]";
    }

    public static String auditActionRecord() {
        return "(//div[@id='control_A14067F4-B0D1-468C-B5B8-D0B7BA136E70']//table)[3]";
    }

    public static String actionFeedbackAddButton() {
        return "(//div[@id='control_2FD21E6C-8939-4BE7-B454-7B622DA3C71B']//div[contains(text(),'Add')])[1]";
    }

    public static String actionFeedback_Textarea() {
        return "//div[@id='control_B9A4BEF3-B4D1-491D-881A-6021DDE86169']//textarea";
    }

    public static String actionCompleted_Dropdown() {
        return "//div[@id='control_40EFC614-1BC1-4F14-934E-928BF0980FF2']";
    }

    public static String actionCompleted_Option(String data) {
        return "(//div[@id='control_25A2CAD4-9F0A-4E67-B62A-6A4EF1C9A920']/../../../../../../../..//a[text()='" + data + "'])[1]";
    }

    public static String sendActionTo_Option(String data) {
        return "(//div[@id='control_4854CDB6-7DB2-48B6-B92F-7F2E008CD5E2']//a[contains(text(),'" + data + "')]/i)[1]";
    }

    public static String actionFeedback_ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_70F855C4-38C3-4717-ADA2-27C0F8880E46']";
    }

    public static String close_X4() {
        return "(//i[@class='close icon cross'])[4]";
    }

    public static String close_X13() {
        return "(//i[@class='close icon cross'])[13]";
    }
    
    public static String signOff_tab() {
        return "//li[@id='tab_9AC05ECF-DEDC-44FE-A265-F2D1600C2EC9']//div[contains(text(),'Sign Off')]";
    }

    public static String signOff_addButton() {
        return "//div[@id='control_76DA08C4-FD33-4D3D-B678-8CBD39CE669E']//div[contains(text(),'Add')]";
    }

    public static String signOffAction_Tab() {
        return "//div[@id='control_7EB577FB-3A6B-4CAF-B5E5-1D9E6D8040F0']";
    }

    public static String signOffAction_YesOption(String data) {
        return "(//a[contains(text(),'" + data + "')])[2]";
    }
    public static String signOffAction_NoOption(String data) {
        return "(//a[contains(text(),'" + data + "')])[6]";
    }

    public static String comments() {
        return "//div[@id='control_55A6635D-1D32-4DB9-953C-3329A76E4F05']//textarea";
    }

    public static String actionSignOff_ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_99C739EB-8FEF-4871-B955-7A27A210A75E']";
    }

    public static String rootCause_Tab() {
        return "//div[@id='control_E4B7948E-2728-48B0-9981-05057C548333']";
    }

    public static String rootCause_Select(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String AuditFindingsAction_processFlow() {
        return "//div[@id='btnProcessFlow_form_1F02B741-1791-47E9-A71C-02B94BA3E095']";
    }

    public static String scheduleApproved() {
        return "//div[@id='control_D8B5D098-EFAB-49EF-A4CE-BA7A23F89942']";
    }

    public static String SubmitAuditPlanning() {
        return "//div[@id='control_E45F5AA8-C8E9-4B38-9423-B986B1B9B1CD']";
    }

    public static String StartAudit() {
        return "//div[@id='control_0B6E981D-78E0-445C-8DD5-9F30DD93799A']";
    }

    public static String Findings_tab() {
        return "//li[@id='tab_64706B3F-76E6-43B1-BB38-D56BA68DD73E']";
    }

    public static String Findings_Add() {
        return "//div[@id='control_FEBCB531-E1C1-414D-8D0C-709F4C3E9F51']//div[text()='Add']";
    }

    public static String Findings_pf() {
        return "//div[@id='btnProcessFlow_form_CEB65E1E-0FE0-410C-9786-1A7C036B69D0']";
    }

    public static String audit_Elements() {
        return "//div[@id='control_C7F5BCF3-5E39-4926-B839-AC444423B8D8']";
    }

    public static String audit_Elements_Dropdown() {
        return "//div[@id='control_6690F99C-7292-4FAA-8032-1DEF729FA474']";
    }

    public static String audit_Elements_Select(String data) {
        return "(//a[text()='" + data  + "'])[2]";
    }

    public static String audit_MoreInformation() {
        return "//div[@id='control_8A5CD399-4A9E-4FAC-AE26-44E49C8D7DE8']//textarea";
    }
    public static String Findings_desc(){
        return "//div[@id='control_40ECC722-B08B-48F3-9906-3CFCE527C5CD']/div/div/textarea";
    }
    public static String Findings_owner_dropdown(){
        return "//div[@id='control_434D9128-5EBC-4E25-9836-72A2C4451733']";
    }
    public static String Findings_owner_select(String text){
        return "(//a[contains(text(),'"+text+"')])[4]";
    }
    public static String Findings_risksource_dropdown(){
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']";
    }
    public static String Findings_risksource_select(String text){
        return "(//a[contains(text(),'"+text+"')]/i)[1]";
    }
    public static String Findings_risksource_select2(String text){
        return "(//a[contains(text(),'"+text+"')]/i[1])[1]";
    }
    public static String Findings_risksource_arrow(){
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']//b[@class='select3-down drop_click']";
    }
    public static String Findings_class_dropdown(){
        return "//div[@id='control_7689E71C-DC7E-41C1-9124-C6312596E956']";
    }
    public static String Findings_class_select (String text){
        return "(//a[contains(text(),'"+text+"')])[1]";
    }
    public static String Findings_risk_dropdown(){
        return "//div[@id='control_1D7F3DE4-F9C4-4626-80C9-0E600A0A8FF8']";
    }
    public static String Findings_risk_select(String text){
        return "//a[text()='"+text+"']";
    }
    public static String Findings_stc(){
        return "//div[@id='btnSave_form_CEB65E1E-0FE0-410C-9786-1A7C036B69D0']";
    }
   
    public static String cfm()
    {
        return "//label[text()='Central Findings Manager']";
    }

    public static String cfm_search()
    {
        return "//div[@class='actionBar filter']//div[@title='Search']";
    }

    public static String finding_record(String data)
    {
        return "//div[text()='Inspired Testing']";
    }

    public static String findings_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_9DE682E7-18A4-4632-80DA-F0261923C0B9']";
    }
    
    public static String findingsClose_Button() {
        return "//div[@id='form_9DE682E7-18A4-4632-80DA-F0261923C0B9']//i[@class='home icon search']/..//i[@class='close icon cross']";
    }
    public static String rootCause_arrow() {
        return "(//div[@id='control_E4B7948E-2728-48B0-9981-05057C548333']//b[@class='select3-down drop_click'])[1]";
    }
    public static String saveFindingsButton(){
        return "(//div[text()='Save'])[4]";
    }
    public static String AuditFindingsActions_add() {
        return "//div[@id='control_C313CB91-30D1-4C52-A657-8A23FCE29C35']//div[text()='Add']";
    }
    public static String auditFindingsAudit_ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_493EF828-C4E6-4204-9AEC-3ADF6E0501C0']";
    }
    public static String select_actionComplete2(String data) {
        return "(//a[contains(text(),'" + data + "')])[1]";
    }
    public static String auditActionFeedback_saveButton() {
        return "//div[@id='btnSave_form_48B03F54-B0C3-46EA-82D3-1550972DFE59']";
    }
    public static String inspectionActionFeedback_SaveButton(){
        return "//div[@id='btnSave_form_3BB28043-1704-4345-A15B-87DDE5381C54']";
    }
    public static String auditActionsFindings_Close(){
         return "(//div[@id='form_48B03F54-B0C3-46EA-82D3-1550972DFE59']//i[@class='close icon cross'])[1]";
     }
    public static String InspectionActionsFindings_Close(){
         return "(//div[@id='form_3BB28043-1704-4345-A15B-87DDE5381C54']//i[@class='close icon cross'])[1]";
     }
    public static String aditionalUsers(String data) {
        return "(//div[@id='control_7DAEC194-0631-40D2-8A13-92B40B9E2FE7']/../..//a[contains(text(),'" + data + "')]/i)[1]";
    }
    public static String ActionSignOff_saveButton(){
        return "//div[@id='btnSave_form_99C739EB-8FEF-4871-B955-7A27A210A75E']";
    }
     public static String close_AuditFindingsActionTab() {
        return "(//div[@id='form_1F02B741-1791-47E9-A71C-02B94BA3E095']//i[@class='close icon cross'])[1]";
    }
    public static String findingStatus(){
        return "//div[@id='control_763B9B77-A105-47D2-8AED-10F4D0E482DE']//li[contains(text(),'Closed')]";
    }
    public static String fa_processflow(){
        return "//div[@id='btnProcessFlow_form_CEB65E1E-0FE0-410C-9786-1A7C036B69D0']";
    }
    public static String Inspection_ProcessFlow(){
        return "//div[@id='btnProcessFlow_form_9DE682E7-18A4-4632-80DA-F0261923C0B9']";
    }

    public static String finding_record()
    {
        return "//tbody[@role='rowgroup']//tr[1]";
    }
}
