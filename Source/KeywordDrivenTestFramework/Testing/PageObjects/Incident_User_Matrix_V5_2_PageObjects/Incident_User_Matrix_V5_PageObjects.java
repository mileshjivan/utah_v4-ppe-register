/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Incident_User_Matrix_V5_2_PageObjects;

/**
 *
 * @author smabe
 */
public class Incident_User_Matrix_V5_PageObjects
{

    public static String inspection_Record_Saved_popup()
    {
        return "//div[@id='divForms']//div[contains(text(),'Record saved')]";
    }

    public static String saveWait2()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }

    public static String personResponsibleTab()
    {
        return "//div[@id='control_C2B7C6FA-10FC-4593-BD27-6869D1790758']";
    }
     public static String failed() {
        return "(//div[@id='txtAlert'])[2]";
    }

    public static String saveWait()
    {
        return "//div[@class='ui inverted dimmer active']";
    }

    public static String Management_Maintenance_Sttings()
    {
        return "//label[contains(text(),'Management Maintenance')]/..";
    }

    public static String Management_Maintenance_Heading()
    {
        return "//h1[text()='Management Maintenance']";
    }

    public static String Social_Sustainability()
    {
        return "//label[contains(text(),'Social Sustainability')]";
    }

    public static String Incident_User_Matrix_Tab()
    {
        return "//label[text()='Incident User Matrix']";
    }

    public static String Incident_User_Matrix_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_840E88DF-336D-4583-ABBB-8AF7A39AACFE']";
    }

    public static String incident_occured_section_drop_down()
    {
        return "//div[@id='control_7BD27C76-484A-48FA-8AC1-92E97F9EAB13']//span//ul//li";
    }

    public static String incident_occured_section_drop_down_option_1()
    {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'Global Company')]/..//i)[1]";
    }

    public static String incident_occured_section_drop_down_option_2()
    {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'South Africa')]/..//i)[1]";
    }

    public static String incident_occured_section_drop_down_option_3()
    {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'Victory Site')])";
    }

    public static String Incident_Owner()
    {
        return "//div[@id='control_2E3D9960-0A69-433A-9236-35817FE9BAFF']//ul//li";
    }

    public static String Incident_Owner_Drop_Down_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String Lead_Investigator()
    {
        return "//div[@id='control_915DE2D5-40AA-49EA-BC18-2844AD68ED8F']//ul//li";
    }

    public static String Lead_Investigator_Drop_Down_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String saveBtn()
    {
        return "(//div[text()='Save'])[2]";
    }
}
