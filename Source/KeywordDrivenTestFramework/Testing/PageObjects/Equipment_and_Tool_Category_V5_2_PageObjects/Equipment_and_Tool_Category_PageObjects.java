/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Equipment_and_Tool_Category_V5_2_PageObjects;

import KeywordDrivenTestFramework.Testing.TestMarshall;

/**
 *
 * @author smabe
 */
public class Equipment_and_Tool_Category_PageObjects {

    public static String AssestDetails() {
        return "(//div[@id='control_7D378A8E-4A25-4196-9AFA-FA2B773B7148']//div//table)[3]";
    }

    public static String RelatedIncidents_Panel() {
        return "//div[text()='Related Incidents']";
    }

    public static String PermittedPersonnel_Panel() {
        return "//div[text()='Permitted Personnel']";
    }

    public static String PermittedPersonnel_Record(String data) {
        return "(//div[@id='control_F69F9124-C25C-458D-9B8A-0BEE04AD7484']//div//table)[3]//div[text()='" + data + "']";
    }

    public static String RelatedIncidents_Record(String data) {
        return "(//div[@id='control_0A6F8C24-1E94-4570-BBD7-6E42E95AC9FF']//div//table)[3]//div[text()='" + data + "']";
    }

    public static String RelatedIncidentsRisksInspectionsHygieneMonitoring_Tab() {
        return "//div[text()='Related Incidents, Risks, Inspections & Hygiene Monitoring']";
    }

    public static String RequiredPermits_Tab() {
        return "//div[text()='Required Permits']";
    }

    public static String SaveToContinue_Btn() {
        return "//div[@id='control_42624A32-59AF-47CB-B31E-535EF2EA9C25']";
    }

    public static String Search() {
        return "//div[@id='control_0A6F8C24-1E94-4570-BBD7-6E42E95AC9FF']//div[@id='act_filter_right']//div[text()='Search']";
    }

    public static String closeButton() {
        return "(//div[@id='form_AB4CF802-14F5-467B-8B87-9D8F7C4A11D0']//i[@class='close icon cross'])[1]";
    }

    public static String navigate_EHS() {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }

    public static String SaveButton() {
        return "//div[@id='btnSave_form_FB5D671F-0F91-4CC2-96B4-A1DA0C94297D']";
    }

    public static String SaveSupportingDocuments_SaveBtn() {
        return "//div[text()='Save Supporting Documents']";
    }

    public static String pin() {
        return "//div[@id='control_0A6F8C24-1E94-4570-BBD7-6E42E95AC9FF']//div[@class='search-options-header']//span[@title='Pin filter']";
    }

    public static String processFlow_PersonalOperatingPermits() {
        return "//div[@id='btnProcessFlow_form_D41EAC51-B764-4D09-99FF-D4A62C402D3E']";
    }

    public static String visible_search_text_box() {
        return "(//div[contains(@class,'transition visible')]//input[@placeholder='Type to search'])[3]";
    }

    public static String Navigate_Plan_Maintenance() {

        return "//label[text()='Plan Maintenance']/..";
    }

    public static String Equipment_Tools_Tab() {
        return "//label[text()='Equipment & Tools']";
    }

    public static String SE_add() {
        return "//div[text()='Add']";
    }

    public static String process_flow() {
        return "//div[@id='btnProcessFlow_form_C8B51579-B5CC-45AC-86BE-2FC9AD3E2E93']";
    }

    public static String process_Asset() {
        return "//div[@id='btnProcessFlow_form_C122505E-159A-4688-B3E5-B4F45CD9DFDB']";
    }

    public static String process_flow_Equipment() {
        return "//div[@id='btnProcessFlow_form_AB4CF802-14F5-467B-8B87-9D8F7C4A11D0']";
    }

    public static String process_flow_Actions() {
        return "//div[@id='btnProcessFlow_form_4C9F8211-ACB5-42C2-92B1-B8F97DF5C8D6']";
    }

    public static String Business_unit() {
        return "(//a[text()='Global Company']//i)[1]";
    }

    public static String Process_Activity() {
        return "//div[@id='control_B6A1B10A-1982-4D89-9696-45C186483CE1']//b[@original-title='Select all']";
    }

    public static String closeBtn() {
        return "(//div[@id='form_C122505E-159A-4688-B3E5-B4F45CD9DFDB']//i[@class='close icon cross'])[1]";
    }

    public static String closeBtn_Actions() {
        return "(//div[@id='form_4C9F8211-ACB5-42C2-92B1-B8F97DF5C8D6']//i[@class='close icon cross'])[1]";
    }

    public static String Impact_type_drop_down_option(String data) {
        return "//a[text()='" + data + "']";
    }

    public static String Impact_type_drop_down() {
        return "//div[@id='control_369071B7-155A-46FA-817E-A87500F6E94A']//ul";
    }

    public static String SaveBtn() {
        return "//div[@id='control_42624A32-59AF-47CB-B31E-535EF2EA9C25']";
    }

    public static String Save_Button_Equipment() {
        return "//div[@id='btnSave_form_C122505E-159A-4688-B3E5-B4F45CD9DFDB']";
    }

    public static String Save_Button_Actions() {
        return "//div[@id='btnSave_form_4C9F8211-ACB5-42C2-92B1-B8F97DF5C8D6']";
    }

    public static String Save_Button() {
        return "//div[@id='btnSave_form_AB4CF802-14F5-467B-8B87-9D8F7C4A11D0']";
    }

    public static String Category_drop_down() {

        return "//div[@id='control_7693EC63-3C78-43C0-A571-F95BCF825CC6']//ul";
    }

    public static String Category_drop_down_option(String data) {
        return "//a[text()='" + data + "']";
    }

    public static String Asset_Details_Add() {
        return "//div[@id='btnAddNew']";
    }

    public static String Asset() {
        return "//div[@id='control_E3B0C574-0A42-4ABA-A575-F17186861205']//input";
    }

    public static String service_required() {
        return "//div[@id='control_A5D4F368-94DB-40B9-9138-CE6F7589882F']//ul";
    }

    public static String service_required_drop_down_option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String date_of_service() {
        return "//div[@id='control_5F3A4F14-7C7E-40A9-B312-23EE0841C485']//input";
    }

    public static String assessment_conducted() {
        return "//div[@id='control_492DCD3E-F963-4B13-ABAE-5FB9AF11E7C8']//ul";
    }

    public static String Service_frequency() {
        return "//div[@id='control_1B3AA5FF-1831-4D31-AAA2-3A253BED03DB']//input";
    }

    public static String next_date_of_service() {
        return "//div[@id='control_DB651ABE-F5CB-4834-BCE7-AC8B33F3ED15']//input";
    }

    public static String Date_check_sheet_completed() {
        return "//div[@id='control_C652C1A0-13A3-4630-8FCD-BEA69EDF8091']//input";
    }

    public static String Next_routine_maintenance() {
        return "//div[@id='control_D0AC409D-7100-4B56-9D83-6F78CF0136BF']//input";
    }

    public static String Date_off_site() {
        return "//div[@id='control_916C8B16-7010-41B4-A816-AEEBEF11CA21']//input";
    }

    public static String PHA_date() {
        return "//div[@id='control_FB880749-1010-4971-A86C-2A2BF152B57F']//input";
    }

    public static String Last_routine_maintenance() {
        return "//div[@id='control_6A625A7F-26A6-4490-AB4A-1828DE252D5A']//input";
    }

    public static String Date_on_site() {
        return "//div[@id='control_282D024A-BF38-4726-9548-2E6F7660266E']//input";
    }

    public static String Make() {
        return "//div[@id='control_9463035D-BB16-4E11-875A-E229F4E9D1A8']//input";
    }

    public static String Model() {
        return "//div[@id='control_4AEB9EB2-B321-4581-A6AD-E4ECD319DC8E']//input";
    }

    public static String Type() {
        return "//div[@id='control_0802DF4C-3620-449E-A811-0CDB979F5251']//input";
    }

    public static String VIN() {
        return "//div[@id='control_86542DB2-DE30-4E3B-A12F-2FA36946680C']//input";
    }

    public static String Reg() {
        return "//div[@id='control_237743FF-B94B-47EB-A061-9037D2E1F3E4']//input";
    }

    public static String License() {
        return "//div[@id='control_229951D4-AFF2-495A-AC88-6E852A8662DC']//input";
    }

    public static String Related_risk_assessment() {
        return "//div[@id='control_BDC1B6FE-CC1E-4A0B-A9A5-1C0BED3E2E25']//ul";
    }

    public static String Related_risk_assessment_drop_down_option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String Required_training() {
        return "//div[@id='control_C8B4AC16-21E6-4093-B09C-19DDA5F619EB']//ul";
    }

    public static String Required_training_drop_down_option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String Required_permit_drop_down_option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String Required_permit() {
        return "//div[@id='control_7EBDAC3C-F73A-47EB-B6EB-46F0A2ADCB25']//ul";
    }

    public static String Required_permit_drop_down_option_1(String data) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']//i[1]";
    }

    public static String Owner() {
        return "//div[@id='control_02B1553D-9DF7-4282-B9EB-C7280F92890B']//ul";
    }

    public static String Owner_drop_down_option(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')])[1]";
    }

    public static String Required_permit_drop_down_select_all() {
        return "//div[@id='control_7EBDAC3C-F73A-47EB-B6EB-46F0A2ADCB25']//b[@original-title='Select all']";
    }

    public static String Required_training_drop_down_select_all() {
        return "//div[@id='control_C8B4AC16-21E6-4093-B09C-19DDA5F619EB']//b[@original-title='Select all']";
    }

    public static String validateSave() {

        return "//div[@class='ui floating icon message transition visible']";
    }

    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }

    public static String supporting_tab() {
        return "//div[text()='Supporting Documents']";
    }

    public static String linkbox() {
        return "//b[@original-title='Link to a document']";
    }

    public static String LinkURL() {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle() {
        return "//div[@class='confirm-popup popup']//input[@id='urlTitle']";
    }

    public static String urlAddButton() {
        return "//div[@class='confirm-popup popup']//div[contains(text(),'Add')]";
    }

    public static String iframeXpath() {
        return "//iframe[@id='ifrMain']";
    }

    public static String owner_drop_down_option(String data) {

        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')])[1]";
    }

    public static String Service_History_Tab() {
        return "//div[text()='Service history']";
    }

    public static String Actions_Tab() {
        return "(//div[text()='Actions'])[1]";
    }

    public static String Trained_Personnel_Tab() {
        return "(//div[text()='Required Training'])[1]";

    }

    public static String Asset_Schedule_Add() {
        return "//div[@id='control_8C748B4A-4E17-400A-9D80-DDEB6C0D47DB']//div[@id='btnAddNew']";
    }

    public static String Schedule_reference() {
        return "//div[@id='control_CE3864E1-7D76-49C1-A925-B20002B392CC']//input";
    }

    public static String Schedule_Start_date() {
        return "//div[@id='control_E4B8A13C-AACB-4457-81B7-56FF33D8CB54']//input";
    }

    public static String Schedule_End_date() {
        return "//div[@id='control_D9DD1946-105A-4994-8B40-5A65A871CF31']//input";
    }

    public static String Availability_drop_down() {
        return "//div[@id='control_F8034ADE-4256-4D91-BC0E-4009AFB7E54B']//ul";
    }

    public static String Schedule_reference_name() {
        return "//div[@id='grid']//a[text()='Schedule reference']/../../../../../../..//div[@class='k-grid-content k-auto-scrollable']//tbody//td[5]//div";
    }

    public static String Actions_Add() {
        return "//div[@id='control_145205E2-CE4C-43A8-B039-7B1A53630E2A']//div[@id='btnAddNew']";
    }

    public static String Actions_description() {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }

    public static String Department_responsible_drop_down() {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//ul";
    }

    public static String Global_Company() {
        return "(//li[@title='Global Company']//a)[1]";
    }

    public static String Responsible_person_drop_down() {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//ul";
    }

    public static String Responsible_person_drop_down_option(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')])[2]";
    }

    public static String Action_due_date() {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }

    public static String Action_description_validation() {
        return "(//div[@id='control_145205E2-CE4C-43A8-B039-7B1A53630E2A']//div[@class='k-grid-content k-auto-scrollable']//tbody//td[7]//div)[1]";
    }

}
