/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Planned_Task_Observations_V5_2_PageObjetcs;

import KeywordDrivenTestFramework.Testing.PageObjects.Permit_To_Work_V5_2_PageObjects.*;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.*;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;

/**
 *
 * @author skhumalo
 */
public class Planned_Task_Observations_PageObjects
{

    public static String BusinessUnitHeader()
    {
        return "//div[text()='Business unit']";
    }

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String viewFullReportsIcon()
    {
        return "//span[@title='Full report ']";
    }

    public static String Concequences_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String continueBtn()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String viewReportsIcon()
    {
        return "//span[@title='View report ']";
    }

    public static String reportsBtn()
    {
        return "//div[@id='btnReports']";
    }

    public static String searchBtn()
    {
        return "//div[@id='searchOptions']//div[text()='Search']";
    }

    public static String Consequence_Block_Plus_Button()
    {
        return "//div[@title='Consequence']/../..//a[@class='item add bRight']";
    }

    public static String Status_Dropdown()
    {
        return "//tr[@module_definition_name='ddl4Status']//span[@role='presentation']";
    }

    public static String LinkedJSA_Panel()
    {
        return "//span[text()='2.5 Linked JSA']";
    }

    public static String RecordNo_Field()
    {
        return "//tr[@module_definition_name='record_number']//input[@class='txt border']";
    }

    public static String Consequence_Input()
    {
        return "(//div[@title='Consequence']/..//..//div[text()='...'])[1]";
    }

    public static String JSA_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_EBF4B94D-9E08-49BD-9899-CD64DC78AE1D']";
    }

    public static String ClickLinkedJSARecord()
    {
        return "//div[@id='control_2B9EEC9E-738B-4E34-87AA-180A4BC84903']//tbody[@role='rowgroup']//tr[1]";
    }

    public static String Consequence_Input1()
    {
        return "(//div[@title='Consequence']/..//..//div[text()=' '])[1]";
    }

    public static String Consequences_Dropdown()
    {
        return "//div[@class='block cControl jtk-endpoint-anchor jtk-connected']//div[text()='Please select']";
    }

    public static String IncidentManagement_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_B6196CB4-4610-463D-9D54-7B18E614025F']";
    }

    public static String LeftRisk_AddButton()
    {
        return "//div[@title='Risk']/../..//a[@class='item add bLeft']";
    }

    public static String RightRisk_AddButton()
    {
        return "//div[@title='Risk']/../..//a[@class='item add bRight']";
    }

    public static String Risk_Block()
    {
        return "//div[@title='Risk']";
    }

    public static String loadingData()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Loading data...']";
    }

    public static String AddNew_Button()
    {
        return "//div[@id='control_8CB8326E-D502-43AE-97BC-424D6D869C5C']//div//div";
    }

    public static String AdditionalInformation()
    {
        return "//div[@id='control_BC6C5FE8-5348-4CC0-942E-85FA43E70F99']//div[@class='c-chk']";
    }

    public static String BowtieAssessmentFrom_Dropdown()
    {
        return "//div[@id='control_4835153C-D8B0-4056-BA6E-146F58B04AA6']";
    }

    public static String BowtieAssessmentFrom_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String BowtieCauses_AddButton()
    {
        return "//div[@id='control_A2CFC545-8B8A-4063-A5F7-21A7B295D1C9']//div[text()='Add']";
    }

    public static String BowtieCauses_BowtiePreventativeControls_AddButton()
    {
        return "//div[@id='control_8AB00884-981A-40FB-A6C4-A4F2D21DF6F8']//div[text()='Add']";
    }

    public static String BowtieCauses_BowtieCorrectiveControls_AddButton()
    {
        return "//div[@id='control_D4D30DB4-E22E-43AE-8955-88C43147D350']//div[text()='Add']";
    }

    public static String BowtieCauses_BowtiePreventativeControls_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_11C9372C-44B6-4010-8DCE-1FD8DE5399E5']";
    }

    public static String BowtieCauses_CloseButton()
    {
        return "(//div[@id='form_94AE510C-C2B9-4C21-AE06-AD657949590F']//i[@class='close icon cross'])[1]";
    }

    public static String BowtieCauses_Record(String data)
    {
        return "(//div[@id='control_A2CFC545-8B8A-4063-A5F7-21A7B295D1C9']//div//table)[3]//div[text()='" + data + "']";
    }

    public static String BowtieConsequences_AddButton()
    {
        return "//div[@id='control_B37149EB-3023-4C48-89E7-3ADF2ABB5382']//div[text()='Add']";
    }

    public static String BowtieConsequences_CloseButton()
    {
        return "(//div[@id='form_68BEE33D-C44A-4ACB-9199-10F722E6AEBA']//i[@class='close icon cross'])[1]";
    }

    public static String BowtieConsequences_Panel()
    {
        return "//div[text()='Bowtie Consequences']";
    }

    public static String BowtieConsequences_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_68BEE33D-C44A-4ACB-9199-10F722E6AEBA']";
    }

    public static String BowtieConsequences_Record(String data)
    {
        return "(//div[@id='control_B37149EB-3023-4C48-89E7-3ADF2ABB5382']//div//table)[3]//div[text()='" + data + "']";
    }

    public static String BowtieCorrectiveControls_CloseButton()
    {
        return "(//div[@id='form_5C8D36FE-0A62-4059-8C47-EDBD94E9EA77']//i[@class='close icon cross'])[1]";
    }

    public static String BowtieCorrectiveControls_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_5C8D36FE-0A62-4059-8C47-EDBD94E9EA77']";
    }

    public static String BowtieCorrectiveControls_Record(String data)
    {
        return "(//div[@id='control_D4D30DB4-E22E-43AE-8955-88C43147D350']//div//table)[3]//div[text()='" + data + "']";
    }

    public static String BowtieCorrectiveControls_SaveButton()
    {
        return "//div[@id='btnSave_form_5C8D36FE-0A62-4059-8C47-EDBD94E9EA77']";
    }

    public static String BowtieCorrectiveControls_SaveToContinue_Button()
    {
        return "//div[@id='control_05221A5C-0B3F-4FE2-BD8B-90D5FCF00322']";
    }

    public static String BowtiePreventativeControls_CloseButton()
    {
        return "(//div[@id='form_11C9372C-44B6-4010-8DCE-1FD8DE5399E5']//i[@class='close icon cross'])[1]";
    }

    public static String BowtiePreventativeControls_Panel()
    {
        return "//div[text()='Bowtie Preventative Controls']";
    }

    public static String BowtieCorrectiveControls_Panel()
    {
        return "//div[text()='Bowtie Corrective Controls']";
    }

    public static String BowtiePreventativeControls_Record(String data)
    {
        return "(//div[@id='control_8AB00884-981A-40FB-A6C4-A4F2D21DF6F8']//div//table)[3]//div[text()='" + data + "']";
    }

    public static String BowtiePreventativeControls_SaveButton()
    {
        return "//div[@id='btnSave_form_11C9372C-44B6-4010-8DCE-1FD8DE5399E5']";
    }

    public static String BowtiePreventativeControls_SaveToContinue_Button()
    {
        return "//div[@id='control_05221A5C-0B3F-4FE2-BD8B-90D5FCF00322']";
    }

    public static String BowtieRiskAssessment_Add()
    {
        return "//div[@id='btnActAddNew']//div[text()='Add']";
    }

    public static String BowtieRiskAssessment_BowtieCauses_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_94AE510C-C2B9-4C21-AE06-AD657949590F']";
    }

    public static String BowtieRiskAssessment_Module()
    {
        return "//label[text()='Bowtie Risk Assessment']";
    }

    public static String BowtieRiskAssessment_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_E5D05DD0-7E8F-4A41-9077-0A1F8DB472E7']";
    }

    public static String BowtieRiskDescription()
    {
        return "//div[@id='control_A6CF6BF0-6F8A-4417-9C05-61B1A2795897']//textarea";
    }

    public static String BowtieRiskReference()
    {
        return "(//div[@id='control_DF2197C5-845E-4592-91EC-5212967FDDD4']//input)[1]";
    }

    public static String BusinessUnit_Dropdown()
    {
        return "//div[@id='control_6EE554CD-9C60-4F07-AF10-63C667649A45']";
    }

    public static String BusinessUnit_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String BusinessUnit_Option1(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[1]";
    }

    public static String Cause()
    {
        return "(//div[@id='control_16ADAF90-6D9A-403B-BA0C-F8D41353A9E2']//input)[1]";
    }

    public static String Causes_Panel()
    {
        return "//div[@id='control_271E1CF4-0933-4031-B490-D2E20691348B']";
    }

    public static String Consequences()
    {
        return "(//div[@id='control_5858AB70-A475-47D2-A794-155CFC765C37']//input)[1]";
    }

    public static String Consequences_Panel()
    {
        return "//span[text()='Consequences']";
    }

    public static String Consequences_SaveButton()
    {
        return "//div[@id='btnSave_form_68BEE33D-C44A-4ACB-9199-10F722E6AEBA']";
    }

    public static String Consequences_SaveToContinue_Button()
    {
        return "//div[@id='control_F4649EEC-490F-4C21-B41D-75E275CB2D16']";
    }

    public static String ControlCategory_Dropdown()
    {
        return "//div[@id='control_B5DF4C13-9442-46D9-8FF0-C7E70CF2E7DC']";
    }

    public static String ControlCategory_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String ControlDescription()
    {
        return "(//div[@id='control_6754FD09-D78D-4DE7-A588-FDDFDCCC78F5']//input)[1]";
    }

    public static String ControlImplementation_Dropdown()
    {
        return "//div[@id='control_6A6FD9CA-8F86-4108-B9FF-E64096C8C47C']";
    }

    public static String ControlMatrix_Panel()
    {
        return "//span[text()='Control Matrix']";
    }

    public static String ControlOwner_Dropdown()
    {
        return "//div[@id='control_7D9D2EE5-75F6-4F44-8896-C03AFF83F7F7']";
    }

    public static String ControlQuality_Dropdown()
    {
        return "//div[@id='control_EE4E7BE4-1899-40AD-B337-94621B1ABEDF']";
    }

    public static String ControlReference()
    {
        return "//div[@id='control_8432E1DB-25DB-4BC8-812B-8547EFD7B6B7']//input";
    }

    public static String ControlsChecklist_Panel()
    {
        return "//div[text()='Control Checklist']";
    }

    public static String Findings_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_0EC338C6-654B-491D-BD66-9C326CADCB73']";
    }

    public static String ImpactDescription()
    {
        return "//div[@id='control_3D3E68DC-D7A2-429A-8F4C-9E0F537D5768']//textarea";
    }

    public static String ImpactType_Dropdown()
    {
        return "//div[@id='control_F065857D-D6E8-46E9-8C2C-63A9D585AC90']";
    }

    public static String ImpactType_Option(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[text()='" + data + "']//i[@class='jstree-icon jstree-checkbox'])[2]";
    }

    public static String ImpactType_SelectAll()
    {
        return "//div[@id='control_F065857D-D6E8-46E9-8C2C-63A9D585AC90']//b[@class='select3-all']";
    }

    public static String Impact_Dropdown()
    {
        return "(//div[@id='control_002897B8-C514-43A0-8A4D-AB34EBE5AC2F']//div)[1]";
    }

    public static String Impact_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String LikelihoodRating_Dropdown()
    {
        return "(//div[@id='control_2EBB8D75-E591-409C-B0E0-719BC842BA8E']//div)[1]";
    }

    public static String PreviousBowtieRisk_Dropdown()
    {
        return "//div[@id='control_682882C1-711D-4069-B623-41C360C3D106']";
    }

    public static String PreviousBowtieRisk_Option(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[text()='" + data + "'])[1]";
    }

    public static String RelatedFindings_OpenedRecordxpath()
    {
        return "(//div[@id='form_D25EA3B6-2F41-4F02-8356-D079FB56E1D2']//div[@class='record'])[1]";
    }

    public static String RelatedIncidents_OpenedRecordxpath()
    {
        return "(//div[@id='form_B6196CB4-4610-463D-9D54-7B18E614025F']//div[@class='record'])[1]";
    }

    public static String RelatedFindings_Openxpath()
    {
        return "((//div[@id='control_594E000D-4925-4518-928E-0FB163B28F3B']//div//table)[3]//tr)[1]";
    }

    public static String RelatedIncidents_Openxpath()
    {
        return "((//div[@id='control_2C80A486-F170-4B3F-8C6E-A61FCAC6DA5D']//div//table)[3]//tr)[1]";
    }

    public static String RelatedFindings_Panel()
    {
        return "//span[text()='Related Findings']";
    }

    public static String RelatedIncidents_Panel()
    {
        return "//span[text()='Related Incidents']";
    }

    public static String RiskAssessment_Panel()
    {
        return "//span[text()='Risk Assessment']";
    }

    public static String RiskImpact_AddButton()
    {
        return "//div[text()='Risk Impact']//..//div[text()='Add']";
    }

    public static String RiskImpact_CloseButton()
    {
        return "(//div[@id='form_D6153DE5-6D15-4368-85E8-EDAFC76E2B18']//i[@class='close icon cross'])[1]";
    }

    public static String RiskImpact_Dropdown()
    {
        return "//div[@id='control_ECDA14DC-13FD-4511-B373-23EEB01C7F01']";
    }

    public static String RiskImpact_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String RiskImpact_Panel()
    {
        return "//div[text()='Risk Impact']";
    }

    public static String RiskImpact_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_D6153DE5-6D15-4368-85E8-EDAFC76E2B18']";
    }

    public static String RiskImpact_Record(String data)
    {
        return "(//div[@id='control_9C30C831-F4CA-4423-B4A3-5A8A69011966']//div//table)[3]//div[text()='" + data + "']";
    }

    public static String RiskImpact_SaveButton()
    {
        return "//div[@id='btnSave_form_D6153DE5-6D15-4368-85E8-EDAFC76E2B18']";
    }

    public static String RiskManagement_Module()
    {
        return "//label[text()='Risk Management']";
    }

    public static String RiskSource_Dropwown()
    {
        return "//div[@id='control_93098483-FC5B-46E1-91DF-CAFA0061578D']";
    }

    public static String RiskSource_SelectAll()
    {
        return "//div[@id='control_93098483-FC5B-46E1-91DF-CAFA0061578D']//b[@class='select3-all']";
    }

    public static String Risk_Dropdown()
    {
        return "//div[@id='control_B77E0F46-408A-46DA-8C60-F963B8AA9E7C']";
    }

    public static String Risk_Option(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[text()='" + data + "'])[1]";
    }

    public static String SaveButton()
    {
        return "//div[@id='btnSave_form_C7B1B364-F7F1-4888-940A-AFD5A5345AAA']";
    }

    public static String BowtieCause_SaveButton()
    {
        return "//div[@id='btnSave_form_94AE510C-C2B9-4C21-AE06-AD657949590F']";
    }

    public static String BowtieCause_SaveToContinue_Button()
    {
        return "//div[@id='control_F1E2583E-95B3-42DA-A6F1-1C1B60C981DE']";
    }

    public static String SaveToContinue_Button()
    {
        return "//div[@id='control_D31A355D-92EE-4131-B449-8B1CAF27A6FC']";
    }

    public static String TailingsManagement_Module()
    {
        return "//label[text()='Tailings Management']";
    }

    public static String validateSave()
    {
        return "//div[@class='ui floating icon message transition visible']";
    }

    public static String saveWait2()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }

    public static String Risk_Description()
    {
        return "//div[@id='control_A6CF6BF0-6F8A-4417-9C05-61B1A2795897']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String Previous_Bowtie_Risk_Reference_Select(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')])[1]";
    }

    public static String Risk_Resource()
    {
        return "//div[@id='control_93098483-FC5B-46E1-91DF-CAFA0061578D']//b[@class='select3-all']";
    }

    public static String Bowtie_Risk_Reference()
    {
        return "//div[@id='control_DF2197C5-845E-4592-91EC-5212967FDDD4']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String Risk_Library_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_75490176-392D-464D-812B-E3F9E9AC942D']";
    }

    public static String Risk()
    {
        return "//div[@id='control_38779557-7763-43A8-A0EF-A5BC98AAF5E7']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String RiskLibrary_ImpactType_Dropdown()
    {
        return "//div[@id='control_3B7B1EE5-F549-4C34-853A-C8218D87BC29']";
    }

    public static String RiskLibrary_ImpactType_SelectAll()
    {
        return "//div[@id='control_3B7B1EE5-F549-4C34-853A-C8218D87BC29']//b[@class='select3-all']";
    }

    public static String Priority_Unwanted_Event_Dropdown()
    {
        return "//div[@id='control_D33817E8-42C9-4AE4-B2D1-00ADD1A8FBE4']";
    }

    public static String Priority_Unwanted_Event_Select(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[text()='" + data + "'])[1]";
    }

    public static String Related_Critical_Control_Inspetion_Dropdown()
    {
        return "//div[@id='control_48E40813-477B-4D8F-968E-0012A53596BC']";
    }

    public static String Related_Critical_Control_Inspetion_Select(String data)
    {
        return "(//div[contains(@class, 'transition visible')]//a[text()='" + data + "'])[1]";
    }

    public static String RiskLibrary_SaveButton()
    {
        return "//div[@id='btnSave_form_75490176-392D-464D-812B-E3F9E9AC942D']";
    }

    public static String RiskLibrary_SaveToContinue_Button()
    {
        return "//div[@id='control_394A6057-B468-40D6-8EEB-858789770EA1']";
    }

    public static String View_Bowtie()
    {
        return "(//div[@id='control_A11FB77F-6183-4BEE-A3AF-9C1E946082FF']//div)[1]";
    }

    public static String Print()
    {
        return "//div[@title='Print']";
    }

    public static String Print_Button()
    {
        return "//div[@id='btnPrint']";
    }

    public static String Nav_Back()
    {
        return "//div[@id='bowtieNav']//i[@class='back icon arrow-left']";
    }

    public static String Add_Cause_Button()
    {
        return "//div[@title='Cause']/../..//a[@class='item add bLeft']";
    }

    public static String Cause_Wording()
    {
        return "//div[@title='Cause']";
    }

    public static String PleaseSelectControlCategory_Dropdown()
    {
        return "//div[@class='block pControl jtk-endpoint-anchor jtk-connected']//div[@title='Please select']";
    }

    public static String Consequence_Block()
    {
        return "//div[@title='Consequence']";
    }

    public static String viewBowtie_SaveButton()
    {
        return "//div[@id='act_bowtie_right']//div[text()='Save']";
    }

    public static String Control_Description()
    {
        return "//div[text()='...']";
    }

    public static String Save_Button()
    {
        return "//div[@id='btnSave']";
    }

    public static String Control__Description()
    {
        return "//div[@title='Please select']/../..//div[@class='content']";
    }

    public static String Delete_Cause_Button()
    {
        return "//div[@title='Cause']/../..//a[@class='item delete']";
    }

    public static String Yes_Button()
    {
        return "//div[@class='confirm-popup popup']//div[text()='Yes']";
    }

    public static String Ok_Button()
    {
        return "//div[text()='Record has successfully been deleted']/..//div[@id='btnHideAlert']";
    }

    public static String Consequence_Wording()
    {
        return "//div[@title='Consequence']";
    }

    public static String Delete_Consequence_Button()
    {
        return "//div[@title='Consequence']/../..//a[@class='item delete']";
    }

    public static String Event_Left_Plus_Button()
    {
        return "//div[@title='Risk']/../..//a[@class='item add bLeft']";
    }

    public static String Cause_Description()
    {
        return "(//div[@title='Cause']/../..//div[@class='content'])[2]";
    }

    public static String Event_Block()
    {
        return "//div[@title='Risk']";
    }

    public static String Cause__Description()
    {
        return "//div[@title='Please select']/../..//div[@class='content']";
    }

    public static String Flip_Button()
    {
        return "//div[@id='btnFlip']";
    }

    public static String Event_Right_Plus_Button()
    {
        return "//div[@title='Risk']/../..//a[@class='item add bRight']";
    }

    public static String Consequence_Description()
    {
        return "(//div[@title='Consequence']/../..//div[@class='content'])[2]";
    }

    public static String Print_Frame()
    {
        return "//html[@class='']//print-preview-app";
    }

    public static String si_location()
    {
        return "Print.PNG";
    }

    public static String printPreview()
    {
        return "//print-preview-app";
    }

    public static String iframeXpath_1()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String Permit_To_Work_Module()
    {
        return "//label[text()='Permit to Work']";
    }

    public static String PermitToWork_Add()
    {
        return "//div[@id='btnActAddNew']//div[text()='Add']";
    }

    public static String PermitToWork_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_41B8AE76-F4F5-4CA6-97C2-8DCB7A749951']";
    }

    public static String Specific_Locaion()
    {
        return "//div[@id='control_5AFE0A15-8630-4563-B20D-DD5B14A9C082']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String ProjectLink_Checkbox()
    {
        return "//div[@id='control_E7B2C75A-9A4C-47F9-AA9D-8D44B05A3E87']";
    }

    public static String Project_Dropdown()
    {
        return "//div[@id='control_E7BCD2D9-1CF8-455E-BF9A-BE49E44B7364']";
    }

    public static String Project_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }

    public static String RelatedStakeholder_Dropdown()
    {
        return "//div[@id='control_CF288CDC-B260-42F4-855F-A703A17910C7']";
    }

    public static String TypeOfPermit_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']/..//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String DescriptionOfWork()
    {
        return "//div[@id='control_C64EF7FA-23D5-4EEF-B1A6-60F61C4F8D54']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String PersonApplying_Dropdown()
    {
        return "//div[@id='control_129CFA8A-5181-4398-81C6-E64FBF08C24C']";
    }

    public static String ValidFromDate()
    {
        return "//div[@id='control_0F9093C5-DC5B-4AA2-BF6F-F1717BDB93AC']//input[@type='text']";
    }

    public static String ValidToDate()
    {
        return "//div[@id='control_326AD8EB-38E1-4EBB-A064-C98B6144CA06']//input[@type='text']";
    }

    public static String ProcessActivity_Dropdown()
    {
        return "//div[@id='control_CADE78B1-6F4A-4CFF-9038-B7DE8A8AB9B3']";
    }

    public static String TypeOfPermit_Dropdown()
    {
        return "(//div[@id='control_6DDDAD68-64B9-4072-A3E7-9A6D5CE44005']//a//span)[2]";
    }

    public static String CloseTypeOfPermit_Dropdown()
    {
        return "//div[@id='control_6DDDAD68-64B9-4072-A3E7-9A6D5CE44005']//b[@class='select3-down drop_click']";
    }

    public static String SubmitPermitToWork_Button()
    {
        return "//div[@id='control_8E1E3771-6044-44C3-BF1B-099629629FD5']";
    }

    public static String supporting_tab()
    {
        return "//div[text()='Supporting Documents']";
    }

    public static String linkbox()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlTitle']";
    }

    public static String urlAddButton()
    {
        return "//div[@class='confirm-popup popup']//div[contains(text(),'Add')]";
    }

    public static String PermitApproval_Dropdown()
    {
        return "//div[@id='control_3A8F7E23-A67D-4D46-8E14-9356BE56BC71']";
    }

    public static String PermitRejectionComments()
    {
        return "//div[@id='control_9FDFE43C-E395-47A1-8EBD-DBCD422061AD']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String CancelPermitRequestButton()
    {
        return "//div[@id='control_82530D83-3FFC-4723-9AC2-2EE6BDF8B69E']";
    }

    public static String ClickPermitToWorkRecord()
    {
        return "//tbody[@role='rowgroup']//tr[1]";
    }

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String PreparationApproval_Dropdown()
    {
        return "//div[@id='control_9508850C-7714-4D2F-81E1-D62EA110EAB1']";
    }

    public static String PermitPlanning_Tab()
    {
        return "//li[@id='tab_B97BE9F5-015A-4414-A661-321880FAC8EA']";
    }

    public static String PreparationRejectionComments()
    {
        return "//div[@id='control_D2B39BDC-4CEA-4C59-B1CD-90DB04175EF8']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String CancelPermit_Button()
    {
        return "//div[@id='control_1BD39A0C-488E-497C-9606-68054E4253C9']";
    }

    public static String Execution_Tab()
    {
        return "//div[text()='3.Execution']";
    }

    public static String Execution_Tab_Add_Button()
    {
        return "//div[text()='Permit Acceptance']/..//div[@id='btnAddNew']";
    }

    public static String editable_grid()
    {
        return "//div[text()='Permit Acceptance']/../..//div[@class='k-grid-content k-auto-scrollable']";
    }

    public static String Stakeholder_Type()
    {
        return "//div[@id='control_F2C9C80B-A3F4-4D72-A772-D355E70164EB']//li";
    }

    public static String Stakeholder_Type(String data)
    {
        return "//a[text()='" + data + "']";
    }

    public static String Name_and_Surname()
    {
        return "//div[@id='control_A2A661F1-2293-4B0A-A0A1-6E56DBCAE5D3']//li";
    }

    public static String Name_and_Surname(String data)
    {
        return "(//a[text()='Aaron Finch 3rd (Finchie)'])[2]";
    }

    public static String Accept()
    {
        return "//div[@id='control_87DF8EC9-EBFA-49BC-BD16-48EC365A893C']";
    }

    public static String Declare()
    {
        return "//div[@id='control_337D1AEE-DB26-4265-8596-92256FB20889']";
    }

    public static String Execution_approval()
    {
        return "//div[@id='control_B426B7AC-A685-4DB2-B677-FAB8B8BECBDF']//li";
    }

    public static String Execution_approval_option(String data)
    {
        return "(//a[text()='" + data + "'])[3]";
    }

    public static String Submit_Completion()
    {
        return "//div[@id='control_47ED747F-8FE1-4F45-B3D6-77D099C41CBA']//div";
    }

    public static String Submit_Completion_2()
    {
        return "(//div[@id='control_452FACEF-F22A-4018-9222-068116D0D858']//div)[2]";
    }

    public static String Execution_Rejection_Comments()
    {
        return "//div[@id='control_10DD7E93-1553-42FC-A3B7-E1A00E639119']//textarea";
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static String EHS_Module()
    {
        return "//label[text()='Environment, Health & Safety']";
    }

    public static String Observations_Module()
    {
        return "(//label[text()='Observations'])[1]";
    }

    public static String Planned_Task_Observations_Module()
    {
        return "//label[text()='Planned Task Observations']";
    }

    public static String PlannedTaskObservations_Add()
    {
        return "//div[@id='btnActAddNew']//div[text()='Add']";
    }

    public static String PlannedTaskObservations_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_C7B1B364-F7F1-4888-940A-AFD5A5345AAA']";
    }

    public static String PlannedTaskObservations_Dropdown()
    {
        return "//div[@id='control_50D790F7-9104-4A86-8A7E-E357594B57A9']";
    }

    public static String TaskToBeObserved_Dropdown()
    {
        return "//div[@id='control_C196B403-AC69-45E6-BDBE-16E2A5EC7800']";
    }

    public static String TaskDescription()
    {
        return "//div[@id='control_30EB4091-B5E0-4FE5-9E11-EAA7E1877006']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String PersonExecutingTask_Dropdown()
    {
        return "//div[@id='control_E561E665-576B-49A5-BD61-64D3C72787D2']";
    }
    
    public static String Date()
    {
        return "//div[@id='control_158D10EC-6E4D-4434-BC0C-59BFFE19208F']//input[@type='text']";
    }

    public static String NotificationInAdvance_Dropdown()
    {
        return "//div[@id='control_6A9161E7-3B04-41AE-BCD9-8817AADD9330']";
    }

    public static String PeriodInJob_Dropdown()
    {
        return "//div[@id='control_BD19D13A-3F38-45DC-8588-59DF585BECEE']";
    }

    public static String RelevantProcedure_Dropdown()
    {
        return "//div[@id='control_C503D9C3-21C3-4446-AAD8-68DED2235639']";
    }

    public static String ReasonForObservation_Dropdown()
    {
        return "//div[@id='control_A20DC9FE-36CB-435B-B8BF-DDF7025E5060']";
    }
    public static String navigate_EHS()
    {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }

    public static String Observations()
    {
        return "//label[text()='Observations']";
    }
    
    public static String Planned_Task_Observations()
    {
        return "//label[text()='Planned Task Observations']";
    }

    public static String Observations_1()
    {
        return "(//label[text()='Observations'])[2]";
    }

    public static String Observations_Label()
    {
        return "//div[text()='Observations']";

    }

    public static String SafetyRules_Dropdown()
    {
        return "//div[@id='control_83E71565-83E7-4F91-985D-2ABB702FC80A']";
    }

    public static String StandardProcedures_Dropdown()
    {
        return "//div[@id='control_4EEE44C0-1A60-4814-9C0F-4441D92C9B72']";
    }

    public static String ProtectiveClothing_Dropdown()
    {
        return "//div[@id='control_EF6B67AC-0205-4C5A-9D9B-8A785AEF5994']";
    }

    public static String PhysicallyFit_Dropdown()
    {
        return "//div[@id='control_02DF458E-AA31-46AE-94D2-4D012EE2B618']";
    }

    public static String EnvironmentalConditions_Dropdown()
    {
        return "//div[@id='control_72481C32-13B7-4504-9435-8C941CF554F7']";
    }

    public static String ExpectedQuality_Dropdown()
    {
        return "//div[@id='control_D59ED995-5662-44E6-83DE-7ECE4BB8069D']";
    }

    public static String ResultsAchieved_Dropdown()
    {
        return "//div[@id='control_8A97D6F5-CBA8-411B-A0FA-CC871E356D96']";
    }

    public static String ActsObserved_SelectAll()
    {
        return "//div[@id='control_2534B4D2-4046-4B9B-AF16-932FE8914ECF']//b[@class='select3-all']";
    }

    public static String Observation_tab()
    {
        return "//div[text()='Observation']";
    }

    public static String FindingsRecommendationsActiond_Panel()
    {
        return "//span[text()='Findings/Recommendations/Actions']";
    }

    public static String Findings_Add()
    {
        return "//div[@id='control_DB3F6816-145E-467B-AF80-DC0EF15FD8EA']//div[text()='Add']";
    }

    public static String Description_TextFieldxpath() {
        return "//div[@id='control_40ECC722-B08B-48F3-9906-3CFCE527C5CD']//textarea";
    }

    public static String FindingOwner_Selectxpath() {
        return "//div[@id='control_434D9128-5EBC-4E25-9836-72A2C4451733']//li";
    }

    public static String RiskSource_Selectxpath() {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']//li";
    }

    public static String RiskSource_TickAllxpath() {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']//b[@class='select3-all']";
    }

    public static String Classification_Selectxpath() {
        return "//div[@id='control_7689E71C-DC7E-41C1-9124-C6312596E956']//li";
    }

    public static String Risk_Selectxpath() {
        return "//div[@id='control_1D7F3DE4-F9C4-4626-80C9-0E600A0A8FF8']//li";
    }

    public static String Findings_Save_Selectxpath() {
        return "//div[@id='btnSave_form_0EC338C6-654B-491D-BD66-9C326CADCB73']//div[@title='Save']";
    }

    public static String saveWait() {
        return "//div[@class='ui inverted dimmer active']";
    }

    public static String Findings_RecordNumber_Selectxpath() {
        return "//div[@id='form_0B2C6ED4-CC58-477E-931E-7949371422D0']//div[contains(text(),'- Record #')]";
    }

    public static String PTO_CheckBox()
    {
        return "//div[@id='control_16A065F6-9F89-4F54-8F06-7CAC19D23D89']";
    }

    public static String PlannedTaskObservations_Module()
    {
        return "//label[text()='Planned Task Observations']";
    }

    public static String RecurrenceFrequency_CheckBox()
    {
        return "//div[@id='control_368F387B-E638-4592-A429-7397542FE787']";
    }

    public static String CloseBtn()
    {
        return "//div[@id='form_0EC338C6-654B-491D-BD66-9C326CADCB73']/div[@class='navbar']/i[@class='close icon cross']";
    }

}
