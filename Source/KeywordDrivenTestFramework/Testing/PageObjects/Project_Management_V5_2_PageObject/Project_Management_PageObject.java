/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_V5_2_PageObject;

/**
 *
 * @author SKhumalo
 */
public class Project_Management_PageObject {

    public static String Actions_pf() {
        return "//div[@id='btnProcessFlow_form_F54AE783-1378-4974-A7E8-4B5D1C4A79F3']";
    }
    public static String BudgetExtension_Checkbox() {
        return "//div[@id='control_1D35A9A4-3615-4BE5-B4F9-1238EC675264']//div//div";
    }
    public static String Expenditure(int i) {
        return "(//div[@id='control_EB59C8C5-1747-4893-9EF3-37AD3077A53D']//input)[" + i + "]";
    }
    public static String PA_Quarter_Dropdown(int y) {
        return "(//div[@id='control_09A9662E-8EDC-4BEA-87C3-0C62227BCFF5'])[" + y + "]";
    }
    public static String PA_Year_Dropdown(int y) {
        return "(//div[@id='control_8B94E076-D52E-415B-BC9F-AE2A6E65EA76'])[" + y + "]";
    }
    public static String PercentageOfTotalBudget() {
        return "(//div[@id='control_39298B05-8ABA-4918-83CD-572F5B96390F']//input)[1]";
    }
    public static String ProjectActions_CloseButton() {
        return "(//div[@id='form_F54AE783-1378-4974-A7E8-4B5D1C4A79F3']//i[@class='close icon cross'])[1]";
    }
    public static String ProjectActions_Record(String data) {
        return "((//div[@id='control_4F298569-07E5-4373-83F4-300C9C350ACF']//div//table)[3]//div[text()='" + data + "'])[1]";
    }
    public static String ProjectActuals_Panel() {
        return "//span[text()='Project Actuals']";
    }
    public static String ForecastedBudget_Panel(){
        return "//span[text()='Forecasted Budget']";
    }
    public static String Quarter_Dropdown() {
        return "//div[@id='control_E0BA8E50-4E2F-4EFB-A7B8-278823A61B83']";
    }
    public static String RemainingBudget() {
        return "(//div[@id='control_6DDE36C9-5245-4A3F-A867-824B4DAF7BEE']//input)[1]";
    }
    public static String Supporting_Doocuments_tab() {
        return "//div[text()='Supporting Documents']";
    }
    public static String Theme_Dropdown() {
        return "//div[@id='control_D18498F9-9022-47BF-A73C-BF3E30A58FBA']";
    }

    public static String Theme_Option(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'Environment')])[2]";
    }
    public static String TotalBudget() {
        return "(//div[@id='control_A56EC1D2-826D-4F38-AE2E-8EEA7D8B72F4']//input)[1]";
    }
    public static String TotalBudgetWithExtension() {
        return "(//div[@id='control_A43D2E22-8796-41E7-A189-280A5AAB4551']//input)[1]";
    }
    public static String TotalExtension() {
        return "(//div[@id='control_B3EB88DA-F550-4AF0-AE8D-0167244448C8']//input)[1]";
    }
    public static String TotalExtension_Label() {
        return "//div[text()='Total extension']";
    }
    public static String Total_Of_Budget() {
        return "//div[@id='control_D5AE2A39-9DF9-4BB3-95EA-8FC3E718A312']//input";
    }
    public static String Year_Dropdown() {
        return "//div[@id='control_150EC693-165C-4E73-BEBB-504FBD502D82']";
    }
    public static String link_buttonxpath() {
        return "//b[@class='linkbox-link']";
    }
    public static String LinkURL() {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }
    public static String urlTitle() {
        return "//div[@class='confirm-popup popup']//input[@id='urlTitle']";
    }
    public static String urlAddButton() {
        return "//div[@class='confirm-popup popup']//div[contains(text(),'Add')]";
    }
    public static String iframeXpath() {
        return "//iframe[@id='ifrMain']";
    }
    public static String SaveSupportingDocuments_Button() {
        return "//div[text()='Save supporting documents']";
    }
    public static String Budget_tab() {
        return "//div[text()='Budget']";
    }
    public static String StrategicObjectives_tab() {
        return "//div[text()='Strategic Objectives']";
    }
    public static String LinkedEngagements_tab() {
        return "//div[text()='Linked Engagements']";
    }
    public static String ProjectActions_tab() {
        return "//div[text()='Project Actions']";
    }
     public static String Actions_desc(){
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }
    public static String Actions_deptresp_dropdown(){
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//li";
    }
    public static String Actions_deptresp_select(String data){
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String Actions_deptresp_select1(String data){
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[1]";
    }
    public static String Actions_respper_dropdown(){
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//li";
    }
    public static String Actions_respper_select(String data){
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')])[2]";
    }
    public static String Actions_ddate(){
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }
    public static String Actions_save(){
        return "//div[@id='btnSave_form_F54AE783-1378-4974-A7E8-4B5D1C4A79F3']";
    }
    public static String Actions_savewait(){
        return "(//div[text()='Action Feedback'])[1]";
    }    
    public static String DueDiligenceEndDate() {
        return "//div[@id='control_DC137F55-ABC4-4DDD-93A1-0EA9C0070A86']//input";
    }
    public static String DueDiligenceNotes() {
        return "(//div[@id='control_3D5045CD-51F0-4805-93B1-9FAE68008EB5']//textarea)[1]";
    }
    public static String DueDiligenceStartDate() {
        return "//div[@id='control_AD0BB29D-A2C2-45FB-9CFC-1EB133D86E62']//input";
    }
    public static String ObjectivesAndProposedActivities() {
        return "//div[@id='control_6860687C-65AE-45F7-BDDA-8CDEA75D220F']//textarea";
    }
    public static String PlannedCompletionDate() {
        return "//div[@id='control_2696F1DC-0178-43DA-8B70-91507F331D0E']//input";
    }
    public static String PlannedStartDate() {
        return "//div[@id='control_5074F575-966B-494C-B1ED-67E9183DA29B']//input";
    }
    public static String ProjectOriginator_dropdown() {
        return "//div[@id='control_9980D6DE-BCBF-457A-BCAC-A88401FC774F']";
    }
    public static String ProjectStatus_Dropdown() {
        return "//div[@id='control_A6891E66-8FCB-44C6-A96B-5E1B8291B66B']";
    }
    public static String Project_Description() {
        return "(//div[@id='control_A85BCF71-D318-48A6-8BFD-97C66773E72A']//textarea)[1]";
    }
    public static String Project_Input() {
        return "(//div[@id='control_25EB4FB1-8A46-4B5C-9419-D850BAC7D5C0']//input)[1]";
    }
    public static String SaveToContinue_saveBtn() {
        return "//div[@id='control_EE0FBD60-BD61-43F4-8795-CB976E20E55F']//div[text()='Save to continue']";
    }
    public static String SpecifyOriginator() {
        return "(//div[@id='control_15207237-9464-4AC8-B349-9C29A4A3D384']//input)[1]";
    }
    public static String SubThemeType_SelectAll() {
        return "//div[@id='control_0BC3832E-6D79-42DF-9F26-657403886FD3']//b[@class='select3-all']";
    }
    public static String KeyWords_SelectAll() {
        return "//div[@id='control_AAC6688B-16F5-4BBD-B954-0DC276F467EA']//b[@class='select3-all']";
    }
    public static String TotalNumberOfBeneficiaries() {
        return "(//div[@id='control_CAF62539-DE9D-432F-B307-09DC6B7C7EB0']//input)[1]";
    }
    public static String impactType_SelectAll() {
        return "//div[@id='control_9A3C6D10-271D-4BAE-86DE-B2FA885D3F00']//b[@class='select3-all']";
    }
    public static String themeType_SelectAll() {
        return "//div[@id='control_715A0D37-4AC8-4BE7-95B2-71553DB78C68']//b[@class='select3-all']";
    }
    public static String navigate_EHS() {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }
    public static String Add_Button() {
        return "//div[text()='Add']";
    }
    public static String ForecastedBudget_Add_Button() {
        return "//div[@id='control_1BA1AE31-5065-4E58-8BD1-708AF6A42E76']//div[text()='Add']";
    }
    public static String ProjectActuals_Add_Button() {
        return "//div[@id='control_7603A440-AC0A-4EE4-82F2-33FF78507A66']//div[text()='Add']";
    }
    public static String businessUnit_Option1(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String businessUnit_Option(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[1]";
    }
    public static String businessUnit_dropdown() {
        return "//div[@id='control_1520F92C-A849-4812-8D9D-48C3B8A475C1']";
    }
    public static String fullName_Dropdown() {
        return "//div[@id='control_6EE2438D-2B28-4A11-8E8E-B81E57A86B27']";
    }
    public static String impactType_Option(String data){
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']//i[@class='jstree-icon jstree-checkbox']";
    }
    public static String impactType_Dropdown() {
        return "//div[@id='control_9A3C6D10-271D-4BAE-86DE-B2FA885D3F00']";
    }
    public static String Charge_Code() {
        return "//label[text()='Charge Code']";
    }
    public static String LabTestDetails() {
        return "(//div[@id='control_A899D7B2-06C7-437E-BD53-7A8E8CD0F8DA']//input)[1]";
    }
    public static String LabTestDetails_TestType() {
        return "(//div[@id='control_A899D7B2-06C7-437E-BD53-7A8E8CD0F8DA']//input)[3]";
    }
    public static String Laboratory_Panel() {
        return "//span[text()='Laboratory']";
    }
    public static String MaintenanceLab_Add_Button() {
        return "//div[text()='Maintenance - Lab']//..//div[text()='Add']";
    }
    public static String MaintenanceRadiology_ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_456DDB8E-4CF6-4324-A828-0D73B314E53D']";
    }
    public static String MaintenanceResultSet_ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_6346CDCD-C59E-4331-A2D2-D52B1E5F8C5A']";
    }
    public static String MaintenanceResultSet_SaveButton() {
        return "//div[@id='btnSave_form_6346CDCD-C59E-4331-A2D2-D52B1E5F8C5A']//div[text()='Save']";
    }
    public static String MaintenanceTestType_AddButton() {
        return "//div[text()='Maintenance - Test type']//..//div[text()='Add']";
    }
    public static String ProjectActions_AddButton() {
        return "//div[text()='Project Actions']//..//div[text()='Add']";
    }
    public static String MaintenanceResultSet_AddButton() {
        return "//div[text()='Maintenance - Result set']//..//div[text()='Add']";
    }
    public static String MaintenanceLab_CloseButton() {
        return "(//div[@id='form_D0029D75-2491-4D6F-BBFD-E6D9D0D0D475']//i[@class='close icon cross'])[1]";
    }
    public static String MaintenanceResultSet_CloseButton() {
        return "(//div[@id='form_6346CDCD-C59E-4331-A2D2-D52B1E5F8C5A']//i[@class='close icon cross'])[1]";
    }
    public static String MaintenanceTestType_CloseButton() {
        return "(//div[@id='form_CBF09758-4352-418C-B417-14212578DBDB']//i[@class='close icon cross'])[1]";
    }
    public static String MaintenanceRadiology_CloseButton() {
        return "(//div[@id='form_456DDB8E-4CF6-4324-A828-0D73B314E53D']//i[@class='close icon cross'])[1]";
    }   
    public static String MaintenanceLab_ProcessFlow() {
       return "//div[@id='btnProcessFlow_form_D0029D75-2491-4D6F-BBFD-E6D9D0D0D475']";
    }
    public static String MaintenanceTestType_ProcessFlow() {
       return "//div[@id='btnProcessFlow_form_CBF09758-4352-418C-B417-14212578DBDB']";
    }
    public static String MaintenanceLab_Record() {
        return "(//div[@id='control_7D9B22B2-19FC-4C18-8ADF-F0BEBDB897FD']//div//table)[3]";
    }
    public static String MaintenanceResultSet_Record() {
        return "(//div[@id='control_FDA44C35-02E9-4530-B53F-637C6B5C3F51']//div//table)[3]";
    }
    public static String MaintenanceRadiology_Record(String data) {
        return "(//div[@id='control_51764267-4ECB-4223-B334-66C7B2A9B8D6']//div//table)[3]//div[text()='" + data + "']";
    }
    public static String MaintenanceRadiology_Add_Button() {
        return "//div[text()='Maintenance - Radiology']//..//div[text()='Add']";
    }
    public static String Maintenance_TestType() {
        return "//div[text()='Maintenance - Test type']";
    }
    public static String Maintenance_ResultSet() {
        return "//div[text()='Maintenance - Result set']";
    }
    public static String RadiologyDetails() {
        return "(//div[@id='control_958D0095-92F1-45C7-8912-9087DC89CB12']//input)[1]";
    }
    public static String Radiology_Panel() {
        return "//span[text()='Radiology']";
    }
    public static String Occupational_Health() {
        return "//label[text()='Occupational Health']";
    }
    public static String Projects() {
        return "//label[text()='Projects']";
    }
    public static String ProcessFlow_Button() {
        return "//div[@id='btnProcessFlow_form_D1FDADB7-365B-4404-86D2-CA8FC2DE7FE8']";
    }
    public static String ResultSet() {
        return "(//div[@id='control_599EA408-079F-4003-BCF0-2F185C01E96F']//input)[1]";
    }
    public static String Type_Of_Test_Dropdown() {
        return "//div[@id='control_10275A0F-6E1A-4B14-A828-0CCA46FA7F46']";
    }
    public static String ProjectStatus_Option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String saveBtn() {
        return "//div[@id='btnSave_form_D1FDADB7-365B-4404-86D2-CA8FC2DE7FE8']//div[text()='Save']";
    }
    public static String MaintenanceLab_SaveButton(){
        return "//div[@id='btnSave_form_D0029D75-2491-4D6F-BBFD-E6D9D0D0D475']//div[text()='Save']";
    }
    public static String MaintenanceRadiology_SaveButton(){
        return "//div[@id='btnSave_form_456DDB8E-4CF6-4324-A828-0D73B314E53D']//div[text()='Save']";
    }
    public static String MaintenanceTestType_SaveButton(){
        return "//div[@id='btnSave_form_CBF09758-4352-418C-B417-14212578DBDB']//div[text()='Save']";
    }
    public static String validateSave() {
        return "//div[@class='ui floating icon message transition visible']";
    }
    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }
    
}
