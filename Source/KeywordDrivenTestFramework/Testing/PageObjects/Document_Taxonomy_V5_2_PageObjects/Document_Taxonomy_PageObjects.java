/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Document_Taxonomy_V5_2_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author
 */
public class Document_Taxonomy_PageObjects extends BaseClass
{

    public static String Compliance_Management_Tab()
    {
        return "//label[text()='Compliance Management']";
    }

    public static String Compliance_Management_Heading()
    {
        return "//strong[text()='Compliance Management']";
    }

    public static String Document_Management_label()
    {
        return "//label[text()='Document Management']";
    }

    public static String Document_Management_Heading()
    {
        return "//h1[text()='Document Management']";
    }

    public static String Document_Taxonomy_label()
    {
        return "//label[text()='Document Taxonomy']";
    }

    public static String Document_Taxonomy_Heading()
    {
        return "//div[text()='Document Taxonomy']";
    }

    public static String Button_Add()
    {
        return "//div[text()='Add']";
    }

    public static String Document_Taxonomy_Text_Feild()
    {
        return "(//div[@name='txb1Document_taxonomy']//input)[1]";
    }

    public static String Applicable_Business_Unit_Select_All()
    {
        return "(//b[@class='select3-all'])[3]";
    }

    public static String Applicable_Impact_Types_Select_All()
    {
        return "(//b[@class='select3-all'])[4]";
    }

    public static String Link_Text_Feild()
    {
        return "(//input[@type='text'])[21]";
    }

    public static String Taxonomy_Abbreviation_Text_Feild()
    {
        return "(//input[@type='text'])[23]";
    }

    public static String Button_Save()
    {
        return "(//div[text()='Save'])[2]";
    }

    public static String Process_Flow()
    {
        return "//span[@class='img icon flow-tree icon-button']";
    }

    public static String Document_Taxonomy_Level_2_label()
    {
        return "//div[text()='Document Taxonomy Level 2']";
    }

    public static String linkbox()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static String link_box()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String Link_URL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String url_Title()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlTitle']";
    }

    public static String url_AddButton()
    {
        return "//div[@class='confirm-popup popup']//div[contains(text(),'Add')]";
    }

    public static String Save_Button_Drop_Dowm()
    {
        return "(//div[@class='actionButton button primary right'])[5]//div[@class='more options icon chevron down']";
    }

    public static String iframe_Xpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String inspection_Record_Saved_popup()
    {
        return "//div[@id='divForms']//div[contains(text(),'Record saved')]";
    }

    public static String failed()
    {
        return "(//div[@id='txtAlert'])[2]";
    }

    public static String Save_Button()
    {
        return "(//div[text()='Save'])[2]";
    }

    public static String Button_Save_And_Close()
    {
        return "//div[text()='Save and close']";
    }

    public static String Loading_Form_Label()
    {
        return "//div[text()='Loading form...']";
    }

    public static String Button_Add_1()
    {
        return "(//div[@class='actionButton button primary right'])[7]";
    }

    public static String Process_Flow_1()
    {
        return "(//span[@class='img icon flow-tree icon-button'])[2]";
    }

    public static String Document_Taxonomy_Text_Feild_1()
    {
        return "(//div[@class='border c-txb']//input[@type='text'])[7]";
    }

    public static String Link_Text_Feild_1()
    {
        return "(//div[@class='border c-txb']//input[@type='text'])[9]";
    }

    public static String Button_Save_13()
    {
        return "(//div[@class='actionButton button primary right'])[13]";
    }

    public static String Document_Taxonomy_Level_3_label()
    {
        return "//div[text()='Document Taxonomy Level 3']";
    }

    public static String SE_savetocontinue()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static String saveWait2()
    {
                return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";

    }

    public static String recordSaved_popup()
    {
               return "(//div[@id='divMessage']//div[contains(text(),'Record saved')])[1]";
    }

    public static String button_save()
    {
        return "(//div[contains(text(),'Save')])[9]";
    }

    public static String navigate_EHS() {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }

    public static String LeaderParticipationMaintenance()
    {
        return "//div[@original-title='Leadership & Participation Maintenance']//i[@class='icon settings size-five cool grey module iso-icon row']";
    }

    public static String DocumentTaxonomy_Module()
    {
        return "//label[text()='Document Taxonomy']";
    }
    
    public static String Button_Add_2()
    {
        return "//div[@id='control_7B6EA441-5F54-4EFC-845F-A31C031475D5']//div[text()='Add']";
    }

    public static String Document_Taxonomy_Text_Feild_2()
    {
        return "//div[@id='control_4C11EDDB-9F3C-4E3D-B40D-B7E5D5CF3F83']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

}
