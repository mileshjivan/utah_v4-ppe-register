/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Observations_V5_2_PageObjects;

/**
 *
 * @author smabe
 */
public class Observations_V5_2_PageObjects {

    public static String BusinessUnitHeader() {
        return "//div[text()='Business unit']";
    }
    public static String BusinessUnit_Option(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[1]";
    }
    public static String BusinessUnit_Option1(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String DepartmentResponsible_Option(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[1]";
    }
    public static String DepartmentResponsible_Option1(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String ObservationActions_CloseButton() {
        return "(//div[@id='form_4477F482-07A2-4919-82A8-1D3F8EE36CE2']//i[@class='close icon cross'])[1]";
    }
    public static String ObservationActions_Gridview(String data) {
        return "(//div[@id='control_8E84CB39-DEEE-4819-832F-E4A10339C343']//div//table)[3]//div[text()='" + data + "']";
    }
    public static String ObservationHeader() {
        return "//div[text()='Observations']";
    }
    public static String ObservationsActions_ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_4477F482-07A2-4919-82A8-1D3F8EE36CE2']";
    }
    public static String incidentActionId() {
        return "//div[@id='form_4477F482-07A2-4919-82A8-1D3F8EE36CE2']//div[@class='recnum']//div[@class='record']";
    }
    public static String inspection_Record_Saved_popup() {
        return "//div[@id='divForms']//div[contains(text(),'Record saved')]";
    }
    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }
    public static String saveBtn() {
        return "//div[@id='btnSave_form_6E7B288D-285E-4D74-976E-B6CC1B96292E']";
    }
    public static String saveBtn2() {
        return "//div[@id='btnSave_form_4477F482-07A2-4919-82A8-1D3F8EE36CE2']//div[@title='Save']";
    }
    public static String navigate_EHS() {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }
    public static String saveWait() {
        return "//div[@class='ui inverted dimmer active']";
    }
    public static String failed() {
        return "(//div[@id='txtAlert'])[2]";
    }
    public static String Observation_tab() {
        return "//label[contains(text(),'Observations')]";
    }
    public static String Observations_tab() {
        return "(//label[contains(text(),'Observations')])[2]";
    }
    public static String Observations_Add() {
        return "//div[@id='btnActAddNew']//div[text()='Add']";
    }
    public static String Observations_Add_2() {
        return "//div[@id='btnAddNew']//div[text()='Add']";
    }
    public static String Observations_ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_6E7B288D-285E-4D74-976E-B6CC1B96292E']";
    }
    public static String BusinessUnit_Dropdown() {
        return "//div[@id='control_7F840C09-6302-464F-B274-355C33FE2232']";
    }
    public static String BusinessUnit_Dropdown_Option() {
        return "//li[@title='Global Company']";
    }
    public static String Date_observed() {
        return "//span[@class='k-widget k-datepicker']//input";
    }
    public static String Time_observed() {
        return "//div[@id='control_573100B6-D270-4B06-AB9E-F8421E30DD70']//input";
    }
    public static String Observation() {
        return "//div[@id='control_1A740402-4144-4BF5-82E0-73179A5C894E']//textarea";
    }
    public static String impactTypeSelect(String data) {
        return "//a[contains(text(),'" + data + "')]";
    }
    public static String Risk_sources() {
        return "//div[@id='control_64162A09-F5AF-46C9-A520-12B7FF61622E']//b[@original-title='Select all']";
    }
    public static String Type_of_observation() {
        return "//div[@id='control_18881E6C-9EE6-4D68-BA8E-4494B5E13581']//div//li";
    }
    public static String Observation_logged_by() {
        return "//div[@id='control_4CFEEB48-2002-4677-BA4E-27EAE8C4105F']//li";
    }
    public static String Type_of_observation(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String impactType() {
        return "//div[@id='control_81A52C73-0966-436D-9973-25C81E5CA966']//li";
    }
    public static String Observation_logged_by_Select(String data) {
        return "//a[contains(text(),'" + data + "')]";
    }
    public static String Status() {
        return "//div[@id='control_6436D4D2-F339-4C3B-B864-8C7739055404']//li";
    }
    public static String Status_Option(String data) {
        return "//a[contains(text(),'" + data + "')]";
    }
    public static String Action_description() {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }
    public static String Department_responsible() {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//li";
    }
    public static String Department_responsible_Global() {
        return "(//a[text()='Global Company'])[2]";
    }
    public static String Responsible_Person() {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//li";
    }
    public static String Responsible_Person_option(String data) {
        return "(//a[contains(text(),'" + data + "')])[3]";
    }
    public static String Action_due_date() {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }
    public static String View_report() {
        return "//span[@title='View report ']";
    }
    public static String Reports() {
        return "//span[@class='img icon report three icon-button']";
    }
    public static String Continue_Btn() {
        return "//div[@title='Continue']/../.";
    }
    public static String Observations_label() {
        return "//div[text()='Observations']";
    }
    public static String Full_report() {
        return "//span[@title='Full report ']";
    }
    public static String Viewer_Report_Viewer() {
        return "//div[@id='viewer_ReportViewer']";
    }
    public static String Search_Btn() {
        return "//div[text()='Search']";
    }
    public static String Reports_grid() {
        return "//div[@class='k-grid-content k-auto-scrollable']";
    }
    public static String iframeXpath(){
        return "//iframe[@id='ifrMain']";
    }
}
