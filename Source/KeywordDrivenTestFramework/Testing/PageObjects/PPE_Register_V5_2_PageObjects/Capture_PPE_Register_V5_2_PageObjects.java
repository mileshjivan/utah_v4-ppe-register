/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.PPE_Register_V5_2_PageObjects;

/**
 *
 * @author smabe
 */
public class Capture_PPE_Register_V5_2_PageObjects {
    public static String BusinessUnitHeader() {
        return "//div[text()='Business unit']";
    }
    public static String ItemsIssued_Add_Button() {
        return "//div[@id='control_A095D850-6452-4C62-B5DD-B666C7A6762E']//div[text()='Add']";
    }
    public static String ItemsIssued_Save() {
        return "//div[@id='btnSaveAll']//div[text()='Save']";
    }
    public static String NumberOfItemsIssued() {
        return "(//div[@id='control_5F7D339D-2E98-4A86-A359-B6EA50FD4C3B']//input)[1]";
    }
    public static String PPE_Dropdown() {
        return "//div[@id='control_0437098B-7B80-4273-8663-7E892DCA01E8']";
    }
    public static String PPE_Option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String PPE_Register() {
        return "//div[text()='PPE Register']";
    }
    public static String SignOffComments() {
        return "//div[@id='control_7EC97411-F499-47DC-85D2-616E2AC35DA4']//textarea";
    }
    public static String SignOff_Dropdown() {
        return "//div[@id='control_939EE724-8FF5-4349-9DC0-7E1F58947469']";
    }
    public static String SignOff_Option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String SignOff_tab() {
        return "//div[text()='2. Sign Off']";
    }
    public static String Register_tab() {
        return "//div[text()='1. Register']";
    }
    public static String SignoffRegister_Button() {
        return "//div[@id='control_24165438-50D8-483D-BAFE-98ABD5805C6E']//div[text()='Sign off register']";
    }
    public static String searchBtn() {
        return "//div[@id='searchOptions']//div[text()='Search']";
    }
    public static String viewFullReportsIcon() {
        return "//span[@title='Full report ']";
    }
    public static String reportsBtn() {
        return "//div[@id='btnReports']";
    }
    public static String viewReportsIcon() {
        return "//span[@title='View report ']";
    }
    public static String continueBtn() {
        return "//div[@id='btnConfirmYes']";
    }
    public static String ItemsIssued_Panel() {
        return "//div[text()='Items Issued']";
    }
    public static String navigate_EHS() {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }
    public static String PPE_Register_Tab() {
        return "//label[contains(text(),'PPE Register')]";
    }
    public static String Add_button() {
        return "//div[contains(text(),'Add')]";
    }
    public static String Register_Tab() {
        return "//div[contains(text(),'1. Register')]";
    }
    public static String Business_Unit() {
        return "//div[@id='control_718604F3-3908-400C-A200-D9FF1327A8A1']//ul";
    }
    public static String Business_Unit_Drop_Down_Option_1(String value) {
        return "//a[text()='" + value + "']/../i";
    }
    public static String Business_Unit_Drop_Down_Option_2(String value) {
        return "//a[text()='" + value + "']";
    }
    public static String PPERigister_ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_FBCFDE5B-4D85-41FC-8BF1-3A4A784CE39D']";
    }
    public static String Register_Notes() {
        return "//div[text()='Notes']/../..//textarea";
    }
    public static String Save_To_Continue() {
        return "//div[@id='control_A00CFA51-0A4D-4C6D-AF3F-0FD4080295CC']";
    }
    public static String Save_Button() {
        return "(//div[text()='Save'])[2]";
    }
    public static String Record_Saved_Msg() {
        return "//div[text()='Record saved']";
    }
    public static String validateSave() {
        return "//div[@class='ui floating icon message transition visible']";
    }
    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }
    public static String Issued_To_Drop_Down() {
        return "(//div[text()='Issued to']/../..//a[@class='select3-choice'])[4]";
    }
    public static String Issued_To_Option(String value) {
        return "//a[text()='" + value + "']";
    }
    public static String Employee_Drop_Down() {
        return "(//div[text()='Employee']/../..//a[@class='select3-choice'])[1]";
    }
    public static String Employee_Drop_Down_Option(String value) {
        return "//a[text()='" + value + "']";
    }
    public static String Date_Issued() {
        return "//div[text()='Date Issued']/../..//input[@class='k-input']";
    }
    public static String Contractor_Drop_Down() {
        return "//div[@name='Contractor']";
    }
    public static String Contractor_Drop_Down_Option_1(String value) {
        return "//a[text()='" + value + "']/../i";
    }
    public static String Contractor_Drop_Down_Option_2(String value) {
        return "//a[text()='" + value + "']";
    }
    public static String ID_Number_Input() {
        return "(//div[text()='ID Number']/../..//input)[6]";
    }
    public static String Visitor_Input() {
        return "(//div[text()='Visitor']/../..//input)[2]";
    }
    public static String Supporting_Doocuments_tab() {
        return "//div[text()='Supporting Documents']";
    }
    public static String link_buttonxpath() {
        return "//b[@class='linkbox-link']";
    }
    public static String LinkURL() {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }
    public static String urlTitle() {
        return "//div[@class='confirm-popup popup']//input[@id='urlTitle']";
    }
    public static String urlAddButton() {
        return "//div[@class='confirm-popup popup']//div[contains(text(),'Add')]";
    }
    public static String iframeXpath() {
        return "//iframe[@id='ifrMain']";
    }
    public static String SaveSupportingDocuments_Button() {
        return "//div[text()='Save supporting documents']";
    }
    public static String EP_SaveBtn() {
        return "//div[@id='btnSave_form_6682D62D-D470-4E11-BA5A-DFC1D1E1D35F']//div[text()='Save']";
    }
}
