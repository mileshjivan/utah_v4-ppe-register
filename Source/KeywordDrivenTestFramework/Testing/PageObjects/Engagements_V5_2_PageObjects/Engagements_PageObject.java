/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Engagements_V5_2_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author SKhumalo
 */
public class Engagements_PageObject extends BaseClass {

    static String grievanceTitle;
    static String projectTitle;
    static String commitmentTitle;

    public static String BusinessUnitHeader() {
        return "//div[text()='Business unit']";
    }
    public static String EngagementHeader() {
        return "//div[text()='Engagements']";
    }
    public static String loadingData() {
        return "//div[@class='ui inverted dimmer']/div[text()='Loading data...']";
    }
    public static String loading() {
        return "//div[@class='ui inverted dimmer']/div[text()='Loading...']";
    }
    public static String AudienceTypeSelect(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String AudienceType_Dropdown() {
        return "//div[@id='control_74C6DEB4-AE07-4476-AF24-753B7187F94A']";
    }
    public static String EngagementStatus_Dropdown() {
        return "(//td[text()='Engagement status']//..//td[@class='sel']//div)[1]";
    }
    public static String EngagementStatus_Option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//i[@class='jstree-icon jstree-checkbox']";
    }
    public static String GrievanceRecord(String data) {
        return "(//div[@id='control_378A887E-105F-4423-AABC-0F15AB543FF5']//div//table)[3] //div[text()='" + data + "']";
    }
    public static String GrievanceRecord() {
        return "((//div[@id='control_1D831041-C982-48B2-A557-C844B69C7643']//div//table)[3] //div[text()='Global Company -> South Africa -> Victory Site'])[1]";
    }
    public static String IndividualsTab() {
        return "//div[text()='Individuals']";
    }
    public static String RelatedGrievancePanel() {
        return "//span[text()='Related Grievances']";
    }
    public static String SaveSupportingDocuments_SaveBtn() {
        return "//div[@id='control_F33B6F6E-13E8-4ABC-8364-61C5086C73A3']";
    }
    public static String SearchButton() {
        return "(//div[@id='control_1D831041-C982-48B2-A557-C844B69C7643']//div[text()='Search'])[1]";
    }
    public static String SearchButton1() {
        return "(//div[@id='control_1D831041-C982-48B2-A557-C844B69C7643']//div[text()='Search'])[2]";
    }
    public static String validateSave(){
        return "//div[@class='ui floating icon message transition visible']";
    }
    public static String Global_Company_Drop_Down() {
        return "(//li[@title='Global Company']//i)[1]";
    }
    public static String SaveToContinue_SaveBtn() {
        return "//div[@id='control_ED751D41-E936-4042-8E41-FB030CA70377']//div[text()='Save to continue']";
    }
    public static String searchBtn1() {
        return "(//div[@id='control_50E85B7D-AC6F-4FBD-9745-B4F4583650CD']//div[text()='Search'])[1]";
    }
    public static String InitiativesRecord() {
        return "//div[@id='control_BEEA2C5C-D12B-4585-BB9E-8A220737D2C3']//div[text()='Drag a column header and drop it here to group by that column']/../div[3]//tr[1]";
    }
    public static String initiatives_add_Btn() {
        return "//div[@id='control_6607FBD8-B7AF-4790-BF76-23119AE7ACA7']//div[text()='Add']";
    }
    public static String approvedBudget() {
        return "//div[@id='control_5E293AF0-02B6-4085-81A8-839A648886CD']//input[@type='number']";
    }
    public static String deliveryDate() {
        return "//div[@id='control_FA763176-78D8-437B-8E60-F14EC6FD89D5']//input";
    }
    public static String commencementDate() {
        return "//div[@id='control_A02E4959-B4FD-4733-BFCD-8153C6F7CD70']//input";
    }
    public static String socialInitiativesProcess_flow() {
        return "//div[@id='btnProcessFlow_form_82A4B7AA-AFD3-4DE7-9834-C3E317EE0ACA']";
    }
    public static String socialInitiativesBusinessUnitTab() {
        return "//div[@id='control_1B017CB2-B482-4AE4-AC1E-3E589A354761']";
    }
    public static String TypeOfInitiative() {
        return "//div[@id='control_2C68539F-B3A7-4844-A919-CCA67BB70A53']";
    }
    public static String TypeOfInitiativeArrow() {
        return "//a[text()='Environmental Initiatives']/../i";
    }
    public static String TypeOfInitiativeSelect(String data) {
        return "//a[text()='" + data + "']";
    }
    public static String addAnInitiativeBtn() {
        return "//div[@id='control_553E1A4E-09B8-4A64-8B9D-A631D30F836E']//div[text()='Add an initiative']";
    }
    public static String navigate_StakeholderIndividual() {
        return "//label[text()='Stakeholder Individual']";
    }
    public static String searchFieldIndividual() {
        return "(//div[@id='searchOptions']//td[@class='sel']//input[@class='txt border'])[1]";
    }
    public static String selectIndividualRecord(String data) {
        return "//div[text()='" + data + "']";
    }
    public static String si_processflow() {
        return "//div[@id='btnProcessFlow_form_E686D312-3E2F-4E66-9EAD-AC71C09267DD']";
    }
    public static String individual_relationshipOwner() {
        return "//div[@id='control_F9D5D37A-1D64-4710-B6A3-BCF77F4D567C']";
    }
    public static String rightArrow() {
        return "//div[@class='tabpanel_right_scroll icon chevron right']";
    }
    public static String initiatives_Tab() {
        return "//div[text()='Initiatives']";
    }
    public static String responsiblePerson_DD() {
        return "//div[@id='control_C1F24402-51E4-4F49-9520-00D62284121D']";
    }
    public static String responsiblePerson_Select(String data) {
        return "//ul[@class='jstree-container-ul jstree-children']//a[contains(text(),'" + data + "')]";
    }
    public static String socialInitiative_SaveBtn() {
        return "//div[@id='btnSave_form_82A4B7AA-AFD3-4DE7-9834-C3E317EE0ACA']//div[text()='Save']";
    }
    public static String stakeholderIndividualPanel() {
        return "//span[text()='Stakeholder Individual Beneficiaries']";
    }
    public static String beneficiries_Tab() {
        return "//div[text()='Beneficiaries']";
    }
    public static String individualBeneficiary() {
        return "//div[@id='control_2BA9C0DA-BB2A-4D66-9800-2DB2BD90F7EA']";
    }
    public static String entityBeneficiary_Select(String data) {
        return "//a[text()='" + data + "']";
    }
    public static String navigate_complaintsGrievances() {
        return "//label[text()='Complaints & Grievances']";
    }
    public static String individualSave() {
        return "//div[@id='control_6607FBD8-B7AF-4790-BF76-23119AE7ACA7']//div[text()='Save']";
    }
    public static String searchBtn2() {
        return "(//div[@id='searchOptions']//div[text()='Search'])[2]";
    }
    public static String viewFullReportsIcon() {
        return "//span[@title='Full report ']";
    }
    public static String reportsBtn() {
        return "//div[@id='btnReports']";
    }
    public static String viewReportsIcon() {
        return "//span[@title='View report ']";
    }
    public static String continueBtn() {
        return "//div[@id='btnConfirmYes']";
    }
    public static void setGrievanceTitle(String value) {
        grievanceTitle = value;
    }
    public static String navigate_commitments() {
        return "//label[text()='Commitments']";
    }
    public static String navigate_socialSustainability() {
        return "//label[text()='Social Sustainability']";
    }
    public static String isoHome() {
        return "//div[@class='iso header item brand']";
    }
    public static String navigate_socialInitiatives() {
        return "//label[text()='Social Initiatives']";
    }
    public static String getGrievanceTitle() {
        return grievanceTitle;
    }
    public static void setProjectTitle(String value) {
        projectTitle = value;
    }
    public static String getProjectTitle() {
        return projectTitle;
    }
    public static void setCommitmentTitle(String value) {
        commitmentTitle = value;
    }
    public static String getCommitmentTitle() {
        return commitmentTitle;
    }
    public static String processFlow() {
        return "//div[@id='btnProcessFlow_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']";
    }
    public static String businessUnitTab() {
        return "//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']";
    }
    public static String expandGlobalCompany() {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'Global Company')]/../i";
    }
    public static String expandSouthAfrica() {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'South Africa')]/../i";
    }
    public static String closeBusinessUnit() {
        return "(//div[@id='control_16C9A7FC-3091-4E0D-9B1A-709C8F0AC8B5']//span[2]//b[1])";
    }
    public static String projectLink() {
        return "//div[@id='control_29AB36D5-E83F-43EF-AFF5-F7353A5353E9']/div[1]";
    }
    public static String projectTab() {
        return "//div[@id='control_963F5190-1317-42C1-AD7A-B277FCBA7101']";
    }
    public static String anyProject(String text) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + text + "')]";
    }
    public static String engagementDate() {
        return "//div[@id='control_C6060E39-99D5-417B-90CF-09077C971E5D']//input";
    }
    public static String navigate_EHS() {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }
    public static String navigate_Stakeholders() {
        return "//label[text()='Stakeholder Management']";
    }
    public static String navigate_Engagements() {
        return "//label[text()='Engagements']";
    }
    public static String SE_add() {
        return "//div[text()='Add']";
    }
    public static String process_flow() {
        return "//div[@id='btnProcessFlow_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']";
    }
    public static String engagementTitle() {
        return "(//div[@id='control_B0DFFFFF-66B3-4CB4-B4E5-23C8E0010864']//input)[1]";
    }
    public static String businessUnit_Select(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String business_UnitSelect(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//i[@class='jstree-icon jstree-checkbox']";
    }
    public static String projectSelct(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String engagementPurposeTab() {
        return "//div[@id='control_0FF334FE-CE57-49BF-BA69-9BE5DA3447CB']";
    }
    public static String anyEngagementPurpose(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')])[2]";
    }
    public static String engagementMethodTab() {
        return "//div[@id='control_4A471537-8229-4E54-A86C-DCEB99BA24D0']";
    }
    public static String engagementMethodArrow() {
        return "//a[text()='In-person engagements']/../i";
    }
    public static String anyEngagementMethod(String data) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }
    public static String confidentialCheckBox() {
        return "//div[@id='control_C108E1A8-9B60-4E8B-B85A-08E47E5C6A7D']/div[1]//div[1]";
    }
    public static String responsiblePersonTab() {
        return "//div[@id='control_213251A2-010A-4BBF-A65A-A1FC8C6F7033']";
    }
    public static String responsiblePersonSelect(String data) {
        return "//div[contains(@class, 'transition visible')]//a[text()='" + data + "']";
    }
    public static String impactTypeSelect(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]/..//i[@class='jstree-icon jstree-checkbox']";
    }
    public static String contactInquiry() {
        return "//div[@id='control_2D1B5E8D-BBF2-448A-9765-F03FA8C31019']";
    }
    public static String contactInquirySelect(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String locationDD() {
        return "//div[@id='control_F703A144-D0B6-4D4D-B5E2-D4E186427A43']";
    }
    public static String location() {
        return "//div[@id='control_E6FA4852-121D-41FE-BC5A-D20212EC2190']";
    }
    public static String locationSelect(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String relatedSaveBtn() {
        return "//div[@id='btnSave_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']//div[text()='Save']";
    }
    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }
    public static String mapPanel() {
        return "//span[text()='Map']";
    }
    public static String si_locationmarker() {
        return "//div[@id='control_10D8EA72-0F58-41B9-92D1-D8583BC3ED96']//div[@class='icon location mark mapbox-icons']";
    }
    public static String engagementDescription() {
        return "(//div[@id='control_1C19AE65-23A1-4ADC-A631-D9273FC0CE9F']//textarea)[1]";
    }
    public static String engagementPurpose() {
        return "(//div[@id='control_180C969D-A1C8-46FF-9F11-EA0457D2F762']//textarea)[1]";
    }
    public static String si_location() {
        return "Montana.PNG";
    }
    public static String latitudeField() {
        return "//div[@id='control_DAA4D814-C0FD-417C-A22F-79CADF81F81A']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }
    public static String longitudeField() {
        return "//div[@id='control_A5D5CA3A-9002-4FF7-A8B2-EC09C0B951D0']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }
    public static String supporting_tab() {
        return "//div[text()='Supporting Documents']";
    }
    public static String linkbox() {
        return "//b[@original-title='Link to a document']";
    }
    public static String LinkURL() {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }
    public static String urlTitle() {
        return "//div[@class='confirm-popup popup']//input[@id='urlTitle']";
    }
    public static String urlAddButton() {
        return "//div[@class='confirm-popup popup']//div[contains(text(),'Add')]";
    }
    public static String iframeXpath() {
        return "//iframe[@id='ifrMain']";
    }
    public static String attendiesTab() {
        return "//div[text()='Attendees']";
    }
    public static String totalNonListedAttendies() {
        return "//div[@id='control_8A28B08F-2857-4EB7-833E-3C7929A500B7']//input[@type='number']";
    }
    public static String individualsTab() {
        return "//div[text()='Individuals']";
    }
    public static String individuals_add() {
        return "//div[@id='control_2E2CA02B-9570-4DC8-89A7-8EE214D2F06E']//div[@id='btnAddNew']";
    }
    public static String individual_AttendeeNameDD() {
        return "//div[@id='control_DA9B0184-991D-4267-B01B-36EF4792AF67']";
    }
    public static String attendeeNameSelect(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String entitiesTab() {
        return "//div[text()='Entities']";
    }
    public static String entities_add() {
        return "//div[@id='control_FBBF4EE8-0B0B-43B6-9B13-642704FDA74B']//div[@id='btnAddNew']";
    }
    public static String entity_AttendeeNameDD() {
        return "//div[@id='control_BCCAA4BD-BD02-428C-A1E3-3BFE31AF66BF']";
    }
    public static String entity_AttendeeNameSelect(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')])[2]";
    }
    public static String attendeeName_Option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String teamTab() {
        return "//div[text()='Team']";
    }
    public static String team_add() {
        return "//div[@id='control_5D1D4343-6774-40DF-AAA5-4BE09A85DDCE']//div[@id='btnAddNew']";
    }
    public static String team_AttendeeDD() {
        return "//div[@id='control_B5CF2925-9A5E-4F93-A5D4-462B76CFEBF9']";
    }
    public static String grievancesTab() {
        return "//div[text()='Grievances']";
    }
    public static String grievance_add() {
        return "//div[@id='control_378A887E-105F-4423-AABC-0F15AB543FF5']//div[text()='Add']";
    }
    public static String createGrievancePanel() {
        return "//span[text()='Create Grievance']";
    }
    public static String grievanceProcess_flow() {
        return "//div[@id='btnProcessFlow_form_D0E4A377-80F0-4BD8-91E7-9C0D6EE2E888']";
    }
    public static String grievanceTitle() {
        return "//div[@id='control_6D9B0F8D-8FF2-4FEA-831E-2EC92E5564CF']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }
    public static String grievanceSummary() {
        return "//div[@id='control_D1C06FD5-4FEE-4053-A367-7B9A98F0FF27']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }
    public static String individualOrEntityDD() {
        return "//div[@id='control_038B094A-7260-4014-99DB-ABB2319AB9C4']";
    }
    public static String individualOrEntitySelect(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String grievantNameDD() {
        return "//div[@id='control_E8957E6E-FFDF-415D-8388-E99E3AD32744']";
    }
    public static String grieventNameSelect(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String dateOfOccurence() {
        return "//div[@id='control_9950BBBE-F62C-4321-A7DD-7828FA9236A3']//input";
    }
    public static String grievance_SaveBtn() {
        return "//div[@id='btnSave_form_D0E4A377-80F0-4BD8-91E7-9C0D6EE2E888']";
    }
    public static String closeBtn() {
        return "//div[@id='form_D0E4A377-80F0-4BD8-91E7-9C0D6EE2E888']/div[@class='navbar']/i[@class='close icon cross']";
    }
    public static String relatedGrievancesPanel() {
        return "//span[text()='Related Grievances']";
    }
    public static String relatedGrievanceProcess_flow() {
        return "//div[@id='btnProcessFlow_form_A38BD6B3-FAD6-47B5-8124-8F7DBEC01739']";
    }
    public static String clickRelatedGrievance() {
        return "//div[@id='control_1D831041-C982-48B2-A557-C844B69C7643']//div[contains(text(),'Drag a column header and drop it here to group by that column')]/../div[3]//tr[1]";
    }
    public static String search1() {
        return "(//div[@id='control_1D831041-C982-48B2-A557-C844B69C7643']//div[contains(text(),'Search')])[1]";
    }
    public static String search2() {
        return "(//div[@id='control_1D831041-C982-48B2-A557-C844B69C7643']//div[contains(text(),'Search')])[2]";
    }
    public static String searchGrievanceTitle() {
        return "//div[@id='searchOptions']//tr[@module_definition_name='txb_Grievance_title']//input[@class='txt border']";
    }
    public static String searchBtn() {
        return "//div[@id='searchOptions']//div[contains(text(),'Search')]";
    }
    public static String clickGrievanceRecord() {
        return "//div[@class='k-grid-content k-auto-scrollable']//tr[1]";
    }
    public static String grievance_Process_flow() {
        return "//div[@id='btnProcessFlow_form_A38BD6B3-FAD6-47B5-8124-8F7DBEC01739']";
    }
    public static String actions_tab() {
        return "//div[text()='Actions']";
    }
    public static String Actions_add() {
        return "//div[@id='control_5C3A47DC-4956-4D6E-BF97-A3967E24667B']//div[text()='Add']";
    }
    public static String Actions_pf() {
        return "//div[@id='btnProcessFlow_form_5110F491-CFCA-42F7-87A2-001109988E70']";
    }
    public static String Actions_desc() {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }
    public static String Actions_deptresp_dropdown() {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//li";
    }
    public static String Actions_deptresp_select(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[1]";
    }
    public static String Actions_deptresp_select1(String text) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + text + "')]";
    }
    public static String Actions_respper_dropdown() {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//li";
    }
    public static String ActionsRespper_select(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')])[2]";
    }
    public static String Actions_ddate() {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }
    public static String Actions_save() {
        return "//div[@id='btnSave_form_5110F491-CFCA-42F7-87A2-001109988E70']";
    }
    public static String Actions_savewait() {
        return "(//div[text()='Action Feedback'])[1]";
    }
    public static String actions_CloseBtn() {
        return "//div[@id='form_5110F491-CFCA-42F7-87A2-001109988E70']/div[@class='navbar']/i[@class='close icon cross']";
    }
    public static String clickEngagementRecord() {
        return "//div[@class='k-grid-content k-auto-scrollable']//tr[1]";
    }
    public static String engagementStatusDD() {
        return "//div[@id='control_C072FF42-2D88-4B84-866B-FE7E9436460F']";
    }
    public static String initiativesTab() {
        return "//div[text()='Initiatives']";
    }
    public static String createInitiativesPanel() {
        return "//span[text()='Create Initiative']";
    }
    public static String initiative_add() {
        return "//div[@id='control_B589B7AE-E477-4356-B348-38C28AC45FB8']//div[text()='Add']";
    }
    public static String project_Title() {
        return "//div[@id='control_2786D8FA-CB59-4542-B173-B5220F2FC786']//input";
    }
    public static String location_DD() {
        return "//div[@id='control_E89884D1-4640-4024-B57E-C1C78775432A']";
    }
    public static String location_Select(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String sector_DD() {
        return "//div[@id='control_2D64B1F8-1C51-48FC-919F-8EB0A3E028A7']";
    }
    public static String initiative_SaveBtn() {
        return "//div[@id='control_B589B7AE-E477-4356-B348-38C28AC45FB8']//div[text()='Save']";
    }
    public static String socialInitiativesPanel() {
        return "//span[text()='Create Initiative']";
    }
    public static String searchProjectTitle() {
        return "//div[@id='searchOptions']//tr[@module_definition_name='txb_n1']//input[@class='txt border']";
    }
    public static String relatedInitiavesPanel() {
        return "//span[text()='Related Initiatives']";
    }
    public static String initiative_search1() {
        return "(//div[@id='control_C46CA6A4-3196-4DB5-8B50-BD18B55A33B5']//div[contains(text(),'Search')])[1]";
    }
    public static String initiative_search2() {
        return "(//div[@id='control_C46CA6A4-3196-4DB5-8B50-BD18B55A33B5']//div[contains(text(),'Search')])[2]";
    }
    public static String clickRelatedInitiative() {
        return "//div[@id='control_C46CA6A4-3196-4DB5-8B50-BD18B55A33B5']//div[contains(text(),'Drag a column header and drop it here to group by that column')]/../div[3]//tr[1]";
    }
    public static String relatedInitiativeProcess_flow() {
        return "//div[@id='btnProcessFlow_form_82A4B7AA-AFD3-4DE7-9834-C3E317EE0ACA']";
    }
    public static String commitmentsTab() {
        return "//div[text()='Commitments']";
    }
    public static String createCommitmentPanel() {
        return "//span[text()='Create Commitment']";
    }
    public static String commitment_add() {
        return "//div[@id='control_23054DE1-A737-47BF-89FF-83818E445F9F']//div[text()='Add']";
    }
    public static String commitment_SaveBtn() {
        return "//div[@id='control_23054DE1-A737-47BF-89FF-83818E445F9F']//div[text()='Save']";
    }
    public static String relatedCommintmentsPanel() {
        return "//span[text()='Related Commitments']";
    }
    public static String commitments_search1() {
        return "(//div[@id='control_AE0D1E29-9AE0-445A-8125-9A6D33A1CF31']//div[contains(text(),'Search')])[1]";
    }
    public static String commitments_search2() {
        return "(//div[@id='control_AE0D1E29-9AE0-445A-8125-9A6D33A1CF31']//div[contains(text(),'Search')])[2]";
    }
    public static String clickRelatedCommitments() {
        return "//div[@id='control_AE0D1E29-9AE0-445A-8125-9A6D33A1CF31']//div[contains(text(),'Drag a column header and drop it here to group by that column')]/../div[3]//tr[1]";
    }
    public static String relatedCommitmentProcess_flow() {
        return "//div[@id='btnProcessFlow_form_8541B36E-740A-4367-94DC-BF0661305F0E']";
    }
    public static String Victory_Site_drop_Down() {
        return "(//li[@title='Global Company']//i)[8]";
    }
    public static String South_Africa_Drop_Down() {
        return "(//li[@title='Global Company']//i)[4]";
    }
    public static String Typeofsearch() {
        return "(//input[@placeholder='Type to search'])[39]";
    }
    public static String Typeofsearch2() {
        return "(//input[@placeholder='Type to search'])[18]";
    }
    public static String individualOrEntitySelect_2(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')])[2]";
    }

    public static String Type_of_observation(String data)
    {
        return"//a[contains(text(),'"+data+"')]";
    }

    public static String impactType()
    {     
        return"//div[@id='control_81A52C73-0966-436D-9973-25C81E5CA966']//li";     
    }
}
