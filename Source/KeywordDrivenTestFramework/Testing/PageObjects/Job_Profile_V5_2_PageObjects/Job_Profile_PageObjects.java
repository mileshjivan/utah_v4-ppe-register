/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Job_Profile_V5_2_PageObjects;

/**
 *
 * @author SKhumalo
 */
public class Job_Profile_PageObjects {

    public static String BusinessUnitHeader() {
        return "//div[text()='Business unit']";
    }
    public static String JobProfile_Header() {
       return "//div[text()='Job Profile']";
    }
    public static String iframeXpath() {
        return "//iframe[@id='ifrMain']";
    }
    public static String searchBtn() {
        return "//div[@id='searchOptions']//div[text()='Search']";
    }
    public static String viewFullReportsIcon() {
        return "//span[@title='Full report ']";
    }
    public static String reportsBtn() {
        return "//div[@id='btnReports']";
    }
    public static String viewReportsIcon() {
        return "//span[@title='View report ']";
    }
    public static String continueBtn() {
        return "//div[@id='btnConfirmYes']";
    }
     public static String SaveSupportingDocuments_SaveBtn() {
        return "//div[text()='Save supporting documents']";
    }
    public static String Supporting_Doocuments_tab() {
        return "//div[text()='Supporting Documents']";
    }
    public static String linkbox() {
        return "//b[@original-title='Link to a document']";
    }
    public static String LinkURL() {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }
    public static String urlTitle() {
        return "//div[@class='confirm-popup popup']//input[@id='urlTitle']";
    }
    public static String urlAddButton() {
        return "//div[@class='confirm-popup popup']//div[contains(text(),'Add')]";
    }
    public static String AdditionalTraining_Dropdown() {
        return "//div[@id='control_C1D2BCE9-071E-45ED-81DF-68B837B08F21']";
    }
    public static String AdditionalTraining_SelectAll() {
        return "//div[@id='control_C1D2BCE9-071E-45ED-81DF-68B837B08F21']//b[@class='select3-all']";
    }
    public static String OccupationCode() {
        return "(//div[@id='control_9EFEEF45-8FCF-40D1-B862-71804B87E1FF']//input)[1]";
    }
    public static String OccupationDescription() {
        return "(//div[@id='control_953671BF-38E3-4DCA-93F5-53CCCE68CC9C']//textarea)[1]";
    }
    public static String OccupationName(){
        return "(//div[@id='control_EAD20C54-AEC5-4A37-8B25-3BF44E5889F9']//input)[1]";
    }
    public static String BusinessUnit_Dropdown() {
        return "//div[@id='control_5C123CBC-E4C6-45A2-9C09-95D56F68069D']";
    }
    public static String BusinessUnit_Option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i[@class='jstree-icon jstree-ocl']";
    }
    public static String BusinessUnit_Option1(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String JobProfiles() {
        return "//label[text()='Job Profiles']";
    }
    public static String OperateMaintenance() {
        return "//label[text()='Operate Maintenance']//..//i";
    }
    public static String ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_19C6D268-D617-4854-96F3-008E2ED91C9C']";
    }
    public static String RequiredTraining_Dropdown() {
        return "//div[@id='control_47842D50-0722-4566-9E22-909E5ED5D745']";
    }
    public static String RequiredTraining_SelectAll() {
        return "//div[@id='control_47842D50-0722-4566-9E22-909E5ED5D745']//b[@class='select3-all']";
    }
    public static String Save_Button() {
        return "//div[@id='btnSave_form_19C6D268-D617-4854-96F3-008E2ED91C9C']//div[text()='Save']";
    }
    public static String navigate_EHS() {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }
    public static String Add_button() {
        return "//div[contains(text(),'Add')]";
    }
    public static String validateSave() {
        return "//div[@class='ui floating icon message transition visible']";
    }
    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }
    
    
}
