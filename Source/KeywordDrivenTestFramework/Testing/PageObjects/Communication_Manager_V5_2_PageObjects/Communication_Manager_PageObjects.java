/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Communication_Manager_V5_2_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author SKhumalo
 */
public class Communication_Manager_PageObjects extends BaseClass{

    public static String RegisterStatus(){
        return "//div[@id='control_1B138716-A791-4A5E-A9B1-EF482AA1D98A']//li";
    }
    public static String CommunicationStatus() {
        return "//div[@id='control_7D314BCF-DB83-4A47-9830-3686F01B0F80']//li";
    }
    public static String Communication_Module(){
        return "//label[text()='Communications']";
    }
    public static String communicationGroups(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]/i[@class='jstree-icon jstree-checkbox'])[1]";
    }
    public static String failed(){
        return "(//div[@id='txtAlert'])[2]";
    }
    public static String inspection_Record_Saved_popup(){
        return "//div[@id='divForms']//div[contains(text(),'Record saved')]";
    }

    public static String navigate_Dashboards() {
        return "//label[text()='Dashboards']";
    }
    public static String serachBtn(){
        return "//div[@class='actionBar filter']//div[text()='Search']";
    }
    public static String selectRecord(String data){
        return "//span[text()='"+data+"']";
    }
    public static String record_No() {
        return "(//input[@class='txt border'])[1]";
    }
    public static String saveWait() {
        return "//div[@class='ui inverted dimmer active']";
    }
    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }
    public static String navigate_OccupationalHealth(){
        return "//label[contains(text(),'Occupational Health')]";
    }
    public static String navigate_CommManager(){
        return "//label[contains(text(),'Communication Manager')]";
    }
    public static String CommManager_ProcessFlow(){
        return "//div[@id='btnProcessFlow_form_F30484E8-2178-4DD5-8847-A61A006BC48E']";
    }
    public static String typeOfCommDD(){
        return "//div[@id='control_181B75EF-86C0-4570-AF30-54F3E9FFBBAC']";
    }
    public static String typeOfCommSelect(String data){
        return "//a[text()='"+data+"']";
    }
    public static String communicationTopic(){
        return "//div[@id='control_2A8E401D-614E-4E33-888D-DF0663049D5A']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }
    public static String communicationDetail(){
        return "//div[@id='control_6CB703FB-F507-4FF0-B3F7-8EC520237681']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }
    public static String dateOfCommunication(){
        return "//div[@id='control_7EECE4A8-B6F1-4F92-9B03-C69981DC8813']//input[@type='text']";
    }
    public static String finalDate(){
        return "//div[@id='control_7CE3AC53-3369-4884-8EE8-EB1D42552A05']//input[@type='text']";
    }
    public static String communicationGroups(){
        return "//div[@id='control_2744331F-AF9D-46EF-8ED5-E09E766136F1']//b[@class='select3-all']";
    }
    public static String commManager_Save(){
        return "//div[@id='btnSave_form_F30484E8-2178-4DD5-8847-A61A006BC48E']//div[text()='Save']";
    }    
    public static String BPM_Add(){
        return "//div[@class='actionBar']//div[text()='Add']";
    }
    public static String linkADoc_buttonxpath(){
        return "//div[@id='control_5D1F4CA2-CE3F-441D-91F8-7F1F75E4D704']//b[@class='linkbox-link']";
    }    
    public static String urlInput_TextAreaxpath() {
        return "//input[@id='urlValue']";
    }    
    public static String tile_TextAreaxpath() {
        return "//input[@id='urlTitle']";
    }    
    public static String linkADoc_Add_buttonxpath() {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }    
    public static String iframeXpath(){
        return "//iframe[@id='ifrMain']";
    }
    public static String declarationCheckBox(){
        return "//div[@id='control_5267CAF5-9F11-46BE-AD39-BAB1D22C5532']//div[@class='c-chk']";
    }
    public static String communicationStatus(){
        return "//div[@id='control_7D314BCF-DB83-4A47-9830-3686F01B0F80']//ul[@class='select3-tagbox']";
    }
    public static String selectCommRegisterRecord(String data){
        return "//div[contains(@class, 'transition visible')]//div[contains(text(),'" + data + "')]";
    }
    public static String declarationCheckBox2(){
        return "//div[@id='control_95335D39-097C-40CC-AE76-7BCDA5C6FB9C']//div[@class='c-chk']";
    }
    public static String doYouAcceptDD(){
        return "//div[@id='control_50291B0B-0C4E-4A28-AF37-6C81C3AC5434']";
    }
    public static String doYouAcceptSelect(String data){
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String comments(){
        return "//div[@id='control_F51A90E2-0362-4214-BE4C-E1BEFD16CB69']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }
    public static String processFlowBtn(){
        return "//div[@id='btnProcessFlow_form_F3160D65-4A3D-4324-AB09-26A9553514F3']";
    }
    public static String saveBtn(){
        return "//div[@id='btnSave_form_F3160D65-4A3D-4324-AB09-26A9553514F3']";
    }    
    public static String searchBtn(){
        return "//div[@id='btnActApplyFilter']//div[text()='Search']";
    }
    public static String reportsBtn(){
        return "//div[@id='btnReports']";
    }
    public static String viewReportsIcon(){
        return "//span[@title='View report ']";
    }    
    public static String continueBtn(){
        return "//div[@id='btnConfirmYes']";
    }    
    public static String viewFullReportsIcon(){
        return "//span[@title='Full report ']";
    }
    public static String x_close(){
        return "(//div[@class='form active transition visible']//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }
    public static String comm_Status(){
        return "//tr[@module_definition_name='ddl5']//span[@class='select3-chosen']";
    }
    public static String actionStatusSelect(String data){
        return "(//a[text()='"+data+"']//i)[1]";
    }
    public static String doYouAcceptSelect1(String data){
        return "(//a[text()='"+data+"'])[2]";
    }
    public static String doYouAcceptSelect2(String data){
        return "(//a[text()='"+data+"'])[3]";
    }
    public static String doYouAcceptSelect3(String data){
        return "(//a[text()='"+data+"'])[4]";
    }
    public static String selectCommRegisterRecord4(String data){
        return "(//Div[@id='control_E8C451DC-72AB-4B12-997A-322AFEC7A1F0']//div[contains(text(),'Manager')])[3]";
    }

}
