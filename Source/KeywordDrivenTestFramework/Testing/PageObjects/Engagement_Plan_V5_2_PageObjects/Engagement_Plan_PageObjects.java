/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Engagement_Plan_V5_2_PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author SKhumalo
 */
public class Engagement_Plan_PageObjects extends BaseClass {

    public static String EngagementPlan_Record(String data) {
        return "(//div[text()='Drag a column header and drop it here to group by that column']/..//div[text()='" + data + "'])[1]";
    }
    public static String EngagementStartDate() {
        return "//td[text()='Engagement date']//..//td[@class='sel']//span//input[@class='cal cal1 k-input']";
    }
    public static String EngagementEndDate() {
        return "//td[text()='Engagement date']//..//td[@class='sel']//span//input[@class='cal cal2 k-input']";
    }
    public static String Frequency_Dropdown() {
        return "(//td[text()='Frequency']//..//td[@class='sel']//div)[1]";
    }
    public static String Frequency_Option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//i[@class='jstree-icon jstree-checkbox']";
    }
    public static String InProgress_Phase() {
        return "//div[@class='step active']//div[text()='In Progress']";
    }
    public static String Completed_Phase() {
        return "(//div[@id='step_4']//div[text()='Completed'])[2]";
    }
    public static String Planning_Phase() {
        return "(//div[@id='step_2']//div[text()='Planning phase'])[2]";
    }
    public static String SaveSupportingDocuments_SaveBtn() {
        return "//div[@id='control_863F97E0-2164-4B4C-AD5E-D699A38753DB']";
    }
    public static String Status() {
        return "//div[@id='control_5479EBA7-F8D2-4926-94E6-DCF0335CE600']//li";
    }
    public static String engagementPlanDetails_Tab() {
        return "//div[text()='Engagement Plan Details']";
    }
    public static String engagementPlan_Title() {
        return "(//td[text()='Engagement title']//..//input)[2]";
    }
    public static String link_buttonxpath() {
        return "//b[@class='linkbox-link']";
    }
    public static String stakeholders_Dropdown() {
        return "//div[@id='control_86E5207D-ECEA-4F6D-9680-376A0EB4C2FE']//b[@class='select3-all']";
    }
    public static String stakeholders_SelectAll() {
        return "//div[@id='control_86E5207D-ECEA-4F6D-9680-376A0EB4C2FE']//b[@class='select3-all']";
    }
    public static String entities_SelectAll() {
        return "//div[@id='control_7524420C-1E4E-49C7-AAF6-C87B97ECEFCF']//b[@class='select3-all']";
    }
    public static String supportDocumentsPanel() {
        return "//div[text()='Supporting Documents']";
    }
    public static String validateSave(){
        return "//div[@class='ui floating icon message transition visible']";
    }
    public static String SaveToContinue_SaveBtn() {
        return "//div[@id='control_B1893417-CAAD-4DC3-BD27-2EC339FBAE64']//div[text()='Save to continue']";
    }
    public static String failed(){
        return "(//div[@id='txtAlert'])[2]";
    }
    public static String Record_Saved_popup(){
        return "//div[@id='divForms']//div[contains(text(),'Record saved')]";
    }
    public static String businessUnit_Option(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[1]";
    }
    public static String businessUnit_Option1(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String engagementsProcessFlow() {
        return "//div[@id='divForms']//div[@id='btnProcessFlow_form_C5D7993E-A223-4AE0-A15D-119FE22E21DC']";
    }
    public static String addAndEngagement() {
        return "//div[@id='control_A50A638E-99DE-44B8-A7B5-2BF8A948862A']//div[text()='Add an engagement']";
    }
    public static String navigate_EngagementPlan() {
        return "//label[text()='Engagement Plan']";
    }
    public static String engagementPlanProcess_flow() {
        return "//div[@id='btnProcessFlow_form_6682D62D-D470-4E11-BA5A-DFC1D1E1D35F']";
    }
    public static String engagementPlanTitle() {
        return "(//div[@id='control_185410E8-D077-4DE6-8958-5772CA36E091']//input)[1]";
    }
    public static String engagementPlanStartDate() {
        return "//div[@id='control_90276DFA-A2DD-4A38-8D96-E84491597886']//input[@type='text']";
    }
    public static String planBusinessUnitTab() {
        return "//div[@id='control_8BE367EF-E449-4165-BC05-74385ECBF771']";
    }
    public static String businessUnit_SelectAllBtn(String data) {
        return "//a[text()='" + data + "']";
    }
    public static String EP_projectLink() {
        return "//div[@id='control_DE6A7B48-B355-48A7-AA3E-8475171708AF']/div[1]";
    }
    public static String EP_projectTab() {
        return "//div[@id='control_301410A3-9118-4FCA-93B7-4C2A90320266']";
    }
    public static String frequency() {
        return "//div[@id='control_0189D12C-09E0-4D04-964C-34044E11A982']";
    }
    public static String EP_engagementPurposeTab() {
        return "//div[@id='control_36951962-3063-4DA2-9846-ED7137AFC783']";
    }
    public static String purposeOfEngagement(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String methodOfEngagement() {
        return "//div[@id='control_5D729CA2-07EF-4E0A-B491-1E223A263474']";
    }
    public static String methodOfEngagementSelect(String data) {
        return "//ul[@class='jstree-children']//a[text()='Project Update']";
    }
    public static String personResponsibleTab() {
        return "//div[@id='control_C2B7C6FA-10FC-4593-BD27-6869D1790758']";
    }
    public static String EP_SaveBtn() {
        return "//div[@id='btnSave_form_6682D62D-D470-4E11-BA5A-DFC1D1E1D35F']//div[text()='Save']";
    }
    public static String navigate_EHS() {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }
    public static String Social_Sustainability() {
        return "//label[contains(text(),'Social Sustainability')]";
    }  
    public static String navigate_Stakeholders() {
        return "//label[text()='Stakeholders']";
    }
    public static String SE_add() {
        return "//div[text()='Add']";
    }
    public static String projectSelct(String data) {
        return "//a[text()='" + data + "']";
    }
    public static String engagementMethodArrow() {
        return "//a[text()='In-person engagements']/../i";
    }
    public static String responsiblePersonSelect(String data) {
        return "//ul[@class='jstree-container-ul jstree-children']//a[contains(text(),'" + data + "')]";
    }
    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }
    public static String participantsTab() {
        return "//div[text()='Participants']";
    }
    public static String stakeholders(String data) {
        return "//div[@id='control_86E5207D-ECEA-4F6D-9680-376A0EB4C2FE']//a[text()='" + data + "']//i[@class='jstree-icon jstree-checkbox']";
    }
    public static String entitiesArrow() {
        return "//div[@id='control_7524420C-1E4E-49C7-AAF6-C87B97ECEFCF']//a[text()='DVT JHB (ITAC)']/../i";
    }
    public static String EP_entities(String data) {
        return "(//div[@id='control_7524420C-1E4E-49C7-AAF6-C87B97ECEFCF']//a[text()='" + data + "']/..//i[@class='jstree-icon jstree-checkbox'])[2]";
    }
    public static String se_search() {
        return "//div[text()='Search']";
    }
    public static String se_select() {
        return "//div[@class='k-grid-content k-auto-scrollable']//tr[1]";
    }
    public static String engagementPlanStatus() {
        return "//div[@id='control_5479EBA7-F8D2-4926-94E6-DCF0335CE600']";
    }
    public static String engagementPlanEndDate() {
        return "//div[@id='control_56848732-729E-4C20-A0FC-1034ACF3D6F4']//input[@type='text']";
    }
    public static String actions_tab() {
        return "//div[text()='Actions']";
    }
    public static String Actions_add() {
        return "//div[@id='control_52657B39-E580-49F5-88CB-D30DCB545A7B']//div[text()='Add']";
    }
    public static String Actions_pf() {
        return "//div[@id='btnProcessFlow_form_C7B74A9B-D0CC-4925-832E-F08CD2272A95']";
    }
    public static String Actions_desc() {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }
    public static String Actions_deptresp_dropdown() {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//li";
    }
    public static String Actions_deptresp_select(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String DepartmentResponsible_Option(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[1]";
    }
    public static String DepartmentResponsible_Option1(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }  
    public static String Actions_respper_dropdown() {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//li";
    }
    public static String ActionsRespper_select(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')])[2]";
    }
    public static String Actions_ddate() {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }
    public static String Actions_save() {
        return "(//div[text()='Save'])[5]";
    }
    public static String closeBtn() {
        return "(//div[@id='form_C7B74A9B-D0CC-4925-832E-F08CD2272A95']//i[@class='close icon cross'])[1]";
    }
    public static String action_record(String data) {
        return "(//div[@id='control_52657B39-E580-49F5-88CB-D30DCB545A7B']//div//table)[3] //div[text()='" + data + "']";
    }
    public static String navigate_Engagements() {
        return "//label[text()='Engagements']";
    }
    public static String engagementTitle() {
        return "(//div[@id='control_B0DFFFFF-66B3-4CB4-B4E5-23C8E0010864']//input)[1]";
    }
    public static String supporting_tab() {
        return "//div[text()='Supporting Documents']";
    }
    public static String linkbox() {
        return "//b[@original-title='Link to a document']";
    }
    public static String LinkURL() {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }
    public static String urlTitle() {
        return "//div[@class='confirm-popup popup']//input[@id='urlTitle']";
    }
    public static String urlAddButton() {
        return "//div[@class='confirm-popup popup']//div[contains(text(),'Add')]";
    }
    public static String iframeXpath() {
        return "//iframe[@id='ifrMain']";
    }
    
    static String engagementPlanTitle;
    
    public static void setEngagementPlanTitle(String value) {
        engagementPlanTitle = value;
    }
    public static String getEngagementPlanTitle() {
        return engagementPlanTitle;
    }
}