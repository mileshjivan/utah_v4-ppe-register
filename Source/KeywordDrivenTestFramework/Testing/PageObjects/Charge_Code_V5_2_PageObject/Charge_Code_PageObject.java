/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Charge_Code_V5_2_PageObject;

/**
 *
 * @author SKhumalo
 */
public class Charge_Code_PageObject {

    public static String Add_Button() {
        return "//div[text()='Add']";
    }
    public static String Charge_Code() {
        return "//label[text()='Charge Code']";
    }
    public static String LabTestDetails() {
        return "(//div[@id='control_A899D7B2-06C7-437E-BD53-7A8E8CD0F8DA']//input)[1]";
    }
    public static String LabTestDetails_TestType() {
        return "(//div[@id='control_A899D7B2-06C7-437E-BD53-7A8E8CD0F8DA']//input)[3]";
    }
    public static String Laboratory_Panel() {
        return "//span[text()='Laboratory']";
    }
    public static String MaintenanceLab_Add_Button() {
        return "//div[text()='Maintenance - Lab']//..//div[text()='Add']";
    }
    public static String MaintenanceRadiology_ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_456DDB8E-4CF6-4324-A828-0D73B314E53D']";
    }
    public static String MaintenanceResultSet_ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_6346CDCD-C59E-4331-A2D2-D52B1E5F8C5A']";
    }
    public static String MaintenanceResultSet_SaveButton() {
        return "//div[@id='btnSave_form_6346CDCD-C59E-4331-A2D2-D52B1E5F8C5A']//div[text()='Save']";
    }
    public static String MaintenanceTestType_AddButton() {
        return "//div[text()='Maintenance - Test type']//..//div[text()='Add']";
    }
    public static String MaintenanceResultSet_AddButton() {
        return "//div[text()='Maintenance - Result set']//..//div[text()='Add']";
    }
    public static String MaintenanceLab_CloseButton() {
        return "(//div[@id='form_D0029D75-2491-4D6F-BBFD-E6D9D0D0D475']//i[@class='close icon cross'])[1]";
    }
    public static String MaintenanceResultSet_CloseButton() {
        return "(//div[@id='form_6346CDCD-C59E-4331-A2D2-D52B1E5F8C5A']//i[@class='close icon cross'])[1]";
    }
    public static String MaintenanceTestType_CloseButton() {
        return "(//div[@id='form_CBF09758-4352-418C-B417-14212578DBDB']//i[@class='close icon cross'])[1]";
    }
    public static String MaintenanceRadiology_CloseButton() {
        return "(//div[@id='form_456DDB8E-4CF6-4324-A828-0D73B314E53D']//i[@class='close icon cross'])[1]";
    }   
    public static String MaintenanceLab_ProcessFlow() {
       return "//div[@id='btnProcessFlow_form_D0029D75-2491-4D6F-BBFD-E6D9D0D0D475']";
    }
    public static String MaintenanceTestType_ProcessFlow() {
       return "//div[@id='btnProcessFlow_form_CBF09758-4352-418C-B417-14212578DBDB']";
    }
    public static String MaintenanceLab_Record() {
        return "(//div[@id='control_7D9B22B2-19FC-4C18-8ADF-F0BEBDB897FD']//div//table)[3]";
    }
    public static String MaintenanceResultSet_Record() {
        return "(//div[@id='control_FDA44C35-02E9-4530-B53F-637C6B5C3F51']//div//table)[3]";
    }
    public static String MaintenanceRadiology_Record(String data) {
        return "(//div[@id='control_51764267-4ECB-4223-B334-66C7B2A9B8D6']//div//table)[3]//div[text()='" + data + "']";
    }
    public static String MaintenanceRadiology_Add_Button() {
        return "//div[text()='Maintenance - Radiology']//..//div[text()='Add']";
    }
    public static String Maintenance_TestType() {
        return "//div[text()='Maintenance - Test type']";
    }
    public static String Maintenance_ResultSet() {
        return "//div[text()='Maintenance - Result set']";
    }
    public static String RadiologyDetails() {
        return "(//div[@id='control_958D0095-92F1-45C7-8912-9087DC89CB12']//input)[1]";
    }
    public static String Radiology_Panel() {
        return "//span[text()='Radiology']";
    }
    public static String Occupational_Health() {
        return "//label[text()='Occupational Health']";
    }
    public static String Patient_File_Maintenance() {
        return "//label[text()='Patient File Maintenance']//..//i";
    }
    public static String Process_Flow_Button() {
        return "//div[@id='btnProcessFlow_form_622A9927-6689-4116-B521-244D36189319']";
    }
    public static String ResultSet() {
        return "(//div[@id='control_599EA408-079F-4003-BCF0-2F185C01E96F']//input)[1]";
    }
    public static String Type_Of_Test_Dropdown() {
        return "//div[@id='control_10275A0F-6E1A-4B14-A828-0CCA46FA7F46']";
    }
    public static String Type_Of_Test_Option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String saveBtn() {
        return "//div[@id='btnSave_form_622A9927-6689-4116-B521-244D36189319']//div[text()='Save']";
    }
    public static String MaintenanceLab_SaveButton(){
        return "//div[@id='btnSave_form_D0029D75-2491-4D6F-BBFD-E6D9D0D0D475']//div[text()='Save']";
    }
    public static String MaintenanceRadiology_SaveButton(){
        return "//div[@id='btnSave_form_456DDB8E-4CF6-4324-A828-0D73B314E53D']//div[text()='Save']";
    }
    public static String MaintenanceTestType_SaveButton(){
        return "//div[@id='btnSave_form_CBF09758-4352-418C-B417-14212578DBDB']//div[text()='Save']";
    }
    public static String validateSave() {
        return "//div[@class='ui floating icon message transition visible']";
    }
    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }
    
}
