/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Locations_V5_PageObjects;

import KeywordDrivenTestFramework.Testing.PageObjects.Inspection_Scheduler_V5_2_PageObjects.*;

/**
 *
 * @author skhumalo
 */
public class Locations_PageObjects
{

    public static String InspectionScheduler_Module;

    public static String navigate_EHS()
    {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }

    public static String LeadershipParticipationMaintenance()
    {
        return "//div[@original-title='Leadership & Participation Maintenance']//i[@class='icon settings size-five cool grey module iso-icon row']";
    }

    public static String Locations_Module()
    {
        return "//label[text()='Locations']";
    }

    public static String Locations_Add()
    {
        return "//div[@id='btnActAddNew']//div[text()='Add']";
    }

    public static String Locations_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_A29F9117-9AF4-4507-9D7C-7B3196998442']";
    }

    public static String Country()
    {
        return "//div[@id='control_D4363AE9-BD90-44B9-B800-91E16ADCFC94']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String CountryLatitude()
    {
        return "//div[@id='control_495260C5-AF73-4364-B9E3-6AAE0DF7799F']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String CountryLongitude()
    {
        return "//div[@id='control_81A070D4-520B-4092-AEE2-E05425C40F69']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String CountrySaveButton()
    {
        return "//div[@id='btnSave_form_A29F9117-9AF4-4507-9D7C-7B3196998442']";
    }

    public static String saveWait2()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }

    public static String validateSave()
    {
        return "//div[@class='ui floating icon message transition visible']";
    }

    public static String StateOrProvince_Add()
    {
        return "//div[@id='btnAddNew']//div[text()='Add']";
    }

    public static String StateOrProvince_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_87A233E6-C667-473A-8ABB-2E8EE42C4E3F']";
    }

    public static String StateOrProvince()
    {
        return "//div[@id='control_8596E9E4-A0E5-4B45-A6DB-23CA9FE8C450']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String ProvinceLatitude()
    {
        return "//div[@id='control_897AD1E2-2353-4269-9045-9CB71AC55F07']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String ProvinceLongitude()
    {
        return "//div[@id='control_AE152E50-0408-4260-B059-3D4D29FCC2C4']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String ProvinceSaveButton()
    {
        return "//div[@id='btnSave_form_87A233E6-C667-473A-8ABB-2E8EE42C4E3F']";
    }

    public static String CityOrTown_Add()
    {
        return "//div[@id='control_75B59F2E-DCEB-4182-87BD-99CFF6E00A12']//div[text()='Add']";
    }

    public static String CityOrTown_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_DA1D5612-2E0A-41E0-994F-6CECDC458849']";
    }

    public static String CityOrTown()
    {
        return "//div[@id='control_97569B31-DFE5-4651-AEA8-A1BF2E49D3D1']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String CityOrTownLatitude()
    {
        return "//div[@id='control_B79560B4-F74B-4541-B71A-4975DB09B881']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String CityOrTownLongitude()
    {
        return "//div[@id='control_DDE6A6AE-7745-445C-8C8F-775CC418ED05']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String CityOrTownSaveButton()
    {
        return "//div[@id='btnSave_form_DA1D5612-2E0A-41E0-994F-6CECDC458849']";
    }

    public static String Community_Add()
    {
        return "//div[@id='control_EF0DB689-7A76-404A-8AE3-F20ADB2FE25B']//div[text()='Add']";
    }

    public static String Community()
    {
        return "//div[@id='control_25C1C79D-CCF6-4EA1-A08B-55CB136105B9']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String CommunityLatitude()
    {
        return "//div[@id='control_0C133FD5-0DE3-4A11-8B9B-ED708A43E621']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String CommunityLongitude()
    {
        return "//div[@id='control_D8ECE2FA-FB26-4BD9-ABEB-B5B18F6391B3']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }


    
    
    

}
