/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Incident_Classification_V5_2_PageObjects;

/**
 *
 * @author SKhumalo
 */
public class Incident_Classification {

    public static String BusinessUnitHeader() {
        return "//div[text()='Business unit']";
    }
    public static String IncidentClassification_Header() {
        return "//div[text()='Incident Classification']";
    }
    public static String iframeXpath() {
        return "//iframe[@id='ifrMain']";
    }
    public static String searchBtn() {
        return "//div[@id='searchOptions']//div[text()='Search']";
    }
    public static String viewFullReportsIcon() {
        return "//span[@title='Full report ']";
    }
    public static String reportsBtn() {
        return "//div[@id='btnReports']";
    }
    public static String viewReportsIcon() {
        return "//span[@title='View report ']";
    }
    public static String continueBtn() {
        return "//div[@id='btnConfirmYes']";
    }
    public static String ISC_addButton() {
        return "//div[text()='Incident Sub Classification']//..//div[text()='Add']";
    }
    public static String IncidentSubClassification() {
        return "(//div[@id='control_E35FB793-02FF-4BD4-A395-DF4D36FEC01C']//input)[1]";
    }
    public static String IncidentSubClassification_Panel() {
        return "//div[text()='Incident Sub Classification']";
    }
    public static String SaveButton() {
        return "//div[@id='btnSave_form_465BB88B-8629-4B97-ABD0-B7297DB61037']";
    }
    public static String SaveToContinue_SaveBtn() {
        return "//div[@id='control_AB3A9568-C6D4-4D4A-A42A-51CD6E16DFC1']";
    }
    public static String validateSave() {
        return "//div[@class='ui floating icon message transition visible']";
    }
    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }
    public static String BusinessUnit_Dropdown() {
        return "//div[@id='control_4053AB99-95CB-4A4D-9F21-C426543BFEBC']";
    }
    public static String BusinessUnit_Option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i[@class='jstree-icon jstree-ocl']";
    }
    public static String BusinessUnit_Option1(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i[@class='jstree-icon jstree-checkbox']";
    }
    public static String BusinessUnit_SelectAll() {
        return "//div[@id='control_EF6D597C-65B4-472D-AE0C-31DD1C9B8B7A']//b[@class='select3-all']";
    }
    public static String ImpactType_Dropdown() {
        return "//div[@id='control_EF6D597C-65B4-472D-AE0C-31DD1C9B8B7A']";
    }
    public static String ImpactType_Option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i[@class='jstree-icon jstree-checkbox']";
    }
    public static String IncidentClassification_Input() {
        return "(//div[@id='control_D6596FC1-A555-43F0-992E-B17EBC7DE47B']//input)[1]";
    }
    public static String addButton() {
        return "//div[@id='btnActAddNew']";
    }
    public static String navigate_EHS() {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }
    public static String navigate_ImproveMaintenance() {
        return "//label[text()='Improve Maintenance']//..//i";
    }
    public static String navigate_IncidentClassification() {
        return "//label[text()='Incident Classification']";
    }
    public static String process_flow() {
        return "//div[@id='btnProcessFlow_form_465BB88B-8629-4B97-ABD0-B7297DB61037']";
    }
    
}
