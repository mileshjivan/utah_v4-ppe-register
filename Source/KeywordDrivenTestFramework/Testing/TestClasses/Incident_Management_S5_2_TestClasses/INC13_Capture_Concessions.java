/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "13-Capture Concessions",
        createNewBrowserInstance = false
)
public class INC13_Capture_Concessions extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    String recordNumber2;
    String concessionRecordNumber;
    String concessionActionRecordNumber;
    
    public INC13_Capture_Concessions() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }
    
    public TestResult executeTest() {
        if (!CaptureConcessions()) {
            return narrator.testFailed("Capture concessions - " + error);
        }
        
        if (!AddConcessionsActions()) {
            return narrator.testFailed("add concessions action - " + error);
        }
        
        if (!openConcessionAction()) {
            return narrator.testFailed("open concession action- " + error);
        }
        
        if (!actionFeedback()) {
            return narrator.testFailed("Action feedback - " + error);
        }
        
        if (!actionSignOff()) {
            return narrator.testFailed("action sign off - " + error);
        }
        
        if(!validateConcession()){
            return narrator.testFailed("Validation - " + error);
        }
        
        return narrator.finalizeTest("Completed Capturing Concessions - Main Scenario");
    }
    
    public boolean CaptureConcessions() {
        //Quality tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessions_QualityTab_xpath())) {
            error = "Failed to wait for Quality tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessions_QualityTab_xpath())) {
            error = "Failed to click Quality tab.";
            return false;
        }
        
        //Quality concession expand
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessions_QualityConcession_Expandxpath())) {
            error = "Failed to wait for Quality concession expand button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessions_QualityConcession_Expandxpath())) {
            error = "Failed to click Quality concession expand button.";
            return false;
        }
        
        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessions_Add_Buttonxpath())) {
            error = "Failed to wait for concession Add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessions_Add_Buttonxpath())) {
            error = "Failed to click concession Add button.";
            return false;
        }

        //handle the save mask
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.activeConcessionPage(), 10)) {
            error = "Failed to wait for concession to be active";
            return false;
        }
        
        //concession Description
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessions_Description_TextAreaxpath())) {
            error = "Failed to wait for concession Description Text Field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.concessions_Description_TextAreaxpath(), testData.getData("Description"))) {
            error = "Failed to EnterText into concession Description Text Field.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessions_Impact_TextAreaxpath())) {
            error = "Failed to wait for concession Impact Text Field.";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.concessions_Impact_TextAreaxpath(), testData.getData("Impact"))) {
            error = "Failed to EnterText into concession Impact Text Field.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessions_AllPartiesAffected_CheckBoxxpath())) {
            error = "Failed to wait for All Parties Accfected CheckBox.";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessions_AllPartiesAffected_CheckBoxxpath())) {
            error = "Failed to click All Parties Accfected CheckBox.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessions_CorrectiveAction_TextAreaxpath())) {
            error = "Failed to wait for concession Corrective Action Text Field.";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.concessions_CorrectiveAction_TextAreaxpath(), testData.getData("Action"))) {
            error = "Failed to EnterText into concession Corrective Action Text Field.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessions_ResponsiblePerson_SelectFieldxpath())) {
            error = "Failed to wait for Responsible Person Select dropdawon list.";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessions_ResponsiblePerson_SelectFieldxpath())) {
            error = "Failed to click for Responsible Person Select dropdawon list.";
            return false;
        }

//        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.concessions_ResponsiblePerson_TypeSearchxpath(), testData.getData("ResponsiblePerson"))) {
//            error = "Failed to EnterText into Responsible Person type search.";
//            return false;
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.responsiblePerson(getData("ResponsiblePerson")))) {
            error = "Failed to wait for Responsible Person: " + testData.getData("ResponsiblePerson");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.responsiblePerson(getData("ResponsiblePerson")))) {
            error = "Failed to click Responsible Person: " + testData.getData("ResponsiblePerson");
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessions_StartDate_InputAreaxpath())) {
            error = "Failed to wait for concession Start date Input Field.";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.concessions_StartDate_InputAreaxpath(), startDate)) {
            error = "Failed to EnterText into concession Start date Input Field.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessions_EndDate_InputAreaxpath())) {
            error = "Failed to wait for concession End date Input Field.";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.concessions_EndDate_InputAreaxpath(), endDate)) {
            error = "Failed to EnterText into concession End date Input Field.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessions_Priority_SelectFieldxpath())) {
            error = "Failed to wait for Priority Select dropdown list.";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessions_Priority_SelectFieldxpath())) {
            error = "Failed to click Priority Select dropdown list.";
            return false;
        }

//        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.concessions_Priority_TypeSearchxpath(), testData.getData("Priority"))) {
//            error = "Failed to EnterText into Priority type search.";
//            return false;
//        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.responsiblePerson(testData.getData("Priority")))) {
            error = "Failed to click Priority: " + testData.getData("ResponsiblePerson");
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessions_Process_Buttonxpath())) {
            error = "Failed to wait for Process Button.";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessions_Process_Buttonxpath())) {
            error = "Failed to click Process Button.";
            return false;
        }
        
        pause(300);
        narrator.stepPassedWithScreenShot("Successfully Entered Concession Details.");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessions_SaveToContinue_Buttonxpath())) {
            error = "Failed to wait for Save To Continue Button.";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessions_SaveToContinue_Buttonxpath())) {
            error = "Failed to click Save To Continue Button.";
            return false;
        }
        
        pause(5000);
        String retrieveRecordNo = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.retrieveConcessionRecordNumberXPath());
        narrator.stepPassed("Successfully retrieved and stored text: " + retrieveRecordNo);
        recordNumber2 = retrieveRecordNo.substring(10);
        
        String retrieveConcessionStatus = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.Concession_Status_XPath());
        narrator.stepPassed("Successfully retrieved and stored text: " + retrieveConcessionStatus);
        if (!retrieveConcessionStatus.equalsIgnoreCase("Logged")) {
            error = "Failed to Validate Concession Status is Logged.";
        } else {
            narrator.stepPassedWithScreenShot("Successfully saved Concession record, status: " + retrieveConcessionStatus);
        }
        
        return true;
    }
    
    private boolean AddConcessionsActions() {
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessionsAction_Add_Buttonxpath())) {
            pause(8000);
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessionsAction_Add_Buttonxpath())) {
                error = "Failed to wait for Concession Action ADD Button.";
                return false;
            }
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessionsAction_Add_Buttonxpath())) {
            error = "Failed to click Concession Action ADD  Button.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessionActionActive(), 10)) {
            error = "Failed to wait for Concession Action active 1.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.flowProcessXpath())) {
            error = "Failed to wait for Concession Actions flow process.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.flowProcessXpath())) {
            error = "Failed to click Concession Actions flow process.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessionsAction_Description_Textfieldxpath())) {
            error = "Failed to wait for Concession Action ADD Button.";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.concessionsAction_Description_Textfieldxpath(), testData.getData("ActionDescription"))) {
            error = "Failed to Enter Text onto Concession Action Description text field.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessionsAction_DepartmentResponsible_Buttonxpath())) {
            error = "Failed to wait for Concession Action Department Responsible select field.";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessionsAction_DepartmentResponsible_Buttonxpath())) {
            error = "Failed to click Concession Action Department Responsible select field.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.responsiblePerson(testData.getData("ActionDepartmentResponsible")))) {
            error = "Failed to wait for Concession Action Department Responsible: " + testData.getData("ActionDepartmentResponsible");
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.responsiblePerson(testData.getData("ActionDepartmentResponsible")))) {
            error = "Failed to click Concession Action Department Responsible: " + testData.getData("ActionDepartmentResponsible");
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessionsAction_ResponsiblePerson_SelectFieldxpath())) {
            error = "Failed to wait for Concession Action Responsible Person Select dropdown list.";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessionsAction_ResponsiblePerson_SelectFieldxpath())) {
            error = "Failed to click Concession Action Responsible Person Select dropdown list.";
            return false;
        }

//        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.concessionsAction_ResponsiblePerson_TypeSearchxpath(), testData.getData("ActionResponsiblePerson"))) {
//            error = "Failed to EnterText into Concession Action Responsible Person type search.";
//            return false;
//        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.responsiblePerson(testData.getData("ActionResponsiblePerson")))) {
            error = "Failed to click Concession Action Responsible Person: " + testData.getData("ActionResponsiblePerson");
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessionsAction_ActionDueDate_Textfieldxpath())) {
            error = "Failed to wait for Concession Action Due Date Text Field.";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.concessionsAction_ActionDueDate_Textfieldxpath(), startDate)) {
            error = "Failed to Enter Text onto Concession Action Due Date text field.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessionsAction_Save_Buttonxpath())) {
            error = "Failed to wait for Concession Action Save  button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessionsAction_Save_Buttonxpath())) {
            error = "Failed to click on Concession Action Save  button";
            return false;
        }
        
        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait())){
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2())){
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.toBeInitiatedXpath())) {
            error = "Failed to wait for to Be Initiated ";
            return false;
        }
        
        concessionActionRecordNumber = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.getConcessionActionRecordID()).split("#")[1];
        
        narrator.stepPassedWithScreenShot("Successfully saved concesssion actions" + concessionActionRecordNumber);
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessionActionsCloseButton())) {
            error = "Failed to click on Concession Action  close button";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.inProgress(), 10)) {
            error = "Failed to move concesssion In Progress";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.allAffectedPartiesCheckbox())) {
            error = "Failed to click on All affected parties have agreed to the proposed corrective action and the duration of this concession button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.sighOffConcessionCheckbox())) {
            error = "Failed to click on Sigh Off Concession Checkbox";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.savebutton())) {
            error = "Failed to click on Concession Action Save  button";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully moved concesssion In Progress.");
        return true;
    }
    
    private boolean openConcessionAction() {
        
        SeleniumDriverInstance.pause(2500);
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessionsAction_OpenSsavedConcessionRecord_SelectField(concessionActionRecordNumber), 10)) {
            error = "Failed to wait for Concession Action Record: #" + concessionActionRecordNumber;
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessionsAction_OpenSsavedConcessionRecord_SelectField(concessionActionRecordNumber))) {
            error = "Failed to click on Concession Action Record: #" + concessionActionRecordNumber;
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessionActionActive(), 10)) {
            error = "Failed to wait for Concession Action active 2";
            return false;
        }
        
        SeleniumDriverInstance.pause(2500);
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.flowProcessXpath())) {
            error = "Failed to click Concession Actions flow process. 1";
            return false;
        }
        
        SeleniumDriverInstance.pause(2500);
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.pinProcessFlowConcessionActions())) {
            error = "Failed to click for action feedback pin process flow";
            return false;
        }
        return true;
    }
    
    private boolean actionFeedback() {
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.actionFeedbackTab())) {
            error = "Failed to click for action feedback Tab";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessionActionFeedbackAdd())) {
            error = "Failed to click for action feedback add button";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.actionFeedbackActiveForm(), 10)) {
            error = "Failed to wait for action feedback ";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.actionFeedbackFlowProcessButton())) {
            error = "Failed to click for action feedback process flow";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.actionFeedbackTextArea(), getData("Action Feedback"))) {
            error = "Failed to enter  action feedback textarea";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.actionFeedbackActionCompleteDropdown())) {
            error = "Failed to click for action feedback - Action Complete Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anySupervisorXpath(getData("Action complete")))) {
            error = "Failed to click for action feedback - Action Complete ";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.actionFeedbackSave())) {
            error = "Failed to click for action feedback - Save button ";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.activeEditPhaseXpath(), 20)) {
            error = "Failed to click for action feedback - Save record ";
            return false;
        }
        
        String feedback = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.getConcessionActionFeedbackRecordID()).split("#")[1];
        
        narrator.stepPassedWithScreenShot("Successfully saved Action Feedback " + feedback);
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessionActionFeedbackCloseButton())) {
            error = "Failed to click for action feedback - close button ";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessions_OpenRecord_Selectxpath(feedback))) {
            error = "Failed to wait for Concession Record: #" + feedback;
            return false;
        }
        return true;
    }
    
    private boolean actionSignOff() {
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.signOffTab())) {
            error = "Failed to click for action Sigh off Tab";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessionSighOffAdd())) {
            error = "Failed to click for action sigh off add button";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.actionSignOffActiveForm(), 10)) {
            error = "Failed to wait for action sigh off ";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.actionSignOffFlowProcessButton())) {
            error = "Failed to click for action sigh off process flow";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.actionSignOffDropdown())) {
            error = "Failed to click for action Sigh off action? Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anySupervisorXpath(getData("Action complete")))) {
            error = "Failed to click for action Sigh off action? ";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.actionSignOffCommentTextArea(), getData("Comments"))) {
            error = "Failed to enter  action sigh off textarea";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.actionOffSave())) {
            error = "Failed to click for action sigh off - Save button ";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.SignOffactiveEditPhaseXpath(), 10)) {
            error = "Failed to click for action sigh off - Save record ";
            return false;
        }
        
        String sighOff = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.getConcessionActionSighOffRecordID()).split("#")[1];
        
        narrator.stepPassedWithScreenShot("Successfully saved Action sigh off " + sighOff);
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessionActionsSighOffCloseButton())) {
            error = "Failed to click for action sigh off - close button ";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.activeConcessionActions())) {
            error = "Failed to wait Concession actions page to be acitve ";
            return false;
        }
        
        SeleniumDriverInstance.pause(2500);
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessionsAction_Save_Buttonxpath())) {
            error = "Failed to click for Concession Action Save  button";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.activeClosedPhaseXpath(), 10)) {
            error = "Failed to wait Concession to move to closed  ";
            return false;
        }
        
        SeleniumDriverInstance.pause(2500);
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessionActionsCloseButton())) {
            error = "Failed to click on Concession Action  close button";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessionsAction_OpenSsavedConcessionRecord_SelectField(concessionActionRecordNumber), 10)) {
            error = "Failed to wait for Concession Action Record: #" + concessionActionRecordNumber;
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.statusCompleted(), 10)) {
            error = "Failed to move Concession Action to Completed";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Completed concessions actions ");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessions_Process_Buttonxpath())) {
            error = "Failed to wait for Process Button.";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessions_Process_Buttonxpath())) {
            error = "Failed to click Process Button.";
            return false;
        }
        
        pause(300);
        
        return true;
    }
    
    public boolean validateConcession(){
        String concession_RecordNo = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.getConcessionRecordID()).split("#")[1];
        
        //Close ConcessionRecord
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.closeConcession())){
            error = "Failed to wait for concession X close.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.closeConcession())){
            error = "Failed to click for concession X close.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully closed Incident Management -> Concession");
        
        pause(5000);
        //2.Verification & Additional Detail
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.verificationAndAddDetTab())){
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.verificationAndAddDetTab())){
                error = "Failed to wait for the '2.Verification & Additional Detail' tab";
                return false;
            }
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.verificationAndAddDetTab())){
            error = "Failed to click the '2.Verification & Additional Detail' tab";
            return false;
        }
        pause(2000);
        
        //Quality tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessions_QualityTab_xpath())) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.concessions_QualityTab_xpath())) {
                error = "Failed to wait for Quality tab.";
                return false;
            }
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessions_QualityTab_xpath())) {
            error = "Failed to click Quality tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Incident Management");
        
        //Quality concession expand
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessions_QualityConcession_Expandxpath())) {
            error = "Failed to wait for Quality concession expand button.";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.concessions_QualityConcession_Expandxpath())) {
            error = "Failed to scroll to Quality concession expand button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessions_QualityConcession_Expandxpath())) {
            error = "Failed to click Quality concession expand button.";
            return false;
        }
        
        //Click the process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.processFlow())){
            error = "Failed to wait for process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.processFlow())){
            error = "Failed to click process flow.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Incident Management process flow");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessionsRecord_Gridview(concession_RecordNo), 10)) {
            error = "Failed to wait for Concession Record: #" + concession_RecordNo;
            return false;
        }
        pause(2000);
        
        return true;
    }
}
