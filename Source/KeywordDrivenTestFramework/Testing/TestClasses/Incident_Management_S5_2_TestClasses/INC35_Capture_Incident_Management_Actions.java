/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "35-Capture Incident Management Actions",
        createNewBrowserInstance = false
)

public class INC35_Capture_Incident_Management_Actions extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;
    String path;

    public INC35_Capture_Incident_Management_Actions() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        String pathofImages = System.getProperty("user.dir") + "\\images";
        path = new File(pathofImages).getAbsolutePath();

    }

    public TestResult executeTest() {
        if (!Capture_Incident_Management_Actions()) {
            return narrator.testFailed("Failed to Incident Actions details - " + error);
        }

        return narrator.finalizeTest("Successfully Incident Actions record is saved.");
    }

    public boolean Capture_Incident_Management_Actions() {
        //Actions Tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.actionTab())) {
            error = "Failed to wait for - Incident Managament - actions tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.actionTab())) {
            error = "Failed to click on - Incident Managament - actions tab ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked - Incident Managament - actions tab");

        //Incident Managament - Incident Actions Add
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentActionAdd())) {
            error = "Failed to wait for - Incident Managament - Incident Actions Add ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentActionAdd())) {
            error = "Failed to click on - Incident Managament - Incident Actions Add ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Incident Managament - Incident Actions Add");

        //Incident Actions - Process Flow button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentActionProcessButton())) {
            error = "Failed to wait on - Incident Actions - Process Flow button ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentActionProcessButton())) {
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentActionProcessButton())) {
                error = "Failed to wait on - Incident Actions - Process Flow button ";
                return false;
            }
            pause(8000);
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentActionProcessButton())) {
                error = "Failed to click on - Incident Actions - Process Flow button";
                return false;
            }
        }

        //Action description
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.actionDescription(), getData("Action description"))) {
            error = "Failed to enter on - Incident Actions - Action description ";
            return false;
        }

        //Incident Actions - Department Responsible
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.departmentResponsibleDropdown())) {
            error = "Failed to click on - Incident Actions - Department Responsible dropdown";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.expandGlobalCompany_IncidentActions())){
            error = "Failed to click the Department responsible - Global Company.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.expandSouthAfrica_IncidentActions())){
            error = "Failed to wait for Department responsible - South Africa.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.anyDropdwonOptionXpath(getData("Department responsible")))) {
            error = "Failed to wait for - Incident Actions - Department Responsible.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyDropdwonOptionXpath(getData("Department responsible")))) {
            error = "Failed to click on - Incident Actions - Department Responsible.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Incident Actions - Department Responsible: " + getData("Department responsible"));

        //Responsible person dropdown
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.responsiblePersonDropdown())) {
            error = "Failed to click on - Incident Actions - Responsible person dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.responsiblePerson(getData("Responsible person")))) {
            error = "Failed to wait for - Incident Actions - Responsible person ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.responsiblePerson(getData("Responsible person")))) {
            error = "Failed to click on - Incident Actions - Responsible person ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Incident Actions - Responsible person: " + getData("Responsible person"));

        //Incident Actions - Action due date
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.actionDueDate(), date)) {
            error = "Failed to enter on - Incident Actions - Action due date ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Incident Actions - Action due date: " + date);

        //Incident Actions - Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentActionsSaveButton())) {
            error = "Failed to wait for - Incident Actions - Save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentActionsSaveButton())) {
            error = "Failed to click on - Incident Actions - Save";
            return false;
        }
        
        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects.saveWait2(), 400)){
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        
        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects.validateSave());
        
        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat+": successfully."); 
       
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.activeToBeInitiatedXpath())) {
            error = "Failed to save on - Incident Actionss record";
            return false;
        }

        String[] Actionid = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.incidentActionId()).split("#");
        setRecordId(Actionid[1]);

        narrator.stepPassed("Control Incident Actions Record Number: " + Actionid[1]);
        narrator.stepPassedWithScreenShot("Successfulled saved Incident Actions ");

        //Incident Management - Incident Actions - Close Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentManagement_IncidentActions_CloseButton())){
            error = "Failed to wait for Incident Management - Incident Actions - Close Button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IncidentManagement_IncidentActions_CloseButton())){
            error = "Failed to click Incident Management - Incident Actions - Close Button";
            return false;
        }
        pause(2000);
        
        //Incident Management - Incident Actions Gridview Record
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.IncidentManagement_IncidentActions_Gridview())){
            error = "Failed to wait for Incident Management - Incident Actions Gridview Record";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.IncidentManagement_IncidentActions_Gridview())){
            error = "Failed to click for Incident Management - Incident Actions Gridview Record";
            return false;
        }

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IM_processFlow())) {
            error = "Fail to wait for Incident Management process flow";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IM_processFlow())) {
            error = "Fail to click Incident Management process flow";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Injury Details process flow.");
        pause(2000);
        
        return true;
    }

}
