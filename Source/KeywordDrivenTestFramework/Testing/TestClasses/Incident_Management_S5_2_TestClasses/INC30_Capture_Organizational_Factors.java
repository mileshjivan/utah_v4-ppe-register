/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "30-Capture Organizational Factors",
        createNewBrowserInstance = false
)

public class INC30_Capture_Organizational_Factors extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;

    public INC30_Capture_Organizational_Factors() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Capture_Organizational_Factors()) {
            return narrator.testFailed(error);
        }

        return narrator.finalizeTest("Successfully Captured Capture Organizational Factors");
    }

    public boolean Capture_Organizational_Factors(){
        //Organizational Factors panel
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Organizational_Factors_Panel())){
            error = "Failed to wait for the Organizational factors panel.";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.Organizational_Factors_Panel())){
            error = "Failed to scroll to the Organizational factors panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Organizational_Factors_Panel())){
            error = "Failed to clicked the Organizational factors panel.";
            return false;
        }
        narrator.stepPassed("Organizational factors panel is displayed");
        
        //Organizational factors
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.OrganizationalFactors_Option(getData("Organizational factors 1")))) {
            error = "Failed to wait for the Organizational factors option: " + getData("Organizational factors 1");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Incident_Management_PageObjects.OrganizationalFactors_Option(getData("Organizational factors 1")))) {
            error = "Failed to click on the Organizational factors option: " + getData("Organizational factors 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.OrganizationalFactors_Option1(getData("Organizational factors 2")))) {
            error = "Failed to click on the Organizational factors option: " + getData("Organizational factors 2");
            return false;
        }
        narrator.stepPassedWithScreenShot("Organizational factors option: " + getData("Organizational factors 1") + " -> " + getData("Organizational factors 2") + ".");

        //Organizational Factors details 
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.OrganizationalFactorsDetails(), getData("Organizational factors details"))) {
            error = "Failed to enter the Organizational factors details: " + getData("Organizational factors details");
            return false;
        }
        narrator.stepPassedWithScreenShot("Organizational factors details: " + getData("Organizational factors details"));
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveButton())) {
            error = "Failed to wait for the Organizational factors details - save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.saveButton())) {
            error = "Failed to click on the Organizational factors details - save";
            return false;
        }
        
        //Save Mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 40)){
            error = "Webside too long to load wait reached the time out";
            return false;
        }        

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Management_PageObjects.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");    

        return true;
    }
}
