/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "8-Capture Injury Details",
        createNewBrowserInstance = false
)

public class INC8_Capture_Injury_Details extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public INC8_Capture_Injury_Details() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!Capture_Injury_Details()) {
            return narrator.testFailed(error);
        }
 
        return narrator.finalizeTest("Completed Injury Details");
    }

    public boolean Capture_Injury_Details() {
        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.injuredPersonPanel_Record())){
            error = "Failed to wait for witness Statements record";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.injuredPersonPanel_Record())){
            error = "Failed to click on witness Statements record";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the record created.");
        
        pause(3000);
        //Injury Details Panel
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.injuryDetails_Panel())){
            error = "Failed to wait for 'Injury Details' panel";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.injuryDetails_Panel())){
            error = "Failed to scroll on 'Injury Details' panel";
            return false;
        }
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.injuredPersonsFlow())) {
            error = "Fail to wait for Injured Details process flow";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.injuredPersonsFlow())) {
            error = "Fail to click Injured Details process flow";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Injury Details process flow.");
        
        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.injuryDetails_AddButton())) {
            error = "Fail to wait for Injury Details Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.injuryDetails_AddButton())) {
            error = "Fail to click Injury Details Add Button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Injury Details Add Button.");
        
        //Body part(s)
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.InjuryDetails_BodyPart())){
            error = "Failed to wait for 'Body part(s)' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.InjuryDetails_BodyPart())){
            error = "Failed to wait for 'Body part(s)' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.bodyPart_Select(getData("Body part")))){
            error = "Failed to wait for '" + getData("Body part") + "' from 'Body part(s)' dropdown.";
            return false;
        }
        pause(2000);
        if(!SeleniumDriverInstance.doubleClickElementbyXpath(Incident_Management_PageObjects.bodyPart_Select(getData("Body part")))){
            error = "Failed to click '" + getData("Body part") + "' from 'Body part(s)' dropdown.";
            return false;
        }
        pause(2000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.bodyPart_Select(getData("Body part 2")))){
            error = "Failed to click '" + getData("Body part 2") + "' from 'Body part(s)' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Body part(s): '" + getData("Body part") + "' -> '" + getData("Body part 2") + "'.");
            
        //Nature of injury
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.InjuryDetails_NatureOfInjury())){
            error = "Failed to wait for 'Nature of injury' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.InjuryDetails_NatureOfInjury())){
            error = "Failed to wait for 'Nature of injury' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.NatureOfInjury_Select(getData("Nature of injury")))){
            error = "Failed to wait for '" + getData("Nature of injury") + "' from 'Nature of injury' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.doubleClickElementbyXpath(Incident_Management_PageObjects.NatureOfInjury_Select(getData("Nature of injury")))){
            error = "Failed to click '" + getData("Nature of injury") + "' from 'Nature of injury' dropdown.";
            return false;
        }
        pause(2000);
        if(!SeleniumDriverInstance.doubleClickElementbyXpath(Incident_Management_PageObjects.NatureOfInjury_Select(getData("Nature of injury 2")))){
            error = "Failed to click '" + getData("Nature of injury 2") + "' from 'Nature of injury' dropdown.";
            return false;
        }
        pause(2000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.NatureOfInjury_Select(getData("Nature of injury 3")))){
            error = "Failed to click '" + getData("Nature of injury 3") + "' from 'Nature of injury' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Nature of injury: '" + getData("Nature of injury") + "' -> '" + getData("Nature of injury 2") + "' -> '" + getData("Nature of injury 3") + "'.");
            
        //Mechanism
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.InjuryDetails_Mechanism())){
            error = "Failed to wait for 'Mechanism' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.InjuryDetails_Mechanism())){
            error = "Failed to wait for 'Mechanism' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Mechanism_Select(getData("Mechanism")))){
            error = "Failed to wait for '" + getData("Mechanism") + "' from 'Mechanism' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.doubleClickElementbyXpath(Incident_Management_PageObjects.Mechanism_Select(getData("Mechanism")))){
            error = "Failed to click '" + getData("Mechanism") + "' from 'Mechanism' dropdown.";
            return false;
        }
        pause(2000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Mechanism_Select(getData("Mechanism 2")))){
            error = "Failed to click '" + getData("Mechanism 2") + "' from 'Mechanism' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Mechanism: '" + getData("Mechanism") + "' -> '" + getData("Mechanism 2") + "'.");

        //Save button
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.save_Button())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.save_Button())){
            error = "Failed to click 'Save' button.";
            return false;
        }
        
       if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait())){
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2())){
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        //Check if the record has been Saved
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.inspection_RecordSaved_popup())){
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.inspection_RecordSaved_popup());
        
        if(saved.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        }else{   
            if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.failed())){
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.failed());

            if(failed.equals("ERROR: Record could not be saved")){
                error = "Failed to save record.";
                return false;
            }
        }
        pause(2000);
        return true;
    }
    
}
