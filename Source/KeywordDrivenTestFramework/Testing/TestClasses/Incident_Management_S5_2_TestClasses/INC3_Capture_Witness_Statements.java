/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "3-Capture Witness Statements",
        createNewBrowserInstance = false
)

public class INC3_Capture_Witness_Statements extends BaseClass{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public INC3_Capture_Witness_Statements(){
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest(){
        if (!ValidatePanelsAreDisplayed()){
            return narrator.testFailed("Failed to validate that the Persons Involved, Witness Statement and Equipment Involved panels are displayed - " + error);
        }
        if (!witnessStatement()){
            return narrator.testFailed("Failed to fill in incident report - " + error);
        }
        return narrator.finalizeTest("Successfully captured the details for Witness Statements");
    }

    public boolean ValidatePanelsAreDisplayed(){
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.PersonsInvolvedXpath())){
            error = "Failed to validate that the Persons Involved panel is dispayed";
            return false;
        } else {
            String text = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.PersonsInvolvedXpath());
            
            if (text.equals(" ")){
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Persons Involved"))){
                narrator.stepPassed("Successfully validated that the Persons Involved panel is displayed: " + text);
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.WitnessStatementsXpath())){
            error = "Failed to validate that the Witness Statement panel is dispayed";
            return false;
        } else {
            String text = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.WitnessStatementsXpath());

            if (text.equals(" ")){
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Witness Statements"))){
                narrator.stepPassed("Successfully validated that the Witness Statement panel is displayed: " + text);
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.EquipmentInvolvedTabXpath())){
            error = "Failed to validate that the Equipment Involved panel is dispayed";
            return false;
        } else {
            String text = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.EquipmentInvolvedTabXpath());

            if (text.equals(" ")){
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Equipment Involved"))) {
                narrator.stepPassed("Successfully validated that the Equipment Involved panel is displayed: " + text);
            }
        }

        narrator.stepPassedWithScreenShot("Successfully validated that the Persons Involved, Witness Statement and Equipment Involved panels are displayed");
        return true;
    }

    public boolean witnessStatement(){
        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait(), 1)){
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 40)){
                error = "Webside too long to load wait reached the time out";
            }
            return false;
        }

        //Witness Statement Panel
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.witnessStatementsXpath())){
            error = "Failed to wait for Witness Statement Panel";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.witnessStatementsXpath())){
            error = "Failed to click Witness Statement Panel";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.ValidateGridIsNotEditableXPath())){
            error = "Failed to validate that the editable grid is displayed";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully validated that the Witness Statement non-editable grid is displayed");
        
        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.witnessStatementsAddXpath())){
            error = "Failed to wait for Witness Statements add button";
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.witnessStatementsAddXpath())){
            error = "Failed to click Witness Statements add button";
        }
        narrator.stepPassedWithScreenShot("Successfully validated that a new module is displayed in the Add Phase");

        //Save mask
        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.loadingFormsActive(), 1)){
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.loadingForms(), 40)){
                error = "Webside too long to load wait reached the time out";
            }
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.loadingPermissionActive(), 1)){
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.loadingPermissions(), 40)){
                error = "Webside too long to load wait reached the time out";
            }
            return false;
        }
        
        pause(2000);
        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.witnessStatement_ProcessFlow())){
            error = "Failed to wait for Witness Statements process flow button";
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.witnessStatement_ProcessFlow())){
            error = "Failed to click Witness Statements process flow button";
        }
        narrator.stepPassedWithScreenShot("Successfully clicked witness process flow button.");

        //witness Name
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.witnessNameXpath(), getData("witness Name"))){
            error = "Failed to enter witness name ";
            return false;
        }

        //witness Surname
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.witnessSurnameXpath(), getData("witness Surname"))){
            error = "Failed to enter witness Surname";
            return false;
        }

        //witness Sunnary
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.witnessSummaryXpath(), getData("witness Sunnary"))){
            error = "Failed to enter witness Sunnary";
            return false;
        }
        
        //Supporting documents        
        //Upload a link 
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.link_buttonxpath())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.link_buttonxpath())) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow()) {
            error = "Failed to switch tab.";
            return false;
        }

        //Link
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.LinkURL())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.LinkURL(), getData("Document Link"))) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.urlTitle())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.urlTitle(), getData("Title"))) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Entered Url: '" + getData("Document Link") + "' and 'Title: '" + getData("Title") + "'.");

        //url Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.urlAddButton())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.urlAddButton())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Incident_Management_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        
        //Save
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.witness_Save_and_Close())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.witness_Save_and_Close())){
            error = "Failed to click 'Save' button.";
            return false;
        }
        
        //Save mask
        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        
        //Witness Statements gridview
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.waitTableWitness(), 20)){
            error = "Failed to click save Witness details";
            return false;
        }
        narrator.stepPassedWithScreenShot("Ssaved Witness details");

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.witnessStatementsXpath())){
            error = "Failed to wait for witness Statements close tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.witnessStatementsXpath())){
            error = "Failed to click witness Statements close tab";
            return false;
        }

        return true;
    }
}
