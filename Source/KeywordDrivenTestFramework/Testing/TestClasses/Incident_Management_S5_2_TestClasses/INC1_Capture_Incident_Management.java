/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.By;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "1-Capture Incident Management",
        createNewBrowserInstance = false
)
public class INC1_Capture_Incident_Management extends BaseClass{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public INC1_Capture_Incident_Management(){
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }
    
    public TestResult executeTest() throws IOException{
        if (!NavigateToIncidentManagement()){
            return narrator.testFailed("Failed due - " + error);
        }
        if (!CaptureIncidentManagement()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!addPhotos()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Successfully Captured Incident Management: ");

    }
    
    private boolean NavigateToIncidentManagement(){
        //Environmental, health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.navigate_EHS())){
            error = "Failed to wait for Environmental, health & Safety button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.navigate_EHS())){
            error = "Failed to click Environmental, health & Safety button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Navigated to Environmental, health & Safety module.");
        
        //Click Incident management tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentManagmentXpath())){
            error = "Failed to wait for Incident Management button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IncidentManagmentXpath())){
            error = "Failed to click Incident Management button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Navigated to Incident Management module.");

        //Click add
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.addNewBtn())){
            error = "Failed to wait for Add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.addNewBtn())){
            error = "Failed to click Add button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successful clicked add button.");
        pause(2000);
        
        //Click the process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.processFlow())){
            error = "Failed to wait for process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.processFlow())){
            error = "Failed to click process flow.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Clicked a process flow");
        
        return true;
    }

    private boolean CaptureIncidentManagement(){
        //Enter the incident tiltle
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentTitle())){
            error = "Failed to wait for incident title text field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.incidentTitle(), getData("Incident Title"))){
            error = "Failed to click incident title text field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered an Incident Title: " + getData("Incident Title") + " to the Incident Title Text Field");

        //Enter the description
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentDescription())){
            error = "Failed to wait for incident description text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.incidentDescription(), getData("Incident Description"))){
            error = "Failed to click incident description text field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered an Incident Description: " + getData("Incident Description") + " to the Incident Description Text Field");

        //Incident occured
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentOccurredDropDown())){
            error = "Failed to wait for incident occured drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentOccurredDropDown())){
            error = "Failed to click incident occured drop down.";
            return false;
        }
        //Global Company
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.GlobalCompanyXpath())){
            error = "Failed to click incident occured drop down.";
            return false;
        }
        //South Africa
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.SouthAfrica())){
            error = "Failed to click incident occured drop down.";
            return false;
        }
        //Victory Site
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.selectGlobalCompany(getData("Incident Occurred")))){
            error = "Failed to wait for incident occured option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.selectGlobalCompany(getData("Incident Occurred")))){
            error = "Failed to click incident occured option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered an Incident Occurred: " + getData("Incident Occurred") + " to the Incident Occurred Text Field");

        //Specific location
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.location())){
            error = "Failed to wait for specific location.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.location(), getData("Location"))){
            error = "Failed to enter the specific location.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered an Location: " + getData("Location") + " to the Location Text Field");

        //Click pin to map
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.PinToMap())){
            error = "Failed to wait for pin to map check box.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.PinToMap())){
            error = "Failed to click pin to map check box.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfuly clicked a pin to map");
        
        //Click link t oa project check box
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.projectLink())){
            error = "Failed to wait for pin to link to a project check box.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.projectLink())){
            error = "Failed to click link to a project check box.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfuly clicked a project link");
        
        //Click the project
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.project())){
            error = "Failed to wait for project drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.project())){
            error = "Failed to click project drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.selectProject(getData("Project")))){
            error = "Failed to wait for project option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.selectProject(getData("Project")))){
            error = "Failed to click project option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Project: " + getData("Project") + " to the Project Text Field");

        //Open the map
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.map())){
            error = "Failed to wait for map panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.map())){
            error = "Failed to click map panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked map panel");
        
        //Click impact
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.impactDropDown())){
            error = "Failed to wait for impact type drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.impactDropDown())){
            error = "Failed to click impact type drop down.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfuly clicked an impact type");
        
        pause(2000);
        //Select the first impact type
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.select_All_Impact_Type())){
            error = "Failed to wait for impact type option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.select_All_Impact_Type())){
            error = "Failed to click impact type option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked an Impact type: " + getData("Impact type") + " to the Impact type Text Field");

        //Incident type
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentTypeDropDown())){
            error = "Failed to wait for incident type drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentTypeDropDown())){
            error = "Failed to click incident type drop down.";
            return false;
        }
        //Select the incident type
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.selectIncidentType(getData("Incident type")))){
            error = "Failed to wait for incident type option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.selectIncidentType(getData("Incident type")))){
            error = "Failed to click incident type option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfuly clicked '" + getData("Incident type") + "' incident type option");

        pause(1000);
        //Date of occurrence
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.dateOfOccurrence())){
            error = "Failed to wait for date of occurrence drop down.";
            return false;
        }
        //Scroll to date of occurrence
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.dateOfOccurrence())){
            error = "Failed to scroll to date of occurrence field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.dateOfOccurrence(), startDate)){
            error = "Failed to click date of occurrence option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Occurrence Date: " + startDate + " to the Occurrence Date Text Field");
        
        //Time of occurrence
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.timeOfOccurrence())){
            error = "Failed to wait for time of occurrence text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.timeOfOccurrence(), getData("Time of occurrence"))){
            error = "Failed to enter occurrence text field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Time of occurrence: " + getData("Time of occurrence") + " to the Time of occurrence Text Field");
        
        //Reported date
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.reportedDate())){
            error = "Failed to wait for reported date text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.reportedDate(), startDate)){
            error = "Failed to enter reported date text field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Reported date: " + getData("Reported date") + " to the Reported date Text Field");
        
        //Reported time
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.reportedTime())){
            error = "Failed to wait for reported time text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.reportedTime(), getData("Reported Time"))){
            error = "Failed to enter reported time text field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Reported Time: " + getData("Reported Time") + " to the Reported Time Text Field");
        
        //Shift drop down
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.shiftDropDown())){
            error = "Failed to wait for shift drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.shiftDropDown())){
            error = "Failed to click shift drop down.";
            return false;
        }
        //Select shift
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.selectShift(getData("Shift")))){
            error = "Failed to wait for shift option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.selectShift(getData("Shift")))){
            error = "Failed to click shift option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Shift: " + getData("Shift") + " to the Shift Text Field");
        
        //Click external parties involved
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.externalPartiesInvolved())){
            error = "Failed to wait for external parties involved check box.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.externalPartiesInvolved())){
            error = "Failed to clickexternal parties involved check box.";
            return false;
        }
        //Select external parties
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.selectAll_externalParties())){
            error = "Failed to wait for external parties option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.selectAll_externalParties())){
            error = "Failed to clickexternal parties option.";
            return false;
        }
        //Immediate action taken
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.immediateActionTaken())){
            error = "Failed to wait for immediate action taken.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.immediateActionTaken(), getData("Immediate Action"))){
            error = "Failed to enter immediate action taken into the text field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Immediate Action: " + getData("Immediate Action") + " to the Immediate Action Text Field");

        //Click reported by
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.reportedBy())){
            error = "Failed to wait for reported by check box.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.reportedBy())){
            error = "Failed to clicke reported by check box.";
            return false;
        }
        //Scroll up
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.scrolUp())){
            error = "Failed to wait for scroll up button.";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.scrolUp())){
            error = "Failed to click scroll up button.";
            return false;
        }
        //Incident owner
//        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentOwnerDropDown())){
//            error = "Failed to wait for incident owner drop down.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentOwnerDropDown())){
//            error = "Failed to click incident owner drop down.";
//            return false;
//        }
//        //Select incident owner
//        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentOwner(getData("Incident owner")))){
//            error = "Failed to wait for incident owner option.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentOwner(getData("Incident owner"))))
//        {
//            error = "Failed to click incident owner option.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully clicked Incident owner: " + getData("Incident owner") + " to the Incident owner Text Field");

        return true;
    }

    public boolean addPhotos() throws IOException{
        String pathofImages = System.getProperty("user.dir") + "\\images";
        String path = new File(pathofImages).getAbsolutePath();
        System.out.println("path " + pathofImages);

        if (getData("More images").equalsIgnoreCase("True")){
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.addMoreImagesRow1())){
                error = "Failed to click Add more images 1";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.addMoreImagesRow2())){
                error = "Failed to click Add more images 2";
                return false;
            }

            Runtime.getRuntime().exec("Z:\\IsoMetrix\\AutoIT\\Scripts\\uploadImage.exe");
            pause(3000);
            //default content
            if (!SeleniumDriverInstance.switchToDefaultContent())
            {
                error = "Failed to switch to default content";
                return false;
            }
            //iframe
            if (!SeleniumDriverInstance.switchToFrameByXpath(Incident_Management_PageObjects.iframeXpath()))
            {
                error = "Failed to switch to iframe ";
                return false;
            }
            narrator.stepPassedWithScreenShot("Add image 3 and entered incident owner");
            pause(2000);
            narrator.stepPassedWithScreenShot("Added 3 more images");

        } else{
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.image())){
                error = "Failed to click image";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.image())){
                error = "Failed to click image";
                return false;
            }

            Runtime.getRuntime().exec("Z:\\IsoMetrix\\AutoIT\\Scripts\\uploadImage.exe");
            pause(3000);
            //default content
            if (!SeleniumDriverInstance.switchToDefaultContent()){
                error = "Failed to switch to default content ";
                return false;
            }
            //iframe
            if (!SeleniumDriverInstance.switchToFrameByXpath(Incident_Management_PageObjects.iframeXpath())){
                error = "Failed to switch to iframe ";
                return false;
            }
            pause(1500);
            narrator.stepPassedWithScreenShot("Successfully added one image ");

            //Save and continue
            if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Management_PageObjects.savePersonsInvolved())){
                error = "Failed to wait for save and continue button.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.savePersonsInvolved())){
                error = "Failed to click save and continue button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully saved.");

            pause(3000);
            if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait())){
                if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2())){
                    error = "Webside too long to load wait reached the time out";
                    return false;
                }
            }

//            //Validate if the record has been saved or not.
//            if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Management_PageObjects.validateSave())){
//                error = "Failed to wait for Save validation.";
//                return false;
//            }
//
//            String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.validateSave());
//
//            if (!SaveFloat.equals("Record saved")){
//                narrator.stepPassedWithScreenShot("Failed to save record.");
//                return false;
//            }
//            narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

            if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.incidentStatus())){
                error = "Failed to scroll to incident status button.";
                return false;
            }
            
            //2.Verification & Additional Detail tab
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.verificationAndAddDetTab())){
                error = "Failed to wait for the '2.Verification & Additional Detail' tab";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.verificationAndAddDetTab()))
            {
                error = "Failed to click the '2.Verification & Additional Detail' tab";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked '2.Verification & Additional Detail' tab");

        }
        return true;
    }

}



    
