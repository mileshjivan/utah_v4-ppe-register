/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "40-Capture Incident Investigation Findings",
        createNewBrowserInstance = false
)
public class INC40_Capture_Incident_Investigation_Findings extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String recordNumber;

    public INC40_Capture_Incident_Investigation_Findings() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!Capture_Incident_Investigation_Findings()) {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Capture_Incident_Investigation_Findings_Actions()) {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Validate_Incident_Management_Findings()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Passed Capturing Incident Management Findings");
    }

    public boolean Capture_Incident_Investigation_Findings() {
        //Findingd and Approval
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_FindingdAndApproval_tabxpath())) {
            error = "Failed to wait for Findings & Approval  tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentInvestigation_FindingdAndApproval_tabxpath())) {
            error = "Failed to click for Findings & Approval tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Findings & Approval tab.");
        
        //Incident Management Findings Add button.
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_Add_Buttonxpath())) {
            error = "Failed to wait for Incident Management Findings Add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_Add_Buttonxpath())) {
            error = "Failed to click Incident Management Findings Add button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Incident Management Findings Add button.");
        pause(3000);
        
        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IMF_processFlow())) {
            error = "Fail to wait for Incident Management Findings process flow";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IMF_processFlow())) {
            error = "Fail to click Incident Management process Findings flow";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Incident Management Findings process flow.");
        
        
        //Incident Management Finding Description
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_Description_TextFieldxpath())) {
            error = "Failed to wait for Incident Management Findings Description.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_Description_TextFieldxpath(), getData("Finding"))) {
            error = "Failed to EnterText into Incident Management Findings Description Textfield.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Incident Management Findings Description Textfield: " + getData("Finding"));

        //Incident Management Finding Owner
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_FindingOwner_Selectxpath())) {
            error = "Failed to wait for Incident Management Finding Owner Select field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_FindingOwner_Selectxpath())) {
            error = "Failed to click Incident Management Finding Owner Select field.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_FindingOwner_xpath(getData("FindingOwner")))) {
            error = "Failed to wait for Incident Management Finding Owner.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_FindingOwner_xpath(getData("FindingOwner")))) {
            error = "Failed to click Incident Management Finding Owner.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Incident Management Finding Owner: " + getData("FindingOwner"));

        //Incident Management Risk Source Select field.
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_RiskSource_Selectxpath())) {
            error = "Failed to wait for Incident Management Risk Source Select field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_RiskSource_Selectxpath())) {
            error = "Failed to click Incident Management Risk Source Select field.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_RiskSource_TickAllxpath())) {
            error = "Failed to wait for Incident Management tick all Risk Source.";
            return false;
        }        
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_RiskSource_TickAllxpath())) {
            error = "Failed to click Incident Management tick all Risk Source.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected all Incident Management Risk Source.");

        //Incident Management Finding Classification Select field
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_Classification_Selectxpath())) {
            error = "Failed to wait for Incident Management Finding Classification Select field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_Classification_Selectxpath())) {
            error = "Failed to click Incident Management Finding Classification Select field.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_Classification_xpath(getData("Classification")))) {
            error = "Failed to wait for Incident Management Finding Classification.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_Classification_xpath(getData("Classification")))) {
            error = "Failed to click Incident Management Finding Classification.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Incident Management Finding Classification: " + getData("Classification"));
        
        //Incident Management Risk Select field
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_Risk_Selectxpath())) {
            error = "Failed to wait for Incident Management Risk Select field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_Risk_Selectxpath())) {
            error = "Failed to click Incident Management Risk Select field.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_Risk_xpath(getData("Risk")))) {
            error = "Failed to wait for Incident Management Risk.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_Risk_xpath(getData("Risk")))) {
            error = "Failed to click Incident Management Risk.";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully captured Incident Management Finding Risk: " + getData("Risk"));

        //Incident Management Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_Save_Selectxpath())) {
            error = "Failed to wait for Incident Management Save button field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_Save_Selectxpath())) {
            error = "Failed to click Incident Management Save button field.";
            return false;
        }
        
        //Save mask
        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait())){
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2())){
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_RecordNumber_Selectxpath())) {
            error = "Failed to wait for Incident Management Record number field.";
            return false;
        }
       
        return true;
    }

    public boolean Capture_Incident_Investigation_Findings_Actions(){
        //Root cause
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.rootCause_Tab())){
            error = "Failed to wait for 'Root Cause' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.rootCause_Tab())){
            error = "Failed to click on 'Root Cause' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.rootCause_Select(getData("Root cause")))){
            error = "Failed to wait for '" + getData("Root cause") + "' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.rootCause_Select(getData("Root cause")))){
            error = "Failed to click on '" + getData("Root cause") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Root cause : '" + getData("Root cause") + "'.");
        
        //Save
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_Save_Selectxpath())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_Save_Selectxpath())){
            error = "Failed to click 'Save' button.";
            return false;
        }
        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait())) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2())) {
                error = "Website too long to load wait reached the time out";
                return false;
            }
        }
        pause(7000);
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Incident_Investigation_Findings_Actions_add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Incident_Investigation_Findings_Actions_add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
       
        pause(5000);
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IMFA_processFlow())) {
            error = "Fail to wait for Incident Management Findings Actions process flow";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IMFA_processFlow())) {
            error = "Fail to click Incident Management process Findings Actions flow";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Incident Management Findings Actions process flow.");
        pause(2000);
        
        //Description
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Actions_desc())){
            error = "Failed to wait for 'Description' textarea.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.Actions_desc(), getData("Description"))){
            error = "Failed to enter '" + getData("Description") + "' into Description textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("'Description' textarea: " + getData("Description"));
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Department_Resp_dropdown())){
            error = "Failed to wait for Department Responsible dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Department_Resp_dropdown())){
            error = "Failed to click Department Responsible dropdown.";
            return false;
        }
        pause(1000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Department_Resp_Option1(getData("Department Responsible 1")))){
            error = "Failed to wait for '" + getData("Department Responsible 1") + "' in Department Responsible dropdown.";
            return false;
        }        
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Department_Resp_Option1(getData("Department Responsible 1")))){
            error = "Failed to click '" + getData("Department Responsible 1") + "' from Department Responsible dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Department_Resp_Option1(getData("Department Responsible 2")))){
            error = "Failed to click '" + getData("Department Responsible 2") + "' from Department Responsible dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Department_Resp_Option(getData("Department Responsible 3")))){
            error = "Failed to click '" + getData("Department Responsible 3") + "' from Department Responsible dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Department Responsible: " + getData("Department Responsible 1") + " -> " + getData("Department Responsible 2") + " -> " + getData("Department Responsible 3"));
        
        //Responsible Person dropdown.
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.ResponsiblePerson_dropdown())){
            error = "Failed to wait for Responsible Person dropdown.";
            return false;
        }        
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.ResponsiblePerson_dropdown())){
            error = "Failed to click Responsible Person dropdown.";
            return false;
        }        
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.ResponsiblePerson_Option(getData("Responsible Person")))){
            error = "Failed to wait for '" + getData("Responsible Person") + "' in Responsible Person dropdown.";
            return false;
        }        
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.ResponsiblePerson_Option(getData("Responsible Person")))){
            error = "Failed to click '" + getData("Responsible Person") + "' from Responsible Person dropdown.";
            return false;
        } 
        narrator.stepPassedWithScreenShot("Responsible Person dropdown: " + getData("Responsible Person"));
        
        //Due Date
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Action_DueDate())){
            error = "Failed to wait for Due Date textarea.";
            return false;
        }        
        if(!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.Action_DueDate(), endDate)){
            error = "Failed to enter '" + endDate + "' into due date textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Due Date: " + endDate);
        
        //Save Mask
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Findings_Actions_saveButton())){
            error = "Failed to wait for Save button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Findings_Actions_saveButton())){
            error = "Failed to click Save button.";
            return false;
        }
        pause(10000);
        //Incident Management Findings Actions Close button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindingsActions_Close_Selectxpath())) {
            error = "Failed to wait for Incident Management Findings Actions Close button field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindingsActions_Close_Selectxpath())) {
            error = "Failed to click Incident Management Findings Actions Close button field.";
            return false;
        }
        pause(2000);
        
        return true;
    }
    
    public boolean Validate_Incident_Management_Findings(){
        //Validate record
        String retrieveRecordNumber = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_RecordNumber_Selectxpath());
        narrator.stepPassed("Saved Incident Management Findings " + retrieveRecordNumber);
        recordNumber = retrieveRecordNumber.substring(10);
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_Close_Selectxpath())) {
            error = "Failed to wait for Incident Management Close button field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_Close_Selectxpath())) {
            error = "Failed to click Incident Management Close button field.";
            return false;
        }
        pause(10000);

        //3.Under Investigation tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.investigationTab())) {
            error = "Failed to wait for 3.Under Investigation tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.investigationTab())) {
            error = "Failed to click 3.Under Investigation tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.investigationTab())) {
            error = "Failed to click second 3.Under Investigation tab";
            return false;
        }
            
        //Findingd and Approval
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_FindingdAndApproval_tabxpath())) {
            error = "Failed to wait for Findings & Approval  tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentInvestigation_FindingdAndApproval_tabxpath())) {
            error = "Failed to click for Findings & Approval tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Findings & Approval tab.");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_ManagementFindings_ListedRecord_Listxpath(recordNumber))) {
            error = "Failed to wait for Listed Incident Management Finding Record #" + retrieveRecordNumber;
            return false;
        }
       
        //Incident Management - Incident Investigation Findings Gridview Record
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.IncidentManagement_IncidentInvestigationFindings_Gridview())){
            error = "Failed to wait for Incident Management - Incident Investigation Findings Gridview Record";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.IncidentManagement_IncidentInvestigationFindings_Gridview())){
            error = "Failed to click for Incident Management - Incident Investigation Findings Gridview Record";
            return false;
        }

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IM_processFlow())) {
            error = "Fail to wait for Incident Management process flow";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IM_processFlow())) {
            error = "Fail to click Incident Management process flow";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Incident Management process flow.");
        pause(2000);
        
        return true;
    }
}
