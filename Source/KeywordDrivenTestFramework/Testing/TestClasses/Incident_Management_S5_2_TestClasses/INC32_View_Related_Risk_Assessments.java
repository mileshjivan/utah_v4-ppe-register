/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import java.io.File;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "32-View Related Risk Assessments",
        createNewBrowserInstance = false
)
public class INC32_View_Related_Risk_Assessments extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public INC32_View_Related_Risk_Assessments() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!View_Related_Risk_Assessments()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed Viewing Related Risk Assessment");
    }

    public boolean View_Related_Risk_Assessments() {
        //Additional Information
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.AdditionalInformation())){
            error = "Failed to wait for 'Additional Information' check box.";
            return false;
        }
        if(!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.AdditionalInformation())){
            error = "Failed to scroll to 'Additional Information' check box.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.AdditionalInformation())){
            error = "Failed to click the 'Additional Information' check box.";
            return false;
        }
        narrator.stepPassed("Additional Information check box is displayed");
                    
        //Related Risk panel.
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_RelatedRisk_Panelxpath())) {
            error = "Failed to wait for Related Risk panel.";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.incidentInvestigation_RelatedRisk_Panelxpath())) {
            error = "Failed to scroll to Related Risk panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentInvestigation_RelatedRisk_Panelxpath())) {
            error = "Failed to wait for Related Risk panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Related Risk panel is displayed.");
        pause(2000);

        //Available Related Risk Gridview
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_RelatedRisk_Openxpath())) {
            error = "Failed to wait for Available Related Risk.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentInvestigation_RelatedRisk_Openxpath())) {
            error = "Failed to click Available Related Risk.";
            return false;
        }

        //Save Mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 40)){
            error = "Webside too long to load wait reached the time out";
            return false;
        }  
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_RelatedRisk_OpenedRecordxpath())) {
            error = "Failed to wait for Available Related Risk to open.";
            return false;
        }

        String openedRelatedRisk = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.incidentInvestigation_RelatedRisk_OpenedRecordxpath());
        narrator.stepPassed("Risk Assessment opened: " + openedRelatedRisk);

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.RiskAssessment_ProcessFlow())) {
            error = "Fail to wait for Risk Assessment process flow";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.RiskAssessment_ProcessFlow())) {
            error = "Fail to click Risk Assessment process flow";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Risk Assessment process flow.");
        
        return true;
    }

}
