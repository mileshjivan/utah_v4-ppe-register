/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import java.io.File;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "20-Capture Investigation Team",
        createNewBrowserInstance = false
)
public class INC20_Capture_Investigation_Team extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public INC20_Capture_Investigation_Team() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!CapturingInvestigationTeam()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed Capturing Investigation Team - Main Scenario");
    }

    public boolean CapturingInvestigationTeam() {
       //Investigation due date
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.investigationTeam_DueDate_textFieldXpath())) {
            error = "Failed to wait for Investigation due date text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.investigationTeam_DueDate_textFieldXpath(), startDate)) {
            error = "Failed to EnterText: " + startDate + ", into Investigation due date text field.";
            return false;
        }

         //Investigation Scope
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.investigationTeam_Scope_textFieldXpath())) {
            error = "Failed to wait for Investigation Scope text field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.investigationTeam_Scope_textFieldXpath())) {
            error = "Failed to click Investigation Scope text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.investigationTeam_Scope_textFieldXpath(), getData("InvestigationScope"))) {
            error = "Failed to EnterText: " + getData("InvestigationScope") + ", into Investigation Scope text field.";
            return false;
        }
        narrator.stepPassed("Investigation Due Date: " + getData("InvestigationDueDate"));
        narrator.stepPassed("Investigation Scope: " + getData("InvestigationScope"));
        pause(500);

        //Investigation Team Panel
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.investigationTeam_InvestigationTeam_DropDownXpath())) {
            error = "Failed to wait for Investigation Team dropdown Button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.investigationTeam_InvestigationTeam_DropDownXpath())) {
            error = "Failed to click Investigation Team dropdown Button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Investigation Team dropdown Button.");
        
        //Investigation Team Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.investigationTeam_InvestigationTeam_ADD_ButtonXpath())) {
            error = "Failed to wait for Investigation Team Add Button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.investigationTeam_InvestigationTeam_ADD_ButtonXpath())) {
            error = "Failed to click Investigation Team Add Button.";
            return false;
        }

        //Investigation Team Fullname
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.investigationTeam_Fullname_SelectFieldXpath())) {
            error = "Failed to wait for Investigation Team Fullname select field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.investigationTeam_Fullname_SelectFieldXpath())) {
            error = "Failed to click Investigation Team Fullname select field.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.investigationTeam_Fullname_Xpath(getData("FullName")))) {
            error = "Failed to wait for Investigation Team Fullname: " + getData("Fullname") + ".";
            return false;
        }
        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.investigationTeam_Fullname_Xpath(getData("FullName")))) {
            error = "Failed to Select Investigation Team Fullname: " + getData("Fullname") + ".";
            return false;
        }
        narrator.stepPassedWithScreenShot("Investigation Team Fullname: " + getData("Fullname") + ".");

        //Investigation Team Role
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.investigationTeam_Role_TextFieldXpath())) {
            error = "Failed to wait for Investigation Team Fullname select field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.investigationTeam_Role_TextFieldXpath(), getData("Role"))) {
            error = "Failed to EnterText into Investigation Team Role: " + getData("Role") + ".";
            return false;
        }
        narrator.stepPassedWithScreenShot("Investigation Team Role: " + getData("Role") + ".");

        //Investigation Team Start Date
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.investigationTeam_StartDate_TextFieldXpath())) {
            error = "Failed to wait for Investigation Team Start Date.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.investigationTeam_StartDate_TextFieldXpath(), startDate)) {
            error = "Failed to EnterText into Investigation Team Start Date: " + startDate + ".";
            return false;
        }
        narrator.stepPassedWithScreenShot("Investigation Team Start Date: " + startDate + ".");

        //Investigation Team End Date 
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.investigationTeam_EndDate_TextFieldXpath())) {
            error = "Failed to wait for Investigation Team End Date field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.investigationTeam_EndDate_TextFieldXpath(), endDate)) {
            error = "Failed to EnterText into Investigation Team End Date: " + endDate + ".";
            return false;
        }
        narrator.stepPassedWithScreenShot("Investigation Team End Date: " + endDate + ".");

        //Investigation Team Hours
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.investigationTeam_Hours_TextFieldXpath())) {
            error = "Failed to wait for Investigation Team Hours field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.investigationTeam_Hours_TextFieldXpath())) {
            error = "Failed to click for Investigation Team Hours field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.investigationTeam_Hours_TextFieldXpath(), getData("Hours"))) {
            error = "Failed to EnterText into Investigation Team Hours: " + getData("Hours") + ".";
            return false;
        }
        narrator.stepPassedWithScreenShot("Investigation Team Hours: " + getData("Hours") + ".");

        //Investigation Team Title
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.investigationTeam_InvestigationTeam_TitleXpath())) {
            error = "Failed to wait for Investigation Team Title.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.investigationTeam_InvestigationTeam_TitleXpath())) {
            error = "Failed to click for Investigation Team Title.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully captured Investigation Team records.");
        narrator.stepPassed("Investigation Team Fullname: " + getData("Fullname"));
        narrator.stepPassed("Investigation Team Hours: " + getData("StartDate"));
        narrator.stepPassed("Investigation Team Hours: " + getData("EndDate"));
        narrator.stepPassed("Investigation Team Role: " + getData("Role"));
        narrator.stepPassed("Investigation Team Hours: " + getData("Hours"));
 
        //Investigation Team Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.investigationTeam_Save_ButtonXpath())) {
            error = "Failed to wait for Investigation Team Save button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.investigationTeam_Save_ButtonXpath())) {
            error = "Failed to click Investigation Team Save button.";
            return false;
        }
        
        //Save Mask
        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait())){
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 40)){
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Management_PageObjects.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        return true;
    }

}
