/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "6-Capture Regulatory Authority",
        createNewBrowserInstance = false
)

public class INC6_Capture_Regulatory_Authority extends BaseClass{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public INC6_Capture_Regulatory_Authority(){
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest(){
        if (!regulatoryAuthority()){
            return narrator.testFailed("Failed to fill in incident report - " + error);
        }
        if (!SupportingDocuments()){
            return narrator.testFailed("Failed to upload a supporting document - " + error);
        }
        return narrator.finalizeTest("Successfully captured the details for Witness Statements");
    }

    public boolean regulatoryAuthority(){
        //2.Verification & Additional Detail tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.verificationAndAddDetTab())){
            error = "Failed to wait for the '2.Verification & Additional Detail' tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.verificationAndAddDetTab())){
            error = "Failed to click the '2.Verification & Additional Detail' tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked '2.Verification & Additional Detail' tab");
        
        //Sign off required
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.signOffRequiredChckBox())){
            error = "Failed to wait for the sign off required checkbox";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.signOffRequiredChckBox())){
            error = "Failed to click sign off required checkbox";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'sign off required' checkbox");
        
        //Sign off dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.signOff_DD())){
            error = "Failed to wait for the Sign off dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.signOff_DD())){
            error = "Failed to click on Sign off dropdown";
            return false;
        }
        //Sign off option
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.signOffSelect(getData("Sign Off")))){
            error = "Failed to wait for the '" + getData("Sign Off") + "' option";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.signOffSelect(getData("Sign Off")))){
            error = "Failed to click on '" + getData("Sign Off") + "' option";
            return false;
        }
        
        //Is this event reportable to any regulatory authority and if reportable to which regulatory authority do you need to report this event? 
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.ifReportableChckBox())){
            error = "Failed to wait for the reportable to regulatory authority checkbox";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.ifReportableChckBox())){
            error = "Failed to click reportable to regulatory authority checkbox";
            return false;
        }
        
        //Regulatory Authority panel
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.regulatoryAuthorityPanel())){
            error = "Failed to wait for 'Regulatory Authority' panel";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.regulatoryAuthorityPanel())){
            error = "Failed to click 'Regulatory Authority' panel";
            return false;
        }
        
        //Add
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.regulatoryAdd())){
            error = "Failed to wait for 'Add' button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.regulatoryAdd())){
            error = "Failed to click 'Add' button";
            return false;
        }
        
        pause(2000);
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.regularyAuthorityFlow())){
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.regularyAuthorityFlow())){
                error = "Failed to wait for Regulatory Authority flow";
            }
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.regularyAuthorityFlow())){
            error = "Failed to click Regulatory Authority flow";
            return false;
        }
        
        //Regulatory authority dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.regulatoryAuthority())){
            error = "Failed to click 'Regulatory authority' dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.regulatoryAuthority())){
            error = "Failed to click 'Regulatory authority' dropdown";
            return false;
        }
        //Regulatory authority select
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.regulatoryAuthorityInput1(getData("Regulatory authority")))){
            error = "Failed to select regulatory authority '" + getData("Regulatory authority")+"'.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.contactPersonXpath(), getData("Contact person"))){
            error = "Failed to enter contact person";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.contactNumberXpath(), getData("Contact number"))){
            error = "Failed to enter contact number";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.contactEmailXpath(), getData("Contact email"))){
            error = "Failed to enter contact email";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.dateXpath(), startDate)){
            error = "Failed to enter date ";
            return false;
        }
        String time = new SimpleDateFormat("HH:mm").format(new Date());

        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.RegulatoryAuthority_TimeXpath(), time)){
            error = "Failed to enter time ";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.communicationSummaryXpath(), getData("Communication summary"))){
            error = "Failed to enter Communication summary ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully enter Regulatory Authority details");

        return true;
    }
    
    public boolean SupportingDocuments(){
        //Supporting documents        
        //Upload a link 
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.link_buttonxpath())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.link_buttonxpath())) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow()) {
            error = "Failed to switch tab.";
            return false;
        }

        //Link
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.LinkURL())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.LinkURL(), getData("Document Link"))) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.urlTitle())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.urlTitle(), getData("Title"))) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Entered Url: '" + getData("Document Link") + "' and 'Title: '" + getData("Title") + "'.");

        //url Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.urlAddButton())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.urlAddButton())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Incident_Management_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        
        //Save and close
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.regularyAuthority_SaveAndClose_Button())){
            error = "Failed to wait for Regulatory Authority save ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.regularyAuthority_SaveAndClose_Button())){
            error = "Failed to click Regulatory Authority  save ";
            return false;
        }

        //Save mask
        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        
        //'Regulatory Authority' panel
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.regulatoryAuthorityPanel())){
            error = "Failed to wait for 'Regulatory Authority' panel";
            return false;
        }
        narrator.stepPassedWithScreenShot("Saved Regulatory Authority");
        
        //Scroll to the record
        if(!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.regulatoryAuthorityPanel())){
            error = "Failed to scroll to the bottom";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.regulatoryAuthority_Record())){
            error = "Failed to wait for witness Statements close tab";
            return false;
        }
        
        //Click the process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.processFlow())){
            error = "Failed to wait for process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.processFlow())){
            error = "Failed to click process flow.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Clicked a process flow");
       
        return true;
    }
}
