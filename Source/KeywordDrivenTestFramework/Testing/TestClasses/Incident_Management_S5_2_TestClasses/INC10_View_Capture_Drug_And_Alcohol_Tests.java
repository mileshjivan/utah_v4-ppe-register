/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "10-View Capture Drug and Alcohol Tests",
        createNewBrowserInstance = false
)

public class INC10_View_Capture_Drug_And_Alcohol_Tests extends BaseClass{
    String date;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String recordNumber;
    String recordNumber2;
    String parentWindow;

    public INC10_View_Capture_Drug_And_Alcohol_Tests(){
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("dd-MM-YYYY").format(new Date());
        recordNumber = "";
        
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest(){
        if (!AddDrugAndAlcoholTest()){
            return narrator.testFailed("Failed to  - " + error);
        }
        return narrator.finalizeTest("Successfully saved the injured person's Drug and Alcohol test");
    }

    public boolean AddDrugAndAlcoholTest(){
        //Click the process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.injuredPersonsFlow())){
            error = "Failed to wait for process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.injuredPersonsFlow())){
            error = "Failed to click process flow.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Clicked a process flow");
        
        //Drug & Alcohol Tests tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.drugAndAlcoholTest_Tab())){
            error = "Failed to wait 'Drug & Alcohol Tests' tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.drugAndAlcoholTest_Tab())){
            error = "Failed to click on 'Drug & Alcohol Tests' tab";
            return false;
        }
        parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        
        //Conduct Drug and Alcohol test button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.conductDrugAndAlcoholTest_Btn())){
            error = "Failed to wait for 'Conduct Drug and Alcohol test' button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.conductDrugAndAlcoholTest_Btn())){
            error = "Failed to click on 'Conduct Drug and Alcohol test' button";
            return false;
        }
        SeleniumDriverInstance.switchToTabOrWindow();
        
        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.iframeXpath())){
            error = "Failed to wait for frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Incident_Management_PageObjects.iframeXpath())){
            error = "Failed to switch to frame.";
            return false;
        }
        
        //Process flow button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.drugAndAlcoholTest_ProcessFlow())){
            error = "Failed to wait for 'Process flow' button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.drugAndAlcoholTest_ProcessFlow())){
            error = "Failed to click on 'Process flow' button";
            return false;
        }
        
        //Employment type dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.EmployeeTypeXPath())){
            error = "Failed to wait for the Employment Type dropdown list";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.EmployeeTypeXPath())){
            error = "Failed to click on the Employment Type dropdown list";
            return false;
        }
        
        //Employment type select
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.dropdownElementXPath(getData("Employment Type")))){
            error = "Failed to click wait for the 'Employment Type' item - " + getData("Employment Type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.dropdownElementXPath(getData("Employment Type")))){
            error = "Failed to click on the 'Employment Type' item - " + getData("Employment Type");
            return false;
        }
        
        //Person being tested dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.PersonBeingTestedXPath())){
            error = "Failed to click wait for the Person Being Tested dropdown list";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.PersonBeingTestedXPath())){
            error = "Failed to click on the Person Being Tested dropdown list";
            return false;
        }
        
        //Person being tested select
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.dropdownElementXPath(getData("Full Name")))){
            error = "Failed to wait for the Person Being Tested item - " + getData("Full Name");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.dropdownElementXPath(getData("Full Name")))){
            error = "Failed to click on the Person Being Tested item - " + getData("Full Name");
            return false;
        }
        
        //Linked incident checkbox
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.LinkedIncidentXPath())){
            error = "Failed to click wait for the Linked Incident checkbox";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.LinkedIncidentXPath())){
            error = "Failed to click on the Linked Incident checkbox";
            return false;
        }
        
        //Linked incident dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.LinkedIncidentDropdownXPath())){
            error = "Failed to click wait for the Linked Incident dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.LinkedIncidentDropdownXPath())){
            error = "Failed to click on the Linked Incident dropdown";
            return false;
        }

//        String linkedIncident = "#" + recordNumber + ": " + getData("Incident title");
//        SeleniumDriverInstance.pause(1000);

        //Linked incident select
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.dropdownElementXPath(getData("Link To Incident Record")))){
            error = "Failed to wait for the following item: " + getData("Link To Incident Record");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.dropdownElementXPath(getData("Link To Incident Record")))){
            error = "Failed to select the following item: " + getData("Link To Incident Record");
            return false;
        }
        
        //Medical test date field
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.MedicalTestDateXPath())){
            error = "Failed to wait for the Medical test date field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.MedicalTestDateXPath(), startDate)){
            error = "Failed to enter the date in the Medical test date field " + startDate;
            return false;
        }
        
//        SeleniumDriverInstance.pressKey(Keys.ENTER);
//        SeleniumDriverInstance.pause(500);

        //Person submit to a test dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.PersonSubmitToATestXPath())){
            error = "Failed to wait for the Person submit to a test dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.PersonSubmitToATestXPath())){
            error = "Failed to click on the Person submit to a test dropdown";
            return false;
        }
        
        //Person submit to a test select
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.dropdownElementXPath(getData("Submit a test")))){
            error = "Failed to wait for the following item: " + getData("Submit a test");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.dropdownElementXPath(getData("Submit a test")))){
            error = "Failed to select the following item: " + getData("Submit a test");
            return false;
        }
        
        //Drug test result dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.DrugTestResultXPath())){
            error = "Failed to wait the Drug test result dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.DrugTestResultXPath())){
            error = "Failed to click on the Drug test result dropdown";
            return false;
        }
        //Drug test result select
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.dropdownElementXPath(getData("Drug test result")))){
            error = "Failed to wait the following item: " + getData("Drug test result");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.dropdownElementXPath(getData("Drug test result")))){
            error = "Failed to select the following item: " + getData("Drug test result");
            return false;
        }
        //Alcohol test result dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.AlcoholTestResultXPath())){
            error = "Failed to wait for the Alcohol test result dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.AlcoholTestResultXPath())){
            error = "Failed to click on the Alcohol test result dropdown";
            return false;
        }
        //Alcohol test result select
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.dropdownElementXPath(getData("Alcohol test result")))){
            error = "Failed to wait for the following item: " + getData("Alcohol test result");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.dropdownElementXPath(getData("Alcohol test result")))){
            error = "Failed to select the following item: " + getData("Alcohol test result");
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.SaveDrugAndAlcoholTestXPath())){
            error = "Failed to click the Save button";
            return false;
        }
        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait(), 2)){
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 400)){
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.RecordSavedMessageXPath())){
            error = "Failed to click on the Save button";
            return false;
        } else {
            String text = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.RecordSavedMessageXPath());

            if (text.equals(" ")){
                error = "String cannot be empty";
                return false;
            } else if (text.equals(getData("Record Saved"))) {
                narrator.stepPassed("Successfully saved the injured Persons record: " + text);
            }
        }
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

        pause(5000);

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.iframeXpath())){
            error = "Failed to wait for frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Incident_Management_PageObjects.iframeXpath())){
            error = "Failed to switch to frame.";
            return false;
        }
        
//        String retrivedRecordNumber2 = SeleniumDriverInstance.retrieveTextByXpath(InjuredPersonsPageObjects.RecordNumberXPath());
//        narrator.stepPassed("Successfully retrieved and stored text: "+retrivedRecordNumber2);
//        recordNumber2 = retrivedRecordNumber2.substring(10);
//        
//        SeleniumDriverInstance.Driver.close();
        
        narrator.stepPassedWithScreenShot("Successfully added a drug and alcohol test for the matching injured person");
        
        //Drug & Alcohol Tests tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.drugAndAlcoholTest_Tab())){
            error = "Failed to wait 'Drug & Alcohol Tests' tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.drugAndAlcoholTest_Tab())){
            error = "Failed to click on 'Drug & Alcohol Tests' tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Drug & Alcohol Tests' tab");
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.seachBtn1())){
            error = "Failed to wait 'Search' button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.seachBtn1())){
            error = "Failed to click on 'Search' button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button");
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.seachBtn2())){
            error = "Failed to wait 'Search' button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.seachBtn2())){
            error = "Failed to click on 'Search' button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button");
        //Select drug and alcohol test record
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.drugAndAlcoholTest_record(startDate))){
            error = "Failed to wait 'Drug and alcohol test' record";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.drugAndAlcoholTest_record(startDate))){
            error = "Failed to click on 'Drug and alcohol test' record";
            return false;
        }
        //Process flow button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.drugAndAlcoholTest_ProcessFlow())){
            error = "Failed to wait for 'Process flow' button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.drugAndAlcoholTest_ProcessFlow())){
            error = "Failed to click on 'Process flow' button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button");
        
        return true;
    }
}
