/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "11-Capture Injury Claim Form",
        createNewBrowserInstance = false
)

public class INC11_Capture_Injury_Claim_Form extends BaseClass {

    String error = "";
    String date;
    SikuliDriverUtility sikuliDriverUtility;
    String recordNumber;
    String recordNumber2;
    String parentWindow;

    public INC11_Capture_Injury_Claim_Form() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("dd-MM-YYYY").format(new Date());
        recordNumber = "";

        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!injuryClaim()) {
            return narrator.testFailed("Failed fill Injured Persons - Injury claim " + error);
        }

        return narrator.finalizeTest("Completed Injured Persons - Injury claim ");
    }

    public boolean injuryClaim() {
        //Injury claim tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.injuryClaimTab())) {
            error = "Failed to wait for injury Claim Tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.injuryClaimTab())) {
            error = "Failed to click injury Claim Tab ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Injury claim tab");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.injuryClaimAdd())) {
            error = "Failed to wait for injury Claim add ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.injuryClaimAdd())) {
            error = "Failed to click injury Claim add ";
            return false;
        }
        //Process flow button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.injuryClaimForm_processFlow())) {
            error = "Failed to wait for Process flow button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.injuryClaimForm_processFlow())) {
            error = "Failed to click Process flow button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click process flow button");
        //Business unit chechbox
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.anyDropDownCheckXpath(getData("Business unit")))) {
            error = "Failed to wait for business unit - " + getData("Business unit");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyDropDownCheckXpath(getData("Business unit")))) {
            error = "Failed to click business unit - " + getData("Business unit");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Business unit checkbox");
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.specificLoca())) {
            error = "Failed to enter injury Claim - Specific location ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.specificLoca(), getData("Specific location"))) {
            error = "Failed to enter injury Claim - Specific location ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + getData("Specific location") + "' into Specific location field");

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.workersTabXpath())) {
            error = "Failed to wait for Worker's Personal Details panel";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.workersTabXpath())) {
            error = "Failed to click Worker's Personal Details  ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.employeeDropdownXpath())) {
            error = "Failed to wait for Employee dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.employeeDropdownXpath())) {
            error = "Failed to click Employee dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.responsiblePerson(getData("Employee")))) {
            error = "Failed to wait for Employee option: " + getData("Employee") + ".";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.responsiblePerson(getData("Employee")))) {
            error = "Failed to click Employee option: " + getData("Employee") + ".";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.languageDropdownXpath())) {
            error = "Failed to wait for 'If you need an interpreter, what language do you speak?' dropdown ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.languageDropdownXpath())) {
            error = "Failed to click 'If you need an interpreter, what language do you speak?' dropdown ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.languageSelect(getData("Language")))) {
            error = "Failed to click injury Claim - Worker's Personal Details - language  ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.languageSelect(getData("Language")))) {
            error = "Failed to click injury Claim - Worker's Personal Details - language  ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.communicationDisability())) {
            error = "Failed to wait for injury Claim - Worker's Personal Details - Do you have special communication needs because of disability? ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.communicationDisability(), getData("special communication"))) {
            error = "Failed to enter injury Claim - Worker's Personal Details - Do you have special communication needs because of disability? ";
            return false;
        }

        if (getData("Is this a compensation claim").equals("Yes")) {
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.compensationClaimDropdownXpath())) {
                error = "Failed to click injury Claim - Worker's Personal Details - compensation Claim dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anySupervisorXpath(getData("Is this a compensation claim")))) {
                error = "Failed to click injury Claim - Worker's Personal Details - compensation claim  " + getData("Is this a compensation claim");
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.partnerDropDownXpath(), 2)) {
                error = "Failed to display injury Claim - Worker's Personal Details - Do you support a partner? ";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.fullTimeStudentsXpath(), 2)) {
                error = "Failed to click injury Claim - Worker's Personal Details - Do you support any children under the age of 18 or full-time students? ";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully filled Is this a compensation claim and 2 fields are displayed.");

        } else if (getData("Is this a compensation claim").equals("No")) {
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.compensationClaimDropdownXpath())) {
                error = "Failed to click injury Claim - Worker's Personal Details - compensation Claim dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anySupervisorXpath(getData("Is this a compensation claim")))) {
                error = "Failed to click injury Claim - Worker's Personal Details - compensation claim  " + getData("Is this a compensation claim");
                return false;
            }
            if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.fullTimeStudentsXpath(), 2)) {
                error = " Worker's Personal Details - Do you support any children under the age of 18 or full-time students fields are triggered ";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully filled Is this a compensation claim and No fields are triggered. ");
        }

        if (getData("Do you support a partner").equals("Yes")) {
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.partnerDropDownXpath())) {
                error = "Failed to click injury Claim - Worker's Personal Details - Do you support a partner? dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anySupervisorXpath(getData("Do you support a partner")))) {
                error = "Failed to click injury Claim - Worker's Personal Details - Do you support a partner?  " + getData("Do you support a partner");
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.averageGrossXpath(), 2)) {
                error = "Failed to display injury Claim - Worker's Personal Details - What were their average gross weekly earnings over 3 months? ";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.averageGrossXpath(), getData("average gross"))) {
                error = "Failed to enter injury Claim - Worker's Personal Details - What were their average gross weekly earnings over 3 months? ";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully filled Do you support a partner and 1 fields are displayed.");

        } else if (getData("Do you support a partner").equalsIgnoreCase("No")) {
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.partnerDropDownXpath())) {
                error = "Failed to click injury Claim - Worker's Personal Details - Do you support a partner? dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anySupervisorXpath(getData("Do you support a partner")))) {
                error = "Failed to click injury Claim - Worker's Personal Details - Do you support a partner? " + getData("Do you support a partner");
                return false;
            }

            if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.averageGrossXpath(), 2)) {
                error = "Worker's Personal Details - What were their average gross weekly earnings over 3 months? fields are triggered.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully filled Do you support a partner? and No fields are triggered.");
        }

        if (getData("Support Any Children").equals("Yes")) {
            if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.fullTimeStudentsXpath())) {
                error = "Failed to scroll injury Claim - Worker's Personal Details - full Time Students dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.fullTimeStudentsXpath())) {
                error = "Failed to click injury Claim - Worker's Personal Details - full Time Students dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anySupervisorXpath(getData("Support Any Children")))) {
                error = "Failed to click injury Claim - Worker's Personal Details - Do you support a partner?  " + getData("Support Any Children");
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.pleaseProvideTheDateOfBirthXpath())) {
                error = "Failed to wait for field on injury Claim - Worker's Personal Details - Please provide the date of birth  " + getData("Support Any Children");
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.pleaseProvideTheDateOfBirthXpath(), date)) {
                error = "Failed to enter for field on injury Claim - Worker's Personal Details - Please provide the date of birth  " + getData("Support Any Children");
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully filled Do you support any children under the age of 18 or full-time students? Fields are displayed.");

        } else if (getData("Support Any Children").equals("No")) {
            if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.fullTimeStudentsXpath())) {
                error = "Failed to scroll injury Claim - Worker's Personal Details - full Time Students dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.fullTimeStudentsXpath())) {
                error = "Failed to click injury Claim - Worker's Personal Details - full Time Students dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anySupervisorXpath(getData("Support Any Children")))) {
                error = "Failed to click injury Claim - Worker's Personal Details - Do you support a partner?  " + getData("Support Any Children");
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully filled o Do you support any children under the age of 18 or full-time students? no fields are triggered.");
        }

//        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.returnWorkDetailsTab())) {
//            error = "Failed to click Treatment and Return To Work Details close Tab  ";
//            return false;
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentTabXpath(), 2)) {
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentTabOpneXpath())) {
                error = "Failed to wait for injury Claim - incident and Worker's Injury Details ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IncidentTabOpneXpath())) {
                error = "Failed to click injury Claim - incident and Worker's Injury Details ";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.anyOptionCheckXpath(getData("What is your injury")))) {
            error = "Failed to wait for injury Claim - Worker's Personal Details - What is your injury / condition and which parts of your body are affected?  " + getData("What is your injury");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyOptionCheckXpath(getData("What is your injury")))) {
            error = "Failed to click injury Claim - Worker's Personal Details - What is your injury / condition and which parts of your body are affected?  " + getData("What is your injury");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.whatHappenedXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - What happened and how were you injured ? ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.whatHappenedXpath(), getData("what happened"))) {
            error = "Failed to enter - Incident and Worker's Injury - What happened and how were you injured ? ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.youWereInjuredXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - What task/s were you doing when you were injured ? ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.youWereInjuredXpath(), getData("you were injured"))) {
            error = "Failed to enter - Incident and Worker's Injury - What task/s were you doing when you were injured ? ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.responsibleEmployerXpath())) {
            error = "Failed to wait for - Incident and Worker's Injury - Responsible employer of workplace  ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.responsibleEmployerXpath(), getData("Responsible employer"))) {
            error = "Failed to enter - Incident and Worker's Injury - Responsible employer of workplace  ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.anyDropDownCheckXpath(getData("incident circumstances apply")))) {
            error = "Failed to wait for Incident and Worker's Injury Details - Which of the following incident circumstances apply?";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyDropDownCheckXpath(getData("incident circumstances apply")))) {
            error = "Failed to click Incident and Worker's Injury Details - Which of the following incident circumstances apply?";
            return false;
        }

        String thirdParty = getData("Third Party Manufacturer");

        if (thirdParty.equalsIgnoreCase("Yes")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.thirdPartyDropdown())) {
                error = "Failed to wait for injury Claim form - incident and Worker's Injury Details - Third Party Manufacturer dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.thirdPartyDropdown())) {
                error = "Failed to click injury Claim form - incident and Worker's Injury Details - Third Party Manufacturer dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.anyClickOptionContainsXpath(getData("Third Party Manufacturer")))) {
                error = "Failed to wait for Incident and Worker's Injury Details - Third party " + getData("Third Party Manufacturer");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyClickOptionContainsXpath(getData("Third Party Manufacturer")))) {
                error = "Failed to click Incident and Worker's Injury Details - Third party " + getData("Third Party Manufacturer");
                return false;
            }

            if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.thirdPartyDiplayedXpath())) {
                error = "Failed to display injury Claim form - Incident and Worker's Injury Details - Third party";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.thirdPartyDiplayedXpath(), 2)) {
                error = "Failed to display injury Claim form - Incident and Worker's Injury Details - Third party";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.anyDropDownCheckXpath(getData("Third party")))) {
                error = "Failed to wait for Incident and Worker's Injury Details - Third party " + getData("Third party");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyDropDownCheckXpath(getData("Third party")))) {
                error = "Failed to click Incident and Worker's Injury Details - Third party " + getData("Third party");
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.additionalDescriptionXpath())) {
                error = "Failed to wait for - Incident and Worker's Injury - Additional description  ";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.additionalDescriptionXpath(), getData("Additional description"))) {
                error = "Failed to enter - Incident and Worker's Injury - Additional description  ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.injuryDateXpath())) {
                error = "Failed to wait for - Incident and Worker's Injury - What was the date the injury  ";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.injuryDateXpath(), date)) {
                error = "Failed to enter - Incident and Worker's Injury - What was the date the injury  ";
                return false;
            }

            String time = new SimpleDateFormat("HH:mm").format(new Date());
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.injuryTimeXpath())) {
                error = "Failed to wait for - Incident and Worker's Injury - What was the Time the injury  ";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.injuryTimeXpath(), time)) {
                error = "Failed to enter - Incident and Worker's Injury - What was the Time the injury  ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.injuryConditionsXpath())) {
                error = "Failed to wait for - Incident and Worker's Injury - When did you first notice the injury / condition?  ";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.injuryConditionsXpath(), getData("Injury Conditions"))) {
                error = "Failed to enter - Incident and Worker's Injury - When did you first notice the injury / condition?  ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.stoppedWorkingDateXpath())) {
                error = "Failed to wait for - Incident and Worker's Injury - If you stopped work, what was the date? ";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.stoppedWorkingDateXpath(), date)) {
                error = "Failed to enter - Incident and Worker's Injury - If you stopped work, what was the date? ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.stoppedWorkingTimeXpath())) {
                error = "Failed to wait for - Incident and Worker's Injury - If you stopped work, what was the date? ";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.stoppedWorkingTimeXpath(), time)) {
                error = "Failed to enter - Incident and Worker's Injury - If you stopped work, what was the date? ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.reportDateXpath())) {
                error = "Failed to wait for - Incident and Worker's Injury - When did you report the injury / condition to your employer ? ";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.reportDateXpath(), date)) {
                error = "Failed to enter - Incident and Worker's Injury - When did you report the injury / condition to your employer ? ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.anyDropDownCheckXpath(getData("Injury reported to")))) {
                error = "Failed to wait for Incident and Worker's Injury Details - TInjury reported to " + getData("Injury reported to");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyDropDownCheckXpath(getData("Injury reported to")))) {
                error = "Failed to click Incident and Worker's Injury Details - TInjury reported to " + getData("Injury reported to");
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully filled Incident and Worker's Injury Details and third party displayed.");

        } else if (thirdParty.equalsIgnoreCase("No")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.thirdPartyDropdown())) {
                error = "Failed to wait for injury Claim form - incident and Worker's Injury Details - Third Party Manufacturer dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.thirdPartyDropdown())) {
                error = "Failed to click injury Claim form - incident and Worker's Injury Details - Third Party Manufacturer dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.anySupervisorXpath(thirdParty))) {
                error = "Failed to wait for Incident and Worker's Injury Details - Third party " + thirdParty;
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anySupervisorXpath(thirdParty))) {
                error = "Failed to click Incident and Worker's Injury Details - Third party " + thirdParty;
                return false;
            }

            if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.thirdPartyDiplayedXpath(), 2)) {
                error = "Display injury Claim form - Incident and Worker's Injury Details - Third party";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully filled Incident and Worker's Injury Details no fields are triggered. ");
        }

        if (getData("Previous Related Injuries").equalsIgnoreCase("Yes")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.prevoiusInjuryDropdownXpath())) {
                error = "Failed to wait for injury Claim form - incident and Worker's Injury Details - Previous Related Injuries dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.prevoiusInjuryDropdownXpath())) {
                error = "Failed to click injury Claim form - incident and Worker's Injury Details - Previous Related Injuries dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.anyClickOptionContainsXpath(getData("Previous Related Injuries")))) {
                error = "Failed to wait for Incident and Worker's Injury Details - Previous Related Injuries " + getData("Previous Related Injuries");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyClickOptionContainsXpath(getData("Previous Related Injuries")))) {
                error = "Failed to click Incident and Worker's Injury Details - Previous Related Injuries " + getData("Previous Related Injuries");
                return false;
            }

            if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.prevoiusRelatedInjuriesTable())) {
                error = "Failed to scroll to injury Claim form - Incident and Worker's Injury Details - Previous Related Injuries table";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.prevoiusRelatedInjuriesTable())) {
                error = "Failed to display injury Claim form - Incident and Worker's Injury Details - Previous Related Injuries table";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully filled Incident and Worker's Injury Details and Previous Related Injuries viewing grid is displayed showing");

        } else if (getData("Previous Related Injuries").equalsIgnoreCase("No")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.prevoiusInjuryDropdownXpath())) {
                error = "Failed to wait for injury Claim form - incident and Worker's Injury Details - Previous Related Injuries dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.prevoiusInjuryDropdownXpath())) {
                error = "Failed to scroll injury Claim form - incident and Worker's Injury Details - Previous Related Injuries dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.prevoiusInjuryDropdownXpath())) {
                error = "Failed to wait for injury Claim form - incident and Worker's Injury Details - Previous Related Injuries dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.prevoiusInjuryDropdownXpath())) {
                error = "Failed to click injury Claim form - incident and Worker's Injury Details - Previous Related Injuries dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.anySupervisorXpath(getData("Previous Related Injuries")))) {
                error = "Failed to wait for Incident and Worker's Injury Details - Third party " + getData("Previous Related Injuries");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anySupervisorXpath(getData("Previous Related Injuries")))) {
                error = "Failed to click Incident and Worker's Injury Details - Third party " + getData("Previous Related Injuries");
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully filled Incident and Worker's Injury Details no fields - Previous Related Injuries table are triggered. ");

//            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.returnWorkDetailsOpenTab(), 2))
//            {
//                if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.returnWorkDetailsTab()))
//                {
//                    error = "Failed to wait for Treatment and Return To Work Details   Tab ";
//                    return false;
//                }
//                if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.returnWorkDetailsTab()))
//                {
//                    error = "Failed to click Treatment and Return To Work Details   Tab ";
//                    return false;
//                }
//            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.workerEmploymentDetailsTab())) {
            error = "Failed to wait for Worker's Employment Details  Tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.workerEmploymentDetailsTab())) {
            error = "Failed to click Worker's Employment Details  Tab ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.nameOrgPayingInjuredXpath())) {
            error = "Failed to enter - Worker's Employment Details  - Name of organisation paying your wages when you were injured ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.nameOrgPayingInjuredXpath(), getData("Name Org paying injured"))) {
            error = "Failed to enter - Worker's Employment Details  - Name of organisation paying your wages when you were injured ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.workPlaceAddressXpath())) {
            error = "Failed to wait for - Worker's Employment Details  - Street address of your usual workplace  ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.workPlaceAddressXpath(), getData("Workplace Address"))) {
            error = "Failed to enter - Worker's Employment Details  - Street address of your usual workplace  ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.suburbXpath())) {
            error = "Failed to wait for - Worker's Employment Details  - Suburb ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.suburbXpath(), getData("Suburb"))) {
            error = "Failed to enter - Worker's Employment Details  - Suburb ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.stateXpath())) {
            error = "Failed to wait for - Worker's Employment Details  - State ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.stateXpath(), getData("Address state"))) {
            error = "Failed to enter - Worker's Employment Details  - State ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.postCodeXpath())) {
            error = "Failed to wait for - Worker's Employment Details  - Postcode ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.postCodeXpath(), getData("Postcode"))) {
            error = "Failed to enter - Worker's Employment Details  - Postcode ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.nameofEmployerXpath())) {
            error = "Failed to wait for - Worker's Employment Details  - Name of employer contact ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.nameofEmployerXpath(), getData("Name of Employer"))) {
            error = "Failed to enter - Worker's Employment Details  - Name of employer contact ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.NumOfEmployerXpath())) {
            error = "Failed to wait for - Worker's Employment Details  - Daytime contact number of employer contact ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.NumOfEmployerXpath(), getData("Number of Contact"))) {
            error = "Failed to enter - Worker's Employment Details  - Daytime contact number of employer contact ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.usualOccupationXpath())) {
            error = "Failed to wait for - Worker's Employment Details  - Daytime contact number of employer contact ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.usualOccupationXpath(), getData("Usual occupation"))) {
            error = "Failed to enter - Worker's Employment Details  - Daytime contact number of employer contact ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.anyDropDownCheckXpath(getData("apply to you")))) {
            error = "Failed to wait for Worker's Employment Details - Which of the following incident circumstances apply? " + getData("apply to you");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyDropDownCheckXpath(getData("apply to you")))) {
            error = "Failed to click Worker's Employment Details - Which of the following incident circumstances apply? " + getData("apply to you");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.startedWorkDateXpath())) {
            error = "Failed to wait for - Worker's Employment Details - When did you start working for this employer? ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.startedWorkDateXpath(), date)) {
            error = "Failed to enter - Worker's Employment Details - When did you start working for this employer? ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.anyContainsDropDownCheckXpath(getData("Indicate if any apply to you")))) {
            error = "Failed to wait for - Worker's Employment Details - Please indicate if any of the following apply to you  " + getData("Indicate if any apply to you");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyContainsDropDownCheckXpath(getData("Indicate if any apply to you")))) {
            error = "Failed to click - Worker's Employment Details - Please indicate if any of the following apply to you  " + getData("Indicate if any apply to you");
            return false;
        }
        String additionalDetails = getData("Additional details");

        if (additionalDetails.equalsIgnoreCase("Yes")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.employmentTimInjuredDropdownXpath())) {
                error = "Failed to wait for - Worker's Employment Details - Did you have any other employment at the time you were injured? dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.employmentTimInjuredDropdownXpath())) {
                error = "Failed to scroll - Worker's Employment Details - Did you have any other employment at the time you were injured? dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.employmentTimInjuredDropdownXpath())) {
                error = "Failed to wait for - Worker's Employment Details - Did you have any other employment at the time you were injured? dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.employmentTimInjuredDropdownXpath())) {
                error = "Failed to click - Worker's Employment Details - Did you have any other employment at the time you were injured? dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.anyClickOptionContainsXpath(additionalDetails))) {
                error = "Failed to wait for Incident and Worker's Injury Details - Did you have any other employment at the time you were injured? " + additionalDetails;
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyClickOptionContainsXpath(additionalDetails))) {
                error = "Failed to click Incident and Worker's Injury Details - Did you have any other employment at the time you were injured? " + additionalDetails;
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.addDetailsFieldXpath(), 2)) {
                error = "Failed to diplay - Worker's Employment Details - Additional details ";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.addDetailsFieldXpath(), getData("Additional details text"))) {
                error = "Failed to enter - Worker's Employment Details - Additional details";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully filled Did you have any other employment at the time you were injured and additional details field is displayed.");

        } else if (additionalDetails.equalsIgnoreCase("No")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.employmentTimInjuredDropdownXpath())) {
                error = "Failed to wait for - Worker's Employment Details - Did you have any other employment at the time you were injured? dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.employmentTimInjuredDropdownXpath())) {
                error = "Failed to scroll - Worker's Employment Details - Did you have any other employment at the time you were injured? dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.employmentTimInjuredDropdownXpath())) {
                error = "Failed to wait for - Worker's Employment Details - Did you have any other employment at the time you were injured? dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.employmentTimInjuredDropdownXpath())) {
                error = "Failed to click - Worker's Employment Details - Did you have any other employment at the time you were injured? dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.anySupervisorXpath(additionalDetails))) {
                error = "Failed to wait for Incident and Worker's Injury Details - Did you have any other employment at the time you were injured? " + additionalDetails;
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anySupervisorXpath(additionalDetails))) {
                error = "Failed to click Incident and Worker's Injury Details - Did you have any other employment at the time you were injured? " + additionalDetails;
                return false;
            }

            if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.addDetailsXpath(), 2)) {
                error = "Diplay - Worker's Employment Details - Additional details ";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully filled did you have any other employment at the time you were injured no fields are triggered. ");

        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.returnWorkDetailsOpenTab(), 2)) {
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.returnWorkDetailsTab())) {
                error = "Failed to wait for Treatment and Return To Work Details   Tab ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.returnWorkDetailsTab())) {
                error = "Failed to click Treatment and Return To Work Details   Tab ";
                return false;
            }
        }

        if (getData("Return to work").equalsIgnoreCase("Yes")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.doctorTreatmentXpath())) {
                error = "Failed to wait for - Treatment and Return To Work Details - Who is your nominated treating doctor?";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.doctorTreatmentXpath(), getData("nominated treating doctor"))) {
                error = "Failed to enter - Treatment and Return To Work Details - Who is your nominated treating doctor?";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.numOfTreatmentXpath())) {
                error = "Failed to wait for - Treatment and Return To Work Details - Number of your nominated treating doctor   ";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.numOfTreatmentXpath(), getData("Number of nominated treating doctor"))) {
                error = "Failed to enter - Treatment and Return To Work Details - Number of your nominated treating doctor   ";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.nameOfClinicXpath())) {
            error = "Failed to wait for - Treatment and Return To Work Details -  name (including Clinics or Hospitals) that have treated your injury";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.nameOfClinicXpath(), getData("Place Treated Injury"))) {
            error = "Failed to enter - Treatment and Return To Work Details -  name (including Clinics or Hospitals) that have treated your injury";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.contactDetailsXpath())) {
            error = "Failed to wait for - Treatment and Return To Work Details - contact details ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.contactDetailsXpath(), getData("Contact Details"))) {
            error = "Failed to enter - Treatment and Return To Work Details - contact details ";
            return false;
        }

        if (getData("Return to work").equalsIgnoreCase("Yes")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.sameEmployerDropdownXpath())) {
                error = "Failed to wait for - Treatment and Return To Work Details - Have you returned to your same employer dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.sameEmployerDropdownXpath())) {
                error = "Failed to enter - Treatment and Return To Work Details - Have you returned to your same employer dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.anyClickOptionContainsXpath(getData("Return to work")))) {
                error = "Failed to click Treatment and Return To Work Details - Have you returned to your same employer " + getData("Return to work");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyClickOptionContainsXpath(getData("Return to work")))) {
                error = "Failed to click Treatment and Return To Work Details - Have you returned to your same employer " + getData("Return to work");
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.returnDateXpath(), 2)) {
                error = "Failed to wait for - Treatment and Return To Work Details - What was the date? ";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.returnDateXpath(), date)) {
                error = "Failed to enter - Treatment and Return To Work Details - What was the date? ";
                return false;
            }

            String time = new SimpleDateFormat("HH:mm").format(new Date());
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.hourWorked())) {
                error = "Failed to wait for - Treatment and Return To Work Details - How many hours are you working? ";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.hourWorked(), time)) {
                error = "Failed to enter - Treatment and Return To Work Details - How many hours are you working? ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.dutiesdropdownXpath(), 2)) {
                error = "Failed to display - Treatment and Return To Work Details - What duties are you doing? ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.dutiesdropdownXpath())) {
                error = "Failed to click - Treatment and Return To Work Details - What duties are you doing? dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.anyClickOptionXpath(getData("duties")))) {
                error = "Failed to wait for - Treatment and Return To Work Details - What duties are you doing?";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyClickOptionXpath(getData("duties")))) {
                error = "Failed to click - Treatment and Return To Work Details - What duties are you doing?";
                return false;
            }

            if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.returnDateXpath())) {
                error = "Failed to diplay - Treatment and Return To Work Details - What was the date?";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully filled  Treatment and Return To Work Details and What duties are you doing? and How many hours are you working? fields are displayed is displayed.");

        } else if (additionalDetails.equalsIgnoreCase("No")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.sameEmployerDropdownXpath())) {
                error = "Failed to wait for - Treatment and Return To Work Details - Have you returned to your same employer dropdown ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.sameEmployerDropdownXpath())) {
                error = "Failed to enter - Treatment and Return To Work Details - Have you returned to your same employer dropdown ";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.anySupervisorXpath(getData("Return to work")))) {
                error = "Failed to wait for Treatment and Return To Work Details - Have you returned to your same employer " + getData("Return to work");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anySupervisorXpath(getData("Return to work")))) {
                error = "Failed to click Treatment and Return To Work Details - Have you returned to your same employer " + getData("Return to work");
                return false;
            }

            if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.returnDateXpath(), 2)) {
                error = "Display - Treatment and Return To Work Details - What was the date? ";
                return false;
            }

            if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.dutiesdropdownXpath(), 2)) {
                error = "Display - Treatment and Return To Work Details - What duties are you doing? ";
                return false;
            }

            if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.returnDateXpath(), 2)) {
                error = "Diplay - Treatment and Return To Work Details - What was the date?";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully filled Treatment and Return To Work Details no fields are triggered.");
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.returnClaimsDateXpath())) {
            error = "Failed to wait for - Treatment and Return To Work Details - When did / will you give your employer this claim form? ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.returnClaimsDateXpath(), startDate)) {
            error = "Failed to enter - Treatment and Return To Work Details - When did / will you give your employer this claim form? ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.DateOfMedCertDateXpath())) {
            error = "Failed to wait for - Treatment and Return To Work Details - When did / will you give your employer the first medical certificate? ";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.DateOfMedCertDateXpath(), startDate)) {
            error = "Failed to enter - Treatment and Return To Work Details - When did / will you give your employer the first medical certificate? ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.formToYouremployerddropdownXpath())) {
            error = "Failed to wait for - Treatment and Return To Work Details - How did / will you give this claim form to your employer? dropdown ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.formToYouremployerddropdownXpath())) {
            error = "Failed to enter - Treatment and Return To Work Details - How did / will you give this claim form to your employer? dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.dutiesOption(getData("Delivery")))) {
            error = "Failed to wait for Treatment and Return To Work Details - How did / will you give this claim form to your employer? dropdown" + getData("Delivery");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.dutiesOption(getData("Delivery")))) {
            error = "Failed to click Treatment and Return To Work Details - How did / will you give this claim form to your employer? dropdown" + getData("Delivery");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.saveXpath())) {
            error = "Failed to click to Save and close ";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        pause(3000);
        narrator.stepPassed("Successfully captured and saved injury claim form ");

        return true;
    }
}
