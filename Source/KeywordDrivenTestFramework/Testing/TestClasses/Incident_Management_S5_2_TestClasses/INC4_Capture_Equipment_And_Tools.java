/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "4-Capture Equipment and Tools",
        createNewBrowserInstance = false
)

public class INC4_Capture_Equipment_And_Tools extends BaseClass{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public INC4_Capture_Equipment_And_Tools(){
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest(){
        if (!ValidatePanelsAreDisplayed()){
            return narrator.testFailed("Failed to validate that the Persons Involved, Witness Statement and Equipment Involved panels are displayed - " + error);
        }
        if (!equipmentAndTools()){
            return narrator.testFailed("Failed to fill in incident report - " + error);
        }
        return narrator.finalizeTest("Successfully captured the details for Witness Statements");
    }

    public boolean ValidatePanelsAreDisplayed(){
        //2.Verification & Additional Detail
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.verificationAndAddDetTab())){
            error = "Failed to wait for the '2.Verification & Additional Detail' tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.verificationAndAddDetTab())){
            error = "Failed to click the '2.Verification & Additional Detail' tab";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.PersonsInvolvedXpath())){
            error = "Failed to validate that the Persons Involved panel is dispayed";
            return false;
        } else {
            String text = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.PersonsInvolvedXpath());

            if (text.equals(" ")) {
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Persons Involved"))){
                narrator.stepPassed("Successfully validated that the Persons Involved panel is displayed: " + text);
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.WitnessStatementsXpath())){
            error = "Failed to validate that the Witness Statement panel is dispayed";
            return false;
        } else {
            String text = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.WitnessStatementsXpath());

            if (text.equals(" ")) {
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Witness Statements"))) {
                narrator.stepPassed("Successfully validated that the Witness Statement panel is displayed: " + text);
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.EquipmentInvolvedTabXpath())) {
            error = "Failed to validate that the Equipment Involved panel is dispayed";
            return false;
        } else {
            String text = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.EquipmentInvolvedTabXpath());

            if (text.equals(" ")){
                error = "String cannot be empty";
                return false;
            } else if (text.equals(testData.getData("Equipment Involved"))){
                narrator.stepPassed("Successfully validated that the Equipment Involved panel is displayed: " + text);
            }
        }

        narrator.stepPassedWithScreenShot("Successfully validated that the Persons Involved, Witness Statement and Equipment Involved panels are displayed");
        return true;
    }

    public boolean equipmentAndTools(){
        //Equipment and Tools tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.euipmentInvolvedTabXpath())){
            error = "Failed to wait for Equipment and Tools tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.euipmentInvolvedTabXpath())){
            error = "Failed to click Equipment and Tools tab ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.EuipmentValidateGridIsNotEditableXPath())){
            error = "Failed to validate that the editable grid is displayed";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully validated that the Equipment & Tools non-editable grid is displayed");
        
        //Add button
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.euipmentAddXpath())){
            error = "Failed to click Equipment and Tools Add button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked add button");
        pause(15000);
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.equipAndToolsProcessFlow())){
            error = "Failed to wait for 'Process flow'.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.equipAndToolsProcessFlow())){
            error = "Failed to click on 'Process flow'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked process flow");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.assetTypeDropDownXpath())){
            error = "Failed to wait for 'Asset Type' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.assetTypeDropDownXpath())){
            error = "Failed to click on 'Asset Type' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyClickOptionContainXpath(getData("Asset Type")))){
            error = "Failed to click Equipment and Tools Asset Type option - " + getData("Asset Type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.assetDropDownXpath())){
            error = "Failed to click asset dropdown";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked '" + getData("Asset Type") + "'.");
        
//        if (!SeleniumDriverInstance.clickElementbyXpath(VerificationAndAdditionalPageObject.anyClickOptionArrowXpath(getData("Asset Type"))))
//        {
//            error = "Failed to click Equipment and Tools Asset Type arrow - " + getData("Asset Type");
//            return false;
//        }

        //Asset
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyClickOptionContainsXpath(getData("Asset")))){
            error = "Failed to click asset - " + getData("Asset");
            return false;
        }
        narrator.stepPassed("Successfully selected asset type : " + getData("Asset"));

        //Equipment description
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.equipmentDescXpath(), getData("Equipment description"))){
            error = "Failed to enter Equipment and Tools - Equipment description - " + getData("Equipment description");
            return false;
        }
        
        //Description of damage
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.descrOfDamageXpath(), getData("Description of damage"))){
            error = "Failed to enter Equipment and Tools - Description of damage - " + getData("Description of damage");
            return false;
        }
        
        //Damage cost
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.damageCostXpath(), getData("Damage cost"))){
            error = "Failed to enter Equipment and Tools - Damage cost - " + getData("Damage cost");
            return false;
        }
        
        //Equipment and Tools - Asset Type
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.equipmentDescXpath())){
            error = "Failed to click Equipment and Tools - Asset Type ";
            return false;
        }
        pause(1500);

        //Operator employment type
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.operatorEmTypeDropdownpath())){
            error = "Failed to click Equipment and Tools - Operator employment type ";
            return false;
        }
        
        //Operator employment type
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyActiveOptionDropdownXpath(getData("Operator employment type")))){
            error = "Failed to select Equipment and Tools - Operator employment type - " + getData("Operator employment type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.operatorDropdownpath())){
            error = "Failed to click Equipment and Tools - Operator  ";
            return false;
        }
        
        //Operator
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.anyActiveOptionsDropdownXpath(getData("Operator")))){
            error = "Failed to select Equipment and Tools - Operator  - " + getData("Operator");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyActiveOptionsDropdownXpath(getData("Operator"))))
        {
            error = "Failed to select Equipment and Tools - Operator  - " + getData("Operator");
            return false;
        }

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.EquipAndTools_Save_Xpath())){
            error = "Failed to wait for Equipment And Tools Save button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.EquipAndTools_Save_Xpath())){
            error = "Failed to click Equipment And Tools Save button.";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait())){
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2())){
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        
        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Management_PageObjects.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
        narrator.stepPassedWithScreenShot("Saved Equipment And Tools Details");

        //Close X
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.EquipAndTools_Close_Button())){
            error = "Failed to wait for X-Close button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.EquipAndTools_Close_Button())){
            error = "Failed to clicked on for X-Close button.";
            return false;
        }
        
        //Equipment and tools gridview
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Equipment_Tools_Record())){
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.Equipment_Tools_Record())){
                error = "Failed to wait for Equipment and tools record";
            }
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully viewed Equipment and tools record");
        pause(2000);
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Equipment_Tools_Record())){
            error = "Failed to click Equipment and tools record";
            return false;
        }
        pause(4000);
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.equipAndToolsProcessFlow())){
            error = "Failed to wait for 'Process flow'.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.equipAndToolsProcessFlow())){
            error = "Failed to click on 'Process flow'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked process flow");
        
        return true;
    }
}
