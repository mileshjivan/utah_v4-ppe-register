/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SKhumalo
 */

@KeywordAnnotation(
        Keyword = "14-View Overdue Concessions",
        createNewBrowserInstance = false
)

public class INC14_View_Overdue_Concessions extends BaseClass{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public INC14_View_Overdue_Concessions(){
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");     
    }

    public TestResult executeTest(){
        if (!Navigate_To_Incident_Management()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean Navigate_To_Incident_Management(){
        //Environmental, health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.navigate_EHS())){
            error = "Failed to wait for Environmental, health & Safety button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.navigate_EHS())){
            error = "Failed to click Environmental, health & Safety button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Navigated to Environmental, health & Safety module.");
        
        //Click Incident management tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentManagmentXpath())){
            error = "Failed to wait for Incident Management button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IncidentManagmentXpath())){
            error = "Failed to click Incident Management button.";
            return false;
        }
        pause(5000);
        narrator.stepPassedWithScreenShot("Successfully Navigated to Incident Management module.");

        //Click view filter
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.viewFilter_IncidentManagement())){
            error = "Failed to wait for Incident Management view filter button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.viewFilter_IncidentManagement())){
            error = "Failed to click Incident Management view filter button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Navigated to Incident Management module.");
        
        //Record number field
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.record_No())){
            error = "Failed to wait for 'Record number' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.record_No(), getData("Record number"))){
            error = "Failed to enter '" + getData("Record number") + "' into 'Record number' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + getData("Record number") + "' into 'Record number' field.");
       
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.serachBtn())){
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.serachBtn())){
            error = "Failed to click on 'Search' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");

        //Select record
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.selectRecord(getData("Record number")))){
            error = "Failed to wait for '" + getData("Record number") + "' record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.selectRecord(getData("Record number")))){
            error = "Failed to click on '" + getData("Record number") + "' record.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Record number") + "' record.");
        
        //Click the process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.processFlow())){
            error = "Failed to wait for process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.processFlow())){
            error = "Failed to click process flow.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Clicked a process flow");
            
        //2.Verification & Additional Detail
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.verificationAndAddDetTab())){
            error = "Failed to wait for the '2.Verification & Additional Detail' tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.verificationAndAddDetTab())){
            error = "Failed to click the '2.Verification & Additional Detail' tab";
            return false;
        }
        pause(2000);
        
        //Quality tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessions_QualityTab_xpath())) {
            error = "Failed to wait for Quality tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessions_QualityTab_xpath())) {
            error = "Failed to click Quality tab.";
            return false;
        }        
        
        //Quality concession expand
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.concessions_QualityConcession_Expandxpath())) {
            error = "Failed to wait for Quality concession expand button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.concessions_QualityConcession_Expandxpath())) {
            error = "Failed to click Quality concession expand button.";
            return false;
        }
        
        String QualityConcession_Status = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.QualityConcession_Status());
        //Validate Status 
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.QualityConcession_Status())){
            error = "Failed to wait for 'Status' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.ValidateByText(Incident_Management_PageObjects.QualityConcession_Status(), getData("Status"))){
            error = "Failed to validate '" + QualityConcession_Status + "' from '" + getData("Status") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully validated Expected Status: '" + QualityConcession_Status + "' and Actual Status: '" + getData("Status") + "'.");
        
        return true;
    }

}
