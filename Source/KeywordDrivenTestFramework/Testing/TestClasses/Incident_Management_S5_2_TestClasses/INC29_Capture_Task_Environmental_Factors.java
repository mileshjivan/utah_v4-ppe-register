/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "29-Capture Task Environmental Factors",
        createNewBrowserInstance = false
)

public class INC29_Capture_Task_Environmental_Factors extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;

    public INC29_Capture_Task_Environmental_Factors() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Capture_Task_Environmental_Factors()) {
            return narrator.testFailed(error);
        }

        return narrator.finalizeTest("Successfully Capture Why Analysis Record");
    }

    public boolean Capture_Task_Environmental_Factors(){
        //Individual / Team actions 
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Task_Environmental_Factors_Panel_1())){
            error = "Failed to wait for the Task / Environmental factors panel.";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.Task_Environmental_Factors_Panel_1())){
            error = "Failed to scroll to the Task / Environmental factors panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Task_Environmental_Factors_Panel_1())){
            error = "Failed to clicked the ask / Environmental factors panel.";
            return false;
        }
        narrator.stepPassed("Task / Environmental factors panel is displayed");
        
        //Task / Environmental factors
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IndividualTeamActions_Option(getData("Task / Environmental factors 1")))) {
            error = "Failed to wait for the Task / Environmental factors option: " + getData("Task / Environmental factors 1");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Incident_Management_PageObjects.IndividualTeamActions_Option(getData("Task / Environmental factors 1")))) {
            error = "Failed to click on the Individual / team actions option: " + getData("Task / Environmental factors 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IndividualTeamActions_Option1(getData("Task / Environmental factors 2")))) {
            error = "Failed to click on the Individual / team actions option: " + getData("Task / Environmental factors 2");
            return false;
        }
        narrator.stepPassedWithScreenShot("Individual / team actions option: " + getData("Task / Environmental factors 1") + " -> " + getData("Task / Environmental factors 2") + ".");

        //Task / Environmental factors details 
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.TaskEnvironmentalFactorsDetails(), getData("Task / Environmental factors details"))) {
            error = "Failed to enter the Task / Environmental factors details: " + getData("Task / Environmental factors details");
            return false;
        }
        narrator.stepPassedWithScreenShot("Task / Environmental factors details: " + getData("Task / Environmental factors details"));
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveButton())) {
            error = "Failed to wait for the Task / Environmental factors - save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.saveButton())) {
            error = "Failed to click on the Task / Environmental factors - save";
            return false;
        }
        
        //Save Mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 40)){
            error = "Webside too long to load wait reached the time out";
            return false;
        }        

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Management_PageObjects.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");    

        return true;
    }
}
