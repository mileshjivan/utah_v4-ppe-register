/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;


import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "19-Update Incident Management",
        createNewBrowserInstance = false
)
public class INC19_Update_Incident_Management extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;

    public INC19_Update_Incident_Management() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        String pathofImages = System.getProperty("user.dir") + "\\images";
        
    }

    public TestResult executeTest() {
        if (!UpdateIncidentManagement()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Completed Updating Incident Management - Main Scenario");
    }

    public boolean UpdateIncidentManagement() {
        String button = getData("Submit Step 2");
        
        //2.Verification & Additional Detail
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.verificationAndAddDetTab())){
            error = "Failed to wait for the '2.Verification & Additional Detail' tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.verificationAndAddDetTab())){
            error = "Failed to click the '2.Verification & Additional Detail' tab";
            return false;
        }
        pause(2000);
              
        //Right Arrow
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Incident_rightarrow())){
            error = "Failed to wait for right arrow.";
            return false;
        }
        for (int i = 0; i < 3; i++) {
            if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Incident_rightarrow())){
                error = "Failed to click on right arrow.";
                return false;
            }
        }
        
        //Consequence / Impact 
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.ConsequenceImpact())) {
            error = "Failed to wait incident management Consequence Impact tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.ConsequenceImpact())) {
            error = "Failed to click incident management Consequence Impact tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Consequence Impact tab.");
        
        if(!SeleniumDriverInstance.scrollDown()){
            error = "Failed to scroll down.";
            return false;
        }
        
        //I declare that I am authorised to ... check box
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.RiskAndImpactAssessment_DeclarationCheckBox_CheckBoxXpath())) {
            error = "Failed to scroll to I declare that I am authorised to ... check box.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.RiskAndImpactAssessment_DeclarationCheckBox_CheckBoxXpath())) {
            error = "Failed to wait for I declare that I am authorised to ... check box.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.RiskAndImpactAssessment_DeclarationCheckBox_CheckBoxXpath())) {
            error = "Failed to click I declare that I am authorised to ... check box.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked I declare that I am authorised to ... check box");

        //Declaration Comment
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.RiskAndImpactAssessment_DeclarationComment_InputXpath())) {
            error = "Failed to wait for Declaration Comment text field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.RiskAndImpactAssessment_DeclarationComment_InputXpath(), getData("DeclarationComments"))) {
            error = "Failed to enter text into Declaration Comment text field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Declaration Comment: '" + getData("DeclarationComments") + "'");

        //Lead inverstigator
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.RiskAndImpactAssessment_LeadInvestigator_SelectFieldxpath())) {
            error = "Failed to wait for Lead inverstigator Select dropdawon list.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.RiskAndImpactAssessment_LeadInvestigator_SelectFieldxpath())) {
            error = "Failed to click for Lead inverstigator Select dropdawon list.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.anyActiveOptionDropdownXpath(getData("LeadInvestigator")))) {
            error = "Failed to wait for EnterText into Lead inverstigator type search.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyActiveOptionDropdownXpath(getData("LeadInvestigator")))) {
            error = "Failed to EnterText into Lead inverstigator type search.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Lead inverstigato: '" + getData("LeadInvestigator") + "'");

        //Save, Save and continue to step 3
        if (button.equalsIgnoreCase("True")) {
            //Save and continue to Step 3 button
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.RiskAndImpactAssessment_SubmitStep2_ButtonXpath())) {
                error = "Failed to wait for Save and continue to Step 3 button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.RiskAndImpactAssessment_SubmitStep2_ButtonXpath())) {
                error = "Failed to click Save and continue to Step 3 button.";
                return false;
            }

            //Save Mask
            if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait())){
                if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2())){
                    error = "Webside too long to load wait reached the time out";
                    return false;
                }
            }

            //Validate if the record has been saved or not.
            if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Management_PageObjects.validateSave())){
                error = "Failed to wait for Save validation.";
                return false;
            }

            String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.validateSave());

            if (!SaveFloat.equals("Record saved")){
                narrator.stepPassedWithScreenShot("Failed to save record.");
                return false;
            }
            narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
            
            //Save
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.RiskAndImpactAssessment_Refresh_ButtonXpath())) {
                error = "Failed to wait for Save dropdown button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.RiskAndImpactAssessment_Refresh_ButtonXpath())) {
                error = "Failed to click Save Save dropdown button.";
                return false;
            }

        } else {
            //Save and continue to Step 3 button
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.RiskAndImpactAssessment_SaveAndContinueToStep3_ButtonXpath())) {
                error = "Failed to wait for Save and continue to Step 3 button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.RiskAndImpactAssessment_SaveAndContinueToStep3_ButtonXpath())) {
                error = "Failed to click Save and continue to Step 3 button.";
                return false;
            }
            
            //Save Mask
            if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait())){
                if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2())){
                    error = "Webside too long to load wait reached the time out";
                    return false;
                }
            }

            //Validate if the record has been saved or not.
            if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Management_PageObjects.validateSave())){
                error = "Failed to wait for Save validation.";
                return false;
            }

            String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.validateSave());

            if (!SaveFloat.equals("Record saved")){
                narrator.stepPassedWithScreenShot("Failed to save record.");
                return false;
            }
            narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

            //3.Under Investigation tab
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.activeUnderInvesgationXpath())) {
                error = "Failed to wait 3.Under Investigation phase";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.investigationTab())) {
                error = "Failed to scroll to 3.Under Investigation tab";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.investigationTab())) {
                error = "Failed to click 3.Under Investigation tab";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.investigationTab())) {
                error = "Failed to click second 3.Under Investigation tab";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully Captured Risk and Impact Assessment.");
            narrator.stepPassed("Declaration comment: " + getData("DeclarationComments"));
            narrator.stepPassed("lead investigator: " + getData("LeadInvestigator"));

        }

        return true;
    }

}