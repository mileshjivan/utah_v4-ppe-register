/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "41-Sign Off",
        createNewBrowserInstance = false
)
public class INC41_Sign_Off extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public INC41_Sign_Off() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!Sign_Off()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Passed Sign Off");
    }

    public boolean Sign_Off() {
        //I have included all required and necessary information in this Incident report, reviewed the details, ranked the incident appropriately and confirm that the investigation is completed
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.InvestigationCompleted_Checkbox())) {
            error = "Failed to wait for incident appropriately and confirm that the investigation is completed checkbox.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.InvestigationCompleted_Checkbox())) {
            error = "Failed to click the incident appropriately and confirm that the investigation is completed checkbox.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the incident appropriately and confirm that the investigation is completed checkbox.");
        
        //Investigation comments 
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.InvestigationComments())){
            error = "Failed to wait for Investigation comments text field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.InvestigationComments(), getData("Investigation comments"))){
            error = "Failed to enter for Investigation comments text field." + getData("Investigation comments");
            return false;
        }
        narrator.stepPassedWithScreenShot("Investigation comments: " + getData("Investigation comments"));
        
        //Do you Require Sign Off?
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.RequireSignOff())){
            error = "Failed to wait for Do you Require Sign Off?.";
            return false;
        }  
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.RequireSignOff())){
            error = "Failed to click for Do you Require Sign Off?.";
            return false;
        } 
        narrator.stepPassedWithScreenShot("Successfully clicked Do you Require Sign Off?.");
        
        if(getData("Step 4").equalsIgnoreCase("Yes")){
            //Save and continue to Step 4
            if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.SaveAndContinue_Step_4())){
                error = "Failed to wait for Save and continue to Step 4.";
                return false;
            }  
            if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.SaveAndContinue_Step_4())){
                error = "Failed to click for Save and continue to Step 4.";
                return false;
            } 
            narrator.stepPassedWithScreenShot("Successfully clicked Save and continue to Step 4.");
            
            //Save Mask
            if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait())){
                if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2())){
                    error = "Webside too long to load wait reached the time out";
                    return false;
                }
            }
            
            //4.Incident Sign Off
            if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Incident_SignOff_Tab())){
                error = "Failed to wait for '4.Incident Sign Off' tab.";
                return false;
            }
            if(!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.Incident_SignOff_Tab())){
                error = "Failed to scroll to the '4.Incident Sign Off' tab.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Incident_SignOff_Tab())){
                error = "Failed to click the '4.Incident Sign Off' tab.";
                return false;
            }
            narrator.stepPassedWithScreenShot("'4.Incident Sign Off' tab successfully displayed.");
            narrator.stepPassedWithScreenShot("Incident Management Process Flow : '4.Awaiting Sign Off'");
            
        }else{
            //Submit Step 3
            if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Submit_Step_3())){
                error = "Failed to wait for Submit Step 3.";
                return false;
            }  
            if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Submit_Step_3())){
                error = "Failed to click for Submit Step 3.";
                return false;
            } 
            narrator.stepPassedWithScreenShot("Successfully clicked Submit Step 3.");
            
            //Sav Mask
            if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait())){
                if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2())){
                    error = "Webside too long to load wait reached the time out";
                    return false;
                }
            }
            
        }
        return true;
    }

}
