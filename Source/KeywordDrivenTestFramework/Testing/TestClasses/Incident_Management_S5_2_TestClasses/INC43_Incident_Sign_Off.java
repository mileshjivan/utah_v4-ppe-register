/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "43-Incident Sign Off",
        createNewBrowserInstance = false
)
public class INC43_Incident_Sign_Off extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public INC43_Incident_Sign_Off() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!Incident_Sign_Off()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Passed Updating Incident Sign Off - Main Scenario");
    }

    public boolean Incident_Sign_Off() {
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentSignOff_Tabxpath())) {
            error = "Failed to wait for 4. Incident Sign Off Tab..";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IncidentSignOff_Tabxpath())){
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentSignOff_Tabxpath())){
                error = "Failed to wait for 4. Incident Sign Off Tab..";
                return false;
            }
            pause(9000);
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IncidentSignOff_Tabxpath())) {
                error = "Failed to click 4. Incident Sign Off Tab.";
                return false;
            }
        }
        pause(5000);
        narrator.stepPassedWithScreenShot("Updating Incident Sign Off.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentSignOff_Admin1_Optionxpath())) {
            error = "Failed to wait for Administrator 1 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IncidentSignOff_Admin1_Optionxpath())) {
            error = "Failed to wait for Administrator 1 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentSignOff_Admin1_Yesxpath())) {
            error = "Failed to wait for Administrator 1 Yes option.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IncidentSignOff_Admin1_Yesxpath())) {
            error = "Failed to click Administrator 1 Yes option.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentSignOff_Admin2_Optionxpath())) {
            error = "Failed to wait for Administrator 2 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IncidentSignOff_Admin2_Optionxpath())) {
            error = "Failed to wait for Administrator 2 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentSignOff_Admin2_Yesxpath())) {
            error = "Failed to wait for Administrator 2 Yes option.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IncidentSignOff_Admin2_Yesxpath())) {
            error = "Failed to click Administrator 2 Yes option.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentSignOff_Admin3_Optionxpath())) {
            error = "Failed to wait for Administrator 3 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IncidentSignOff_Admin3_Optionxpath())) {
            error = "Failed to wait for Administrator 3 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentSignOff_Admin3_Yesxpath())) {
            error = "Failed to wait for Administrator 3 Yes option.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IncidentSignOff_Admin3_Yesxpath())) {
            error = "Failed to click Administrator 3 Yes option.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentSignOff_Admin4_Optionxpath())) {
            error = "Failed to wait for Administrator 4 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IncidentSignOff_Admin4_Optionxpath())) {
            error = "Failed to wait for Administrator 4 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentSignOff_Admin4_Yesxpath())) {
            error = "Failed to wait for Administrator 4 Yes option.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IncidentSignOff_Admin4_Yesxpath())) {
            error = "Failed to click Administrator 4 Yes option.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentSignOff_Admin5_Optionxpath())) {
            error = "Failed to wait for Administrator 5 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IncidentSignOff_Admin5_Optionxpath())) {
            error = "Failed to wait for Administrator 5 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentSignOff_Admin5_Yesxpath())) {
            error = "Failed to wait for Administrator 5 Yes option.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IncidentSignOff_Admin5_Yesxpath())) {
            error = "Failed to click Administrator 5 Yes option.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentSignOff_Admin6_Optionxpath())) {
            error = "Failed to wait for Administrator 6 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IncidentSignOff_Admin6_Optionxpath())) {
            error = "Failed to wait for Administrator 6 select arrow.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentSignOff_Admin6_Yesxpath())) {
            error = "Failed to wait for Administrator 6 Yes option.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IncidentSignOff_Admin6_Yesxpath())) {
            error = "Failed to click Administrator 6 Yes option.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully selected YES for all the logged users.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentManagement_Save_Buttonxpath())) {
            error = "Failed to wait for Incident Management Save button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IncidentManagement_Save_Buttonxpath())) {
            error = "Failed to click Incident Management Save button.";
            return false;
        }

        if (!SeleniumDriverInstance.handleMask()) {
            error = "Failed to handle Mask.";
            return false;
        }
        pause(3000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentManagementId())) {
            error = "Failed to wait for Incident Management Record number.";
            return false;
        }

        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.IncidentManagementId())) {
            error = "Failed to scroll to Incident Management Record number.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentInvestigationStatus())) {
            error = "Failed to wait for Incident Status.";
            return false;
        }

        String status = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.IncidentInvestigationStatus());

        if (!status.equalsIgnoreCase("5.Closed")) {
            error = "Failed to validate investigation status 5.Closed with retrieved text: " + status;
            return false;
        } else {
            narrator.stepPassed("Incident Investigation Status: " + status);
        }

        return true;
    }

}
