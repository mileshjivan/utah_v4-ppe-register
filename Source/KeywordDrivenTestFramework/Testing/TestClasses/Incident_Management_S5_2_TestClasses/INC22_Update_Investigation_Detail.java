/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "22-Update Investigation Detail",
        createNewBrowserInstance = false
)
public class INC22_Update_Investigation_Detail extends BaseClass{
    String date;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String recordNumber2;

    public INC22_Update_Investigation_Detail(){
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        recordNumber2 = "";
    }

    public TestResult executeTest(){
        if (!AddInvestigationDetails()){
            return narrator.testFailed("Failed to add the investigation details - " + error);
        }
        return narrator.finalizeTest("Successfully validated panels based on Investigation type - " + getData("Investigation Type"));
    }

    public boolean AddInvestigationDetails(){
        //investigation details
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.InvestigationDetailTabXPath())){
            error = "Failed to wait for the investigation details sub tab ";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.InvestigationDetailTabXPath())){
            error = "Failed to scroll to the investigation details sub tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.InvestigationDetailTabXPath())){
            error = "Failed to click on the investigation details sub tab ";
            return false;
        }

        //Process/Activity
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.ProcessActivityDropdownXPath())){
            error = "Failed to wait for the Process/Activity dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.ProcessActivityDropdownXPath())){
            error = "Failed to click on the Process/Activity dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.specificTreeItemXPath(getData("Process Activity 1")))){
            error = "Failed to click on the Process/Activity option checkbox - " + getData("Process Activity 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.bodyPartsSelectInjuredLocationsXPath(getData("Process Activity 2")))){
            error = "Failed to click on the Process/Activity option checkbox - " + getData("Process Activity 2");
            return false;
        }
        narrator.stepPassedWithScreenShot("Process/Activity option checkbox - " + getData("Process Activity 1") + " " + getData("Process Activity 2"));

        //investigation details
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.InvestigationDetailTabXPath())){
            error = "Failed to click on the investigation details sub tab ";
            return false;
        }

        //Risk Source
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.RiskSourceDropdownXPath())){
            error = "Failed to wait for the Risk Source dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.RiskSourceDropdownXPath())){
            error = "Failed to click on the Risk Source dropdown.";
            return false;
        }     
        pause(2000);
        if (!SeleniumDriverInstance.clickElementByJavascript(Incident_Management_PageObjects.bodyPartsSelectInjuredLocationsXPath1(getData("Risk Source 1")))){
            error = "Failed to click to the Risk Source option checkbox - " + getData("Risk Source 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.bodyPartsSelectInjuredLocationsXPath(getData("Risk Source 2")))){
            error = "Failed to click on the Risk Source option checkbox - " + getData("Risk Source 2");
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.bodyPartsSelectInjuredLocationsXPath(getData("Risk Source 3")))){
            error = "Failed to click on the Risk Source option checkbox - " + getData("Risk Source 3");
            return false;
        }
        narrator.stepPassedWithScreenShot("Risk Source option checkbox - " + getData("Risk Source 1") + " -> " + getData("Risk Source 2"));
        narrator.stepPassedWithScreenShot("Risk Source option checkbox - " + getData("Risk Source 1") + " -> " + getData("Risk Source 3"));
        
        //investigation details sub tab
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.InvestigationDetailTabXPath())){
            error = "Failed to click on the investigation details sub tab ";
            return false;
        }

        //Risk
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.RiskDropdownXPath())){
            error = "Failed to wait for the Risk dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.RiskDropdownXPath())){
            error = "Failed to click on the Risk dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.bodyPartsSelectInjuredLocationsXPath2(getData("Risk")))){
            error = "Failed to wait for the Risk option checkbox - " + getData("Risk");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.bodyPartsSelectInjuredLocationsXPath2(getData("Risk")))){
            error = "Failed to click on the Risk option checkbox - " + getData("Risk");
            return false;
        }
        narrator.stepPassedWithScreenShot("Risk option checkbox - " + getData("Risk"));
                
        //investigation details
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.InvestigationDetailTabXPath())){
            error = "Failed to click on the investigation details sub tab ";
            return false;
        }
        String[] Investigation_Type = getData("Investigation Type").split(",");
        
        for(int i = 0; i < Investigation_Type.length; i++){
            //analysis Type
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.InvestigationTypeDropdownXPath())){
                error = "Failed to wait for the Investigation analysis Type dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.InvestigationTypeDropdownXPath())){
                error = "Failed to click on the Investigation analysis Type dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.dropdownElementXPath(Investigation_Type[i]))){
                error = "Failed to click on the Investigation analysis Type option - " + Investigation_Type[i];
                return false;
            }

            switch(Investigation_Type[i]){
                case "No Analysis Required":
                    //Reason for no investigation
                    if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.ReasonForNoInvestigation())){
                        error = "Failed to wait for the Reason for no investigation panel.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.ReasonForNoInvestigation(), getData("Reason for no investigation"))){
                        error = "Failed to wait for the Reason for no investigation panel.";
                        return false;
                    }
                    narrator.stepPassed("Reason for no investigation textarea is displayed");
                    break;
                    
                case "RCAT Analysis":
                    //RCAT Analysis Panel
                    if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.RCAT_Analysis_Panel())){
                        error = "Failed to wait for the RCAT Analysis Panel.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.RCAT_Analysis_Panel())){
                        error = "Failed to click the RCAT Analysis Panel.";
                        return false;
                    }
                    narrator.stepPassed("RCAT Analysis Panel is displayed");

                    //RCAT Immediate Causes Panel
                    if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.RCAT_Immediate_Causes_Panel())){
                        error = "Failed to wait for the RCAT Immediate Causes Panel.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.RCAT_Immediate_Causes_Panel())){
                        error = "Failed to scroll to the RCAT Immediate Causes Panel.";
                        return false;
                    }
                    
                    narrator.stepPassed("RCAT Immediate Causes Panel is displayed");

                    //RCAT Root Causes
                    if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.RCAT_Root_Causes_Panel())){
                        error = "Failed to click the RCAT Root Causes Panel.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.RCAT_Root_Causes_Panel())){
                        error = "Failed to click the RCAT Root Causes Panel.";
                        return false;
                    }
                    narrator.stepPassed("RCAT Root Causes Panel is displayed");
                    
                    //Absent and Failed Defenses
                    if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Absent_And_Failed_Defenses_Panel())){
                        error = "Failed to wait for the Absent and Failed Defenses Panel.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.Absent_And_Failed_Defenses_Panel())){
                        error = "Failed to scroll to the Absent and Failed Defenses Panel.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Absent_And_Failed_Defenses_Panel())){
                        error = "Failed to click the Absent and Failed Defenses Panel.";
                        return false;
                    }
                    narrator.stepPassed("Absent and Failed Defenses Panel is displayed");
                    break;
                    
                case "Why Analysis":
                    //Why Analysis
                    if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.WhyAnalysisPanelXPath())){
                        error = "Failed to wait for the Why Analysis panel.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.WhyAnalysisPanelXPath())){
                        error = "Failed to click the Why Analysis panel.";
                        return false;
                    }
                    narrator.stepPassed("Why Analysis panel is displayed");
                    
                    //Absent and Failed Defenses
                    if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Absent_And_Failed_Defenses_Panel())){
                        error = "Failed to wait for the Absent and Failed Defenses Panel.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.Absent_And_Failed_Defenses_Panel())){
                        error = "Failed to scroll to the Absent and Failed Defenses Panel.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.doubleClickElementbyXpath(Incident_Management_PageObjects.Absent_And_Failed_Defenses_Panel())){
                        error = "Failed to click the Absent and Failed Defenses Panel.";
                        return false;
                    }
                    narrator.stepPassed("Absent and Failed Defenses Panel is displayed");
                    
                    //Additional Information
                    if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.AdditionalInformation())){
                        error = "Failed to wait for 'Additional Information' check box.";
                        return false;
                    }
                    if(!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.AdditionalInformation())){
                        error = "Failed to scroll to 'Additional Information' check box.";
                        return false;
                    }
                    narrator.stepPassed("Additional Information check box is displayed");
                    break;
                    
                case "Mini ICAM Analysis":
                    //Why Analysis
                    if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.WhyAnalysisPanelXPath())){
                        error = "Failed to wait for the Why Analysis panel.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.WhyAnalysisPanelXPath())){
                        error = "Failed to click the Why Analysis panel.";
                        return false;
                    }
                    narrator.stepPassed("Why Analysis panel is displayed");
                    
                    //Absent and Failed Defenses
                    if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Absent_And_Failed_Defenses_Panel())){
                        error = "Failed to wait for the Absent and Failed Defenses Panel.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.Absent_And_Failed_Defenses_Panel())){
                        error = "Failed to scroll to the Absent and Failed Defenses Panel.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.doubleClickElementbyXpath(Incident_Management_PageObjects.Absent_And_Failed_Defenses_Panel())){
                        error = "Failed to click the Absent and Failed Defenses Panel.";
                        return false;
                    }
                    narrator.stepPassed("Absent and Failed Defenses Panel is displayed");
                    
                    //Additional Information
                    if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.AdditionalInformation())){
                        error = "Failed to wait for 'Additional Information' check box.";
                        return false;
                    }
                    if(!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.AdditionalInformation())){
                        error = "Failed to scroll to 'Additional Information' check box.";
                        return false;
                    }
                    narrator.stepPassed("Additional Information check box is displayed");
                    break;
                    
                case "Full ICAM Analysis":
                    //Why Analysis
                    if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.WhyAnalysisPanelXPath())){
                        error = "Failed to wait for the Why Analysis panel.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.WhyAnalysisPanelXPath())){
                        error = "Failed to click the Why Analysis panel.";
                        return false;
                    }
                    narrator.stepPassed("Why Analysis panel is displayed");
                    
                    //Absent and Failed Defenses
                    if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Absent_And_Failed_Defenses_Panel())){
                        error = "Failed to wait for the Absent and Failed Defenses Panel.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.Absent_And_Failed_Defenses_Panel())){
                        error = "Failed to scroll to the Absent and Failed Defenses Panel.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.doubleClickElementbyXpath(Incident_Management_PageObjects.Absent_And_Failed_Defenses_Panel())){
                        error = "Failed to click the Absent and Failed Defenses Panel.";
                        return false;
                    }
                    narrator.stepPassed("Absent and Failed Defenses Panel is displayed");
                    
                    //Individual / Team
                    if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Individual_Team_Panel())){
                        error = "Failed to wait for the Individual / Team panel.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.Individual_Team_Panel())){
                        error = "Failed to scroll to the Individual / Team panel.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Individual_Team_Panel())){
                        error = "Failed to click the Individual / Team panel.";
                        return false;
                    }
                    narrator.stepPassed("Individual / Team panel is displayed");
                    
                    //Task / Environmental factors 
                    if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Task_Environmental_Factors_Panel())){
                        error = "Failed to wait for the Task / Environmental factors panel.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.Task_Environmental_Factors_Panel())){
                        error = "Failed to scroll to the Task / Environmental factors panel.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Task_Environmental_Factors_Panel())){
                        error = "Failed to click the Task / Environmental factors panel.";
                        return false;
                    }
                    narrator.stepPassed("Task / Environmental factors panel is displayed");
                    
                    //Organisational factors 
                    if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Organisational_Factors_Panel())){
                        error = "Failed to wait for the Organisational factors panel.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.Organisational_Factors_Panel())){
                        error = "Failed to scroll for the Organisational factors panel.";
                        return false;
                    }
                    if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Organisational_Factors_Panel())){
                        error = "Failed to click the Organisational factors panel.";
                        return false;
                    }
                    narrator.stepPassed("Organisational factors panel is displayed");
                    
                    //Additional Information checkbox
                    if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.AdditionalInformation())){
                        error = "Failed to wait for 'Additional Information' check box.";
                        return false;
                    }
                    if(!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.AdditionalInformation())){
                        error = "Failed to scroll to 'Additional Information' check box.";
                        return false;
                    }
                    narrator.stepPassed("Additional Information check box is displayed");
                    
                    break;
                    default:
                        error = "No such option for 'Reason for investigation'.";
            }
            //investigation details sub 
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.InvestigationDetailTabXPath())){
                error = "Failed to click on the investigation details sub tab ";
                return false;
            }
            
            //Refresh
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Refresh_Button())){
                error = "Failed to wait for the Refresh button.";
                return false;
            }
            if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.Refresh_Button())){
                error = "Failed to scroll to the Refresh button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Refresh_Button())){
                error = "Failed to click for the Refresh button.";
                return false;
            }
            narrator.stepPassed("Successfully clicked the Refresh button.");
            
            //Save Mask
            if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait())){
                if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 40)){
                    error = "Webside too long to load wait reached the time out";
                    return false;
                }
            }
            
//            //Validate if the record has been saved or not.
//            if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Management_PageObjects.validateSave())){
//                error = "Failed to wait for Save validation.";
//                return false;
//            }
//
//            String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.validateSave());
//
//            if (!SaveFloat.equals("Record saved")){
//                narrator.stepPassedWithScreenShot("Failed to save record.");
//                return false;
//            }
//            narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");            
        }
        
        pause(2000);
        
        return true;
    }
}
