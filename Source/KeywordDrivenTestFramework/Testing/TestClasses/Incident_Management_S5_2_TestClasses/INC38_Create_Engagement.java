/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "38-Create Engagement",
        createNewBrowserInstance = false
)

public class INC38_Create_Engagement extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;
    String path;

    public INC38_Create_Engagement() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        String pathofImages = System.getProperty("user.dir") + "\\images";
        path = new File(pathofImages).getAbsolutePath();

    }

    public TestResult executeTest() {
        if (!Create_Engagement()) {
            return narrator.testFailed("Failed to Create Communication - " + error);
        }

        return narrator.finalizeTest("Successfully Create Communication record is saved.");
    }

    public boolean Create_Engagement(){
        //Engagement Tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.CommunicationTab())) {
            error = "Failed to wait for - Incident Managament - Communication tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.CommunicationTab())) {
            error = "Failed to click on - Incident Managament - Communication tab ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked - Incident Managament - communication tab");

        //External Communication
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.ExternalCommunicationTab())) {
            error = "Failed to wait for - Incident Managament - External Communication tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.ExternalCommunicationTab())) {
            error = "Failed to click on - Incident Managament - External Communication tab ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked - Incident Managament - External Communication tab");
        
        //Create Engagement Panel
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.CreateEngagements_Panel())) {
            error = "Failed to wait for - Incident Managament - Create Engagement panel ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.CreateEngagements_AddButton())) {
            error = "Failed to wait for - Incident Managament - Create Engagement Add Button ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.CreateEngagements_AddButton())) {
            error = "Failed to click on - Incident Managament - Create Engagement Add Button ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked - Incident Managament - Create Engagement Add Button.");
        
        //Create Engagement Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.CreateEngagements_ProcessFlow())) {
            error = "Failed to wait for - Incident Managament - Create Engagement Process Flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.CreateEngagements_ProcessFlow())) {
            error = "Failed to click the - Incident Managament - Create Engagement Process flow.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Create Engagement Process Flow.");
        
        //EngagementTitle
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.EngagementTitle())) {
            error = "Failed to wait for - Incident Managament - Engagement Title: " + getData("Engagement Title");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.EngagementTitle(), getData("Engagement Title"))) {
            error = "Failed to click on - Incident Managament - Engagement Title: " + getData("Engagement Title");
            return false;
        }
        narrator.stepPassedWithScreenShot("Incident Managament - Engagement Title: " + getData("Engagement Title"));
        
        //EngagementDescription
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.EngagementDescription())) {
            error = "Failed to wait for - Incident Managament - Engagement Description: " + getData("Engagement Description");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.EngagementDescription(), getData("Engagement Description"))) {
            error = "Failed to click on - Incident Managament - Engagement Description: " + getData("Engagement Description");
            return false;
        }
        narrator.stepPassedWithScreenShot("Incident Managament - Engagement Description: " + getData("Engagement Description"));
        
        //Upload Supporting Documents
        //Upload a link 
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.link_buttonxpath())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.link_buttonxpath())) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow()) {
            error = "Failed to switch tab.";
            return false;
        }

        //Link
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.LinkURL())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.LinkURL(), getData("Document Link"))) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.urlTitle())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.urlTitle(), getData("Title"))) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Entered Url: '" + getData("Document Link") + "' and 'Title: '" + getData("Title") + "'.");

        //url Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.urlAddButton())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.urlAddButton())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Incident_Management_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        
        //Communication - Save Button
        if(getData("Save To Continue").equalsIgnoreCase("Yes")){
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentEngagement_SaveButton())) {
                error = "Failed to wait for Communication - Save Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentEngagement_SaveButton())) {
                error = "Failed to click on Communication - Save Button";
                return false;
            }
        } else{
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentEngagement_SaveToContinue_Button())) {
                error = "Failed to wait for Communication - Save to continue Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentEngagement_SaveToContinue_Button())) {
                error = "Failed to click on Communication - Save to continue Button";
                return false;
            }
        }
        
        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects.saveWait2(), 400)){
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        
        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects.validateSave());
        
        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully."); 
       
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.activeToBeInitiatedXpath())) {
            error = "Failed to save on - Create Communication record";
            return false;
        }

        String[] CommunicationID = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.incidentCommunicationID()).split("#");
        setCommunicationId(CommunicationID[1]);

        narrator.stepPassed("Create Communication Record Number: " + CommunicationID[1]);
        narrator.stepPassedWithScreenShot("Successfully saved Create Communication ");

        //Incident Management - Create Communication - Close Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentManagement_CreateEngagement_CloseButton())){
            error = "Failed to wait for Incident Management - Create Engagement - Close Button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IncidentManagement_CreateEngagement_CloseButton())){
            error = "Failed to click Incident Management - Create Engagement - Close Button";
            return false;
        }
        pause(2000);
        
        //Incident Management - Create Communication Gridview Record
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.IncidentManagement_CreateEngagement_Gridview())){
            error = "Failed to wait for Incident Management - Create Engagement Gridview Record";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.IncidentManagement_CreateEngagement_Gridview())){
            error = "Failed to click for Incident Management - Create Engagement Gridview Record";
            return false;
        }

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IM_processFlow())) {
            error = "Fail to wait for Incident Management process flow";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IM_processFlow())) {
            error = "Fail to click Incident Management process flow";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Incident Management process flow.");
        pause(2000);
        
        return true;
    }

}
