/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "25-Capture Full ICAM Individual Team",
        createNewBrowserInstance = false
)

public class INC25_Capture_Full_ICAM_Individual_Team extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;

    public INC25_Capture_Full_ICAM_Individual_Team() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Capture_Why_Analysis()) {
            return narrator.testFailed(error);
        }

        return narrator.finalizeTest("Successfully Capture Why Analysis Record");
    }

    public boolean Capture_Why_Analysis() {
        //Individual / Team
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Individual_Team_Panel())){
            error = "Failed to wait for the Individual / Team panel.";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.Individual_Team_Panel())){
            error = "Failed to scroll to the Individual / Team panel.";
            return false;
        }
        narrator.stepPassed("Individual / Team panel is displayed");
        
        //Individual / Team add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.individualTeam_AddButton())) {
            error = "Failed to wait for the Individual / Team add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.individualTeam_AddButton())) {
            error = "Failed to click on the Individual / Team add button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Individual / Team add button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.CauseAnalysis_Dropdown(), 3)) {
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.individualTeam_AddButton())) {
                error = "Failed to click on the Individual / Team add button";
                return false;
            }
        }
        
        //Cause analysis
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.CauseAnalysis_Dropdown())) {
            error = "Failed to wait for the Cause analysis dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.CauseAnalysis_Dropdown())) {
            error = "Failed to click the Cause analysis dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.CauseAnalysis_Option(getData("Cause analysis")))) {
            error = "Failed to wait for the Cause analysis option: " + getData("Cause analysis");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.CauseAnalysis_Option(getData("Cause analysis")))) {
            error = "Failed to click the Cause analysis option: " + getData("Cause analysis");
            return false;
        }
        narrator.stepPassedWithScreenShot("Cause analysis option: " + getData("Cause analysis"));
        
        //Inappropriate behaviour
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.InappropriateBehaviour_Dropdown())) {
            error = "Failed to wait for the Inappropriate behaviour dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.InappropriateBehaviour_Dropdown())) {
            error = "Failed to click the Inappropriate behaviour dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.InappropriateBehaviour_Option(getData("Inappropriate behaviour")))) {
            error = "Failed to wait for the Inappropriate behaviour option: " + getData("Inappropriate behaviour");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.InappropriateBehaviour_Option(getData("Inappropriate behaviour")))) {
            error = "Failed to click the Inappropriate behaviour option: " + getData("Inappropriate behaviour");
            return false;
        }
        narrator.stepPassedWithScreenShot("Inappropriate behaviour option: " + getData("Inappropriate behaviour"));
        
        //Cause selection
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.CauseSelection_Dropdown())) {
            error = "Failed to wait for the Cause selection dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.CauseSelection_Dropdown())) {
            error = "Failed to click the Cause selection dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.CauseSelection_Option(getData("Cause selection")))) {
            error = "Failed to wait for the Cause selection option: " + getData("Cause selection");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.CauseSelection_Option(getData("Cause selection")))) {
            error = "Failed to click the Cause selection option: " + getData("Cause selection");
            return false;
        }
        narrator.stepPassedWithScreenShot("Cause selection option: " + getData("Cause selection"));
        
        //Description
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.Description(), getData("Description"))) {
            error = "Failed to enter the Individual / Team - Description";
            return false;
        }
        narrator.stepPassedWithScreenShot("Individual / Team - Description: " + getData("Description"));

        //Task/Environmental Conditions
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.TaskEnvironmentalConditions_Dropdown())) {
            error = "Failed to wait for the Task/Environmental Conditions dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.TaskEnvironmentalConditions_Dropdown())) {
            error = "Failed to click the Task/Environmental Conditions dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.TaskEnvironmentalConditions_Option(getData("Task/Environmental Conditions")))) {
            error = "Failed to wait for the Task/Environmental Conditions option: " + getData("Task/Environmental Conditions");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.TaskEnvironmentalConditions_Option(getData("Task/Environmental Conditions")))) {
            error = "Failed to click the Task/Environmental Conditions option: " + getData("Task/Environmental Conditions");
            return false;
        }
        narrator.stepPassedWithScreenShot("Task/Environmental Conditions option: " + getData("Task/Environmental Conditions"));
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Individual_Environment_SaveButton())) {
            error = "Failed to wait for the why Analusis - save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Individual_Environment_SaveButton())) {
            error = "Failed to click on the why Analusis - save";
            return false;
        }
        
        //Save Mask
        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait())){
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 40)){
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Management_PageObjects.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");    

        return true;
    }
}
