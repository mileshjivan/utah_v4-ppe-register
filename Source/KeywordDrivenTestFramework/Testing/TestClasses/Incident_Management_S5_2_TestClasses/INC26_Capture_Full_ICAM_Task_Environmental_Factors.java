/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "26-Capture Full ICAM Task Environmental Factors",
        createNewBrowserInstance = false
)

public class INC26_Capture_Full_ICAM_Task_Environmental_Factors extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;

    public INC26_Capture_Full_ICAM_Task_Environmental_Factors() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Capture_Full_ICAM_Task_Environmental_Factors()) {
            return narrator.testFailed(error);
        }

        return narrator.finalizeTest("Successfully Capture Why Analysis Record");
    }

    public boolean Capture_Full_ICAM_Task_Environmental_Factors() {
        //Task / Environmental factors 
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Task_Environmental_Factors_Panel())){
            error = "Failed to wait for the Task / Environmental factors panel.";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.Task_Environmental_Factors_Panel())){
            error = "Failed to scroll to the Task / Environmental factors panel.";
            return false;
        }
        narrator.stepPassed("Task / Environmental factors panel is displayed");
        
        //Individual / Team add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.TaskEnvironmentalFactors_AddButton())) {
            error = "Failed to wait for the Task / Environmental factors add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.TaskEnvironmentalFactors_AddButton())) {
            error = "Failed to click on the Task / Environmental factors add button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the Task / Environmental factors add button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.TaskEnvironmentalFactors_CauseAnalysis_Dropdown(), 3)) {
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.individualTeam_AddButton())) {
                error = "Failed to click on the Task / Environmental factors add button";
                return false;
            }
        }
        
        //Cause analysis
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.TaskEnvironmentalFactors_CauseAnalysis_Dropdown())) {
            error = "Failed to wait for the Cause analysis dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.TaskEnvironmentalFactors_CauseAnalysis_Dropdown())) {
            error = "Failed to click the Cause analysis dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.CauseAnalysis_Option(getData("Cause analysis")))) {
            error = "Failed to wait for the Cause analysis option: " + getData("Cause analysis");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.CauseAnalysis_Option(getData("Cause analysis")))) {
            error = "Failed to click the Cause analysis option: " + getData("Cause analysis");
            return false;
        }
        narrator.stepPassedWithScreenShot("Cause analysis option: " + getData("Cause analysis"));
       
        //Cause selection
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.TaskEnvironmentalFactors_CauseSelection_Dropdown())) {
            error = "Failed to wait for the Cause selection dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.TaskEnvironmentalFactors_CauseSelection_Dropdown())) {
            error = "Failed to click the Cause selection dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.CauseSelection_Option(getData("Cause selection")))) {
            error = "Failed to wait for the Cause selection option: " + getData("Cause selection");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.CauseSelection_Option(getData("Cause selection")))) {
            error = "Failed to click the Cause selection option: " + getData("Cause selection");
            return false;
        }
        narrator.stepPassedWithScreenShot("Cause selection option: " + getData("Cause selection"));
        
        //Organisational factors
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.OrganisationalFactors_Dropdown())) {
            error = "Failed to wait for the Organisational factors dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.OrganisationalFactors_Dropdown())) {
            error = "Failed to click the Organisational factors dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.OrganisationalFactors_Option(getData("Organisational factors")))) {
            error = "Failed to wait for the Organisational factors option: " + getData("Organisational factors");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.OrganisationalFactors_Option(getData("Organisational factors")))) {
            error = "Failed to click the Organisational factors option: " + getData("Organisational factors");
            return false;
        }
        narrator.stepPassedWithScreenShot("Organisational factors option: " + getData("Organisational factors"));
        
        //Description
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.TaskEnvironmentFactors_Description(), getData("Description"))) {
            error = "Failed to enter the Task / Environmental factors - Description";
            return false;
        }
        narrator.stepPassedWithScreenShot("Task / Environmental factors - Description: " + getData("Description"));

        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.TaskEnvironmentFactors_SaveButton())) {
            error = "Failed to wait for the Task / Environmental factors - save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.TaskEnvironmentFactors_SaveButton())) {
            error = "Failed to click on the Task / Environmental factors - save";
            return false;
        }
        
        //Save Mask
        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait())){
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 40)){
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Management_PageObjects.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");    

        return true;
    }
}
