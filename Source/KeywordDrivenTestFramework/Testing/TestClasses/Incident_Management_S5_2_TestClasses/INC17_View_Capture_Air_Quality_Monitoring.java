/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.util.Set;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SKhumalo
 */

@KeywordAnnotation(
        Keyword = "17-View Capture Air Quality Monitoring",
        createNewBrowserInstance = false
)

public class INC17_View_Capture_Air_Quality_Monitoring extends BaseClass{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String monitorAreas = "";
      
    public INC17_View_Capture_Air_Quality_Monitoring(){
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest(){
        if (!NavigateToAirQualityMonitoring()){
            return narrator.testFailed("Failed due - " + error);
        }
        if(!CaptureAirQualityMonitoring()){
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Capture Air Quality Monitoring");
    }

    public boolean NavigateToAirQualityMonitoring(){
        //Environment tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.EnvironmentTabXPath())) {
            error = "Failed to wait to Environment tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.EnvironmentTabXPath())) {
            error = "Failed to click to Environment tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environment' tab.");
        
        //Monitoring required
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.monitoringRequired_Checkbox())){
            error = "Failed to wait for 'Monitoring required' checkbox.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.monitoringRequired_Checkbox())){
            error = "Failed to click on 'Monitoring required' checkbox.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Monitoring required' checkbox.");
        
        //Monitoring panel
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.monitoring_Panel())){
            error = "Failed to wait for 'Monitoring' panel.";
            return false;
        }
        if(!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.monitoring_Panel())){
            error = "Failed to scroll to 'Monitoring' panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully viewed 'Monitoring' panel.");
        
        //Select all monitoring areas
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.monitoringAreas_selectAll())){
            error = "Failed to wait for select all element.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.monitoringAreas_selectAll())){
            error = "Failed to click select all element.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked select all element.");
        
        //Monitoring - Air panel
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.monitoringAir_Panel())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.monitoringAir_Panel())){
                error = "Failed to wait for 'Monitoring - Air' panel.";
                return false;
            }
        }
        
        //Monitoring - Air Add Button 
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.monitoringAir_AddButton())){
            error = "Failed to wait for 'Monitoring - Air' Add Button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.monitoringAir_AddButton())){
            error = "Failed to click 'Monitoring - Air' Add Button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Monitoring - Air' Add Button.");
        
        return true;
    }
    
    public boolean CaptureAirQualityMonitoring(){
        
        String businessUnit = getData("Business unit");
        String airQualityType = getData("Air Quality Type");
        String month = getData("Month");
        String year = getData("Year");

        //switch to new window
        if (!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new window or tab.";
            return false;
        }
        pause(300);

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.iframe_Xpath())){
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Incident_Management_PageObjects.iframe_Xpath())){
            error = "Failed to switch to frame.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.airMonitorFlowProcessButton())) {
            error = "Failed to wait Air flow process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.airMonitorFlowProcessButton())) {
            error = "Failed to click Air flow process flow.";
            return false;
        }

        //Business Unit
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.businessUnit_DropDown())){
            error = "Failed to wait for incident occured drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.businessUnit_DropDown())){
            error = "Failed to click incident occured drop down.";
            return false;
        }        
        //Global Company
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.GlobalCompanyXpath())){
            error = "Failed to click Global Company.";
            return false;
        }
        //South Africa
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.SouthAfrica())){
            error = "Failed to click South Africa drop down.";
            return false;
        }
        //Victory Site
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.selectGlobalCompany(getData("Business Unit")))){
            error = "Failed to wait for Business Unit option: " + getData("Business Unit");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.selectGlobalCompany(getData("Business Unit")))){
            error = "Failed to click Business Unit option: " + getData("Business Unit");
            return false;
        }
        narrator.stepPassedWithScreenShot("Business unit : '" + getData("Business Unit") + "'.");
        
        //Air quality type
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.airQualityType())){
            error = "Failed to wait for 'Air Quality Type' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.airQualityType())){
            error = "Failed to click on 'Air Quality Type' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.airQualityTypeSelect(getData("Air Quality Type")))){
            error = "Failed to wait for Air Quality Type option: '" + getData("Air Quality Type") + "'.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.airQualityTypeSelect(getData("Air Quality Type")))){
            error = "Failed to select Air Quality Type option: '" + getData("Air Quality Type") + "'.";
            return false;
        }
        airQualityType = getData("Air Quality Type");
        
        switch(airQualityType){
            case "Emission":
                //Monitoring point
                narrator.stepPassedWithScreenShot("Air Quality Type : '" + airQualityType + "'.");
                
                if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.monitoringPointTab())){
                    error = "Failed to wait for 'Monitoring point' tab.";
                    return false;
                }
                if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.monitoringPointTab())){
                    error = "Failed to click on 'Monitoring point' tab.";
                    return false;
                }
                if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.monitoringPointOption(getData("Monitoring Point")))){
                    error = "Failed to wait for '" + getData("Monitoring Point") + "' option.";
                    return false;
                }
                if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.monitoringPointOption(getData("Monitoring Point")))){
                    error = "Failed to click on '" + getData("Monitoring Point") + "' option.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Monitoring Point : '" + getData("Monitoring Point") + "'.");
                break;
            default:
                narrator.stepPassedWithScreenShot("Air Quality Type : '" + airQualityType + "'.");
        } 
        
        //Month / Year
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.monthTab())){
            error = "Failed to wait for 'Month' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.monthTab())){
            error = "Failed to click on 'Month' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.monthOption(getData("Month")))){
            error = "Failed to wait for '" + getData("Month") + "' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.monthOption(getData("Month")))){
            error = "Failed to click on '" + getData("Month") + "' option.";
            return false;
        }
        
        //Year
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.yearTab())){
            error = "Failed to wait for 'Year' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.yearTab())){
            error = "Failed to click on 'Year' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.yearOption(getData("Year")))){
            error = "Failed to wait for '" + getData("Year") + "' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.yearOption(getData("Year")))){
            error = "Failed to click on '" + getData("Year") + "' tab.";
            return false;
        }
        
        //Add support document
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.uploadLinkbox())){
            error = "Failed to wait for 'Link box' link.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.uploadLinkbox())){
            error = "Failed to click on 'Link box' link.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");
        
        //switch to new window
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new window or tab.";
            return false;
        }
        
        //URL https
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.LinkURL())){
            error = "Failed to wait for 'URL value' field.";
            return false;
        }      
        if(!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.LinkURL(), getData("Document Link") )){
            error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");
        
        //Title
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.urlTitle())){
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.urlTitle(), getData("Title"))){
            error = "Failed to enter '" + getData("Title") + "' into 'Url Title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Title : '" + getData("Title") + "'.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.urlAddButton())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.urlAddButton())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");
        
         //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Incident_Management_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        
        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.airSaveButton())) {
            error = "Failed to click on save button ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.airSaveButton())) {
            error = "Failed to click on save button ";
            return false;
        }
        
        //Saving mask
        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        
        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Management_PageObjects.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        String[] airRecord = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.getAirMonitorRecord()).split("#");

        narrator.stepPassedWithScreenShot("Successfully saved Air Quality Monitoring " + airRecord[1]);

        Set<String> set = SeleniumDriverInstance.Driver.getWindowHandles();
        String[] win = set.stream().toArray(String[]::new);
        SeleniumDriverInstance.Driver.switchTo().window(win[1]).close();
        SeleniumDriverInstance.Driver.switchTo().window(win[0]);

        SeleniumDriverInstance.switchToDefaultContent();

        if (!SeleniumDriverInstance.switchToFrameByXpath(Incident_Management_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame of " + monitorAreas;
            return false;
        }
        
        //Water tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.water_Tab())){
            error = "Failed to wait for 'Water' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.water_Tab())){
            error = "Failed to click 'Water' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Air_Tab())){
            error = "Failed to click 'Air' tab.";
            return false;
        }

        //Refresh
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.refButton())) {
            error = "Failed to wait for refreach button ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.refButton())) {
            error = "Failed to click on refreach button ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.validateEnvirSpillgridSavedRecord(airRecord[1]), 14)) {
            error = "Failed to save record in Incident Management edit page " + airRecord[1];
            return false;
        }
        
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.validateEnvirSpillgridSavedRecord(airRecord[1]))) {
            error = "Failed to scrol record in Incident Management edit page " + airRecord[1];
            return false;
        }
        
        return true;
    }
    
}
