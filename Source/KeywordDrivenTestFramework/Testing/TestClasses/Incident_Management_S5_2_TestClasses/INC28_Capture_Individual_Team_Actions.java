/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "28-Capture Individual Team actions",
        createNewBrowserInstance = false
)

public class INC28_Capture_Individual_Team_Actions extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;

    public INC28_Capture_Individual_Team_Actions() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Capture_Individual_Team_Actions()) {
            return narrator.testFailed(error);
        }

        return narrator.finalizeTest("Successfully Capture Why Analysis Record");
    }

    public boolean Capture_Individual_Team_Actions(){
        //Individual / Team actions
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Individual_Team_Actions_Panel())){
            error = "Failed to wait for the Individual / Team Actions panel.";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.Individual_Team_Actions_Panel())){
            error = "Failed to scroll to the Individual / Team Actions panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Individual_Team_Actions_Panel())){
            error = "Failed to clicked to the Individual / Team Actions panel.";
            return false;
        }
        narrator.stepPassed("Individual / Team Actions panel is displayed");
        
        //Individual / team actions
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IndividualTeamActions_Option(getData("Individual / Team Actions 1")))) {
            error = "Failed to wait for the Individual / team actions option: " + getData("Individual / Team Actions 1");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Incident_Management_PageObjects.IndividualTeamActions_Option(getData("Individual / Team Actions 1")))) {
            error = "Failed to click on the Individual / team actions option: " + getData("Individual / Team Actions 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IndividualTeamActions_Option1(getData("Individual / Team Actions 2")))) {
            error = "Failed to click on the Individual / team actions option: " + getData("Individual / Team Actions 2");
            return false;
        }
        narrator.stepPassedWithScreenShot("Individual / team actions option: " + getData("Individual / Team Actions 1") + " -> " + getData("Individual / Team Actions 2") + ".");

        //Individual / team actions details 
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.IndividualTeamActionsDetails(), getData("Individual / team actions details"))) {
            error = "Failed to enter the Individual / team actions details: " + getData("Individual / team actions details");
            return false;
        }
        narrator.stepPassedWithScreenShot("Individual / team actions details: " + getData("Individual / team actions details"));
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveButton())) {
            error = "Failed to wait for the Individual / team actions - save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.saveButton())) {
            error = "Failed to click on the Individual / team actions - save";
            return false;
        }
        
        //Save Mask
        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait())){
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 40)){
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Management_PageObjects.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");    

        return true;
    }
}
