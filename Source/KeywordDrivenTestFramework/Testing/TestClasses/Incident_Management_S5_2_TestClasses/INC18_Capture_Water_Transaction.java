/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import java.util.Set;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "18-Capture Water Transaction",
        createNewBrowserInstance = false
)

public class INC18_Capture_Water_Transaction extends BaseClass {
    String error = "";
    String monitorAreas = "";

    public INC18_Capture_Water_Transaction() {

    }

    public TestResult executeTest() {
        if(!Navigate_To_Water_Transaction()){
            return narrator.testFailed("Error: - " + error);
        }
        if (!Capture_Water_Transaction()) {
            return narrator.testFailed("Failed fill Monitoring areas - " + monitorAreas + " " + error);
        }

        return narrator.finalizeTest("Succesfully open a new window of " + monitorAreas + " ");
    }

    public boolean Navigate_To_Water_Transaction(){
        //Environment tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.EnvironmentTabXPath())) {
            error = "Failed to wait to Environment tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.EnvironmentTabXPath())) {
            error = "Failed to click to Environment tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environment' tab.");
        
        //Monitoring required
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.monitoringRequired_Checkbox())){
            error = "Failed to wait for 'Monitoring required' checkbox.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.monitoringRequired_Checkbox())){
            error = "Failed to click on 'Monitoring required' checkbox.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Monitoring required' checkbox.");
        
        //Monitoring panel
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.monitoring_Panel())){
            error = "Failed to wait for 'Monitoring' panel.";
            return false;
        }
        if(!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.monitoring_Panel())){
            error = "Failed to scroll to 'Monitoring' panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully viewed 'Monitoring' panel.");
        
        //Select all monitoring areas
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.monitoringAreas_selectAll())){
            error = "Failed to wait for select all element.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.monitoringAreas_selectAll())){
            error = "Failed to click select all element.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked select all element.");
        
        //Water tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.water_Tab())){
            error = "Failed to wait for 'Water' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.water_Tab())){
            error = "Failed to click 'Water' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to water tab.");
        
        //Monitoring - Water Add Button 
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.monitoringWater_AddButton())){
            error = "Failed to wait for 'Monitoring - Water' Add Button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.monitoringWater_AddButton())){
            error = "Failed to click 'Monitoring - Water' Add Button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Monitoring - Water' Add Button.");
        
        return true;
    }

    public boolean Capture_Water_Transaction() {
        String businessUnit = getData("Business unit");
        String[] waterType = getData("Water Type").split(",");
        String measuremenType = getData("Measurement Type");
        String year = getData("Year");
        String month = getData("Month");

        //switch to new window
        if (!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new window or tab.";
            return false;
        }
        pause(300);

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.iframe_Xpath())){
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Incident_Management_PageObjects.iframe_Xpath())){
            error = "Failed to switch to frame.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        
        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.waterTransaction_FlowProcessButton())) {
            error = "Failed to wait Water Transacion flow process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.waterTransaction_FlowProcessButton())) {
            error = "Failed to click Water Transacion flow process flow.";
            return false;
        }

        //Business Unit
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.WT_businessUnit_DropDown())){
            error = "Failed to wait for incident occured drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.WT_businessUnit_DropDown())){
            error = "Failed to click incident occured drop down.";
            return false;
        }        
        //Global Company
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.GlobalCompanyXpath())){
            error = "Failed to click Global Company.";
            return false;
        }
        //South Africa
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.SouthAfrica())){
            error = "Failed to click South Africa drop down.";
            return false;
        }
        //Victory Site
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.selectGlobalCompany(getData("Business Unit")))){
            error = "Failed to wait for Business Unit option: " + getData("Business Unit");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.selectGlobalCompany(getData("Business Unit")))){
            error = "Failed to click Business Unit option: " + getData("Business Unit");
            return false;
        }
        narrator.stepPassedWithScreenShot("Business unit : '" + getData("Business Unit") + "'.");
        
        //Water type
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.waterType_DropdownXpath())) {
            error = "Failed to wait on water type dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.waterType_DropdownXpath())) {
            error = "Failed to click on water type dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyActiveDropdownXpath(waterType[0]))) {
            error = "Failed to click on water type " + waterType[0];
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.TypeOfMonitoringAreaOption(waterType[1]), 6)) {
            error = "Failed to wait on water type " + waterType[1];
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.TypeOfMonitoringAreaOption(waterType[1]))) {
            error = "Failed to click on water type " + waterType[1];
            return false;
        }

        //measurement type
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.waterMeasurementTypeDropdownXpath())) {
            error = "Failed to click on measurement type dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.TypeOfMonitoringAreaOption(measuremenType))) {
            error = "Failed to click on measurement type " + measuremenType;
            return false;
        }

        //Month
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.waterMonthDropdownXpath())) {
            error = "Failed to click on month dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.TypeOfMonitoringAreaOption(month))) {
            error = "Failed to wait for month " + month;
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.TypeOfMonitoringAreaOption(month))) {
            error = "Failed to click on month " + month;
            return false;
        }

        //Year
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.waterYearDropdownXpath())) {
            error = "Failed to click on year dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.TypeOfMonitoringAreaOption(year))) {
            error = "Failed to wait for year " + year;
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.TypeOfMonitoringAreaOption(year))) {
            error = "Failed to click on year " + year;
            return false;
        }
        
        //Discharge To
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.discharge_to())){
            error = "Failed to wait for Discharge To input filed.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.discharge_to(), "Inspired Testing")){
            error = "Failed to enter text into Discharge To input filed.";
            return false;
        }

        //Add support document
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.uploadLinkbox())){
            error = "Failed to wait for 'Link box' link.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.uploadLinkbox())){
            error = "Failed to click on 'Link box' link.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");
        
        //switch to new window
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new window or tab.";
            return false;
        }
        
        //URL https
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.LinkURL())){
            error = "Failed to wait for 'URL value' field.";
            return false;
        }      
        if(!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.LinkURL(), getData("Document Link") )){
            error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");
        
        //Title
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.urlTitle())){
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.urlTitle(), getData("Title"))){
            error = "Failed to enter '" + getData("Title") + "' into 'Url Title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Title : '" + getData("Title") + "'.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.urlAddButton())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.urlAddButton())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");
        
         //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Incident_Management_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.waterSaveXpath())) {
            error = "Failed to wait on save button ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.waterSaveXpath())) {
            error = "Failed to click on save button ";
            return false;
        }
        
        //Saving mask
        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        
        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Management_PageObjects.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        String[] airRecord = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.getAirMonitorRecord()).split("#");

        narrator.stepPassedWithScreenShot("Successfully saved Water Transaction Monitoring " + airRecord[1]);

        Set<String> set = SeleniumDriverInstance.Driver.getWindowHandles();
        String[] win = set.stream().toArray(String[]::new);
        SeleniumDriverInstance.Driver.switchTo().window(win[1]).close();
        SeleniumDriverInstance.Driver.switchTo().window(win[0]);

        SeleniumDriverInstance.switchToDefaultContent();

        if (!SeleniumDriverInstance.switchToFrameByXpath(Incident_Management_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame of " + monitorAreas;
            return false;
        }

        //Air
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Air_Tab())){
            error = "Failed to wait for 'Air' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Air_Tab())){
            error = "Failed to click 'Air' tab.";
            return false;
        }
        
        //Water tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.water_Tab())){
            error = "Failed to wait for 'Water' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.water_Tab())){
            error = "Failed to click 'Water' tab.";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.refButtonWater())) {
            error = "Failed to click on refreach button ";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.validateEnvirSpillgridSavedRecord(airRecord[1]), 14)) {
            error = "Failed to save record in Incident Management edit page " + airRecord[1];
            return false;
        }

        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.validateEnvirSpillgridSavedRecord(airRecord[1]))) {
            error = "Failed to scrol record in Incident Management edit page " + airRecord[1];
            return false;
        }

        return true;
    }

}
