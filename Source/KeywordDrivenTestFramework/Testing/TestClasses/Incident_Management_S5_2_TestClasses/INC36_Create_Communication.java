/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "36-Create Communication",
        createNewBrowserInstance = false
)

public class INC36_Create_Communication extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;
    String path;

    public INC36_Create_Communication() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        String pathofImages = System.getProperty("user.dir") + "\\images";
        path = new File(pathofImages).getAbsolutePath();

    }

    public TestResult executeTest() {
        if (!Create_Communication()) {
            return narrator.testFailed("Failed to Create Communication - " + error);
        }

        return narrator.finalizeTest("Successfully Create Communication record is saved.");
    }

    public boolean Create_Communication(){
        //Communication Tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.CommunicationTab())) {
            error = "Failed to wait for - Incident Managament - Communication tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.CommunicationTab())) {
            error = "Failed to click on - Incident Managament - Communication tab ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked - Incident Managament - communication tab");

        //Internal Communication
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.InternalCommunicationTab())) {
            error = "Failed to wait for - Incident Managament - Internal Communication tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.InternalCommunicationTab())) {
            error = "Failed to click on - Incident Managament - Internal Communication tab ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked - Incident Managament - Internal Communication tab");
        
        //Create Communication Panel
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.CreateCommunition_Panel())) {
            error = "Failed to wait for - Incident Managament - Create Communication panel ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.CreateCommunition_AddButton())) {
            error = "Failed to wait for - Incident Managament - Create Communication Add Button ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.CreateCommunition_AddButton())) {
            error = "Failed to click on - Incident Managament - Create Communication Add Button ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked - Incident Managament - Create Communication Add Button.");
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.CreateCommunication_ProcessFlow())) {
            error = "Failed to wait for - Incident Managament - Type Of Communication Process Flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.CreateCommunication_ProcessFlow())) {
            error = "Failed to click the - Incident Managament - Type of Communication Process flow.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Create Communication Process Flow.");
        
        //Type Of Communication
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.TypeOfCommunication_Dropdown())) {
            error = "Failed to wait for - Incident Managament - Type Of Communication dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.TypeOfCommunication_Dropdown())) {
            error = "Failed to click the - Incident Managament - Type of Communication dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.TypeOfCommunication_Option(getData("Type of Communication")))) {
            error = "Failed to wait for - Incident Managament - Type of Communication: " + getData("Type of Communication");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.TypeOfCommunication_Option(getData("Type of Communication")))) {
            error = "Failed to click on - Incident Managament - Type of Communication: " + getData("Type of Communication");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked - Incident Managament - Type of Communication: " + getData("Type of Communication"));
        
        //CommunicationTopic
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.CommunicationTopic())) {
            error = "Failed to wait for - Incident Managament - Communication Topic: " + getData("Communication Topic");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.CommunicationTopic(), getData("Communication Topic"))) {
            error = "Failed to click on - Incident Managament - Communication Topic: " + getData("Communication Topic");
            return false;
        }
        narrator.stepPassedWithScreenShot("Incident Managament - Communication Topic: " + getData("Communication Topic"));
        
        //CommunicationDetails
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.CommunicationDetails())) {
            error = "Failed to wait for - Incident Managament - Communication Details: " + getData("Communication Details");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.CommunicationDetails(), getData("Communication Details"))) {
            error = "Failed to click on - Incident Managament - Communication Details: " + getData("Communication Details");
            return false;
        }
        narrator.stepPassedWithScreenShot("Incident Managament - Communication Details: " + getData("Communication Details"));
        
        //CommunicationGroup
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.CommunicationGroup_Option(getData("Communication Group")))) {
            error = "Failed to wait for - Incident Managament - Communication Group: " + getData("Communication Group");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.CommunicationGroup_Option(getData("Communication Group")))) {
            error = "Failed to click on - Incident Managament - Communication Group: " + getData("Communication Group");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked - Incident Managament - Communication Group: " + getData("Communication Group"));
        
        //Upload Supporting Documents
        //Upload a link 
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.link_buttonxpath())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.link_buttonxpath())) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow()) {
            error = "Failed to switch tab.";
            return false;
        }

        //Link
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.LinkURL())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.LinkURL(), getData("Document Link"))) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.urlTitle())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.urlTitle(), getData("Title"))) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Entered Url: '" + getData("Document Link") + "' and 'Title: '" + getData("Title") + "'.");

        //url Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.urlAddButton())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.urlAddButton())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Incident_Management_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        
        //Communication - Save Button
        if(getData("Save To Continue").equalsIgnoreCase("Yes")){
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentCommunication_SaveButton())) {
                error = "Failed to wait for Communication - Save Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentCommunication_SaveButton())) {
                error = "Failed to click on Communication - Save Button";
                return false;
            }
        } else{
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentCommunication_SaveToContinue_Button())) {
                error = "Failed to wait for Communication - Save to continue Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentCommunication_SaveToContinue_Button())) {
                error = "Failed to click on Communication - Save to continue Button";
                return false;
            }
        }
        
        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects.saveWait2(), 400)){
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        
        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects.validateSave());
        
        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully."); 
       
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.activeToBeInitiatedXpath())) {
            error = "Failed to save on - Create Communication record";
            return false;
        }

        String[] CommunicationID = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.incidentCommunicationID()).split("#");
        setCommunicationId(CommunicationID[1]);

        narrator.stepPassed("Create Communication Record Number: " + CommunicationID[1]);
        narrator.stepPassedWithScreenShot("Successfully saved Create Communication ");

        //Incident Management - Create Communication - Close Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IncidentManagement_IncidentActions_CloseButton())){
            error = "Failed to wait for Incident Management - Create Communication - Close Button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IncidentManagement_CreateCommunication_CloseButton())){
            error = "Failed to click Incident Management - Create Communication - Close Button";
            return false;
        }
        pause(2000);
        
        //Incident Management - Create Communication Gridview Record
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.IncidentManagement_CreateCommunication_Gridview())){
            error = "Failed to wait for Incident Management - Create Communication Gridview Record";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.IncidentManagement_CreateCommunication_Gridview())){
            error = "Failed to click for Incident Management - Create Communication Gridview Record";
            return false;
        }

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IM_processFlow())) {
            error = "Fail to wait for Incident Management process flow";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IM_processFlow())) {
            error = "Fail to click Incident Management process flow";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Injury Details process flow.");
        pause(2000);
        
        return true;
    }

}
