/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "5-Capture Assets",
        createNewBrowserInstance = false
)

public class INC5_Capture_Assets extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public INC5_Capture_Assets(){
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest(){
        if (!CaptureAssets()) {
            return narrator.testFailed("Failed to Capture Assets- " + error);
        }
        return narrator.finalizeTest("Successfully Captured Assets");
    }

    public boolean CaptureAssets() {
        //Assets
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.EquipAndTools_Assets_PanelArrowXPath())) {
            error = "Failed to wait Assets Panel Arrow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.EquipAndTools_Assets_PanelArrowXPath())) {
            error = "Failed to click Assets Panel Arrow.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Assets gridview.");
        
        //Add button
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.EquipAndTools_AssetsAdd_ButtonXPath())) {
            error = "Failed to click Assets Add button.";
            return false;
        }
        
        pause(10000);
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.assets_ProcessFlow())) {
            error = "Failed to wait for Assets Process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.assets_ProcessFlow())) {
            error = "Failed to click Assets Process flow.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked process flow.");

        //Process
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.assets_Process_SelectXPath())) {
            error = "Failed to wait for Assets Process Select Field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.assets_Process_SelectXPath())) {
            error = "Failed to click Assets Process Select Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Assets gridview record.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.process_Production_ExpandXPath(getData("Process")))) {
            error = "Failed to wait for Assets Process option.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.process_Production_ExpandXPath(getData("Process")))) {
            error = "Failed to click Assets Process option.";
            return false;
        }

        //Assets process
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.assets_RTORequired_SelectXPath())) {
            error = "Failed to wait for Assets Process Select Field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.assets_RTORequired_SelectXPath())) {
            error = "Failed to click Assets Process Select Field.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.assets_RTORequired_XPath())) {
            error = "Failed to wait for Assets Process Select Field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.assets_RTORequired_XPath())) {
            error = "Failed to click Assets Process Select Field.";
            return false;
        }

        //Quantity Field
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.assets_Quantity_XPath())) {
            error = "Failed to wait for Quantity Field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.assets_Quantity_XPath(), getData("Quantity"))) {
            error = "Failed to Enter text into Quantity.";
            return false;
        }

        //Assets Mandatory
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.assets_Mandatory_SelectXPath())) {
            error = "Failed to wait for Assets Mandatory Select Field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.assets_Mandatory_SelectXPath())) {
            error = "Failed to click Assets Mandatory Select Field.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.process_Mandatory_XPath(getData("Mandatory")))) {
            error = "Failed to wait for Assets Mandatory.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.process_Mandatory_XPath(getData("Mandatory")))) {
            error = "Failed to click Assets Mandatory.";
            return false;
        }

        //Assets Backup
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.assets_Backup_SelectXPath())) {
            error = "Failed to wait for Assets Backup Select Field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.assets_Backup_SelectXPath())) {
            error = "Failed to click Assets Backup Select Field.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.process_Backup_XPath(getData("Backup")))) {
            error = "Failed to wait for Assets Backup.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.process_Backup_XPath(getData("Backup")))) {
            error = "Failed to click Assets Backup.";
            return false;
        }

        //Comment
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.assets_Comments_XPath())) {
            error = "Failed to wait for Comment Field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.assets_Comments_XPath(), getData("Comments"))) {
            error = "Failed to Enter text into Comment field.";
            return false;
        }
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.assets_Save_XPath())) {
            error = "Failed to wait for Assets Save button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.assets_Save_XPath())) {
            error = "Failed to click Assets Save button.";
            return false;
        }

        //Save mask
        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait())){
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2())){
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        
        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Management_PageObjects.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
        narrator.stepPassedWithScreenShot("Saved Equipment And Tools Details");

        //Close X
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.EquipAndTools_Asset_Close_Button())){
            error = "Failed to wait for X-Close button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.EquipAndTools_Asset_Close_Button())){
            error = "Failed to clicked on for X-Close button.";
            return false;
        }
        
        //Equipment and tools gridview
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.EquipmentTools_Asset_Record())){
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.EquipmentTools_Asset_Record())){
                error = "Failed to wait for Equipment and tools record";
            }
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully viewed Equipment and tools record");
        pause(2000);
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.equipAndToolsProcessFlow())){
            error = "Failed to wait for 'Process flow'.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.equipAndToolsProcessFlow())){
            error = "Failed to click on 'Process flow'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked process flow");
        
//        if (!SeleniumDriverInstance.waitForElementByXpath(MainScenario_PageObjects.assetId())) {
//            error = "Failed to wait for Assets Record number.";
//            return false;
//        }
//
//        String[] Actionid = SeleniumDriverInstance.retrieveTextByXpath(MainScenario_PageObjects.assetId()).split("#");
//        String AssetRec = Actionid[1];
//        narrator.stepPassed("Asset Record Number: " + AssetRec);

        return true;
    }
}
