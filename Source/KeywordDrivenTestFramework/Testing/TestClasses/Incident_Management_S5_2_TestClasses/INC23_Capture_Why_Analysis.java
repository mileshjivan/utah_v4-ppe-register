/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "23-Capture Why Analysis",
        createNewBrowserInstance = false
)

public class INC23_Capture_Why_Analysis extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;

    public INC23_Capture_Why_Analysis() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Capture_Why_Analysis()) {
            return narrator.testFailed(error);
        }

        return narrator.finalizeTest("Successfully Capture Why Analysis Record");
    }

    public boolean Capture_Why_Analysis() {
        //why Analysis add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.WhyAnalysisAddButton())) {
            error = "Failed to wait for the why Analysis add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.WhyAnalysisAddButton())) {
            error = "Failed to click on the why Analysis add button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on the why Analysis add button.");


        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.whyAnalysisOrder(), 3)) {
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.WhyAnalysisAddButton())) {
                error = "Failed to click on the why Analusis add button ";
                return false;
            }
        }

        //why Analysis Oder Number.
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.whyAnalysisOrder(), getData("Order Number"))) {
            error = "Failed to enter the why Analysis Oder Number.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Order Number: " + getData("Order Number"));
        
        //Analusis - Why
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.whyAnalysisText(), getData("why"))) {
            error = "Failed to enter the why Analusis - Why";
            return false;
        }
        narrator.stepPassedWithScreenShot("why Analusis - Why: " + getData("why"));

        //why Analusis - Answer
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.whyAnalysisAnswerText(), getData("Answer"))) {
            error = "Failed to click on the why Analusis - Answer";
            return false;
        }
        narrator.stepPassedWithScreenShot("why Analusis - Answer: " + getData("Answer"));

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWhyAnalysisXpath())) {
            error = "Failed to wait for the why Analusis - save";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.saveWhyAnalysisXpath())) {
            error = "Failed to click on the why Analusis - save";
            return false;
        }
        
        //Save Mask
        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait())){
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 40)){
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Management_PageObjects.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");    

        return true;
    }
}
