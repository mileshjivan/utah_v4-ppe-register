/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "16-Add View Grievances",
        createNewBrowserInstance = false
)

public class INC16_Add_View_Grievances extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date, parentWindow;

    public INC16_Add_View_Grievances() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest() {
        if (!Add_View_Grievances()) {
            return narrator.testFailed("Failed - " + error);
        }
        if(!Add_Grievances()){
            return narrator.testFailed("Failed - " + error);
        }

        return narrator.finalizeTest("Successfully Record is saved and automatically moves to the Edit phase");
    }

    public boolean Add_View_Grievances() {
        //Social Sustainability
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.SocialSustainability_TabXPath())) {
            error = "Failed to wait to Social Sustainability tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.SocialSustainability_TabXPath())) {
            error = "Failed to click to Social Sustainability tab.";
            return false;
        }
        
//        //Social Sustainability panel
//        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.SocialSustabilityGrievances_Panel())) {
//            error = "Failed to click on the Social Sustainability panel";
//            return false;
//        }

        //Social Sustainability add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.SocialSustabilityGrievances_AddButtonXPath())) {
            error = "Failed to wait for the Social Sustainability add button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.SocialSustabilityGrievances_AddButtonXPath())) {
            error = "Failed to click on the Social Sustainability add button";
            return false;
        }
        parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        
        return true;
    }
    
    public boolean Add_Grievances(){
        //switch to new window
        if (!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new window or tab.";
            return false;
        }
        pause(300);

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.iframe_Xpath())){
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Incident_Management_PageObjects.iframe_Xpath())){
            error = "Failed to switch to frame.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Grievance_FlowProcessButton())) {
            error = "Failed to wait Grievance Flow Process Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Grievance_FlowProcessButton())) {
            error = "Failed to click Grievance Flow Process Button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process Flow");

        //Grievance title
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.Grievance_title(), getData("Grievance title"))){
            error = "Failed to enter  :" + getData("Grievance title");
            return false;
        }
        
        //Business_Unit
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Business_Unit())){
            error = "Failed to click business unit";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Business_Unit_Option(getData("Business unit")))){
            error = "Failed to wait for business unit option";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Business_Unit_Option(getData("Business unit")))){
            error = "Failed to click  business unit option";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked option: " + getData("Business unit"));
        
        //Grievance summary
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.Grievance_summary(), getData("Grievance summary"))){
            error = "Failed to enter :" + getData("Grievance summary");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked option: " + getData("Grievance summary"));

        //grievances recieved by
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Grievances_Received_by())){
            error = "Failed to click grievances recieved by";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Grievances_Received_by_option(getData("Grievance recieved by")))){
            error = "Failed to click grievances recieved by option " + getData("Grievance recieved by");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Grievances_Received_by_option(getData("Grievance recieved by")))){
            error = "Failed to click grievances recieved by option";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked option: " + getData("Grievance recieved by"));

        //Individual Or Entity
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Individual_Entity())){
            error = "Failed to wait for Individual / Entity";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Individual_Entity())){
            error = "Failed to click Individual / Entity";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Individual_Entity_option(getData("Individual Or Entity")))){
            error = "Failed to wait for Individual / Entity option";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Individual_Entity_option(getData("Individual Or Entity")))){
            error = "Failed to click grievances Individual / Entity option  ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked option: " + getData("Individual Or Entity"));

        //Grievance reference code
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.Grievance_reference_code(), getData("Grievance reference code"))){
            error = "Failed to enter: " + getData("Grievance reference code");
            return false;
        }

        String current_date = SeleniumDriverInstance.generateDateTimeString();
        String[] formate_date = current_date.split("_");
        String date_current = formate_date[0];
        String time_current = formate_date[1];

        //Grievances reception date
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.Grievances_Reception_Date(), date_current)){
            error = "Failed to enter Grievances reception date in the input field.";
            return false;
        }

        //Grievance Enter Name
        if (getData("Enter Name").equalsIgnoreCase("True")){
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Grievances_Name())){
                if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Grievances_Name_2())){
                    error = "Failed to wait for grievances name";
                    return false;
                }
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Grievances_Name())){
                if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Grievances_Name_2())){
                    error = "Failed to click grievances name";
                    return false;
                }
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Grievances_Name_Enter())){
                error = "Failed to wait for search text box";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.Grievances_Name_Enter(), getData("Grievant name"))){
                error = "Failed to enter grievances name";
                return false;
            }
            narrator.stepPassed("Grievant name  :" + getData("Grievant name"));

            //Grievant name
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Search_Text_Box_Results(getData("Grievant name")))){
                if (getData("Individual Or Entity").equalsIgnoreCase("Entity")){
                    if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Button_Grievant_Not_Registered_2())){
                        error = "Failed to click grievant not registered button";
                        return false;
                    }
                } else {
                    if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Button_Grievant_Not_Registered_1())){
                        error = "Failed to click grievant not registered button";
                        return false;
                    }
                    pause(6000);

                }
                narrator.stepPassed("Grievance title  :" + getData("Grievance title"));
                narrator.stepPassed("Grievance summary   :" + getData("Grievance summary "));
                narrator.stepPassed("Grievance recieved by  :" + getData("Grievance recieved by"));
                narrator.stepPassed("Grievances Individual Or Entity :" + getData("Individual Or Entity"));
            } 
            
            
        } else if(getData("Enter Name").equalsIgnoreCase("false")){
                if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Grievances_Name_1())){
                    error = "Failed to click grievances name";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Grievances_Name_Search())){
                    error = "Failed to wait for Grievant name search";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Grievances_Name_Search())){
                    error = "Failed to click grievances name search";
                    return false;
                }

                //Location
                if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Grievances_Location())){
                    error = "Failed to click grievances location";
                    return false;
                }
                if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Grievances_Location_Search(getData("Grievance location")))){
                    error = "Failed to wait for  Grievant location option";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Grievances_Location_Search(getData("Grievance location")))){
                    error = "Failed to click grievances Grievant name option  ";
                    return false;
                }
                
                //Latitude
                if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.Latitude(), getData("Latitude"))){
                    error = "Failed to enter Latitude";
                    return false;
                }
                if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.Longitude(), getData("Longitude"))){
                    error = "Failed to enter Longitude";
                    return false;
                }

                //grievances responsibility
                if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Grievances_Responsibility())){
                    error = "Failed to click grievances responsibility";
                    return false;
                }
                if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Responsibility_Search(getData("Responsibility")))){
                    error = "Failed to wait for Grievant Responsibility option";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Responsibility_Search(getData("Responsibility")))){
                    error = "Failed to click  Grievant responsibility option";
                    return false;
                }

                //grievances reception channel
                if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Grievances_Reception_Channel())){
                    error = "Failed to click grievances reception channel";
                    return false;
                }
                if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Reception_Search(getData("Reception channel")))){
                    error = "Failed to wait for  Grievant Reception channel option";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Reception_Search(getData("Reception channel")))){
                    error = "Failed to click  Grievant reception channel option";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully captured option: " + getData("Reception channel"));

                //Save button
                if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Button_Save())){
                    error = "Failed to wait for button save";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Button_Save())){
                    error = "Failed to click button save";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Successfully clicked save button.");

                if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait(), 2)){
                    if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 400)){
                        error = "Webside too long to load wait reached the time out";
                        return false;
                    }
                }

                String saved = "";
                if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.inspection_Record_Saved_popup())){
                    saved = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.inspection_Record_Saved_popup());
                } else {
                    if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.inspection_Record_Saved_popup())){
                        error = "Failed to wait for 'Record Saved' popup.";
                        return false;
                    }
                }

                if (saved.equals("Record saved")){
                    narrator.stepPassedWithScreenShot("Successfully clicked save");
                } else {
                    if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.failed())){
                        error = "Failed to wait for error message.";
                        return false;
                    }

                    String failed = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.failed());

                    if (failed.equals("ERROR: Record could not be saved")){
                        error = "Failed to save record.";
                        return false;
                    }
                }

                narrator.stepPassed("Grievance title  :" + getData("Grievance title"));
                narrator.stepPassed("Grievance summary   :" + getData("Grievance summary"));
                narrator.stepPassed("Grievance recieved by  :" + getData("Grievance recieved by"));
                narrator.stepPassed("Grievances Individual Or Entity :" + getData("Individual Or Entity"));
                narrator.stepPassed("Grievant name  :" + getData("Grievant name"));
                narrator.stepPassed("Grievance location  :" + getData("Grievance location"));
                narrator.stepPassed("Grievances Latitude  :" + getData("Latitude"));
                narrator.stepPassed("Grievances Longitude :" + getData("Longitude"));
                narrator.stepPassed("Grievances Responsibility :" + getData("Responsibility"));
                narrator.stepPassed("Grievances Reception channel :" + getData("Reception channel"));
                narrator.stepPassed("Grievance reference code  :" + getData("Grievance reference code"));
                narrator.stepPassed("Grievances Business unit :" + getData("Business unit"));
        }
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.iframeXpath())){
            error = "Failed to switch to frame ";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Incident_Management_PageObjects.iframeXpath())){
            error = "Failed to switch to frame ";
            return false;
        }
        
        //Social Sustainability add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.SocialSustabilityGrievances_AddButtonXPath())) {
            error = "Failed to wait for the Social Sustainability add button";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.SocialSustabilityGrievances_AddButtonXPath())) {
            error = "Failed to scroll to the Social Sustainability add button";
            return false;
        }
        
        //Search
        
        return true;
    }

}
