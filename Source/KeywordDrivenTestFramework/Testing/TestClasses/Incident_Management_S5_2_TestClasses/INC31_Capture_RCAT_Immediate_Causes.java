/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "31-Capture RCAT Immediate Causes",
        createNewBrowserInstance = false
)

public class INC31_Capture_RCAT_Immediate_Causes extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;

    public INC31_Capture_RCAT_Immediate_Causes() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Capture_RCAT_Immediate_Causes()) {
            return narrator.testFailed(error);
        }

        return narrator.finalizeTest("Successfully Captured RCAT Immediate Causes");
    }

    public boolean Capture_RCAT_Immediate_Causes(){
        //RCAT Analysis Panel
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.RCAT_Analysis_Panel())){
            error = "Failed to wait for the RCAT Analysis Panel.";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.RCAT_Analysis_Panel())){
            error = "Failed to scroll to the RCAT Analysis Panel.";
            return false;
        }
        narrator.stepPassed("RCAT Analysis Panel is displayed");

        //RCAT Immediate Causes Panel
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.RCAT_Immediate_Causes_Panel())){
            error = "Failed to wait for the RCAT Immediate Causes Panel.";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.RCAT_Immediate_Causes_Panel())){
            error = "Failed to scroll to the RCAT Immediate Causes Panel.";
            return false;
        }
        narrator.stepPassed("RCAT Immediate Causes Panel is displayed");
        
        //RCAT Immediate Causes Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.RCAT_Immediate_Causes_AddButton())){
            error = "Failed to wait for the RCAT Immediate Causes Add Button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.RCAT_Immediate_Causes_AddButton())){
            error = "Failed to click the RCAT Immediate Causes Add Button.";
            return false;
        }
        narrator.stepPassed("RCAT Immediate Causes Add Button is displayed");
        
        //RCAT Immediate Causes Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.RCAT_Immediate_Causes_ProcessFlow())){
            error = "Failed to wait for the RCAT Immediate Causes Process Flow Button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.RCAT_Immediate_Causes_ProcessFlow())){
            error = "Failed to click the RCAT Immediate Causes Process Flow Button.";
            return false;
        }
        narrator.stepPassed("RCAT Immediate Causes Process Flow Successfully clicked.");
        
        //Immediate cause
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.RCAT_Immediate_Causes_Dropdown())){
            error = "Failed to wait for the Immediate cause dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.RCAT_Immediate_Causes_Dropdown())){
            error = "Failed to click the Immediate cause dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.RCAT_Immediate_Causes_Option_Dropdown(getData("Immediate cause 1")))){
            error = "Failed to wait for the Immediate cause Option: " + getData("Immediate cause 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.RCAT_Immediate_Causes_Option_Dropdown(getData("Immediate cause 1")))){
            error = "Failed to click the Immediate cause Option: " + getData("Immediate cause 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.RCAT_Immediate_Causes_Option(getData("Immediate cause 2")))){
            error = "Failed to click the Immediate cause Option: " + getData("Immediate cause 2");
            return false;
        }
        narrator.stepPassed("RCAT Immediate Causes - Immediate cause: " + getData("Immediate cause 1") + " - " + getData("Immediate cause 2"));
        
        //Root causes
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.RootCauses_Option(getData("Root causes 1")))) {
            error = "Failed to wait for the Root causes option: " + getData("Root causes 1");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Incident_Management_PageObjects.RootCauses_Option(getData("Root causes 1")))) {
            error = "Failed to click on the Root causes option: " + getData("Root causes 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.RootCauses_Option1(getData("Root causes 2")))) {
            error = "Failed to click on the Root causes option: " + getData("Root causes 2");
            return false;
        }
        narrator.stepPassedWithScreenShot("Root causes option: " + getData("Root causes 1") + " -> " + getData("Root causes 2") + ".");
        
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.ImmediateCause_Comments(), getData("Comments"))) {
            error = "Failed to enter the Comments: " + getData("Comments");
            return false;
        }
        narrator.stepPassedWithScreenShot("Immediate cause comments: " + getData("Comments"));
       
        //Save to continue
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.save_To_Continue_Button())) {
            error = "Failed to wait for the Immediate cause - save to continue";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.save_To_Continue_Button())) {
            error = "Failed to click on the Immediate cause - save to continue";
            return false;
        }
        
        //Save Mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 40)){
            error = "Webside too long to load wait reached the time out";
            return false;
        }        

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Management_PageObjects.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");   
        
        //RCAT Immediate Cause Actions panel
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.RCATImmediateCauseActions_Panel())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.RCATImmediateCauseActions_Panel())){
                error = "Failed to wait for RCAT Immediate Cause Actions to be present";
                return false;
            }
        }
        
        //RCAT Immediate Cause Actions Close Button
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.RCATImmediateCauseActions_CloseButton())){
            error = "Failed to wait for RCAT Immediate Cause Actions close button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.RCATImmediateCauseActions_CloseButton())){
            error = "Failed to click for RCAT Immediate Cause Actions close button.";
            return false;
        }
        pause(2000);
        
        //RCAT Immediate Cause Gridview
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.RCATImmediateCause_Gridview())){
            error = "Failed to wait for RCAT Immediate Cause Gridview";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.RCATImmediateCause_Gridview())){
            error = "Failed to click for RCAT Immediate Cause Gridview";
            return false;
        }

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IM_processFlow())) {
            error = "Fail to wait for Incident Management process flow";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IM_processFlow())) {
            error = "Fail to click Incident Management process flow";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Injury Details process flow.");
        pause(2000);
        
        return true;
    }
}
