/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "15-Capture Environmental Spill",
        createNewBrowserInstance = false
)

public class INC15_Capture_Environmental_Spill extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;

    public INC15_Capture_Environmental_Spill() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
    }

    public TestResult executeTest() {
        if (!CaptureEnvironmentSpill()) {
            return narrator.testFailed("Failed to fill out the Environment Spill details - " + error);
        }

        return narrator.finalizeTest("Successfully Record is saved and automatically moves to the Edit phase");
    }

    public boolean CaptureEnvironmentSpill() {
        //Environment tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.EnvironmentTabXPath())) {
            error = "Failed to wait to Environment tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.EnvironmentTabXPath())) {
            error = "Failed to click to Environment tab.";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.UncontrolledReleaseOrSpillXPath())) {
            error = "Failed to click on the Uncontrolled Release/Spill heading";
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(EnvironmentPageObjects.environmentalPillGrip())) {
//            error = "Failed to display Environmental Spill non editable grid  ";
//            return false;
//        }
//
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.UncontrolledReleaseOrSpillAddButtonXPath())) {
            error = "Failed to wait for the Uncontrolled Release/Spill add button ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.UncontrolledReleaseOrSpillAddButtonXPath())) {
            error = "Failed to click on the Uncontrolled Release/Spill add button ";
            return false;
        }
        pause(1500);

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.evironmentSpillFlowProcessButton())) {
            error = "Failed to wait evironmental Spill Flow Process Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.evironmentSpillFlowProcessButton())) {
            error = "Failed to click evironmental Spill Flow Process Button";
            return false;
        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(EnvironmentPageObjects.evironSpillAddPhaseXpath(), 2)) {
//            error = "Failed to diplay evironmental Spill Flow Process of Add phase ";
//            return false;
//        }
        narrator.stepPassedWithScreenShot("Environmental Spill module opens in the Add phase successfully ");

        //Product dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.ProductDropdownXPath())) {
            error = "Failed to wait for the Product dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.ProductDropdownXPath())) {
            error = "Failed to click on the Product dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.dropdownElementXPath(getData("Product")))) {
            error = "Failed to select the following item: " + getData("Product");
            return false;
        }
        
        //Quality Released
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.qualityReleased_Input(), getData("Quality Released"))) {
            error = "Failed to enter text into the Detailed Description Text Area";
            return false;
        }
         
        //Quantity contained
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.quantityContained_Input(), getData("Quantity contained"))) {
            error = "Failed to enter text into the Quantity Contained Text Area";
            return false;
        }

        //Unit OF Measure
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.UnitOFMeasureDropdownXPath())) {
            error = "Failed to wait for the Unit OF Measure Dropdown ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.UnitOFMeasureDropdownXPath())) {
            error = "Failed to click on the Unit OF Measure Dropdown ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.dropdownElementXPath(getData("Unit Of Measure")))) {
            error = "Failed to select the following item: " + getData("Unit Of Measure");
            return false;
        }
        
        //Detailed Description
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.DetailedDescriptionTextAreaXPath(), getData("Detailed description"))) {
            error = "Failed to enter text into the Detailed Description Text Area";
            return false;
        }
        
        //Clean-up Cost
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.clean_up_Cost(), getData("Clean-up Cost"))) {
            error = "Failed to enter text into the Clean-up Cost Text Area";
            return false;
        }
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.evironSpillSaveButtonXpath())) {
            error = "Failed to wait for the save button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.evironSpillSaveButtonXpath())) {
            error = "Failed to click the save button";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait())){
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2())){
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        
        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Save_Close_ButtonXPath())) {
            error = "Failed to wait for the save and close button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Save_Close_ButtonXPath())) {
            error = "Failed to click the save and close button";
            return false;
        }

        //Click the process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.processFlow())){
            error = "Failed to wait for process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.processFlow())){
            error = "Failed to click process flow.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Clicked a process flow");
        
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.EnvironmentTabXPath())) {
            error = "Failed to scroll to Environment tab.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.EnvironmentSpillXPath_Record())) {
            error = "Failed to wait for Environment Spill Record to be present.";
            return false;
        }

        return true;
    }

}
