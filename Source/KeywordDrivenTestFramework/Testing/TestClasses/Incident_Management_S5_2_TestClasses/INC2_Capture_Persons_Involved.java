/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "2-Capture Persons Involved",
        createNewBrowserInstance = false
)

public class INC2_Capture_Persons_Involved extends BaseClass{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public INC2_Capture_Persons_Involved(){
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest(){
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());

        if (!navigateTo2VerificationAndAdditionalDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully navigated ");

    }

    private boolean navigateTo2VerificationAndAdditionalDetails(){
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.verificationAndAddDetTab())){
            error = "Failed to wait for the '2.Verification & Additional Detail' tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.verificationAndAddDetTab())){
            error = "Failed to click the '2.Verification & Additional Detail' tab";
            return false;
        }
        pause(2000);
        //Incident Classification
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Incident_Classification())){
            error = "Failed to wait for 'Incident Classification' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Incident_Classification())){
            error = "Failed to click on 'Incident Classification' dropdown.";
            return false;
        }
        pause(3000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Incident_Classification_All())){
            error = "Failed to wait for 'Incident Classification' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Incident_Classification_All())){
            error = "Failed to click on 'Incident Classification' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click all 'Incident Classification'.");
        
        //Persons Involved panel
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.persons_Involved())){
            error = "Failed to wait for 'Persons Involved' panel.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.persons_Involved())){
            error = "Failed to click on 'Persons Involved' panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Persons Involved' panel.");
                
        //Add
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.personsInvolvedAdd())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }      
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.personsInvolvedAdd())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Succesfully click 'Add' button.");
        
        //Full name
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.fullName())){
            error = "Failed to wait for 'Full name' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.fullName(), getData("Full name"))){
            error = "Failed to enter '" + getData("Full name") + "' into 'Full name' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Full name : '" + getData("Full name") + "'.");
        
        //Description of involvement
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.descriptionOfInvolvement())){
            error = "Failed to wait for 'Description of involvement' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.descriptionOfInvolvement(), getData("Description of involvement"))){
            error = "Failed to enter '" + getData("Description of involvement") + "' into 'Description of involvement' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Description of involvement : '" + getData("Description of involvement") + "'.");
        
        //Save
        if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Management_PageObjects.savePersonsInvolved())){
            error = "Failed to wait for save and continue button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.savePersonsInvolved())){
            error = "Failed to click save and continue button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully saved.");

        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait(), 2)){
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 400)){
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Management_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.validateSave());
        
        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat+": successfully."); 
        
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.incidentStatus())){
            error = "Failed to scroll to incident status button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        
        //Saving mask
        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        return true;
    }

}
