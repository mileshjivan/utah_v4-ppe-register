/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import java.io.File;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "33-View Job Safety Analysis",
        createNewBrowserInstance = false
)
public class INC33_View_Job_Safety_Analysis extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public INC33_View_Job_Safety_Analysis() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!View_Job_Safety_Analysis()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed View Job Safety Analysis");
    }

    public boolean View_Job_Safety_Analysis() {
        //Additional Information
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.AdditionalInformation())){
            error = "Failed to wait for 'Additional Information' check box.";
            return false;
        }
        if(!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.AdditionalInformation())){
            error = "Failed to scroll to 'Additional Information' check box.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.AdditionalInformation())){
            error = "Failed to click the 'Additional Information' check box.";
            return false;
        }
        narrator.stepPassed("Additional Information check box is displayed");
                    
        //Related JSA panel.
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_RelatedJSA_Panelxpath())) {
            error = "Failed to wait for Related JSA panel.";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.incidentInvestigation_RelatedJSA_Panelxpath())) {
            error = "Failed to scroll to Related JSA panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentInvestigation_RelatedJSA_Panelxpath())) {
            error = "Failed to wait for Related JSA panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Related JSA panel is displayed.");
        pause(2000);

        //Available Related Risk Gridview
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_RelatedJSA_Openxpath())) {
            error = "Failed to wait for Available Related JSA Record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentInvestigation_RelatedJSA_Openxpath())) {
            error = "Failed to click Available Related JSA Record.";
            return false;
        }

        //Save Mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 40)){
            error = "Webside too long to load wait reached the time out";
            return false;
        }  
        
        pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_RelatedRJSA_OpenedRecordxpath())) {
            error = "Failed to wait for Available Related Risk to open.";
            return false;
        }

        String openedRelatedJSA = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.incidentInvestigation_RelatedRJSA_OpenedRecordxpath());
        narrator.stepPassed("Risk Assessment opened: " + openedRelatedJSA);

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Incident_Job_Safety_Analysis_ProcessFlow())) {
            error = "Fail to wait for Risk JSA process flow";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Incident_Job_Safety_Analysis_ProcessFlow())) {
            error = "Fail to click Risk JSA process flow";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Risk JSA process flow.");
        
        return true;
    }

}
