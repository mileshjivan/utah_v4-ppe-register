/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "7-Capture Injured Persons",
        createNewBrowserInstance = false
)

public class INC7_Capture_Injured_Persons extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public INC7_Capture_Injured_Persons() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!Capture_Injured_Persons()) {
            return narrator.testFailed("Failed fill Injured Persons - " + error);
        }
        if(getData("On Duty").equalsIgnoreCase("Yes")){
            if (!Capute_Injury_Illness_Details()) {
                return narrator.testFailed("Failed fill Injured Persons - " + error);
            }
        }
 
        return narrator.finalizeTest("Completed Injured Persons");
    }

    public boolean Capture_Injured_Persons() {
        //2.Verification & Additional Detail tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.verificationAndAddDetTab())){
            error = "Failed to wait for the '2.Verification & Additional Detail' tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.verificationAndAddDetTab())){
            error = "Failed to click the '2.Verification & Additional Detail' tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked '2.Verification & Additional Detail' tab");
        
        //safety tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.safetyHealth_Tab())) {
            error = "Failed to wait for the Safety/Health tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.safetyHealth_Tab())) {
            error = "Failed to click the Safety/Health tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Safety/Health tab.");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.addInjuredPersonsButtonXPath())) {
            error = "Failed to wait for the Add button ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.addInjuredPersonsButtonXPath())) {
            error = "Failed to click the Add button ";
            return false;
        }
        pause(1500);
        narrator.stepPassedWithScreenShot("Successfully navigated to Injury persons form.");
        
        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 40)) {
                error = "Webside too long to load wait reached the time out save";
                return false;
            }
        }
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.injuredPersonsFlow())) {
            error = "Fail to wait for Injured persons process flow";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.injuredPersonsFlow())) {
            error = "Fail to click Injured persons process flow";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Injury persons process flow.");

        //employee type
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.employeeTypeDropdownXPath())) {
            error = "Failed to click the employee type dropdown ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.anySupervisorXpath(getData("Employment Type")))) {
            error = "Failed to wait for the employee type element: " + getData("Employment Type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anySupervisorXpath(getData("Employment Type")))) {
            error = "Failed to click the employee type element: " + getData("Employment Type");
            return false;
        }
        narrator.stepPassedWithScreenShot("Employee: '" + getData("Employee") + "'.");
        
        //Full name
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.fullNameDropdownXPath())) {
            error = "Failed to click the full name dropdown ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.anySupervisorXpath(getData("Full Name")))) {
            error = "Failed to wait for the full name element: " + getData("Full Name");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anySupervisorXpath(getData("Full Name")))) {
            error = "Failed to click the full name element: " + getData("Full Name");
            return false;
        }
        narrator.stepPassedWithScreenShot("Full Name: '" + getData("Full Name") + "'.");

        //Illness on duty
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.illnessonduty_dropown())){
            error = "Failed to wait for Illness on duty dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.illnessonduty_dropown())){
            error = "Failed to click Illness on duty dropdown.";
            return false;
        }
        pause(2000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.illnessonduty_select(getData("On Duty")))){
            error = "Failed to wait for Illness on duty option '" + getData("On Duty") + "'.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.illnessonduty_select(getData("On Duty")))){
            error = "Failed to click Illness on duty option '" + getData("On Duty") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("On Duty: '" + getData("On Duty") + "'.");

        //Drug and alcohol test required
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.drugAlcoholTestRequiredDropdownXPath())){
            error = "Failed to wait for Drug and alcohol test required dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.drugAlcoholTestRequiredDropdownXPath())){
            error = "Failed to click Drug and alcohol test required dropdown.";
            return false;
        }
        pause(2000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.dropdownElementXPath(getData("Drug and alcohol")))){
            error = "Failed to wait for Drug and alcohol test required option '" + getData("Drug and alcohol") + "'.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.dropdownElementXPath(getData("Drug and alcohol")))){
            error = "Failed to click Drug and alcohol test required option '" + getData("Drug and alcohol") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Drug and alcohol: '" + getData("Drug and alcohol") + "'.");
        
        //Save button
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.save_Button())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.save_Button())){
            error = "Failed to click 'Save' button.";
            return false;
        }
        
//        if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingForms(), 40)){
//            error = "Webside too long to load wait reached the time out";
//            return false;
//        }

        //Check if the record has been Saved
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.inspection_RecordSaved_popup())){
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.inspection_RecordSaved_popup());
        
        if(saved.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        }else{   
            if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.failed())){
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.failed());

            if(failed.equals("ERROR: Record could not be saved")){
                error = "Failed to save record.";
                return false;
            }
        }

        return true;
    }

    public boolean Capute_Injury_Illness_Details(){
        //Injury/Illness
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.Injury_And_Illness_Tab())){
            error = "Failed to wait for Injury/Illness tab.";
            return false;
        }
        
        //Injury/Illness classification
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.classification())){
            error = "Failed to wait for 'Injury/Illness classification' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.classification())){
            error = "Failed to wait for 'Injury/Illness classification' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.classification_Select(getData("classification")))){
            error = "Failed to wait for '" + getData("classification") + "' from 'Injury/Illness classification' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.doubleClickElementbyXpath(Incident_Management_PageObjects.classification_Select(getData("classification")))){
            error = "Failed to click '" + getData("classification") + "' from 'Injury/Illness classification' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.classification_Select(getData("classification 2")))){
            error = "Failed to click '" + getData("classification 2") + "' from 'Injury/Illness classification' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Injury/Illness classification: '" + getData("classification") + "' -> '" + getData("classification 2") + "'");
            
        //Injury/Illness description
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.description())){
            error = "Failed to wait for 'Injury/Illness description' textarea.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.description(), getData("description"))){
            error = "Failed to enter '" + getData("description") + "' into 'Injury/Illness description' textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Injury/Illness description: '" + getData("description") + "'");
            
        //Activity at the time
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Activity())){
            error = "Failed to wait for 'Activity at the time' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Activity())){
            error = "Failed to wait for 'Activity at the time' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Activity_Select(getData("Activity")))){
            error = "Failed to wait for '" + getData("Activity") + "' from 'Activity at the time' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Activity_Select(getData("Activity")))){
            error = "Failed to click '" + getData("Activity") + "' from 'Activity at the time' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Activity at the time: '" + getData("Activity") + "'");
          
        //Follow up required
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.FollowUpRequired())){
            error = "Failed to wait for 'Follow up required' checkbox.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.FollowUpRequired())){
            error = "Failed to click 'Follow up required' checkbox.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully tick 'Follow up required' checkbox.");
        
        //Additional treatment required
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.AdditionalTreatmentRequired())){
            error = "Failed to wait for 'Additional treatment required' checkbox.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.AdditionalTreatmentRequired())){
            error = "Failed to click 'Additional treatment required' checkbox.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully tick 'Additional treatment required' checkbox.");

        //Treatment away from workplace
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Treatment())){
            error = "Failed to wait for 'Treatment away from workplace' checkbox.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Treatment())){
            error = "Failed to click 'Treatment away from workplace' checkbox.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully tick 'Additional treatment required' checkbox.");

        //Is this injury recordable? 
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.recordable())){
           error = "Failed to wait for 'Is this injury recordable?' checkbox.";
           return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.recordable())){
            error = "Failed to click 'Is this injury recordable?' checkbox.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully tick 'Is this injury recordable?' checkbox.");  

        //Is this injury reportable? 
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.reportable())){
           error = "Failed to wait for 'Is this injury reportable?' checkbox.";
           return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.reportable())){
            error = "Failed to click 'Is this injury reportable?' checkbox.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully tick 'Is this injury reportable?' checkbox.");  

        //Injury claim
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.InjuryClaim())){
            error = "Failed to wait for 'Injury claim' checkbox.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.InjuryClaim())){
            error = "Failed to click 'Injury claim' checkbox.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully tick 'Injury claim' checkbox.");  
        
        //Save button
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.save_Button())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.save_Button())){
            error = "Failed to click 'Save' button.";
            return false;
        }
        
//        if (!SeleniumDriverInstance.waitForElementPresentByXpath(VerificationAndAdditionalPageObject.loadingForms(), 40)){
//            error = "Webside too long to load wait reached the time out";
//            return false;
//        }

        //Check if the record has been Saved
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.inspection_RecordSaved_popup())){
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.inspection_RecordSaved_popup());
        
        if(saved.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        }else{   
            if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.failed())){
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.failed());

            if(failed.equals("ERROR: Record could not be saved")){
                error = "Failed to save record.";
                return false;
            }
        }
        
        //Wait for Injury Claim
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Injury_Claim_Tab())){
            error = "Failed to wait for Injury Claim Tab.";
            return false;
        }
        if(!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.Injury_Claim_Tab())){
            error = "Failed to scroll to Injury Claim Tab.";
            return false;
        }
        
        //Injuryed Person Close X
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.injuredPerson_Close())){
            error = "Failed to wait for X Close.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.injuredPerson_Close())){
            error = "Failed to wait for X Close.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully closed the Injuryed Person X button.");
        
        //'Injured Person' panel
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.injuredPersonsSubTabXPath())){
            error = "Failed to wait for 'Injured Person' panel";
            return false;
        }
        narrator.stepPassedWithScreenShot("Saved Injured Person");
        
        //Scroll to the record
        if(!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.injuredPersonsSubTabXPath())){
            error = "Failed to scroll to the bottom";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.injuredPersonPanel_Record())){
            error = "Failed to wait for witness Statements close tab";
            return false;
        }
        
        //Click the process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.processFlow())){
            error = "Failed to wait for process flow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.processFlow())){
            error = "Failed to click process flow.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Clicked a process flow");
        
        return true;
    }
    
}
