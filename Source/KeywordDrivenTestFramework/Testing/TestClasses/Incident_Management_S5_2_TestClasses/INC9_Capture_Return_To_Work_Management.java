/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "9-Capture Return to Work Management",
        createNewBrowserInstance = false
)

public class INC9_Capture_Return_To_Work_Management extends BaseClass{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;

    public INC9_Capture_Return_To_Work_Management(){
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
       this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 6);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest(){
        if (!ReturnToWorkManagement()){
            return narrator.testFailed("Failed to add a work management item- " + error);
        }

        if (!DoctorsNotesDetails()){
            return narrator.testFailed("Failed to enter Doctor's notes details - " + error);
        }
        return narrator.finalizeTest("Successfully saved an Return Work Management record");
    }

    public boolean ReturnToWorkManagement(){
        //Treatment facility details
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.ReturnToWorkManagementXPath())){
            error = "Failed to scroll to the Treatment facility details label ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.ReturnToWorkManagementXPath())){
            error = "Failed to click on Treatment facility details label ";
            return false;
        }
        
        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.ReturnToWorkManagementAddButtonXPath())){
            error = "Failed to wait for Add button ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.ReturnToWorkManagementAddButtonXPath())){
            error = "Failed to click the Add button ";
            return false;
        }
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.returnToWork_processFlow())){
            error = "Failed to wait for Process flow";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.returnToWork_processFlow())){
            error = "Failed to click the Process flow";
            return false;
        }
        
        //Status dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.StatusDropdownXPath())){
            error = "Failed to wait for Status Dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.StatusDropdownXPath())){
            error = "Failed to click the Status Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.dropdownElementXPath(getData("Status")))){
            error = "Failed to select the Reportable to item: " + testData.getData("Status");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.OriginOfCaseDropdownXPath())){
            error = "Failed clicked on the dropdown ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.specificTreeItemXPath(getData("Classification")))){
            error = "Failed to click the Origin Of Case Dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.dropdownElementXPath(getData("Injury 2")))){
            error = "Failed to select the following item item: " + getData("Injury 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.LinkToIncidentRecordDropdownXPath())){
            error = "Failed clicked on the Link Case To Incident Record dropdown";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.dropdownElementXPath(getData("Link To Incident Record")))){
            error = "Failed to wait for the following item: " + getData("Link To Incident Record");
            return false;
        }
//        if (!SeleniumDriverInstance.moveToElementByXpath(InjuredPersonsPageObjects.dropdownElementXPath(getData("Link To Incident Record")), 500))
//        {
//            error = "Failed to move to the following item: " + getData("Link To Incident Record");
//            return false;
//        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.dropdownElementXPath(getData("Link To Incident Record")))){
            error = "Failed to select the following item: " + getData("Link To Incident Record");
            return false;
        }

        narrator.stepPassed("Successfully selected this item on the Origin Of Case dropdown: " + getData("Classification") + " -> " + getData("Injury 2"));
        narrator.stepPassedWithScreenShot("Link to Incident record is displayed");
        
        return true;
    }

    public boolean DoctorsNotesDetails(){
        //Medical Practionar
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.MedicalPractionerDropdownXPath())){
            error = "Failed clicked on the Link Case To Incident Record dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.dropdownElementXPath(getData("Medical Practionar")))){
            error = "Failed to select the following item: " + getData("Medical Practionar");
            return false;
        }

        //Doctors note
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.DoctorsNotesTextAreaXPath(), getData("Doctors Notes"))){
            error = "Failed to fill in the Doctor's Notes text area";
            return false;
        }
        
        //Date issued
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.DateIssuedXPath(), startDate)){
            error = "Failed to enter text into the Date Issued text field";
            return false;
        }

        //Fitness
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.FitnessForWorkDropdownXPath())){
            error = "Failed clicked on the Link Case To Incident Record dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.dropdownElementXPath(getData("Fitness")))){
            error = "Failed to select the following item: " + getData("Fitness");
            return false;
        }
        
        //Date from
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.DateFromXPath(), startDate)){
            error = "Failed to enter text into the Date From text field ";
            return false;
        }
        
        //Date To
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.DateToXPath(), endDate)){
            error = "Failed to enter text into the Date to text field ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.RestrictionsTextAreaXPath())){
            error = "Failed to wait for the Restrictions text area";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.RestrictionsTextAreaXPath(), getData("Restrictions"))){
            error = "Failed to enter text for the Restrictions text area";
            return false;
        }

        
        //Supporting documents        
        //Upload a link 
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.link_buttonxpath())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.link_buttonxpath())) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow()) {
            error = "Failed to switch tab.";
            return false;
        }

        //Link
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.LinkURL())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.LinkURL(), getData("Document Link"))) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.urlTitle())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.urlTitle(), getData("Title"))) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Entered Url: '" + getData("Document Link") + "' and 'Title: '" + getData("Title") + "'.");

        //url Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.urlAddButton())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.urlAddButton())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Incident_Management_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        
        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.SaveFormButtonXPath())){
            error = "Failed to wait for Save button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.SaveFormButtonXPath())){
            error = "Failed to click on Save button";
            return false;
        }
        pause(4000);
        narrator.stepPassedWithScreenShot("Successfully Saved Return To Work Management record");
        
        //Return to Work Management Close button
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.ReturnedWorkManagement_Close())){
            error = "Failed to wait for X Close.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.ReturnedWorkManagement_Close())){
            error = "Failed to wait for X Close.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully closed the Agenda X button.");
        
        //Return to Work Management
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.returnedWorkManagement_GridView())){
            error = "Failed to wait for 'Return to Work Management' grid view.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.returnedWorkManagement_GridView())){
            error = "Failed to wait for 'Return to Work Management' grid view.";
            return false;
        }
        
         //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.injuredPersonsFlow())) {
            error = "Fail to wait for Injured Details process flow";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.injuredPersonsFlow())) {
            error = "Fail to click Injured Details process flow";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Injury Details process flow.");
        pause(2000);
        
        return true;
    }
}
