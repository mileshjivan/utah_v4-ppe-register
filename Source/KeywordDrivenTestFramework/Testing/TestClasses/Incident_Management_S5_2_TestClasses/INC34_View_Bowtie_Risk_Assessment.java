/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import java.io.File;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "34-View Bowtie Risk Assessment",
        createNewBrowserInstance = false
)
public class INC34_View_Bowtie_Risk_Assessment extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public INC34_View_Bowtie_Risk_Assessment() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!View_Bowtie_Risk_Assessment()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed View Bowtie Risk Assessment");
    }

    public boolean View_Bowtie_Risk_Assessment() {
        //Additional Information
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.AdditionalInformation())){
            error = "Failed to wait for 'Additional Information' check box.";
            return false;
        }
        if(!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.AdditionalInformation())){
            error = "Failed to scroll to 'Additional Information' check box.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.AdditionalInformation())){
            error = "Failed to click the 'Additional Information' check box.";
            return false;
        }
        narrator.stepPassed("Additional Information check box is displayed");
                    
        //Related Bowties panel.
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_Related_Bowties_Panelxpath())) {
            error = "Failed to wait for Related Bowties panel.";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.incidentInvestigation_Related_Bowties_Panelxpath())) {
            error = "Failed to scroll to Related Bowties panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentInvestigation_Related_Bowties_Panelxpath())) {
            error = "Failed to wait for Related Bowties panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Related Bowties panel is displayed.");
        pause(2000);

        //Available Related Bowties panel Gridview
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_Related_Bowties_Openxpath())) {
            error = "Failed to wait for Available Related Bowties Record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.incidentInvestigation_Related_Bowties_Openxpath())) {
            error = "Failed to click Available Related Bowties Record.";
            return false;
        }

        //Save Mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2(), 40)){
            error = "Webside too long to load wait reached the time out";
            return false;
        }  
        
        pause(4000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.incidentInvestigation_Related_Bowties_OpenedRecordxpath())) {
            error = "Failed to wait for Available Related Risk to open.";
            return false;
        }

        String openedRelatedBowties = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.incidentInvestigation_Related_Bowties_OpenedRecordxpath());
        narrator.stepPassed("Risk Assessment opened: " + openedRelatedBowties);

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Incident_Related_Bowties_ProcessFlow())) {
            error = "Fail to wait for Risk Bowties process flow";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Incident_Related_Bowties_ProcessFlow())) {
            error = "Fail to click Risk Bowties process flow";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Risk Bowties process flow.");
        
        return true;
    }

}
