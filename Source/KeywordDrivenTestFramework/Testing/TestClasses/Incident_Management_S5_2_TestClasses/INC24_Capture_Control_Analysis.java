/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "24-Capture Control Analysis",
        createNewBrowserInstance = false
)

public class INC24_Capture_Control_Analysis extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String date;
    String path;
    String ControlAnalysisNo;

    public INC24_Capture_Control_Analysis() {
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        String pathofImages = System.getProperty("user.dir") + "\\images";
        path = new File(pathofImages).getAbsolutePath();

    }

    public TestResult executeTest() {
        if (!Capture_Control_Analysis()) {
            return narrator.testFailed("Failed to capture control analysis details - " + error);
        }
        if (!CaptureControlAnalysisActions()) {
            return narrator.testFailed("Failed to capture control analysis details - " + error);
        }
        if (!NavigateToIncidentInvestigation()) {
            return narrator.testFailed("Failed to capture control analysis details - " + error);
        }
        return narrator.finalizeTest("Successfully Captured Control Analysis record.");
    }

    public boolean Capture_Control_Analysis() {
        //Absent and Failed Defenses
        pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Absent_And_Failed_Defenses_Panel())){
            error = "Failed to wait for the Absent and Failed Defenses Panel.";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.Absent_And_Failed_Defenses_Panel())){
            error = "Failed to scroll to the Absent and Failed Defenses Panel.";
            return false;
        }
        narrator.stepPassed("Absent and Failed Defenses Panel is displayed");
        
        //Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.controlAnalysisAddButon())) {
            error = "Failed to wait for - Absent and Failed Defenses - add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.controlAnalysisAddButon())) {
            error = "Failed to click - Absent and Failed Defenses - add button.";
            return false;
        }
        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.loadingPermissionActive(), 3)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.loadingPermissions(), 40)) {
                error = "Webside too long to load wait reached the time out";
            }
            return false;
        }
        SeleniumDriverInstance.pause(1500);

        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.controlFlowProcess(), 15)) {
            error = "Failed to wait on - Control Analysis - process button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.controlFlowProcess())) {
            error = "Failed to click on - Control Analysis - process button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfukky clicked Control Analysis - process button.");
        
        //Control Analysis - add phase
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.activeAddPhaseXpath(), 7)) {
            error = "Failed to wait on - Control Analysis - add phase ";
            return false;
        }

        //Control Analysis - Control Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.controlDropdown())) {
            error = "Failed to wait for - Control Analysis - Control Dropdown ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.controlDropdown())) {
            error = "Failed to click on - Control Analysis - Control Dropdown ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyDropdwonOptionXpath(getData("Control")))) {
            error = "Failed to selet on - Control Analysis - Control ";
            return false;
        }

        //Control Analysis - Control description
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.controlDescription(), getData("Control description"))) {
            error = "Failed to enter - Control Analysis - Control description ";
            return false;
        }

        //Critical Control
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.criticalControl())) {
            error = "Failed to click - Control Analysis - Critical Control";
            return false;
        }
        
        //Control reference
        if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.controlReference(), getData("Control reference"))) {
            error = "Failed to enter - Control Analysis - Control reference";
            return false;
        }
        
        String controlAnalysis = getData("Control analysis");
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.controlAnalysisDropdown())) {
            error = "Failed to click on - Control Analysis Dropdown ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyDropdwonOptionXpath(controlAnalysis))) {
            error = "Failed to selet on - Control Analysis ";
            return false;
        }

        if (controlAnalysis.equalsIgnoreCase("Effective")) {
            //Related Link panel
            if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.relatedLinkXpath(), 3)) {
                error = "Failed - Related Link panel is displayed  ";
                return false;
            }
        } else {
            //Related Link panel
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.relatedLinkXpath(), 3)) {
                error = "Failed - Related Link panel was displayed  ";
                return false;
            }
            
            //Control Analysis - Cause selection - dropdwon 
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.causeSelectionDropdown())) {
                error = "Failed to click on - Control Analysis - Cause selection - dropdwon ";
                return false;
            }

            //Control Analysis - Cause selection
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyDropdwonOptionXpath(getData("Cause selection")))) {
                error = "Failed to selet on - Control Analysis - Cause selection ";
                return false;
            }

            //Control Analysis - Description
            if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.description(), getData("Description"))) {
                error = "Failed to enetr - Control Analysis - Description ";
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.controlFollowProDropdown())) {
            error = "Failed to click on - Control Analysis - Did the control follow procedure - dropdown ";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyDropdwonOptionXpath(getData("Did the control follow procedure")))) {
            error = "Failed to selet on - Control Analysis -Did the control follow procedure ";
            return false;
        }

        //Set True in the testpack to add Support documents
        String supportingDocuments = getData("Supporting Documents");

        if (supportingDocuments.equalsIgnoreCase("True")) {
            //Supporting documents
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.supportDocumentsPanel())) {
                error = "Failed to wait for - Control Analysis  - Support documents panel ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.supportDocumentsPanel())) {
                error = "Failed to click on - Control Analysis  - Support documents panel ";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked - Control Analysis  - Support documents panel.");
     
            //Upload a link 
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.link_buttonxpath())) {
                error = "Failed to wait for 'Link a document' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.link_buttonxpath())) {
                error = "Failed to click on 'Link a document' button.";
                return false;
            }
            pause(2000);
            narrator.stepPassedWithScreenShot("Successfully click 'Link a document' button.");

            if (!SeleniumDriverInstance.switchToTabOrWindow()) {
                error = "Failed to switch tab.";
                return false;
            }

            //Link
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.LinkURL())) {
                error = "Failed to wait for 'Link a document' button.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.LinkURL(), getData("Document Link"))) {
                error = "Failed to click on 'Link a document' button.";
                return false;
            }

            //Title
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.urlTitle())) {
                error = "Failed to wait for 'Link a document' button.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.urlTitle(), getData("Title"))) {
                error = "Failed to click on 'Link a document' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully Entered Url: '" + getData("Document Link") + "' and 'Title: '" + getData("Title") + "'.");

            //url Add Button
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.urlAddButton())) {
                error = "Failed to wait for 'Add' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.urlAddButton())) {
                error = "Failed to click on 'Add' button.";
                return false;
            }
            pause(2000);
            narrator.stepPassedWithScreenShot("Successfully uploaded a Supporting Document.");

            if (!SeleniumDriverInstance.switchToDefaultContent()) {
                error = "Failed to switch tab.";
                return false;
            }

            //iFrame
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.iframeXpath())) {
                error = "Failed to switch to frame.";
                return false;
            }
            if (!SeleniumDriverInstance.switchToFrameByXpath(Incident_Management_PageObjects.iframeXpath())) {
                error = "Failed to switch to frame.";
                return false;
            }
        }
        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.controlAnalysisSaveButoon())) {
            error = "Failed to wait for - Control Analysis - Save - Button ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.controlAnalysisSaveButoon())) {
            error = "Failed to click on - Control Analysis - Save - Button ";
            return false;
        }
        
        //Save Mask
        if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait())){
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2())){
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Management_PageObjects.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.activeEditPhaseXpath(), 9)) {
            error = "Failed to save - Control Analysis";
            return false;
        }

        String[] id = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.controlAnalysisId()).split("#");
        ControlAnalysisNo = id[1];
       
        narrator.stepPassed("Record Number: " + id[1]);
        narrator.stepPassedWithScreenShot("Successfulled saved control Analysis");

        return true;
    }
    
    public boolean CaptureControlAnalysisActions(){
        String controlAnalysis = getData("Control analysis");
        if (controlAnalysis.equalsIgnoreCase("Effective")) {
            //Control Analysis - Actions panel
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.controlAnalysisActionTab())) {
                error = "Failed to wait for - Control Analysis - Actions panel ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.controlAnalysisActionTab())) {
                error = "Failed to click on - Control Analysis - Actions panel ";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked Control Analysis - Actions panel.");

            //Control Analysis Actions - add button
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.controlAnalysisActionAddButton())) {
                error = "Failed to click on - Control Analysis Actions - add button ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.controlAnalysisActionAddButton())) {
                error = "Failed to click on - Control Analysis Actions - add button ";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked Control Analysis Actions - add button.");

            //Control Analysis Actions win
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.controlAnalysisActionProcessButton(), 15)) {
                error = "Failed to wait on - Control Analysis Actions win ";
                return false;
            }

            //Process Flow
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.controlAnalysisActionProcessButton())) {
                error = "Failed to wait for - Control Analysis  - Control Analysis Actions - process flow button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.controlAnalysisActionProcessButton())) {
                error = "Failed to click on - Control Analysis  - Control Analysis Actions - process flow button";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked Control Analysis  - Control Analysis Actions - process flow button.");

            //Control Analysis Actions - Action description
            if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.actionDescription(), getData("Action description"))) {
                error = "Failed to enter on - Control Analysis  - Control Analysis Actions - Action description ";
                return false;
            }
            narrator.stepPassedWithScreenShot("Control Analysis  - Control Analysis Actions - Action description: " + getData("Action description"));

            //Control Analysis  - Control Analysis Actions - Department Responsible dropdown
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.departmentResponsibleDropdown())) {
                error = "Failed to click on - Control Analysis  - Control Analysis Actions - Department Responsible dropdown";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.anyDropdwonOptionXpath(getData("Department responsible")))) {
                error = "Failed to click on - Control Analysis  - Control Analysis Actions - Department Responsible ";
                return false;
            }
            narrator.stepPassedWithScreenShot("Control Analysis  - Control Analysis Actions - Department Responsible: " + getData("Department responsible"));
            
            //Control Analysis  - Control Analysis Actions - Responsible person dropdown
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.responsiblePersonDropdown())) {
                error = "Failed to click on - Control Analysis  - Control Analysis Actions - Responsible person dropdown";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.responsiblePerson(getData("Responsible person")))) {
                error = "Failed to wait for - Control Analysis  - Control Analysis Actions - Responsible person ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.responsiblePerson(getData("Responsible person")))) {
                error = "Failed to click on - Control Analysis  - Control Analysis Actions - Responsible person ";
                return false;
            }
            narrator.stepPassedWithScreenShot("Control Analysis  - Control Analysis Actions - Responsible person: " + getData("Responsible person"));

            //Control Analysis  - Control Analysis Actions - Action due date
            if (!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.actionDueDate(), date)) {
                error = "Failed to enter on - Control Analysis  - Control Analysis Actions - Action due date ";
                return false;
            }
            narrator.stepPassedWithScreenShot("Control Analysis  - Control Analysis Actions - Action due date: " + date);

            //Control Analysis - Control Analysis Actions - Save
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.controlAnalysisActionSaveButton())) {
                error = "Failed to wait for - Control Analysis - Control Analysis Actions - Save button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.controlAnalysisActionSaveButton())) {
                error = "Failed to click on - Control Analysis - Control Analysis Actions - Save button";
                return false;
            }

            //Save Mask
            if (SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.saveWait())){
                if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.saveWait2())){
                    error = "Webside too long to load wait reached the time out";
                    return false;
                }
            }

            //Validate if the record has been saved or not.
            if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Management_PageObjects.validateSave())){
                error = "Failed to wait for Save validation.";
                return false;
            }

            String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.validateSave());

            if (!SaveFloat.equals("Record saved")){
                narrator.stepPassedWithScreenShot("Failed to save record.");
                return false;
            }
            narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
                
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.activeToBeInitiatedXpath(), 20)) {
                error = "Failed to save on - Control Analysis  - Control Analysis Actions record";
                return false;
            }
            String[] Actionid = SeleniumDriverInstance.retrieveTextByXpath(Incident_Management_PageObjects.controlAnalysisActionId()).split("#");
            narrator.stepPassed("Control Analysis Actions  Record Number: " + Actionid[1]);
            narrator.stepPassedWithScreenShot("Successfulled saved Control Analysis Actions ");
            
            //Close Control Analysis  - Control Analysis Actions
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.controlAnalysisAction_CloseButton())) {
                error = "Failed to wait for - Control Analysis - Control Analysis Actions - Close button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.controlAnalysisAction_CloseButton())) {
                error = "Failed to click on - Control Analysis - Control Analysis Actions - Close button";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully Closed Control Analysis - Control Analysis Actions - Close button.");
        
            //Return to Incident Management - Control Analysis
            if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.controlAnalysisActions_GridView())){
                error = "Failed to wait for 'Incident Management - Control Analysis' grid view.";
                return false;
            }
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.controlAnalysisActions_GridView())){
                error = "Failed to wait for 'Incident Management - Control Analysis' grid view.";
                return false;
            }
        
            //Close Control Analysis  - Control Analysis 
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.controlAnalysis_CloseButton())) {
                error = "Failed to wait for - Control Analysis - Control Analysis - Close button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.controlAnalysis_CloseButton())) {
                error = "Failed to click on - Control Analysis - Control Analysis - Close button";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully Closed Control Analysis - Control Analysis - Close button.");
        }
         
        //Loading data Mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.loadingData())){
            error = "Webside too long to load wait reached the time out";
            return false;
        }
            
        return true;
    }
    
    public boolean NavigateToIncidentInvestigation(){
        //3.Incident Investigation
        pause(1000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.investigationTab())) {
            error = "Failed to wait 3.Incident Investigation phase";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.investigationTab())) {
            error = "Failed to click second 3.Incident Investigation tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 3.Incident Investigation");
        
        //investigation details
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.InvestigationDetailTabXPath())){
            error = "Failed to wait for the investigation details sub tab ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.InvestigationDetailTabXPath())){
            error = "Failed to click on the investigation details sub tab ";
            return false;
        }
        
        //Absent and Failed Defenses
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.Absent_And_Failed_Defenses_Panel())){
            error = "Failed to wait for the Absent and Failed Defenses Panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.Absent_And_Failed_Defenses_Panel())){
            error = "Failed to clicked to the Absent and Failed Defenses Panel.";
            return false;
        }
        narrator.stepPassed("Absent and Failed Defenses Panel is displayed");
        
        //Search Filter
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.SearchFilter())){
            error = "Failed to wait for 'Incident Management - Control Analysis' Search Filter.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.SearchFilter())){
            error = "Failed to click for 'Incident Management - Control Analysis' Search Filter.";
            return false;
        }
        
        //Search RecordNo
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.searchRecordNo())){
            error = "Failed to wait for Record No.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Incident_Management_PageObjects.searchRecordNo(), ControlAnalysisNo)){
            error = "Failed to enter Record No: " + ControlAnalysisNo;
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Record No: " + ControlAnalysisNo);
        
        //Search Filter button
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.SearchFilter_Button())){
            error = "Failed to wait for 'Incident Management - Control Analysis' Search Filter Button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.SearchFilter_Button())){
            error = "Failed to click for 'Incident Management - Control Analysis' Search Filter Button.";
            return false;
        }
        
        //Return to Incident Management - Control Analysis
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.controlAnalysis_GridView())){
            error = "Failed to wait for 'Incident Management - Control Analysis' grid view.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Management_PageObjects.controlAnalysis_GridView())){
            error = "Failed to wait for 'Incident Management - Control Analysis' grid view.";
            return false;
        }
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.IM_processFlow())) {
            error = "Fail to wait for Incident Management process flow";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.IM_processFlow())) {
            error = "Fail to click Incident Management process flow";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Injury Details process flow.");
        pause(2000);
        
        return true;
    }

}
