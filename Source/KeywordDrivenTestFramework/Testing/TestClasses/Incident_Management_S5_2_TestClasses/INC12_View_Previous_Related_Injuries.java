/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "12-View Previous Related Injuries",
        createNewBrowserInstance = false
)

public class INC12_View_Previous_Related_Injuries extends BaseClass{
    String error = "";
    String date;
    SikuliDriverUtility sikuliDriverUtility;
    String recordNumber;
    String recordNumber2;
    String parentWindow;

    public INC12_View_Previous_Related_Injuries(){
        sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        date = new SimpleDateFormat("dd-MM-YYYY").format(new Date());
        recordNumber = "";
        
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest(){
        if (!View_Previous_Related_Injuries()){
            return narrator.testFailed("Error" + error);
        }

        return narrator.finalizeTest("Successfully viewed previous related injuries");
    }

    public boolean View_Previous_Related_Injuries(){
        pause(2000);
        //Previous Related Injuries
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.PreviousRelatedInjuries_Panel())){
            error = "Failed to wait for Previous related injuries panel";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.PreviousRelatedInjuries_Panel())){
            error = "Failed to wait for Previous related injuries panel";
            return false;
        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.PreviousRelatedInjuries_Panel())){
//            error = "Failed to click Previous related injuries panel";
//            return false;
//        }
        narrator.stepPassedWithScreenShot("Successfully clicked Previous related injuries panel");

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.previousRelatedInjuries_Record(getData("Employee")))){
            error = "Failed to wait for Previous related injuries record";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Management_PageObjects.previousRelatedInjuries_Record(getData("Employee")))){
            error = "Failed to click Previous related injuries record";
            return false;
        }
        pause(2000);
        
        // employee dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Management_PageObjects.employeeDropdownXpath())){
            error = "Failed to wait for employee dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Incident_Management_PageObjects.employeeDropdownXpath())){
            error = "Failed to scroll to employee dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.scrollUp()){
            error = "Failed to scroll up";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click process flow button");
        
        return true;
    }
}
