/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Bowtie_Risk_Assessment_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.Bowtie_Risk_Assessment_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */

@KeywordAnnotation(
        Keyword = "FR3-Capture Bowtie Consequences and Corrective Controls - Main Scenario",
        createNewBrowserInstance = false
)

public class FR3_Capture_Bowtie_Consequences_And_Corrective_Controls_MainScenario extends BaseClass{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR3_Capture_Bowtie_Consequences_And_Corrective_Controls_MainScenario(){
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime()); 
    }

    public TestResult executeTest(){
        if (!Navigate_To_Bowtie_Consequences_and_Corrective_Controls()){
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Capture_Bowtie_Consequences_and_Corrective_Controls()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Successfully Captured Bowtie Bowtie Consequences and Corrective Controls");
    }
    
    public boolean Navigate_To_Bowtie_Consequences_and_Corrective_Controls(){
        //Consequences Panel
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Consequences_Panel())){
            error = "Failed to wait for 'Consequences' Panel.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Consequences_Panel())){
            error = "Failed to click on 'Consequences' Panel.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Consequences' Panel.");

        //Consequences Panel
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieConsequences_Panel())){
            error = "Failed to wait for 'Bowtie Consequences' Panel.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieConsequences_Panel())){
            error = "Failed to click on 'Bowtie Consequences' Panel.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Bowtie Causes' Panel.");
        
        //Bowtie Consequences Add Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieConsequences_AddButton())){
            error = "Failed to wait for Bowtie Consequences Add Button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieConsequences_AddButton())){
            error = "Failed to click the Bowtie Consequences Add Button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Bowtie Consequences Add Button.");
                
        return true;
    }

    public boolean Capture_Bowtie_Consequences_and_Corrective_Controls(){
        //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieConsequences_ProcessFlow())){
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(3000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieConsequences_ProcessFlow())){
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        
       //Consequences
       if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Consequences())){
           error = "Failed to wait for Cause input.";
           return false;
       }
       if(!SeleniumDriverInstance.enterTextByXpath(Bowtie_Risk_Assessment_PageObject.Consequences(), getData("Consequences"))){
           error = "Failed to wait for Consequences input.";
           return false;
       }
       narrator.stepPassedWithScreenShot("Successfully entered Consequences: " + getData("Consequences"));
        
        //Save Button
        if(!getData("Save To Continue").equalsIgnoreCase("Yes")){
            if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Consequences_SaveButton())) {
                error = "Failed to wait for Save Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Consequences_SaveButton())) {
                error = "Failed to click on Save Button";
                return false;
            }
        } else{
            if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.Consequences_SaveToContinue_Button())) {
                error = "Failed to wait for Save to continue Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.Consequences_SaveToContinue_Button())) {
                error = "Failed to click on Save to continue Button";
                return false;
            }
        }
        
        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Bowtie_Risk_Assessment_PageObject.saveWait2(), 400)){
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        
        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Bowtie_Risk_Assessment_PageObject.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Bowtie_Risk_Assessment_PageObject.validateSave());
        
        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully."); 
       
        //Bowtie Corrective Controls
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieCorrectiveControls_Panel())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Bowtie_Risk_Assessment_PageObject.BowtieCorrectiveControls_Panel())){
                error = "Failed to wait for Bowtie Corrective Controls Panel.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully displayed Bowtie Corrective Controls Panel");
        
        //Bowtie Consequences
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieConsequences_CloseButton())){
            error = "Failed to wait for Bowtie Consequences Close Button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieConsequences_CloseButton())){
            error = "Failed to click the Bowtie Consequences Close Button.";
            return false;
        }
        pause(4000);
        
        //Bowtie Causes
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieConsequences_Record(getData("Consequences")))){
            error = "Failed to wait for Bowtie Consequences Record: " + getData("Consequences");
            return false;
        }
        if(!SeleniumDriverInstance.scrollToElement(Bowtie_Risk_Assessment_PageObject.BowtieConsequences_Panel())){
            error = "Failed to scroll to 'Bowtie Consequences' Panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot(getData("Consequences") + " - Record Successfully created.");
        
        //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskAssessment_ProcessFlow())){
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskAssessment_ProcessFlow())){
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        
        return true;
    }

}
