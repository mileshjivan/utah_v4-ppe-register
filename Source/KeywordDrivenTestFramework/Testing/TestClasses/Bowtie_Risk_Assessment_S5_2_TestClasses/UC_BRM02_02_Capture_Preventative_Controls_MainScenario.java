/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Bowtie_Risk_Assessment_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.Bowtie_Risk_Assessment_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */

@KeywordAnnotation(
        Keyword = "UC BRM 02-02-Capture Preventative Controls - Main scenario",
        createNewBrowserInstance = false
)

public class UC_BRM02_02_Capture_Preventative_Controls_MainScenario extends BaseClass{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public UC_BRM02_02_Capture_Preventative_Controls_MainScenario(){
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime()); 
    }

    public TestResult executeTest(){
        if (!Capture_Bowtie_Preventative_Controls_Panel()){
            return narrator.testFailed("Failed due - " + error);
        }        
        return narrator.finalizeTest("Successfully Captured Bowtie Causes - Bowtie Preventative Controls");
    }
    
    public boolean Capture_Bowtie_Preventative_Controls_Panel(){
        //Bowtie Causes
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieCauses_Record(getData("Cause")))){
            error = "Failed to wait for Bowtie Causes Record.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieCauses_Record(getData("Cause")))){
            error = "Failed to click the Bowtie Causes Record.";
            return false;
        }
        
        //Bowtie Preventative Controls
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtiePreventativeControls_Panel())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Bowtie_Risk_Assessment_PageObject.BowtiePreventativeControls_Panel())){
                error = "Failed to wait for Bowtie Preventative Controls Panel.";
                return false;
            }
        }
        pause(5000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtiePreventativeControls_Panel())){
            error = "Failed to click the Bowtie Preventative Controls Panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully displayed Bowtie Preventative Controls Panel");
                
        //Bowtie Preventative Controls Add Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieCauses_BowtiePreventativeControls_AddButton())){
            error = "Failed to wait for Bowtie Cause - Bowtie Preventative Controls Add Button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieCauses_BowtiePreventativeControls_AddButton())){
            error = "Failed to click the Bowtie Cause - Bowtie Preventative Controls Add Button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Bowtie Causes - Bowtie Preventative Controls Add Button.");
        
        //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieCauses_BowtiePreventativeControls_ProcessFlow())){
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieCauses_BowtiePreventativeControls_ProcessFlow())){
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        
       //Control category
       if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.ControlCategory_Dropdown())){
           error = "Failed to wait for Control category dropdown.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.ControlCategory_Dropdown())){
           error = "Failed to click the Control category dropdown.";
           return false;
       }
       if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.ControlCategory_Option(getData("Control category 1")))){
           error = "Failed to wait for Control category Option: " + getData("Control category 1");
           return false;
       }
       if(!SeleniumDriverInstance.doubleClickElementbyXpath(Bowtie_Risk_Assessment_PageObject.ControlCategory_Option(getData("Control category 1")))){
           error = "Failed to double click Control category Option: " + getData("Control category 1");
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.ControlCategory_Option(getData("Control category 2")))){
           error = "Failed to click Control category Option: " + getData("Control category 2");
           return false;
       }
       narrator.stepPassedWithScreenShot("Control category Option: " + getData("Control category 1") + " -> " + getData("Control category 2"));
        
       //Control description
       if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.ControlDescription())){
           error = "Failed to wait for 'Control description'";
           return false;
       }
       if(!SeleniumDriverInstance.enterTextByXpath(Bowtie_Risk_Assessment_PageObject.ControlDescription(), getData("Control description"))){
           error = "Failed to enter 'Control description': " + getData("Control description");
           return false;
       }
       narrator.stepPassedWithScreenShot("'Control description': " + getData("Control description"));
       
       //Control implementation
       if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.ControlImplementation_Dropdown())){
           error = "Failed to wait for Control implementation dropdown.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.ControlImplementation_Dropdown())){
           error = "Failed to click the Control implementation dropdown.";
           return false;
       }
       if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.ControlCategory_Option(getData("Control implementation")))){
           error = "Failed to wait for Control implementation Option: " + getData("Control implementation");
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.ControlCategory_Option(getData("Control implementation")))){
           error = "Failed to click Control implementation Option: " + getData("Control implementation");
           return false;
       }
       narrator.stepPassedWithScreenShot("Control implementation Option: " + getData("Control implementation"));
       
       //Control owner
       if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.ControlOwner_Dropdown())){
           error = "Failed to wait for Control owner dropdown.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.ControlOwner_Dropdown())){
           error = "Failed to click the Control owner dropdown.";
           return false;
       }
       if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.ControlCategory_Option(getData("Control owner")))){
           error = "Failed to wait for Control owner Option: " + getData("Control owner");
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.ControlCategory_Option(getData("Control owner")))){
           error = "Failed to click Control owner Option: " + getData("Control owner");
           return false;
       }
       narrator.stepPassedWithScreenShot("Control owner Option: " + getData("Control owner"));
       
       //Control quality
       if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.ControlQuality_Dropdown())){
           error = "Failed to wait for Control quality dropdown.";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.ControlQuality_Dropdown())){
           error = "Failed to click the Control quality dropdown.";
           return false;
       }
       if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.ControlCategory_Option(getData("Control quality")))){
           error = "Failed to wait for Control quality Option: " + getData("Control quality");
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.ControlCategory_Option(getData("Control quality")))){
           error = "Failed to click Control quality Option: " + getData("Control quality");
           return false;
       }
       narrator.stepPassedWithScreenShot("Control quality Option: " + getData("Control quality"));
       
       //Control reference
       if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.ControlReference())){
           error = "Failed to wait for 'Control reference'";
           return false;
       }
       if(!SeleniumDriverInstance.enterTextByXpath(Bowtie_Risk_Assessment_PageObject.ControlReference(), getData("Control reference"))){
           error = "Failed to enter 'Control reference': " + getData("Control reference");
           return false;
       }
       narrator.stepPassedWithScreenShot("'Control reference': " + getData("Control reference"));
       
       //Control Matrix panel
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.ControlMatrix_Panel())){
           error = "Failed to wait for Control Matrix panel";
           return false;
       }
       if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.ControlMatrix_Panel())){
           error = "Failed to click Control Matrix panel";
           return false;
       }
       narrator.stepPassedWithScreenShot("Successfully clicked Control Matrix Panel");
       
        //Save Button
        if(!getData("Save To Continue").equalsIgnoreCase("Yes")){
            if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtiePreventativeControls_SaveButton())) {
                error = "Failed to wait for Save Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtiePreventativeControls_SaveButton())) {
                error = "Failed to click on Save Button";
                return false;
            }
        } else{
            if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtiePreventativeControls_SaveToContinue_Button())) {
                error = "Failed to wait for Save to continue Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtiePreventativeControls_SaveToContinue_Button())) {
                error = "Failed to click on Save to continue Button";
                return false;
            }
        }
        
        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Bowtie_Risk_Assessment_PageObject.saveWait2(), 400)){
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        
        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Bowtie_Risk_Assessment_PageObject.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Bowtie_Risk_Assessment_PageObject.validateSave());
        
        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully."); 
       
        //Bowtie Preventative Controls
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.ControlsChecklist_Panel())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Bowtie_Risk_Assessment_PageObject.ControlsChecklist_Panel())){
                error = "Failed to wait for Control Checklist Panel.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully displayed Control Checklist Panel");
        
        //Bowtie cause - Bowtie Preventative Controls
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtiePreventativeControls_CloseButton())){
            error = "Failed to wait for Bowtie Causes - Bowtie Preventative Controls Close Button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtiePreventativeControls_CloseButton())){
            error = "Failed to click the Bowtie Causes - Bowtie Preventative Controls Close Button.";
            return false;
        }
        pause(4000);
        
        //Bowtie Causes
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtiePreventativeControls_Record(getData("Control description")))){
            error = "Failed to wait for Bowtie Causes - Bowtie Preventative Controls Record.";
            return false;
        }
        
        //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskAssessment_BowtieCauses_ProcessFlow())){
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieRiskAssessment_BowtieCauses_ProcessFlow())){
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        pause(4000);
        //Bowtie cause - Bowtie Preventative Controls
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.BowtieCauses_CloseButton())){
            error = "Failed to wait for Bowtie Causes - Bowtie Preventative Controls Close Button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.BowtieCauses_CloseButton())){
            error = "Failed to click the Bowtie Causes - Bowtie Preventative Controls Close Button.";
            return false;
        }
        pause(4000);
        
        return true;
    }

}
