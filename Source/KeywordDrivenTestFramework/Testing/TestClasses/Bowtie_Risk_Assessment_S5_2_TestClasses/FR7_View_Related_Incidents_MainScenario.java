/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Bowtie_Risk_Assessment_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.Bowtie_Risk_Assessment_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR7-View Related Incidents - Main scenario",
        createNewBrowserInstance = false
)
public class FR7_View_Related_Incidents_MainScenario extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR7_View_Related_Incidents_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!View_Related_Incidents()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed View Related Incidents");
    }

    public boolean View_Related_Incidents() {
        //Additional Information
        if(!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.AdditionalInformation())){
            error = "Failed to wait for 'Additional Information' check box.";
            return false;
        }
        if(!SeleniumDriverInstance.scrollToElement(Bowtie_Risk_Assessment_PageObject.AdditionalInformation())){
            error = "Failed to scroll to 'Additional Information' check box.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.AdditionalInformation())){
            error = "Failed to click the 'Additional Information' check box.";
            return false;
        }
        narrator.stepPassed("Additional Information check box is displayed");
                    
        //Related Findings panel.
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.RelatedIncidents_Panel())) {
            error = "Failed to wait for Related Incidents panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.RelatedIncidents_Panel())) {
            error = "Failed to wait for Related Incidents panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Related Incidents panel is displayed.");
        pause(2000);

        //Available Related Incidents Gridview
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.RelatedIncidents_Openxpath())) {
            error = "Failed to wait for Available Related Incidents.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.RelatedIncidents_Openxpath())) {
            error = "Failed to click Available Related Incidents.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.RelatedIncidents_OpenedRecordxpath())) {
            error = "Failed to wait for Available Related Incidents to open.";
            return false;
        }

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Bowtie_Risk_Assessment_PageObject.IncidentManagement_ProcessFlow())) {
            error = "Fail to wait for Findings process flow";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Bowtie_Risk_Assessment_PageObject.IncidentManagement_ProcessFlow())) {
            error = "Fail to click Findings process flow";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Incident Management process flow.");
        
        return true;
    }

}
