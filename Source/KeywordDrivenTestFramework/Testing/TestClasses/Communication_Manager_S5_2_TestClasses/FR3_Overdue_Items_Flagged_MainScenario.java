/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Communication_Manager_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Communication_Manager_V5_2_PageObjects.Communication_Manager_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */

@KeywordAnnotation(
        Keyword = "FR3–Overdue Items Flagged v5.2 - Main scenario",
        createNewBrowserInstance = false
)

public class FR3_Overdue_Items_Flagged_MainScenario extends BaseClass{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR3_Overdue_Items_Flagged_MainScenario(){
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime()); 
    }

    public TestResult executeTest(){
        if (!Navigate_To_Communication_Manager()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Successfully Overdue Items Flagged");
    }
    
    public boolean Navigate_To_Communication_Manager(){
        //Navigate to Occupational Health
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.navigate_OccupationalHealth())){
            error = "Failed to wait for 'Occupational Health' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.navigate_OccupationalHealth())){
            error = "Failed to click on 'Occupational Health' tab.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Occupational Health' tab.");
                
        //Navigate to Communication Manager
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.Communication_Module())){
            error = "Failed to wait for 'Communication Manager' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.Communication_Module())){
            error = "Failed to click on 'Communication Manager' tab.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Communication Manager' search page.");
         
        //Record number field
        if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.record_No())){
            error = "Failed to wait for 'Record number' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Communication_Manager_PageObjects.record_No(), getData("Record number"))){
            error = "Failed to enter '" + getData("Record number") + "' into 'Record number' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + getData("Record number") + "' into 'Record number' field.");
       
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.serachBtn())){
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.serachBtn())){
            error = "Failed to click on 'Search' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");

        //Select record
        if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.selectRecord(getData("Record number")))){
            error = "Failed to wait for '" + getData("Record number") + "' record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.selectRecord(getData("Record number")))){
            error = "Failed to click on '" + getData("Record number") + "' record.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Record number") + "' record.");
        
        //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.CommManager_ProcessFlow())){
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(2000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.CommManager_ProcessFlow())){
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        
         //Communication status
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.CommunicationStatus())){
            error = "Faied to wait for 'Communication status'";
            return false;
        }
        String CommunicationStatus = SeleniumDriverInstance.retrieveTextByXpath(Communication_Manager_PageObjects.communicationStatus());
        
        if(CommunicationStatus.equalsIgnoreCase(getData("Status"))){
            if (!SeleniumDriverInstance.ValidateByText(Communication_Manager_PageObjects.communicationStatus(), getData("Status")))
            {
                error = "Failed to validate '" + CommunicationStatus + "' from '" + getData("Status") + "'.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully validated Expected Status: '" + CommunicationStatus + "' and Actual Status: '" + getData("Status") + "'.");
        }
        else
        {
            error = "Communication Status: '" + CommunicationStatus;
            return false;
        }
      
        return true;
    }

}
