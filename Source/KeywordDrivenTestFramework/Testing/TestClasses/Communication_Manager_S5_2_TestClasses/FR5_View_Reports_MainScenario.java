/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Communication_Manager_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Communication_Manager_V5_2_PageObjects.Communication_Manager_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR5-View Reports v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR5_View_Reports_MainScenario extends BaseClass{

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR5_View_Reports_MainScenario(){
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest(){
        if (!Navigate_To_Communication_Manager()){
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully clicked Communication Manager Reports.");
    }

    public boolean Navigate_To_Communication_Manager(){
        //Navigate to Occupational Health
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.navigate_OccupationalHealth())){
            error = "Failed to wait for 'Occupational Health' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.navigate_OccupationalHealth())){
            error = "Failed to click on 'Occupational Health' tab.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Occupational Health' tab.");
                
        //Navigate to Communication Manager
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.Communication_Module())){
            error = "Failed to wait for 'Communication Manager' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.Communication_Module())){
            error = "Failed to click on 'Communication Manager' tab.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Communication Manager' search page.");
        
        //Search button
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.searchBtn())){
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.searchBtn())){
            error = "Failed to click on 'Search' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Serach' button.");

        //Reports button
        if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.reportsBtn())){
            error = "Failed to wait for 'Reports' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.reportsBtn())){
            error = "Failed to click on 'Reports' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Reports' button.");
        parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        
        //View reports icon
        if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.viewReportsIcon())){
            error = "Failed to wait for 'View reports' icon.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.viewReportsIcon())){
            error = "Failed to click on 'View reports' icon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'View reports' icon.");
        SeleniumDriverInstance.switchToTabOrWindow();
        
        //Continue button
        if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.continueBtn())){
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.continueBtn())){
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        pause(5000);
        SeleniumDriverInstance.switchToTabOrWindow();
        narrator.stepPassedWithScreenShot("Successfully click 'Continue' button.");
        narrator.stepPassedWithScreenShot("Viewing Rreports.");
        
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        
         //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.iframeXpath())){
            error = "Failed to wait for frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Communication_Manager_PageObjects.iframeXpath())){
            error = "Failed to switch to frame.";
            return false;
        }
        
        //View full reports icon
        if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.viewFullReportsIcon())){
            error = "Failed to wait for 'View full reports' icon.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.viewFullReportsIcon())){
            error = "Failed to click on 'View full reports' icon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'View full reports' icon.");
        SeleniumDriverInstance.switchToTabOrWindow();
        
        //Continue button
        if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.continueBtn())){
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.continueBtn())){
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        pause(5000);
        SeleniumDriverInstance.switchToTabOrWindow();
        narrator.stepPassedWithScreenShot("Successfully click 'Continue' button.");
        narrator.stepPassedWithScreenShot("Viewing Full Rreports.");
        pause(2000);
        
        return true;
    }

}
