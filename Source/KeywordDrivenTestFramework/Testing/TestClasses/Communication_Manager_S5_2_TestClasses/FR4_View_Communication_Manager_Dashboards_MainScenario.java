/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Communication_Manager_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Communication_Manager_V5_2_PageObjects.Communication_Manager_PageObjects;

/**
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR4-View Communication Manager Dashboards v5.2 - Main scenario",
        createNewBrowserInstance = false
)
public class FR4_View_Communication_Manager_Dashboards_MainScenario extends BaseClass {
    String error = "";

    public FR4_View_Communication_Manager_Dashboards_MainScenario() {

    }

   public TestResult executeTest() {
        if (!navigateToDashboards()) {
            return narrator.testFailed("Failed to Navigate to dashboards - " + error);
        }
        
        return narrator.finalizeTest("Successfully navigated to Risk Register Sheet");
    }

   public boolean navigateToDashboards() {
        //Navigate to Occupational Health
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.navigate_OccupationalHealth())){
            error = "Failed to wait for 'Occupational Health' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.navigate_OccupationalHealth())){
            error = "Failed to click on 'Occupational Health' tab.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Occupational Health' tab.");
        
        //Navigate to Dashboards
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.navigate_Dashboards())){
            error = "Failed to wait for 'Dashboards' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.navigate_Dashboards())){
            error = "Failed to click for 'Dashboards' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Dashboards' tab.");
        
//        //Risk and control registers and location
//        if(!SeleniumDriverInstance.waitForElementByXpath(RiskRegister_PageObjects.anySheets(getData("Sheet Name")))){
//            error = "Failed to wait for '" + getData("Sheet Name") + "' sheet.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(RiskRegister_PageObjects.anySheets(getData("Sheet Name")))){
//            error = "Failed to wait for '" + getData("Sheet Name") + "' sheet.";
//            return false;
//        }
//        
//        SeleniumDriverInstance.pause(2000);
//        narrator.stepPassedWithScreenShot("Successfully navigated to '" + getData("Sheet Name") + "' sheet.");

        return true;
    }
}
