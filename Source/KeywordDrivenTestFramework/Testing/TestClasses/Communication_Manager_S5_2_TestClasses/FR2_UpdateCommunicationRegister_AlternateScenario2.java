/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Communication_Manager_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Communication_Manager_V5_2_PageObjects.Communication_Manager_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR2-Update Communication Register v5.2 - Alternate Scenario 2",
        createNewBrowserInstance = false
)

public class FR2_UpdateCommunicationRegister_AlternateScenario2 extends BaseClass{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_UpdateCommunicationRegister_AlternateScenario2(){
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest(){
        if (!updateCommunicationRegister()){
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Updated Communication Register.");
    }

    public boolean updateCommunicationRegister(){
        String[] ResponsiblePerson = getData("Responsible Person").split(",");
        String saved = "";
         
        for(int i = 0; i < ResponsiblePerson.length; i++){
            //Communication register grid
            if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.selectCommRegisterRecord(ResponsiblePerson[i]))){
                error = "Failed to wait for '" + ResponsiblePerson[i] + "' record.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.selectCommRegisterRecord(ResponsiblePerson[i]))){
                error = "Failed to click on '" + ResponsiblePerson[i] + "' record.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully click '" + ResponsiblePerson[i] + "' record.");

            //Process flow button
            if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.processFlowBtn())){
                error = "Failed to wait for 'Process flow' button.";
                return false;
            }
            pause(3000);
            if (!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.processFlowBtn())){
                error = "Failed to click on 'Process flow' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

            //Declaration checkbox
            if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.declarationCheckBox2())){
                error = "Failed to wait for 'Declaration' checkbox.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.declarationCheckBox2())){
                error = "Failed to click on 'Declaration' checkbox.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully click 'Declaration' checkbox.");

            //Do you accept dropdown
            if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.doYouAcceptDD())){
                error = "Failed to wait for 'Do you accept?' dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.doYouAcceptDD())){
                error = "Failed to click on 'Do you accept?' dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.doYouAcceptSelect(getData("Do You Accept")))){
                error = "Failed to click on '" + getData("Do You Accept") + "' option.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully click '" + getData("Do You Accept") + "' option.");

            //Comments field
            if (!SeleniumDriverInstance.enterTextByXpath(Communication_Manager_PageObjects.comments(), getData("Comments"))){
                error = "Failed to enter '" + getData("Comments") + "' into comments field.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully entered '" + getData("Comments") + "' into comments field.");

            //Save
            if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.saveBtn())){
                error = "Failed to wait for 'Save' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.saveBtn())){
                error = "Failed to click on 'Save' button.";
                return false;
            }

            //Saving mask
            if (SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.saveWait(), 2)) {
                if (!SeleniumDriverInstance.waitForElementPresentByXpath(Communication_Manager_PageObjects.saveWait2(), 400)) {
                    error = "Webside too long to load wait reached the time out";
                    return false;
                }
            }
           
            //Register status
            if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.RegisterStatus())){
                error = "Faied to wait for 'Register status'";
                return false;
            }
            String RegisterStatus = SeleniumDriverInstance.retrieveTextByXpath(Communication_Manager_PageObjects.RegisterStatus());

            if(RegisterStatus == "Completed"){
                error = "Faied to validate the 'Register status': " + RegisterStatus + " with Completed";
                return false;
            }
            narrator.stepPassedWithScreenShot("Register Status: " + RegisterStatus);

            //Close 'X' button
            if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.x_close())){
                error = "Failed to wait for 'X' button.";
                return false;
            }
            pause(3000);
            if (!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.x_close())){
                error = "Failed to click on 'X' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully click 'X' button.");

            //Process flow
            if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.CommManager_ProcessFlow())){
                error = "Failed to wait for 'Process flow' button.";
                return false;
            }
            pause(4000);
            if (!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.CommManager_ProcessFlow())){
                error = "Failed to click on 'Process flow' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        }
        
        return true;
    }

}
