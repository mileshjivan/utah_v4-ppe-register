/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Communication_Manager_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Communication_Manager_V5_2_PageObjects.Communication_Manager_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */

@KeywordAnnotation(
        Keyword = "UC_CMM_01-02 Update Declaration Status v5.2 - MainScenario",
        createNewBrowserInstance = false
)

public class UC_CMM_0102_UpdateDeclarationStatus_MainScenario extends BaseClass{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public UC_CMM_0102_UpdateDeclarationStatus_MainScenario(){
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime()); 
    }

    public TestResult executeTest(){
        if (!updateDeclarationStatus()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Successfully Updated Declaration Status.");
    }
    
    public boolean updateDeclarationStatus(){
        //Declaration status checkbox
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.declarationCheckBox())){
            error = "Failed to wait for 'Declaration status' checkbox.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.declarationCheckBox())){
            error = "Failed to click on 'Declaration status' checkbox.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Declaration status' checkbox.");
                
        //Save
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.commManager_Save())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.commManager_Save())){
            error = "Failed to click on 'Save' button.";
            return false;
        }
        
        //Saving mask
        if (SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Communication_Manager_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        
        //Validation
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.inspection_Record_Saved_popup())){
            saved = SeleniumDriverInstance.retrieveTextByXpath(Communication_Manager_PageObjects.inspection_Record_Saved_popup());
        } else {
            if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.inspection_Record_Saved_popup())){
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Successfully clicked save");
        } else {
            if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.failed())){
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Communication_Manager_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved")){
                error = "Failed to save record.";
                return false;
            }
        }
        
        //Communication status
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.CommunicationStatus())){
            error = "Faied to wait for 'Communication status'";
            return false;
        }
        String CommunicationStatus = SeleniumDriverInstance.retrieveTextByXpath(Communication_Manager_PageObjects.communicationStatus());
        
        if(CommunicationStatus == "Approved"){
            error = "Faied to validate the 'Communication status': " + CommunicationStatus + " with Approved";
            return false;
        }
        narrator.stepPassedWithScreenShot("Communication Status: " + CommunicationStatus);
        
        return true;
    }

}
