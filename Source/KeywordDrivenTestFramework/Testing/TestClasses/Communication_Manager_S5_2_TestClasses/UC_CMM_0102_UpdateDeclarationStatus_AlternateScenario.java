/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Communication_Manager_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Communication_Manager_V5_2_PageObjects.Communication_Manager_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "UC_CMM_01-02 Update Declaration Status v5.2 - AlternateScenario",
        createNewBrowserInstance = false
)

public class UC_CMM_0102_UpdateDeclarationStatus_AlternateScenario extends BaseClass{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public UC_CMM_0102_UpdateDeclarationStatus_AlternateScenario(){
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest(){
        if (!updateDeclarationStatus()){
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Updated Declaration Status.");
    }

    public boolean updateDeclarationStatus(){  
        //Communication status
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.CommunicationStatus())){
            error = "Faied to wait for 'Communication status'";
            return false;
        }
        String CommunicationStatus = SeleniumDriverInstance.retrieveTextByXpath(Communication_Manager_PageObjects.communicationStatus());
        
        if(CommunicationStatus == "Draft"){
            error = "Faied to validate the 'Communication status': " + CommunicationStatus + " with Draft";
            return false;
        }
        narrator.stepPassedWithScreenShot("Communication Status: " + CommunicationStatus);
        
        return true;
    }

}
