/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Communication_Manager_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Communication_Manager_V5_2_PageObjects.Communication_Manager_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */

@KeywordAnnotation(
        Keyword = "FR1-Capture Communication Manager v5.2 - Main scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Communication_Manager_MainScenario extends BaseClass{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String endDate2;
    
    public FR1_Capture_Communication_Manager_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime()); 
        cal.add(Calendar.DAY_OF_MONTH, 1);
        endDate2 = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime()); 
    }

    public TestResult executeTest(){
        if (!navigateToCommunicationManager()){
            return narrator.testFailed("Failed due - " + error);
        }
        if (!CaptureCommunicationManager()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Successfully captured 'Stakeholder Details'.");
    }
    
    public boolean navigateToCommunicationManager(){
        //Navigate to Occupational Health
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.navigate_OccupationalHealth())){
            error = "Failed to wait for 'Occupational Health' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.navigate_OccupationalHealth())){
            error = "Failed to click on 'Occupational Health' tab.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Occupational Health' tab.");
                
        //Navigate to Communication Manager
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.Communication_Module())){
            error = "Failed to wait for 'Communication Manager' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.Communication_Module())){
            error = "Failed to click on 'Communication Manager' tab.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Communication Manager' search page.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.BPM_Add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.BPM_Add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean CaptureCommunicationManager(){
        //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.CommManager_ProcessFlow())){
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.CommManager_ProcessFlow())){
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        
        //Type of communication dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.typeOfCommDD())){
            error = "Failed to wait for 'Type of communication' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.typeOfCommDD())){
            error = "Failed to click on 'Type of communication' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.typeOfCommSelect(getData("Type Of Communication")))){
            error = "Failed to wait for '" + getData("Type Of Communication") + "' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.typeOfCommSelect(getData("Type Of Communication")))){
            error = "Failed to click on '" + getData("Type Of Communication") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Type Of Communication") + "'.");
        
        //Communication topic field
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.communicationTopic())){
            error = "Failed to wait for 'Description' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Communication_Manager_PageObjects.communicationTopic(), getData("Communication Topic"))){
            error = "Failed to enter '" + getData("Communication Topic") + "' into 'Communication topic' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + getData("Communication Topic") + "'.");
        
        //Communication detail field
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.communicationDetail())){
            error = "Failed to wait for 'Lifestyle' select all button.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Communication_Manager_PageObjects.communicationDetail(), getData("Communication Detail"))){
            error = "Failed to enter '"+getData("Communication Detail")+"' into 'Communication detail' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '"+getData("Communication Detail")+"'.");
        
        //Date of communication field
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.dateOfCommunication())){
            error = "Failed to wait for 'Date of communication' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Communication_Manager_PageObjects.dateOfCommunication(), startDate)){
            error = "Failed to enter '" + startDate + "' into 'Date of communication' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + startDate + "' into 'Date of communication' field.");
        
        //Final date for acknowledgement field
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.finalDate())){
            error = "Failed to wait for 'Final date for acknowledgement' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Communication_Manager_PageObjects.finalDate(), endDate2)){
            error = "Failed to enter '" + endDate2 + "' into 'Final date for acknowledgement' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + endDate2 + "' into 'Final date for acknowledgement' field.");
        
        //Please select the communication groups that need to acknowledge this communication
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.communicationGroups(getData("Communication groups")))){
            error = "Failed to wait for 'Communication groups': " + getData("Communication groups");
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.communicationGroups(getData("Communication groups")))){
            error = "Failed to click on 'Communication groups': " + getData("Communication groups");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Communication groups': " + getData("Communication groups"));
      
        //Upload a link
        if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.linkADoc_buttonxpath())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.linkADoc_buttonxpath())) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }
        narrator.stepPassed("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow()) {
            error = "Failed to switch tab.";
            return false;
        }

        //URL Input
        if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.urlInput_TextAreaxpath())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Communication_Manager_PageObjects.urlInput_TextAreaxpath(), getData("Document url"))) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title Input
        if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.tile_TextAreaxpath())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Communication_Manager_PageObjects.tile_TextAreaxpath(), getData("Title"))) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.linkADoc_Add_buttonxpath())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.linkADoc_Add_buttonxpath())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        
        narrator.stepPassed("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Communication_Manager_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully uploaded the document link.");
        
        //Save
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.commManager_Save())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Manager_PageObjects.commManager_Save())){
            error = "Failed to click on 'Save' button.";
            return false;
        }
        
        //Saving mask
        if (SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Communication_Manager_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        
        //Validation
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.inspection_Record_Saved_popup())){
            saved = SeleniumDriverInstance.retrieveTextByXpath(Communication_Manager_PageObjects.inspection_Record_Saved_popup());
        } else {
            if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.inspection_Record_Saved_popup())){
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Successfully clicked save");
        } else {
            if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Manager_PageObjects.failed())){
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Communication_Manager_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved")){
                error = "Failed to save record.";
                return false;
            }
        }
        
        return true;
    }

}
