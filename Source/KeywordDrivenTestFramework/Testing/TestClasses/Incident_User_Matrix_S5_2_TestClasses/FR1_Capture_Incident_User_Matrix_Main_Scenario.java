/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_User_Matrix_S5_2_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Engagement_Plan_S5_2_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagement_Plan_V5_2_PageObjects.Engagement_Plan_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_User_Matrix_V5_2_PageObjects.Incident_User_Matrix_V5_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Observations_V5_2_PageObjects.Observations_V5_2_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Incident User Matrix v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Incident_User_Matrix_Main_Scenario extends BaseClass
{

    String error = "";

    public FR1_Capture_Incident_User_Matrix_Main_Scenario()
    {

        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!Navigate_To_Management_Maintenance())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        if (!Capture_User_Matrix())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Capture Incident User Matrix");
    }

    public boolean Navigate_To_Management_Maintenance()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_User_Matrix_V5_PageObjects.Social_Sustainability()))
        {
            error = "Failed to wait for 'Social Sustainability' tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_User_Matrix_V5_PageObjects.Social_Sustainability()))
        {
            error = "Failed to wait for 'Social Sustainability' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Governance, Risk & Compliance.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_User_Matrix_V5_PageObjects.Management_Maintenance_Sttings()))
        {
            error = "Failed to click on management maintenance setting wheel";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_User_Matrix_V5_PageObjects.Management_Maintenance_Sttings()))
        {
            error = "Failed to click on management maintenance setting wheel";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Social Sustainability' tab.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_User_Matrix_V5_PageObjects.Management_Maintenance_Heading()))
        {
            error = "Failed to wait for management maintenance heading";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_User_Matrix_V5_PageObjects.Incident_User_Matrix_Tab()))
        {
            error = "Failed to click on Incident User Matrix tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to management maintenance page.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.SE_add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.SE_add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        return true;
    }

    public boolean Capture_User_Matrix()
    {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_User_Matrix_V5_PageObjects.Incident_User_Matrix_ProcessFlow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_User_Matrix_V5_PageObjects.Incident_User_Matrix_ProcessFlow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //Section where incident occured 
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_User_Matrix_V5_PageObjects.incident_occured_section_drop_down()))
        {
            error = "Failed to wait for Section where incident occurred drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_User_Matrix_V5_PageObjects.incident_occured_section_drop_down()))
        {
            error = "Failed to click Section where incident occurred drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_User_Matrix_V5_PageObjects.incident_occured_section_drop_down_option_1()))
        {
            error = "Failed to wait Section where incident occurred drop down option 'Global Company' ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_User_Matrix_V5_PageObjects.incident_occured_section_drop_down_option_1()))
        {
            error = "Failed to click Section where incident occurred drop down option 'Global Company' ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Section where incident occurred :Global Company");

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_User_Matrix_V5_PageObjects.incident_occured_section_drop_down_option_2()))
        {
            error = "Failed to wait Section where incident occurred drop down option 'South Africa' ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_User_Matrix_V5_PageObjects.incident_occured_section_drop_down_option_2()))
        {
            error = "Failed to click Section where incident occurred drop down option 'South Africa' ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Section where incident occurred :South Africa");

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_User_Matrix_V5_PageObjects.incident_occured_section_drop_down_option_3()))
        {
            error = "Failed to wait Section where incident occurred drop down option 'Victory Site' ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_User_Matrix_V5_PageObjects.incident_occured_section_drop_down_option_3()))
        {
            error = "Failed to click Section where incident occurred drop down option 'Victory Site' ";
            return false;
        }

        narrator.stepPassedWithScreenShot("Section where incident occurred : Victory Site ");

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_User_Matrix_V5_PageObjects.Incident_Owner()))
        {
            error = "Failed to wait for Incident owner drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_User_Matrix_V5_PageObjects.Incident_Owner()))
        {
            error = "Failed to click for Incident owner drop down";
            return false;
        }
        narrator.stepPassedWithScreenShot("Incident owner drop down ");

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_User_Matrix_V5_PageObjects.Incident_Owner_Drop_Down_Option(getData("Incident owner"))))
        {
            error = "Failed to wait for Incident owner drop down option :";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_User_Matrix_V5_PageObjects.Incident_Owner_Drop_Down_Option(getData("Incident owner"))))
        {
            error = "Failed to wait for Incident owner drop down option :";
            return false;
        }

        narrator.stepPassedWithScreenShot("Incident owner drop down :" + getData("Incident owner"));

       
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_User_Matrix_V5_PageObjects.Lead_Investigator()))
        {
            error = "Failed to wait for Lead investigator drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_User_Matrix_V5_PageObjects.Lead_Investigator()))
        {
            error = "Failed to click  Lead investigator drop down";
            return false;
        }
        narrator.stepPassedWithScreenShot("Incident owner drop down ");

        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_User_Matrix_V5_PageObjects.Lead_Investigator_Drop_Down_Option(getData("Lead investigator"))))
        {
            error = "Failed to wait for Lead investigator drop down option :";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_User_Matrix_V5_PageObjects.Lead_Investigator_Drop_Down_Option(getData("Lead investigator"))))
        {
            error = "Failed to click Lead investigator drop down option :";
            return false;
        }

        narrator.stepPassedWithScreenShot("Lead investigator drop down :" + getData("Lead investigator"));

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_User_Matrix_V5_PageObjects.saveBtn()))
        {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_User_Matrix_V5_PageObjects.saveBtn()))
        {
            error = "Failed to click on 'Save' button.";
            return false;
        }

        //Saving mask
        if (SeleniumDriverInstance.waitForElementByXpath(Incident_User_Matrix_V5_PageObjects.saveWait(), 2))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_User_Matrix_V5_PageObjects.saveWait2(), 400))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        //Validation
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Incident_User_Matrix_V5_PageObjects.inspection_Record_Saved_popup()))
        {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Incident_User_Matrix_V5_PageObjects.inspection_Record_Saved_popup());
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_User_Matrix_V5_PageObjects.inspection_Record_Saved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked save");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_User_Matrix_V5_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Incident_User_Matrix_V5_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
                      
        }
         narrator.stepPassedWithScreenShot(saved + ": successfully.");

        return true;
    }

}
