/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Job_Profile_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Job_Profile_V5_2_PageObjects.Job_Profile_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR2-View Reports v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_View_Reports_MainScenario extends BaseClass {
    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_View_Reports_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Navigate_To_PPE_Type_Library()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed Viewed Engagements Reports");
    }

    public boolean Navigate_To_PPE_Type_Library() {
        //Navigate to Environmental Health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.navigate_EHS())) {
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.navigate_EHS())) {
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");

        //Operate Maintenance
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.OperateMaintenance())) {
            error = "Failed to wait for 'Operate Maintenance' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.OperateMaintenance())) {
            error = "Failed to click on 'Operate Maintenance' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Operate Maintenance' tab.");
                
        //Navigate To Job Profiles
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.JobProfiles())) {
            error = "Failed to wait for Job Profiles tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.JobProfiles())) {
            error = "Failed to click on Job Profiles tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Job Profiles.");
        
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.searchBtn())) {
            error = "Failed to wait for 'Serach' button.";
            return false;
        }        
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.searchBtn())) {
            error = "Failed to click on 'Serach' button.";
            return false;
        }
        SeleniumDriverInstance.pause(5000);
        narrator.stepPassedWithScreenShot("Successfully click 'Serach' button.");

        //Reports button
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.reportsBtn())) {
            error = "Failed to wait for 'Reports' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.reportsBtn())) {
            error = "Failed to click on 'Reports' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Reports' button.");
        parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        //View reports icon
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.viewReportsIcon())) {
            error = "Failed to wait for 'View reports' icon.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.viewReportsIcon())) {
            error = "Failed to click on 'View reports' icon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'View reports' icon.");
        SeleniumDriverInstance.switchToTabOrWindow();
        //Continue button
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.continueBtn())) {
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.continueBtn())) {
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        pause(5000);
        SeleniumDriverInstance.switchToTabOrWindow();
              
        if(!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.JobProfile_Header())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Job_Profile_PageObjects.JobProfile_Header())){
                error = "Failed to wait for Engagements header to be present.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Continue' button.");
        narrator.stepPassedWithScreenShot("Viewing Rreports.");

        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.iframeXpath())) {
            error = "Failed to wait for frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Job_Profile_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        //View full reports icon
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.viewFullReportsIcon())) {
            error = "Failed to wait for 'View full reports' icon.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.viewFullReportsIcon())) {
            error = "Failed to click on 'View full reports' icon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'View full reports' icon.");
        SeleniumDriverInstance.switchToTabOrWindow();
        //Continue button
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.continueBtn())) {
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.continueBtn())) {
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        pause(5000);
        SeleniumDriverInstance.switchToTabOrWindow();
        narrator.stepPassedWithScreenShot("Successfully click 'Continue' button.");
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.BusinessUnitHeader())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Job_Profile_PageObjects.BusinessUnitHeader())){
                error = "Failed to wait for Engagements header to be present.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Viewing Full Rreports.");
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        
        pause(2000);
        return true;
    }

}
