/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Job_Profile_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Job_Profile_V5_2_PageObjects.Job_Profile_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Job Profile v5.2 - Main Scenario",
        createNewBrowserInstance = false
)
public class FR1_Capture_Job_Profile_MainScenario extends BaseClass {
    String error = "";

    public FR1_Capture_Job_Profile_MainScenario() {

        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());

    }

    public TestResult executeTest() {
        if (!Navigate_To_Job_Profile()) {
            return narrator.testFailed("Navigate To Job Profile Failed due - " + error);
        }
        if (!Capture_Job_Profile()) {
            return narrator.testFailed("Capture Job Profile Failed due - " + error);
        }
        if (!Supporting_Documents()) {
            return narrator.testFailed("Capture Job Profile Suppoting Documents Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully");
    }

    public boolean Navigate_To_Job_Profile() {
        //Navigate to Environmental Health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.navigate_EHS())) {
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.navigate_EHS())) {
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");

        //Operate Maintenance
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.OperateMaintenance())) {
            error = "Failed to wait for 'Operate Maintenance' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.OperateMaintenance())) {
            error = "Failed to click on 'Operate Maintenance' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Operate Maintenance' tab.");
                
        //Navigate To Job Profiles
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.JobProfiles())) {
            error = "Failed to wait for Job Profiles tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.JobProfiles())) {
            error = "Failed to click on Job Profiles tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Job Profiles.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.Add_button())) {
            error = "Failed to wait for Add button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.Add_button())) {
            error = "Failed to click on Add button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Add button");

        return true;
    }

    public boolean Capture_Job_Profile() {
        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.ProcessFlow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.ProcessFlow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //Business unit dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.BusinessUnit_Dropdown())) {
            error = "Failed to wait for Business Unit Dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.BusinessUnit_Dropdown())) {
            error = "Failed to click on Business Unit Dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.BusinessUnit_Option(getData("Business Unit 1")))) {
            error = "Failed to wait for the Business Unit Dropdown Option: '" + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.BusinessUnit_Option(getData("Business Unit 1")))) {
            error = "Failed to click on Business Unit Dropdown Option: '" + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.BusinessUnit_Option(getData("Business Unit 2")))) {
            error = "Failed to click on Business Unit Dropdown Option: '" + getData("Business Unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.BusinessUnit_Option1(getData("Business Unit 3")))) {
            error = "Failed to click on Business Unit Dropdown Option: '" + getData("Business Unit 3");
            return false;
        }        
        narrator.stepPassedWithScreenShot("Successfully selected business unit: " + getData("Business Unit 1") + " -> " + getData("Business Unit 2") + " -> " + getData("Business Unit 3"));

        //Occupation name
        if(!SeleniumDriverInstance.enterTextByXpath(Job_Profile_PageObjects.OccupationName(), getData("Occupation name"))){
            error = "Failed to enter Occupation name: " + getData("Occupation name");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Occupation name: " + getData("Occupation name"));
        
        //Occupation code
        if(!SeleniumDriverInstance.enterTextByXpath(Job_Profile_PageObjects.OccupationCode(), getData("Occupation code"))){
            error = "Failed to enter Occupation code: " + getData("Occupation code");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Occupation code: " + getData("Occupation code"));
        
        //Occupation description
        if(!SeleniumDriverInstance.enterTextByXpath(Job_Profile_PageObjects.OccupationDescription(), getData("Occupation description"))){
            error = "Failed to enter Occupation code: " + getData("Occupation description");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Occupation description: " + getData("Occupation description"));
        
        //Required training
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.RequiredTraining_Dropdown())) {
            error = "Failed to wait for Required training Dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.RequiredTraining_SelectAll())) {
            error = "Failed to wait for the Required training Select All";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected all required training options.");
        
        //Additional training
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.AdditionalTraining_Dropdown())) {
            error = "Failed to wait for Additional training Dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.AdditionalTraining_SelectAll())) {
            error = "Failed to wait for the Additional training Select All";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected all additional training options.");
                
        //Save 
       if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.Save_Button())) {
            error = "Failed to wait for 'Save' button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.Save_Button())) {
            error = "Failed to click the 'Save' button";
            return false;
        }
        narrator.stepPassed("Successfully clicked the 'Save' button");

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Job_Profile_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Job_Profile_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Job_Profile_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        return true;
    }
   
    public boolean Supporting_Documents() {
        //Supporting Documents
        if (testData.getData("Supporting Documents").equalsIgnoreCase("Yes")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.Supporting_Doocuments_tab())) {
                error = "Failed to wait for the 'Supporting Documents' tab";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.Supporting_Doocuments_tab())) {
                error = "Failed to click the 'Supporting Documents' tab";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the Supporting Documents tab");

            //Upload a link 
            if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.linkbox())) {
                error = "Failed to wait for 'Link a document' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.linkbox())) {
                error = "Failed to click on 'Link a document' button.";
                return false;
            }
            pause(2000);
            narrator.stepPassedWithScreenShot("Successfully click 'Link a document' button.");

            if (!SeleniumDriverInstance.switchToTabOrWindow()) {
                error = "Failed to switch tab.";
                return false;
            }

            //Link
            if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.LinkURL())) {
                error = "Failed to wait for 'Link a document' button.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Job_Profile_PageObjects.LinkURL(), getData("Document Link"))) {
                error = "Failed to click on 'Link a document' button.";
                return false;
            }

            //Title
            if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.urlTitle())) {
                error = "Failed to wait for 'Link a document' button.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Job_Profile_PageObjects.urlTitle(), getData("Title"))) {
                error = "Failed to click on 'Link a document' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully Entered Url: '" + getData("Document Link") + "' and 'Title: '" + getData("Title") + "'.");

            //url Add Button
            if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.urlAddButton())) {
                error = "Failed to wait for 'Add' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.urlAddButton())) {
                error = "Failed to click on 'Add' button.";
                return false;
            }
            pause(2000);
            narrator.stepPassedWithScreenShot("Successfully uploaded a Supporting Document.");

            if (!SeleniumDriverInstance.switchToDefaultContent()) {
                error = "Failed to switch tab.";
                return false;
            }

            //iFrame
            if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.iframeXpath())) {
                error = "Failed to switch to frame.";
                return false;
            }
            if (!SeleniumDriverInstance.switchToFrameByXpath(Job_Profile_PageObjects.iframeXpath())) {
                error = "Failed to switch to frame.";
                return false;
            }

            if (getData("Save supporting documents").equalsIgnoreCase("Yes")) {
                //Save to continue button        
                if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.SaveSupportingDocuments_SaveBtn())) {
                    error = "Failed to wait for 'Save supporting documents' button.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.SaveSupportingDocuments_SaveBtn())) {
                    error = "Failed to click on 'Save supporting documents' button.";
                    return false;
                }
            } else {
                //Save button        
                if (!SeleniumDriverInstance.waitForElementByXpath(Job_Profile_PageObjects.Save_Button())) {
                    error = "Failed to wait for 'Save' button.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Job_Profile_PageObjects.Save_Button())) {
                    error = "Failed to click on 'Save' button.";
                    return false;
                }
            }

            //Saving mask
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Job_Profile_PageObjects.saveWait2())) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }

            //Validate if the record has been saved or not.
            if (!SeleniumDriverInstance.waitForElementsByXpath(Job_Profile_PageObjects.validateSave())) {
                error = "Failed to wait for Save validation.";
                return false;
            }

            String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Job_Profile_PageObjects.validateSave());

            if (!SaveFloat.equals("Record saved")) {
                narrator.stepPassedWithScreenShot("Failed to save record.");
                return false;
            }
            narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
        }

        return true;
    }
}
