/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.PPE_Type_Library_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Communication_Manager_V5_2_PageObjects.MailSlurper_PageObjects;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "Sign In To MailSlurper v5.2 - PPE Type Library",
        createNewBrowserInstance = true
)
public class MailSlurperSignIn extends BaseClass{
    String error = "";

    public MailSlurperSignIn(){

    }

    public TestResult executeTest(){
        if (!NavigateToMailSlurperSignInPage()){
            return narrator.testFailed("Failed to navigate to MailSlurper Sign In Page");
        }

        // This step will sign into the specified gmail account with the provided credentials
        if (!SignInToIsomoterics()){
            return narrator.testFailed("Failed to sign into the MailSlurper Home Page");
        }
        //This step will click the newly added record
        if (!clickEmailLink()){
            return narrator.testFailed("Failed to click record link");
        }

        return narrator.finalizeTest("Successfully Navigated through the Isometrix web page");
    }

    public boolean NavigateToMailSlurperSignInPage(){
        if (!SeleniumDriverInstance.navigateTo(getData("URL"))){
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        return true;
    }

    public boolean SignInToIsomoterics(){
        if (!SeleniumDriverInstance.enterTextByXpath(MailSlurper_PageObjects.Username(), testData.getData("Username"))){
            error = "Failed to enter text into email text field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Added the Username: " + testData.getData("Username") + " to the Username Text Field");

        if (!SeleniumDriverInstance.enterTextByXpath(MailSlurper_PageObjects.Password(), testData.getData("Password"))){
            error = "Failed to enter text into email text field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Added the Password: " + testData.getData("Password") + " to the Password Text Field");

        if (!SeleniumDriverInstance.clickElementbyXpath(MailSlurper_PageObjects.LoginBtn())){
            error = "Failed to click sign in button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Clicked The Sign In Button");
        pause(5000);
        
        return true;
    }
    
    public boolean clickEmailLink(){
        //Click the newly added record
        if (!SeleniumDriverInstance.waitForElementByXpath(MailSlurper_PageObjects.recordLink())){
            error = "Failed to wait for the record link.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(MailSlurper_PageObjects.recordLink())){
            error = "Failed to click on the record link.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Clicked The Record Link: "+ SeleniumDriverInstance.retrieveTextByXpath(MailSlurper_PageObjects.recordLink()));

        pause(1000);
        return true;

    }
    

}
