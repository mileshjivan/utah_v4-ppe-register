/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.PPE_Type_Library_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PPE_Type_Library_V5_2_PageObjects.PPE_Type_Library_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR2-Capture PPE Items v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_PPE_Items_MainScenario extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_Capture_PPE_Items_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Capture_To_PPE_Items()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured PPE Type Items record.");
    }

    public boolean Capture_To_PPE_Items() {
        //Navigate to PPE Items
        if (!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.PPE_Items())) {
            error = "Failed to wait for 'PPE Items' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.PPE_Items())) {
            error = "Failed to click on 'PPE Items' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'PPE Items' button.");

        //Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.PPE_Items_AddButton())) {
            error = "Failed to wait for 'PPE Items' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.PPE_Items_AddButton())) {
            error = "Failed to click on 'PPE Items' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'PPE Items' button.");

        //PPE Type
        if (!SeleniumDriverInstance.enterTextByXpath(PPE_Type_Library_PageObject.PPE(), getData("PPE"))) {
            error = "Failed to '" + getData("PPE") + "' into PPE field";
            return false;
        }
        narrator.stepPassedWithScreenShot("PPE : '" + getData("PPE") + "'.");
        
        //URL
        if (!SeleniumDriverInstance.enterTextByXpath(PPE_Type_Library_PageObject.URL(), getData("URL"))) {
            error = "Failed to '" + getData("URL") + "' into URL field";
            return false;
        }
        narrator.stepPassedWithScreenShot("URL : '" + getData("URL") + "'.");
        
        //Save button        
        if (!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.SaveButton())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.SaveButton())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(PPE_Type_Library_PageObject.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(PPE_Type_Library_PageObject.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(PPE_Type_Library_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        return true;
    }
}
