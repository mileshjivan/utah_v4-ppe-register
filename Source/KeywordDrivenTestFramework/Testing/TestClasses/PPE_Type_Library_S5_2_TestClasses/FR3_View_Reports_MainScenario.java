/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.PPE_Type_Library_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PPE_Type_Library_V5_2_PageObjects.PPE_Type_Library_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR3-View Reports PPE v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR3_View_Reports_MainScenario extends BaseClass {
    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR3_View_Reports_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Navigate_To_PPE_Type_Library()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed Viewed Engagements Reports");
    }

    public boolean Navigate_To_PPE_Type_Library() {
        //Navigate to Environmental Health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.navigate_EHS())) {
            error = "Failed to wait for 'Environmental, Health & Safety' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.navigate_EHS())) {
            error = "Failed to click on 'Environmental, Health & Safety' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' button.");

        //Navigate to Operate Maintenance
        if (!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.OperateMaintenance())) {
            error = "Failed to wait for 'Operate Maintenance' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.OperateMaintenance())) {
            error = "Failed to click on 'Operate Maintenance' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Operate Maintenance' button.");

        //Navigate to PPE Type Library
        if (!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.PPE_Type_Library_Module())) {
            error = "Failed to wait for 'PPE Type Library' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.PPE_Type_Library_Module())) {
            error = "Failed to click on 'PPE Type Library' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'PPE Type Library' Module.");

        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.searchBtn())) {
            error = "Failed to wait for 'Serach' button.";
            return false;
        }        
        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.searchBtn())) {
            error = "Failed to click on 'Serach' button.";
            return false;
        }
        SeleniumDriverInstance.pause(5000);
        narrator.stepPassedWithScreenShot("Successfully click 'Serach' button.");

        //Reports button
        if (!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.reportsBtn())) {
            error = "Failed to wait for 'Reports' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.reportsBtn())) {
            error = "Failed to click on 'Reports' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Reports' button.");
        parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        //View reports icon
        if (!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.viewReportsIcon())) {
            error = "Failed to wait for 'View reports' icon.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.viewReportsIcon())) {
            error = "Failed to click on 'View reports' icon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'View reports' icon.");
        SeleniumDriverInstance.switchToTabOrWindow();
        //Continue button
        if (!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.continueBtn())) {
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.continueBtn())) {
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        pause(5000);
        SeleniumDriverInstance.switchToTabOrWindow();
              
        if(!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.PPE_Type_Library())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(PPE_Type_Library_PageObject.PPE_Type_Library())){
                error = "Failed to wait for Engagements header to be present.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Continue' button.");
        narrator.stepPassedWithScreenShot("Viewing Rreports.");

        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.iframeXpath())) {
            error = "Failed to wait for frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(PPE_Type_Library_PageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        //View full reports icon
        if (!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.viewFullReportsIcon())) {
            error = "Failed to wait for 'View full reports' icon.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.viewFullReportsIcon())) {
            error = "Failed to click on 'View full reports' icon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'View full reports' icon.");
        SeleniumDriverInstance.switchToTabOrWindow();
        //Continue button
        if (!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.continueBtn())) {
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(PPE_Type_Library_PageObject.continueBtn())) {
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        pause(5000);
        SeleniumDriverInstance.switchToTabOrWindow();
        narrator.stepPassedWithScreenShot("Successfully click 'Continue' button.");
        
        if(!SeleniumDriverInstance.waitForElementByXpath(PPE_Type_Library_PageObject.BusinessUnitHeader())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(PPE_Type_Library_PageObject.BusinessUnitHeader())){
                error = "Failed to wait for Engagements header to be present.";
                return false;
            }
        }
        
        narrator.stepPassedWithScreenShot("Viewing Full Rreports.");
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        
        pause(2000);
        return true;
    }

}
