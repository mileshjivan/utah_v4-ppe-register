/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Locations_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Locations_V5_PageObjects.Locations_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Locations - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Location_MainScenario extends BaseClass
{

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Location_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Navigate_To_Locations())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Capture_Locations())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Permit To Work");
    }

    public boolean Navigate_To_Locations()
    {
        //Environmental Health and Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Locations_PageObjects.navigate_EHS()))
        {
            error = "Failed to wait for 'Environmental Health and Safety' module.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Locations_PageObjects.navigate_EHS()))
        {
            error = "Failed to click on 'Environmental Health and Safety' module.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental Health and Safety' module.");

        //Evaluate Performance Maintenance
        if (!SeleniumDriverInstance.waitForElementByXpath(Locations_PageObjects.LeadershipParticipationMaintenance()))
        {
            error = "Failed to wait for 'Evaluate Performance Maintenance' module.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Locations_PageObjects.LeadershipParticipationMaintenance()))
        {
            error = "Failed to click on 'Evaluate Performance Maintenance' module.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Evaluate Performance Maintenance' module.");

        //Location
        if (!SeleniumDriverInstance.waitForElementByXpath(Locations_PageObjects.Locations_Module()))
        {
            error = "Failed to wait for 'Location' module";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Locations_PageObjects.Locations_Module()))
        {
            error = "Failed to click on 'Location' module";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Location' module search page.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Locations_PageObjects.Locations_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Locations_PageObjects.Locations_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean Capture_Locations()
    {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Locations_PageObjects.Locations_ProcessFlow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Locations_PageObjects.Locations_ProcessFlow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //Country
        if (!SeleniumDriverInstance.waitForElementByXpath(Locations_PageObjects.Country()))
        {
            error = "Failed to wait for Country field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Locations_PageObjects.Country(), getData("Country")))
        {
            error = "Failed to enter '" + getData("Country") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Country") + "'.");
        
        //Latitude
        if (!SeleniumDriverInstance.waitForElementByXpath(Locations_PageObjects.CountryLatitude()))
        {
            error = "Failed to wait for Latitude field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Locations_PageObjects.CountryLatitude(), getData("Latitude")))
        {
            error = "Failed to enter '" + getData("Latitude") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Latitude") + "'.");
        
        //Longitude
        if (!SeleniumDriverInstance.waitForElementByXpath(Locations_PageObjects.CountryLongitude()))
        {
            error = "Failed to wait for Longitude field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Locations_PageObjects.CountryLongitude(), getData("Longitude")))
        {
            error = "Failed to enter '" + getData("Longitude") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Longitude") + "'.");

        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Locations_PageObjects.CountrySaveButton()))
        {
            error = "Failed to wait for Save Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Locations_PageObjects.CountrySaveButton()))
        {
            error = "Failed to click on Save Button";
            return false;
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Locations_PageObjects.saveWait2(), 400))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Locations_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Locations_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        return true;
    }

}
