/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Locations_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Locations_V5_PageObjects.Locations_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR3-Capture City Or Town - Main Scenario",
        createNewBrowserInstance = false
)

public class FR3_Capture_City_Or_Town_MainScenario extends BaseClass
{

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR3_Capture_City_Or_Town_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Capture_StateOrProvince())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully State / Province");
    }

    public boolean Capture_StateOrProvince()
    {
        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Locations_PageObjects.CityOrTown_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Locations_PageObjects.CityOrTown_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Locations_PageObjects.CityOrTown_ProcessFlow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Locations_PageObjects.CityOrTown_ProcessFlow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //City/Town
        if (!SeleniumDriverInstance.waitForElementByXpath(Locations_PageObjects.CityOrTown()))
        {
            error = "Failed to wait for Country field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Locations_PageObjects.CityOrTown(), getData("City/Town")))
        {
            error = "Failed to enter '" + getData("City/Town") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("City/Town") + "'.");
        
        //Latitude
        if (!SeleniumDriverInstance.waitForElementByXpath(Locations_PageObjects.CityOrTownLatitude()))
        {
            error = "Failed to wait for Latitude field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Locations_PageObjects.CityOrTownLatitude(), getData("Latitude")))
        {
            error = "Failed to enter '" + getData("Latitude") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Latitude") + "'.");
        
        //Longitude
        if (!SeleniumDriverInstance.waitForElementByXpath(Locations_PageObjects.CityOrTownLongitude()))
        {
            error = "Failed to wait for Longitude field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Locations_PageObjects.CityOrTownLongitude(), getData("Longitude")))
        {
            error = "Failed to enter '" + getData("Longitude") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Longitude") + "'.");

        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Locations_PageObjects.CityOrTownSaveButton()))
        {
            error = "Failed to wait for Save Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Locations_PageObjects.CityOrTownSaveButton()))
        {
            error = "Failed to click on Save Button";
            return false;
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Locations_PageObjects.saveWait2(), 400))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Locations_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Locations_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        return true;
    }

}
