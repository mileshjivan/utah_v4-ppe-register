/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Emergency_Response_Team_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Emergency_Response_Team_S5_2_PageObject.Emergency_Response_Team_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Emergency Response Team v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Emergency_Response_Team_MainScenario extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Emergency_Response_Team_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Navigate_To_Emergency_Response_Teams()) {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Capture_Emergency_Response_Teams()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured Emergency Response Teams record.");
    }

    public boolean Navigate_To_Emergency_Response_Teams() {
        //Navigate to Environmental Health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.navigate_EHS())) {
            error = "Failed to wait for 'Environmental, Health & Safety' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.navigate_EHS())) {
            error = "Failed to click on 'Environmental, Health & Safety' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' button.");

        //Navigate to Operate Maintenance
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.OperateMaintenance())) {
            error = "Failed to wait for 'Operate Maintenance' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.OperateMaintenance())) {
            error = "Failed to click on 'Operate Maintenance' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Operate Maintenance' button.");

        //Navigate to Emergency Response Teams
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.Emergency_Response_Teams())) {
            error = "Failed to wait for 'Emergency Response Teams' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.Emergency_Response_Teams())) {
            error = "Failed to click on 'Emergency Response Teams' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Emergency Response Teams' search page.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.Add_Button())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.Add_Button())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean Capture_Emergency_Response_Teams() {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.process_flow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.process_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");

        //Business unit dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.businessUnit_dropdown())) {
            error = "Failed to wait for 'Business unit' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.businessUnit_dropdown())) {
            error = "Failed to click on 'Business unit' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.businessUnit_Option(getData("Business Unit 1")))) {
            error = "Failed to wait for Business Unit: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.businessUnit_Option(getData("Business Unit 1")))) {
            error = "Failed to click the Business Unit: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.businessUnit_Option(getData("Business Unit 2")))) {
            error = "Failed to click the Business Unit: " + getData("Business Unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.businessUnit_Option1(getData("Business Unit 3")))) {
            error = "Failed to click the Business Unit: " + getData("Business Unit 3");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Business Unit: " + getData("Business Unit 1") + " -> " + getData("Business Unit 2") + " -> " + getData("Business Unit 3"));
 
        //Impact type
        if(!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.impactType_Dropdown())){
            error = "Failed to wait for the 'Impact type' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.impactType_Dropdown())){
            error = "Failed to click the 'Impact type' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.impactType_Option(getData("Impact Type")))) {
            error = "Failed to wait for 'Impact type': " + getData("Impact Type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.impactType_Option(getData("Impact Type")))) {
            error = "Failed to click on 'Impact type': " + getData("Impact Type");
            return false;
        }
        narrator.stepPassedWithScreenShot("Impact type: " + getData("Impact Type"));
        
        //Team name
        if (!SeleniumDriverInstance.enterTextByXpath(Emergency_Response_Team_PageObject.TeamName(), getData("Team name"))) {
            error = "Failed to '" + getData("Team name") + "' into Team name field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Team name : '" + getData("Team name") + "'.");
        
        //Save button        
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.SaveButton())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.SaveButton())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Emergency_Response_Team_PageObject.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Emergency_Response_Team_PageObject.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Emergency_Response_Team_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        return true;
    }
}
