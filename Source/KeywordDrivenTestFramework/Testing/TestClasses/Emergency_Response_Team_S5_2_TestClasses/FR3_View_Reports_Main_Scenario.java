/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Emergency_Response_Team_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Emergency_Response_Team_S5_2_PageObject.Emergency_Response_Team_PageObject;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR3-View Reports v5.2 ERT - Main Scenario",
        createNewBrowserInstance = false
)
public class FR3_View_Reports_Main_Scenario extends BaseClass {
    String parentWindow;
    String error = "";

    public FR3_View_Reports_Main_Scenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());

    }

    public TestResult executeTest() {
        if (!navigateToRelatedInitiatives()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully");
    }

    public boolean navigateToRelatedInitiatives() {
        //Navigate to Environmental Health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.navigate_EHS())) {
            error = "Failed to wait for 'Environmental, Health & Safety' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.navigate_EHS())) {
            error = "Failed to click on 'Environmental, Health & Safety' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' button.");

        //Navigate to Operate Maintenance
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.OperateMaintenance())) {
            error = "Failed to wait for 'Operate Maintenance' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.OperateMaintenance())) {
            error = "Failed to click on 'Operate Maintenance' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Operate Maintenance' button.");

        //Navigate to Emergency Response Teams
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.Emergency_Response_Teams())) {
            error = "Failed to wait for 'Emergency Response Teams' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.Emergency_Response_Teams())) {
            error = "Failed to click on 'Emergency Response Teams' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Emergency Response Teams' search page.");
        
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.searchBtn())) {
            error = "Failed to wait for 'Serach' button.";
            return false;
        }        
        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.searchBtn())) {
            error = "Failed to click on 'Serach' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Serach' button.");

        //Reports button
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.reportsBtn())) {
            error = "Failed to wait for 'Reports' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.reportsBtn())) {
            error = "Failed to click on 'Reports' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Reports' button.");
        parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        
        //View reports icon
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.viewReportsIcon())) {
            error = "Failed to wait for 'View reports' icon.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.viewReportsIcon())) {
            error = "Failed to click on 'View reports' icon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'View reports' icon.");
        SeleniumDriverInstance.switchToTabOrWindow();
        
        //Continue button
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.continueBtn())) {
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.continueBtn())) {
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        pause(5000);
        SeleniumDriverInstance.switchToTabOrWindow();
              
        if(!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.ERT_Register())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Emergency_Response_Team_PageObject.ERT_Register())){
                error = "Failed to wait for PPE Register header to be present.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Continue' button.");
        narrator.stepPassedWithScreenShot("Viewing Rreports.");

        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.iframeXpath())) {
            error = "Failed to wait for frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Emergency_Response_Team_PageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        //View full reports icon
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.viewFullReportsIcon())) {
            error = "Failed to wait for 'View full reports' icon.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.viewFullReportsIcon())) {
            error = "Failed to click on 'View full reports' icon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'View full reports' icon.");
        SeleniumDriverInstance.switchToTabOrWindow();
        
        //Continue button
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.continueBtn())) {
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.continueBtn())) {
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        pause(5000);
        SeleniumDriverInstance.switchToTabOrWindow();
        narrator.stepPassedWithScreenShot("Successfully click 'Continue' button.");
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.TeamName_Header())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Emergency_Response_Team_PageObject.TeamName_Header())){
                error = "Failed to wait for Engagements header to be present.";
                return false;
            }
        }        
        narrator.stepPassedWithScreenShot("Viewing Full Rreports.");
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        
        pause(2000);
        return true;
    }
}
