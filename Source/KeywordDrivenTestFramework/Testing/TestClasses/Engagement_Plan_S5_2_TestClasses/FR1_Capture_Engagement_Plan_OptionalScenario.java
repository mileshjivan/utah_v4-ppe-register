/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagement_Plan_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagement_Plan_V5_2_PageObjects.Engagement_Plan_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Engagement Plan v5.2 - Optional Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Engagement_Plan_OptionalScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String engagementName;

    public FR1_Capture_Engagement_Plan_OptionalScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());

        engagementName = testData.getData("Engagement plan title") + "_" + SeleniumDriverInstance.generateDateTimeString();
        Engagement_Plan_PageObjects.setEngagementPlanTitle(engagementName);
    }

    public TestResult executeTest() {
        if(getData("Save supporting documents").equalsIgnoreCase("Yes")){
            if (!CaptureSupportingDocuments()) {
                return narrator.testFailed("Failed due - " + error);
            }
        }
        return narrator.finalizeTest("Successfully captured 'Engangement plan' supporting documents.");
    }
    
    public boolean CaptureSupportingDocuments(){
        //Engagement Plan Supporting Documents
        if(!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.supportDocumentsPanel())){
            error = "Failed to wait for Engagement Plan Supporting Documents panel ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.supportDocumentsPanel())) {
            error = "Failed to click on Engagement Plan Supporting Documents panel ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked - Control Analysis  - Engagement_Plan_PageObjects panel.");

        //Upload a link 
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.link_buttonxpath())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.link_buttonxpath())) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow()) {
            error = "Failed to switch tab.";
            return false;
        }

        //Link
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.LinkURL())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagement_Plan_PageObjects.LinkURL(), getData("Document Link"))) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.urlTitle())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagement_Plan_PageObjects.urlTitle(), getData("Title"))) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Entered Url: '" + getData("Document Link") + "' and 'Title: '" + getData("Title") + "'.");

        //url Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.urlAddButton())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.urlAddButton())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Engagement_Plan_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }

        if(getData("Save supporting documents").equalsIgnoreCase("Yes")){
            //Save to continue button        
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.SaveSupportingDocuments_SaveBtn())) {
                error = "Failed to wait for 'Save supporting documents' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.SaveSupportingDocuments_SaveBtn())) {
                error = "Failed to click on 'Save supporting documents' button.";
                return false;
            }
        }else{
            //Save button        
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.EP_SaveBtn())) {
                error = "Failed to wait for 'Save' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.EP_SaveBtn())) {
                error = "Failed to click on 'Save' button.";
                return false;
            }
        }
        
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Engagement_Plan_PageObjects.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
       
        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Engagement_Plan_PageObjects.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Engagement_Plan_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
        
        return true;
    }
}
