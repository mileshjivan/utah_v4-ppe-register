/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagement_Plan_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagement_Plan_V5_2_PageObjects.Engagement_Plan_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "UC EDP 0103-Update Engagement Status v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class UC_EDP_0103_Update_Engagement_Status_MainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public UC_EDP_0103_Update_Engagement_Status_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!updateEngagementPlanStatus()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully updated 'Endgagement Plan Status'.");
    }
    
    public boolean updateEngagementPlanStatus() {
        //Engagement Plan Details' tab.
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.engagementPlanDetails_Tab())) {
            error = "Failed to wait for 'Engagement Plan Details' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.engagementPlanDetails_Tab())) {
            error = "Failed to click on 'Engagement Plan Details' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Engagement Plan Details' tab.");

        //Engagement plan status dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.engagementPlanStatus())) {
            error = "Failed to wait for 'Engagement plan status' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.engagementPlanStatus())) {
            error = "Failed to click on 'Engagement plan status' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.purposeOfEngagement(getData("Status")))) {
            error = "Failed to wait for '" + getData("Status") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.purposeOfEngagement(getData("Status")))) {
            error = "Failed to click on '" + getData("Status") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Status") + "' option.");

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.EP_SaveBtn())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.EP_SaveBtn())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Engagement_Plan_PageObjects.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Engagement_Plan_PageObjects.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Engagement_Plan_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        if(getData("Status").equalsIgnoreCase("In Progress")){
            //process flow In Progress
            if(!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.InProgress_Phase())){
                error = "Failed to wait for In Progress Phase.";
                return false;
            }
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Engagement_Plan_PageObjects.InProgress_Phase())){
                error = "Failed to wait for In Progress Phase.";
                return false;
            }
            if(!SeleniumDriverInstance.ValidateByText(Engagement_Plan_PageObjects.InProgress_Phase(), getData("Status"))){
                error = "Failed to validate the In Progress Phase. with " + getData("Status");
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully validated the process flow phase: " + getData("Status"));
        }
        else if(getData("Status").equalsIgnoreCase("Completed")){
            //process flow Completed
            if(!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.Completed_Phase())){
                error = "Failed to wait for Completed Phase.";
                return false;
            }
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Engagement_Plan_PageObjects.Completed_Phase())){
                error = "Failed to wait for Completed Phase.";
                return false;
            }
            if(!SeleniumDriverInstance.ValidateByText(Engagement_Plan_PageObjects.Completed_Phase(), getData("Status"))){
                error = "Failed to validate the Completed Phase. with " + getData("Status");
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully validated the process flow phase: " + getData("Status"));
        }
        
        return true;
    }
}
