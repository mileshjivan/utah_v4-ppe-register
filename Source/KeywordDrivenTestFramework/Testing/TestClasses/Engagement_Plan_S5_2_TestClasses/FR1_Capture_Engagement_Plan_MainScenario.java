/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagement_Plan_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagement_Plan_V5_2_PageObjects.Engagement_Plan_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Engagement Plan v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Engagement_Plan_MainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String engagementName;

    public FR1_Capture_Engagement_Plan_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());

        engagementName = testData.getData("Engagement plan title") + "_" + SeleniumDriverInstance.generateDateTimeString();
        Engagement_Plan_PageObjects.setEngagementPlanTitle(engagementName);
    }

    public TestResult executeTest() {

        if (!navigateToEngagementPlan()) {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!captureEngagementPlan()) {
            return narrator.testFailed("Failed due - " + error);
        }  
        
        return narrator.finalizeTest("Successfully captured 'Engangement plan' record.");
    }

    public boolean navigateToEngagementPlan() {
        //Navigate to Environmental Health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.Social_Sustainability())) {
            error = "Failed to wait for 'Social Sustainability' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.Social_Sustainability())) {
            error = "Failed to click on 'Social Sustainability' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Social Sustainability' tab.");
        
        //Navigate to Engagement Plan
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.navigate_EngagementPlan())) {
            error = "Failed to wait for 'Engagement Plan' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.navigate_EngagementPlan())) {
            error = "Failed to click on 'Engagements Plan' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Engagements Plan' search page.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.SE_add())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.SE_add())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean captureEngagementPlan() {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.engagementPlanProcess_flow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.engagementPlanProcess_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }

        //Engagement title field
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.engagementPlanTitle())) {
            error = "Failed to wait for 'Engagement plan title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagement_Plan_PageObjects.engagementPlanTitle(), Engagement_Plan_PageObjects.getEngagementPlanTitle())) {
            error = "Failed to enter '" + Engagement_Plan_PageObjects.getEngagementPlanTitle() + "' into engagement plan title field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + Engagement_Plan_PageObjects.getEngagementPlanTitle() + "' into engagement plan title field.");
        
        //Engagement start date field
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.engagementPlanStartDate())) {
            error = "Failed to wait for 'Engagement start date' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagement_Plan_PageObjects.engagementPlanStartDate(), startDate)) {
            error = "Failed to enter '" + startDate + "' into engagement start date field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + startDate + "' into engagement start date field.");
        
        //Engagement end date field
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.engagementPlanEndDate())) {
            error = "Failed to wait for 'Engagement end date' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagement_Plan_PageObjects.engagementPlanEndDate(), endDate)) {
            error = "Failed to enter '" + endDate + "' into engagement end date field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + endDate + "' into engagement end date field.");

        //Business unit dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.planBusinessUnitTab())) {
            error = "Failed to wait for 'Business unit' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.planBusinessUnitTab())) {
            error = "Failed to click on 'Business unit' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.businessUnit_Option(getData("Business unit 1")))) {
            error = "Failed to wait for Business Unit option: " + getData("Business unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.businessUnit_Option(getData("Business unit 1")))) {
            error = "Failed to click the Business Unit option: " + getData("Business unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.businessUnit_Option(getData("Business unit 2")))) {
            error = "Failed to click the Business Unit option: " + getData("Business unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.businessUnit_Option1(getData("Business unit 3")))) {
            error = "Failed to click the Business Unit option: " + getData("Business unit 3");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Business Unit: " + getData("Business unit 1") + " -> " + getData("Business unit 2") + " -> " + getData("Business unit 3"));
        
        if(getData("Project link checkbox").equalsIgnoreCase("Yes")){
            //Project link checkbox
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.EP_projectLink())) {
                error = "Failed to wait for 'Project link' checkbox.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.EP_projectLink())) {
                error = "Failed to click on 'Project link' checkbox.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully click 'Project link' checkbox.");
            
            //Project dropdown
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.EP_projectTab())) {
                error = "Failed to wait for 'Project' dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.EP_projectTab())) {
                error = "Failed to click on 'Project' dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.projectSelct(getData("Project")))) {
                error = "Failed to wait for '" + getData("Project") + "' option.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.projectSelct(getData("Project")))) {
                error = "Failed to click on '" + getData("Project") + "' option.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully click '" + getData("Project") + "' option.");
        }
       
        //Frequency dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.frequency())) {
            error = "Failed to wait for 'Frequency' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.frequency())) {
            error = "Failed to click on 'Frequency' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.projectSelct(getData("Frequency")))) {
            error = "Failed to wait for '" + getData("Frequency") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.projectSelct(getData("Frequency")))) {
            error = "Failed to click on '" + getData("Frequency") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Frequency") + "' option.");
        
        //Purpose of engagement dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.EP_engagementPurposeTab())) {
            error = "Failed to wait for 'Purpose of engagement' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.EP_engagementPurposeTab())) {
            error = "Failed to click on 'Purpose of engagement' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Engagement purpose' dropdown.");
        
        //Engagement purpose  select
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.purposeOfEngagement(getData("Engagement purpose")))) {
            error = "Failed to wait for '" + getData("Engagement purpose") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.purposeOfEngagement(getData("Engagement purpose")))) {
            error = "Failed to click on '" + getData("Engagement purpose") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Engagement purpose") + "' option.");
        
        //Engagement method dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.methodOfEngagement())) {
            error = "Failed to wait for 'Engagement method' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.methodOfEngagement())) {
            error = "Failed to click on 'Engagement method' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.engagementMethodArrow())) {
            error = "Failed to wait for 'Engagement method' arrow.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.engagementMethodArrow())) {
            error = "Failed to click on 'Engagement method' arrow.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.methodOfEngagementSelect(getData("Engagement method")))) {
            error = "Failed to wait for '" + getData("Engagement method") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.methodOfEngagementSelect(getData("Engagement method")))) {
            error = "Failed to click on '" + getData("Engagement method") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Engagement method") + "' option.");

        //Responsible person dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.personResponsibleTab())) {
            error = "Failed to wait for 'Responsible person' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.personResponsibleTab())) {
            error = "Failed to click on 'Responsible person' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.responsiblePersonSelect(getData("Responsible person")))) {
            error = "Failed to wait for '" + getData("Responsible person") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.responsiblePersonSelect(getData("Responsible person")))) {
            error = "Failed to click on '" + getData("Responsible person") + "' option.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Responsible person") + "' option.");
       
        if(getData("Supporting documents").equalsIgnoreCase("True")){
            if(getData("Save to continue").equalsIgnoreCase("Yes")){
                //Save to continue button        
                if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.SaveToContinue_SaveBtn())) {
                    error = "Failed to wait for 'Save to continue' button.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.SaveToContinue_SaveBtn())) {
                    error = "Failed to click on 'Save to continue' button.";
                    return false;
                }
            }else{
                //Save button        
                if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.EP_SaveBtn())) {
                    error = "Failed to wait for 'Save' button.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.EP_SaveBtn())) {
                    error = "Failed to click on 'Save' button.";
                    return false;
                }
            }

            //Saving mask
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Engagement_Plan_PageObjects.saveWait2())) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }

            //Validate if the record has been saved or not.
            if (!SeleniumDriverInstance.waitForElementsByXpath(Engagement_Plan_PageObjects.validateSave())){
                error = "Failed to wait for Save validation.";
                return false;
            }

            String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Engagement_Plan_PageObjects.validateSave());

            if (!SaveFloat.equals("Record saved")){
                narrator.stepPassedWithScreenShot("Failed to save record.");
                return false;
            }
            narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
        }
        return true;
    }
  
}
