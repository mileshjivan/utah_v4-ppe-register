/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Engagement_Plan_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagement_Plan_V5_2_PageObjects.Engagement_Plan_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "UC EDP 0102 - Capture Participants v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class UC_EDP_0102_Capture_Participants_MainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public UC_EDP_0102_Capture_Participants_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!captureParticipants()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured 'Participants'.");
    }

    public boolean captureParticipants() {
        //Participants tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.participantsTab())) {
            error = "Failed to wait for 'Participants' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.participantsTab())) {
            error = "Failed to click on 'Participants' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Participants' tab.");
        
        //Stakeholders dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.stakeholders_Dropdown())){
            error = "Failed to wait for Stakeholders dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.stakeholders_Dropdown())) {
            error = "Failed to click the Stakeholders dropdown.";
            return false;
        }pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.stakeholders_SelectAll())) {
            error = "Failed to wait for select all Stakeholders options.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.stakeholders_SelectAll())) {
            error = "Failed to click on select all Stakeholders options.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected all the stakeholders options.");
        
        //Entities dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.entities_SelectAll())) {
            error = "Failed to wait for select all Entities options.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.entities_SelectAll())) {
            error = "Failed to click on select all Entities options.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected all the entities options.");
        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagement_Plan_PageObjects.EP_SaveBtn())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagement_Plan_PageObjects.EP_SaveBtn())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Engagement_Plan_PageObjects.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Engagement_Plan_PageObjects.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Engagement_Plan_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        return true;
    }

}
