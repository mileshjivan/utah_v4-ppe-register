/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Document_Taxonomy_V5_2_TestClasses;

import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Testing.PageObjects.Document_Taxonomy_V5_2_PageObjects.Document_Taxonomy_PageObjects;
/**
 *
 * @author smabe
 */

@KeywordAnnotation
        (
        Keyword = "FR2-Capture Document Taxonomy v5.2 - Level 2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_Document_Taxonomy_Level_2_Main_Scenario
{
    String error = "";
    
     public TestResult executeTest()
    {
        if (!Document_Taxonomy ())
        {
            return narrator.testFailed("Navigate To Document Taxonomy Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Captured Document Taxonomy Level 2");
    }
         
     public boolean Document_Taxonomy()
     {
    
        if (!SeleniumDriverInstance.clickElementbyXpath(Document_Taxonomy_PageObjects.Button_Add_1()))
        {
            error = "Failed to click button add.";
            return false;
        }
        SeleniumDriverInstance.pause(3000);
        
        
         if (!SeleniumDriverInstance.clickElementbyXpath(Document_Taxonomy_PageObjects.Process_Flow_1()))
        {
            error = "Failed to click process flow";
            return false;
        }
         
          if (!SeleniumDriverInstance.enterTextByXpath(Document_Taxonomy_PageObjects.Document_Taxonomy_Text_Feild_1(), testData.getData("Document Taxonomy")))
        {
            error = "Failed to wait for Document Taxonomy input field.";
            return false;
        }
          
          
          if (!SeleniumDriverInstance.enterTextByXpath(Document_Taxonomy_PageObjects.Link_Text_Feild_1(), testData.getData("Link")))
        {
            error = "Failed to enter link in the input field.";
            return false;
        }
          
   
    
        narrator.stepPassed("Document Taxonomy :" + testData.getData("Document Taxonomy"));       
        narrator.stepPassed("Link :" + testData.getData("Link"));
        
        
         if(!SeleniumDriverInstance.clickElementbyXpath(Document_Taxonomy_PageObjects.button_save())){
            error = "Failed to click on Save button.";
            return false;
        }
        
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Document_Taxonomy_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
      
        //Check if the record has been Saved
        if(!SeleniumDriverInstance.waitForElementByXpath(Document_Taxonomy_PageObjects.recordSaved_popup())){
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }
        
        String saved = SeleniumDriverInstance.retrieveTextByXpath(Document_Taxonomy_PageObjects.recordSaved_popup());
        
        if(saved.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        }else{   
            if(!SeleniumDriverInstance.waitForElementByXpath(Document_Taxonomy_PageObjects.failed())){
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Document_Taxonomy_PageObjects.failed());

            if(failed.equals("ERROR: Record could not be saved")){
                error = "Failed to save record.";
                return false;
            }
        }
        

        if (!SeleniumDriverInstance.waitForElementByXpath(Document_Taxonomy_PageObjects.Document_Taxonomy_Level_3_label()))
        {
            error = "Failed to wait for Document Taxonomy Level 3";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully navigated Document Management Page");
         
        
         return true;
     }
    
}
