/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Equipment_and_Tool_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Equipment_and_Tool_Category_V5_2_PageObjects.Equipment_and_Tool_Category_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Grievances_V5_2_PageObjects.Engagements_PageObject;import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR4 Capture Equipment and Tools Asset Details Actions - Main Scenario",
        createNewBrowserInstance = false
)
public class FR4_Capture_Equipment_and_Tools_Asset_Details_Actions extends BaseClass
{

    String error = "";

    public FR4_Capture_Equipment_and_Tools_Asset_Details_Actions()
    {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Actions())
        {
            return narrator.testFailed("Actions Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Capture Asset Schedule");
    }

    public boolean Actions()
    {

        //Actions        
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Actions_Tab()))
        {
            error = "Failed to wait for Actions Tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Actions_Tab()))
        {
            error = "Failed to click on Actions Tab";
            return false;
        }

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Actions_Add()))
        {
            error = "Failed to wait for  Actions add button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Actions_Add()))
        {
            error = "Failed to click on  Actions add button";
            return false;
        }
        pause(3000);
        
         //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.process_flow_Actions()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.process_flow_Actions()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");

        //Action description
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Actions_description()))
        {
            error = "Failed to wait for  Action description";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.Actions_description(), getData("Action description")))
        {
            error = "Failed to enter Action description :" + getData("Action description");
            return false;
        }
        //Department responsible
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Department_responsible_drop_down()))
        {
            error = "Failed to wait for  Department responsible drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Department_responsible_drop_down()))
        {
            error = "Failed to wait for  Department responsible drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Global_Company()))
        {
            error = "Failed to wait for  Department responsible drop down option";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Global_Company()))
        {
            error = "Failed to wait for  Department responsible drop down option";
            return false;
        }

        //Responsible person
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Responsible_person_drop_down()))
        {
            error = "Failed to wait for Responsible person drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Responsible_person_drop_down()))
        {
            error = "Failed to wait for Responsible person drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Responsible_person_drop_down_option(getData("Responsible person"))))
        {
            error = "Failed to wait for Responsible person drop down option";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Responsible_person_drop_down_option(getData("Responsible person"))))
        {
            error = "Failed to wait for Responsible person drop down option";
            return false;
        }

        //Action due date
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.Action_due_date(), startDate))
        {
            error = "Failed to enter Action due date :" + startDate;
            return false;
        }

        //Save button        
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Save_Button_Actions()))
        {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Save_Button_Actions()))
        {
            error = "Failed to click on 'Save' button.";
            return false;
        }

        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Engagements_PageObject.saveWait2()))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Equipment_and_Tool_Category_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Equipment_and_Tool_Category_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");    
        
          //close 
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.closeBtn_Actions()))
        {
            error = "Failed to wait for close button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.closeBtn_Actions()))
        {
            error = "Failed to click on close button";
            return false;
        }
        //validate record saved 
       if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Actions_Add()))
        {
            error = "Failed to wait for  Actions add button";
            return false;
        }
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.process_flow_Equipment()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.process_flow_Equipment()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");

        String Schedule_reference = SeleniumDriverInstance.retrieveTextByXpath(Equipment_and_Tool_Category_PageObjects.Action_description_validation());
        if (Schedule_reference.equalsIgnoreCase(getData("Action description")))
        {
            narrator.stepPassedWithScreenShot("Successfully validated action");

        } else
        {
            narrator.stepPassedWithScreenShot("Failed to validated action");
            error = "Failed to validated action";
            return false;

        }

        narrator.stepPassed("Action due date :"+startDate);
        narrator.stepPassed("Responsible person :"+getData("Responsible person"));
        narrator.stepPassed("Action description :"+getData("Action description"));

        return true;
    }

}
