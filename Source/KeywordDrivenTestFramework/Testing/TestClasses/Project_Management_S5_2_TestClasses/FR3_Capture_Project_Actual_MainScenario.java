/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_V5_2_PageObject.Project_Management_PageObject;
import KeywordDrivenTestFramework.Entities.TestResult;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.By;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR3-Capture Project Actual v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR3_Capture_Project_Actual_MainScenario extends BaseClass {
    String error = "";

    public FR3_Capture_Project_Actual_MainScenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Capture_Project_Actuals_MainScenario()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Captured Project Actuals Record");
    }

    public boolean Capture_Project_Actuals_MainScenario() {
        //budget tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Budget_tab())){
            error = "Failed to wait for the Budget tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Budget_tab())){
            error = "Failed to click the Budget tab.";
            return false;
        }
        
        //Project Actuals
        String Project_Actuals = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObject.ProjectActuals_Panel());
        if(!SeleniumDriverInstance.ValidateByText(Project_Management_PageObject.ProjectActuals_Panel(), "Project Actuals")){
            error = "Failed to validate the Project Actuals tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully validated the Forecasted Budget Expected: 'Project Actuals' and Actual '" + Project_Actuals + "'.");
               
        //Project Actuals
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProjectActuals_Panel())){
            error = "Failed to wait for the Project Actuals tab.";
            return false;
        }
        if(!SeleniumDriverInstance.scrollToElement(Project_Management_PageObject.ProjectActuals_Panel())){
            error = "Failed to scroll to the Project Actuals tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProjectActuals_Panel())){
            error = "Failed to click the Project Actuals tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully expanded the Project Actuals panel.");
        
        String[] Expenditure = getData("Expenditure").split(",");
        String[] Year = getData("Year").split("-");
        String[] Quarter = getData("Quarter").split(",");
        int j = 1;
        int y = 1;
            
        for(int i = 0; i < Expenditure.length; i++){
            //Add button
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProjectActuals_Add_Button())) {
                error = "Failed to wait for 'Add' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProjectActuals_Add_Button())) {
                error = "Failed to click on 'Add' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

            //Expenditure      
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Expenditure(j))) {
                error = "Failed to wait for Expenditure option: " + Expenditure[i];
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.Expenditure(j), Expenditure[i])) {
                error = "Failed to click on Expenditure option: " + Expenditure[i];
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully selected Expenditure option: " + Expenditure[i]);        

            //Year
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.PA_Year_Dropdown(y))) {
                error = "Failed to wait for Year dropdown";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.PA_Year_Dropdown(y))) {
                error = "Failed to click on Year dropdown";
                return false;
            }        
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProjectStatus_Option(Year[i]))) {
                error = "Failed to wait for Year option: " + Year[i];
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProjectStatus_Option(Year[i]))) {
                error = "Failed to click on Year option: " + Year[i];
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully selected Year option: " + Year[i]);          

            //Quarter
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.PA_Quarter_Dropdown(y))) {
                error = "Failed to wait for Quarter dropdown";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.PA_Quarter_Dropdown(y))) {
                error = "Failed to click on Quarter dropdown";
                return false;
            }        
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProjectStatus_Option(Quarter[i]))) {
                error = "Failed to wait for Quarter option: " + Quarter[i];
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProjectStatus_Option(Quarter[i]))) {
                error = "Failed to click on Quarter option: " + Quarter[i];
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully selected Quarter option: " + Quarter[i]);    
            
            //Save Button
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.saveBtn())) {
                error = "Failed to wait for 'Save' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.saveBtn())) {
                error = "Failed to click on 'Save' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the save button.");

            //Saving mask
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }

            //Save mask
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }

            //Validate if the record has been saved or not.
            if (!SeleniumDriverInstance.waitForElementsByXpath(Project_Management_PageObject.validateSave())) {
                error = "Failed to wait for Save validation.";
                return false;
            }
            String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObject.validateSave());

            if (!SaveFloat.equals("Record saved")) {
                narrator.stepPassedWithScreenShot("Failed to save record.");
                return false;
            }
            narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

            j+=2;
            y++;
        }
        
         //budget tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Budget_tab())){
            error = "Failed to wait for the Budget tab.";
            return false;
        }
        if(!SeleniumDriverInstance.scrollToElement(Project_Management_PageObject.Budget_tab())){
            error = "Failed to scroll to the Budget tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Budget_tab())){
            error = "Failed to click the Budget tab.";
            return false;
        }
        
        return true;
    }
   
}
