/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_V5_2_PageObject.Project_Management_PageObject;
import KeywordDrivenTestFramework.Entities.TestResult;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.apache.derby.client.am.Decimal;
import org.openqa.selenium.By;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR2-Capture Forecasted Budget v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_Forecasted_Budget_MainScenario extends BaseClass {
    String error = "";

    public FR2_Capture_Forecasted_Budget_MainScenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Capture_Forecasted_Budget_MainScenario()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Captured Forecasted Budget Record");
    }

    public boolean Capture_Forecasted_Budget_MainScenario() {
        //budget tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Budget_tab())){
            error = "Failed to wait for the Budget tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Budget_tab())){
            error = "Failed to click the Budget tab.";
            return false;
        }
        
        //Validate the panels
        //Forecasted Budget
        String Forecasted_Budget = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObject.ForecastedBudget_Panel());        
        if(!SeleniumDriverInstance.ValidateByText(Project_Management_PageObject.ForecastedBudget_Panel(), "Forecasted Budget")){
            error = "Failed to validate the Forecasted Budget tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully validated the Forecasted Budget Expected: 'Forecasted Budget' and Actual '" + Forecasted_Budget + "'.");
        
        //Project Actuals
        String Project_Actuals = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObject.ProjectActuals_Panel());
        if(!SeleniumDriverInstance.ValidateByText(Project_Management_PageObject.ProjectActuals_Panel(), "Project Actuals")){
            error = "Failed to validate the Project Actuals tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully validated the Forecasted Budget Expected: 'Project Actuals' and Actual '" + Project_Actuals + "'.");
                
        //Total Budget
        String Total_Budget = getData("Total Budget");
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.TotalBudget())){
            error = "Failed to wait for the Total Budget.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.TotalBudget(), Total_Budget)){
            error = "Failed to click the Total Budget.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Budget_tab())){
            error = "Failed to click the Budget tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Total Budget: " + Total_Budget);
        
//        //Validation Total budget
//        //Total budget with extension
//        String TotalBudgetWithExtension = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObject.TotalBudgetWithExtension());
//        if(!SeleniumDriverInstance.ValidateByText(Project_Management_PageObject.TotalBudgetWithExtension(), Total_Budget)){
//            error = "Failed to validate the Total budget with extension.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully validated the Total budget with extension Expected: '" + Total_Budget + "'  and Actual: '" + TotalBudgetWithExtension + "'.");
//        
//        //Remaining budget
//        String RemainingBudget = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObject.RemainingBudget());
//        if(!SeleniumDriverInstance.ValidateByText(Project_Management_PageObject.RemainingBudget(), Total_Budget)){
//            error = "Failed to validate the Remaining budget.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully validated the Remaining budget Expected: '" + Total_Budget + "'  and Actual: '" + RemainingBudget + "'.");
//        
        //Budget extension check box
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.BudgetExtension_Checkbox())){
            error = "Failed to click the Budget extension check box.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Budget_tab())){
            error = "Failed to click the Budget tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the Budget extension check box.");
        
        //Total extension
        if(!SeleniumDriverInstance.ValidateByText(Project_Management_PageObject.TotalExtension_Label(), "Total extension")){
            error = "Failed to validate the Total extension field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Succefully displated the Total extension field.");
        
        SeleniumDriverInstance.Driver.findElement(By.xpath(Project_Management_PageObject.TotalExtension())).clear();
        if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.TotalExtension(), getData("Total extension"))){
            error = "Failed to enter the Total extension field: " + getData("Total extension");
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Budget_tab())){
            error = "Failed to click the Budget tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Succefully displated the Total extension field.");
                
//        //Validating the new Total Budget
//        double newTotalBudget = Double.valueOf(SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObject.TotalBudget()));
//        double newTotalExtension = Double.valueOf(getData("Total extension"));
//        double newtotalBugetWithExtension;
//        
//        newtotalBugetWithExtension = newTotalBudget + newTotalExtension;
//        
//        //Validation Total budget
//        //Total budget with extension
//        if(!SeleniumDriverInstance.ValidateByText(Project_Management_PageObject.TotalBudgetWithExtension(), String.valueOf(newtotalBugetWithExtension))){
//            error = "Failed to validate the Total budget with extension.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully validated the Total budget with extension Expected: '" + String.valueOf(newtotalBugetWithExtension) + "'  and Actual: '" + TotalBudgetWithExtension + "'.");
//        
//        //Remaining budget
//        if(!SeleniumDriverInstance.ValidateByText(Project_Management_PageObject.RemainingBudget(), String.valueOf(newtotalBugetWithExtension))){
//            error = "Failed to validate the Remaining budget.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully validated the Remaining budget Expected: '" + String.valueOf(newtotalBugetWithExtension) + "'  and Actual: '" + RemainingBudget + "'.");
//                
        //Forecasted Budget
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ForecastedBudget_Panel())){
            error = "Failed to wait for the Forecasted Budget tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ForecastedBudget_Panel())){
            error = "Failed to click the Forecasted Budget tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully expanded the Forecasted Budget panel.");
        
        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ForecastedBudget_Add_Button())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ForecastedBudget_Add_Button())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
             
        //Theme
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Theme_Dropdown())) {
            error = "Failed to wait for Theme dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Theme_Dropdown())) {
            error = "Failed to click on Theme dropdown";
            return false;
        }        
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Theme_Option(getData("Theme")))) {
            error = "Failed to wait for Theme option: " + getData("Theme");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Theme_Option(getData("Theme")))) {
            error = "Failed to click on Theme option: " + getData("Theme");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected Theme option: " +  getData("Theme"));        
        
        //Year
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Year_Dropdown())) {
            error = "Failed to wait for Year dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Year_Dropdown())) {
            error = "Failed to click on Year dropdown";
            return false;
        }        
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProjectStatus_Option(getData("Year")))) {
            error = "Failed to wait for Year option: " + getData("Year");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProjectStatus_Option(getData("Year")))) {
            error = "Failed to click on Year option: " + getData("Year");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected Year option: " +  getData("Year"));          
                
        //Quarter
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Quarter_Dropdown())) {
            error = "Failed to wait for Quarter dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Quarter_Dropdown())) {
            error = "Failed to click on Quarter dropdown";
            return false;
        }        
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProjectStatus_Option(getData("Quarter")))) {
            error = "Failed to wait for Quarter option: " + getData("Quarter");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProjectStatus_Option(getData("Quarter")))) {
            error = "Failed to click on Quarter option: " + getData("Quarter");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected Quarter option: " +  getData("Year"));     
                
        //% of the total be
        if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.PercentageOfTotalBudget(), getData("% of total budget"))){
            error = "Failed to enter the : "+ getData("% of total budget");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered % of total budget: " + getData("% of total budget"));
        
        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.saveBtn())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.saveBtn())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the save button.");

        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Project_Management_PageObject.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");
        
//        //Total % of budget
//        DecimalFormat df = new DecimalFormat("#.00");
//        String newTotal_Of_Budget = df.format(getData("% of total budget"));                
//        String revTotal_Of_Budget = Project_Management_PageObject.Total_Of_Budget();
//        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Total_Of_Budget())){
//            error = "Faild to wait for the 'Total % of budget': " + getData("% of total budget");
//            return false;
//        }
//        if(!SeleniumDriverInstance.ValidateByText(Project_Management_PageObject.Total_Of_Budget(), newTotal_Of_Budget)){
//            error = "Faild to wait for the 'Total % of budget': " + getData("% of total budget");
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Total % of budget Expected : '" + revTotal_Of_Budget + "'  and Actual: '" + newTotal_Of_Budget + "'.");
        
        return true;
    }
   
}
