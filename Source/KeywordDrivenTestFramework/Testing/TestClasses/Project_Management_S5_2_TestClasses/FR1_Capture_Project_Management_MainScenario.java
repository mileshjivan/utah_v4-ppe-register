/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_V5_2_PageObject.Project_Management_PageObject;
import KeywordDrivenTestFramework.Entities.TestResult;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Project Management v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Project_Management_MainScenario extends BaseClass {
    String error = "";

    public FR1_Capture_Project_Management_MainScenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Navigate_To_Project_Management()) {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Capture_Project_Management()) {
            return narrator.testFailed("Failed due - " + error);
        }
        if (testData.getData("Supporting Documents").equalsIgnoreCase("Yes")) {
            if (!Supporting_Documents()) {
                return narrator.testFailed("Failed due - " + error);
            }
        } 
        return narrator.finalizeTest("Successfully Captured Project Management Record");
    }

    public boolean Navigate_To_Project_Management() {
        //Environmental, Health & Safety
       if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.navigate_EHS())) {
            error = "Failed to wait for 'Environmental, Health & Safety' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.navigate_EHS())) {
            error = "Failed to click on 'Environmental, Health & Safety' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' button.");

        //Projects
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Projects())) {
            error = "Failed to wait for Projects tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Projects())) {
            error = "Failed to click Projects tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Projects tab");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Add_Button())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Add_Button())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        
        return true;
    }

    public boolean Capture_Project_Management() {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProcessFlow_Button())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProcessFlow_Button())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //Business unit dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.businessUnit_dropdown())) {
            error = "Failed to wait for 'Business unit' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.businessUnit_dropdown())) {
            error = "Failed to click on 'Business unit' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.businessUnit_Option(getData("Business Unit 1")))) {
            error = "Failed to wait for Business Unit: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.businessUnit_Option(getData("Business Unit 1")))) {
            error = "Failed to click the Business Unit: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.businessUnit_Option(getData("Business Unit 2")))) {
            error = "Failed to click the Business Unit: " + getData("Business Unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.businessUnit_Option1(getData("Business Unit 3")))) {
            error = "Failed to click the Business Unit: " + getData("Business Unit 3");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Business Unit: " + getData("Business Unit 1") + " -> " + getData("Business Unit 2") + " -> " + getData("Business Unit 3"));
 
        //Impact type
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.impactType_Dropdown())){
            error = "Failed to wait for the 'Impact type' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.impactType_Dropdown())){
            error = "Failed to click the 'Impact type' dropdown.";
            return false;
        }
        
        if(getData("Select All Impact Type").equalsIgnoreCase("Yes")){
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.impactType_SelectAll())) {
                error = "Failed to wait for 'Impact type' Select All";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.impactType_SelectAll())) {
                error = "Failed to click on 'Impact type' Select All";
                return false;
            }
            narrator.stepPassedWithScreenShot("Impact type Select All");
        } else {
            String[] Impact_Type = getData("Impact Type").split(",");
        
            for(int i = 0; i < Impact_Type.length; i++){
                if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.impactType_Option(Impact_Type[i]))) {
                    error = "Failed to wait for 'Impact type': " + Impact_Type[i];
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.impactType_Option(Impact_Type[i]))) {
                    error = "Failed to click on 'Impact type': " + Impact_Type[i];
                    return false;
                }
                narrator.stepPassedWithScreenShot("Impact type: " + Impact_Type[i]);
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.impactType_Dropdown())){
                error = "Failed to click the 'Impact type' dropdown.";
                return false;
            }
        }
        
        //Project
        if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.Project_Input(), getData("Project"))){
            error = "Failed to enter the Project field: " + getData("Project");
            return false;
        }
        narrator.stepPassedWithScreenShot("Project: " + getData("Project"));
        
        //Project description
        if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.Project_Description(), getData("Project description"))){
            error = "Failed to enter the Project description field: " + getData("Project description");
            return false;
        }
        narrator.stepPassedWithScreenShot("Project description: " + getData("Project description"));
        
        //Project status
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProjectStatus_Dropdown())) {
            error = "Failed to wait for Project status dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProjectStatus_Dropdown())) {
            error = "Failed to click on Project status dropdown";
            return false;
        }        
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProjectStatus_Option(getData("Project status")))) {
            error = "Failed to wait for Project status option: " + getData("Project status");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProjectStatus_Option(getData("Project status")))) {
            error = "Failed to click on Project status option: " + getData("Project status");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected Project status option: " +  getData("Project status"));

        //Theme
        if(getData("Select All Theme").equalsIgnoreCase("Yes")){
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.themeType_SelectAll())) {
                error = "Failed to wait for 'Theme' Select All";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.themeType_SelectAll())) {
                error = "Failed to click on 'Theme' Select All";
                return false;
            }
            narrator.stepPassedWithScreenShot("Theme Select All");
        } else {
            String[] Theme = getData("Theme").split(",");
        
            for(int i = 0; i < Theme.length; i++){
                if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.impactType_Option(Theme[i]))) {
                    error = "Failed to wait for 'Theme': " + Theme[i];
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.impactType_Option(Theme[i]))) {
                    error = "Failed to click on 'Theme': " + Theme[i];
                    return false;
                }
                narrator.stepPassedWithScreenShot("Theme: " + Theme[i]);
            }
        }
        
        //Sub Theme
        if(getData("Select All Sub Theme").equalsIgnoreCase("Yes")){
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.SubThemeType_SelectAll())) {
                error = "Failed to wait for 'Sub Theme' Select All";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.SubThemeType_SelectAll())) {
                error = "Failed to click on 'Sub Theme' Select All";
                return false;
            }
            narrator.stepPassedWithScreenShot("Sub Theme Select All");
        } else {
            String[] Sub_Theme = getData("Sub Theme").split(",");
        
            for(int i = 0; i < Sub_Theme.length; i++){
                if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.impactType_Option(Sub_Theme[i]))) {
                    error = "Failed to wait for 'Sub Theme': " + Sub_Theme[i];
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.impactType_Option(Sub_Theme[i]))) {
                    error = "Failed to click on 'Sub Theme': " + Sub_Theme[i];
                    return false;
                }
                narrator.stepPassedWithScreenShot("Sub Theme: " + Sub_Theme[i]);
            }
        }
        
        //Planned start date
        if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.PlannedStartDate(), startDate)){
            error = "Failed to enter the planned start date";
            return false;
        }
        narrator.stepPassedWithScreenShot("Planned start date: " + startDate);
        
        //Planned completion date
        if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.PlannedCompletionDate(), endDate)){
            error = "Failed to enter the planned completion date";
            return false;
        }
        narrator.stepPassedWithScreenShot("Planned completion date: " + endDate);
        
        //Objectives and proposed activities
        if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.ObjectivesAndProposedActivities(), getData("Objectives and proposed activities"))){
            error = "Failed to enter the Objectives and proposed activities";
            return false;
        }
        narrator.stepPassedWithScreenShot("Objectives and proposed activities: " + getData("Objectives and proposed activities"));
        
        //Due diligence start date
        if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.DueDiligenceStartDate(), startDate)){
            error = "Failed to enter the Due diligence start date";
            return false;
        }
        narrator.stepPassedWithScreenShot("Due diligence start date: " + startDate);
        
        //Planned completion date
        if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.DueDiligenceEndDate(), endDate)){
            error = "Failed to enter the Due diligence end date";
            return false;
        }
        narrator.stepPassedWithScreenShot("Due diligence end date: " + endDate);
        
        //Total number of beneficiaries
        if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.TotalNumberOfBeneficiaries(), getData("Total number of beneficiaries"))){
            error = "Failed to enter the Total number of beneficiaries.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Total number of beneficiaries: " + endDate);
        
        //Due diligence notes
        if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.DueDiligenceNotes(), getData("Due diligence notes"))){
            error = "Failed to enter the Due diligence notes";
            return false;
        }
        narrator.stepPassedWithScreenShot("Due diligence end date: " + getData("Due diligence notes"));
        
        //Key words
        if(getData("Select All Key words").equalsIgnoreCase("Yes")){
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.KeyWords_SelectAll())) {
                error = "Failed to wait for 'Key words' Select All";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.KeyWords_SelectAll())) {
                error = "Failed to click on 'Key words' Select All";
                return false;
            }
            narrator.stepPassedWithScreenShot("Key words Select All");
        } else {
            String[] Key_words = getData("Key words").split(",");
        
            for(int i = 0; i < Key_words.length; i++){
                if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.impactType_Option(Key_words[i]))) {
                    error = "Failed to wait for 'Key words': " + Key_words[i];
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.impactType_Option(Key_words[i]))) {
                    error = "Failed to click on 'Key words': " + Key_words[i];
                    return false;
                }
                narrator.stepPassedWithScreenShot("Key words: " + Key_words[i]);
            }
        }
        
        //Project originator
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProjectOriginator_dropdown())) {
            error = "Failed to wait for 'Project originator' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProjectOriginator_dropdown())) {
            error = "Failed to click on 'Project originator' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.businessUnit_Option1(getData("Project originator")))) {
            error = "Failed to wait for Project originator: " + getData("Project originator");
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Project_Management_PageObject.businessUnit_Option1(getData("Project originator")))) {
            error = "Failed to scroll to the Project originator: " + getData("Project originator");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.businessUnit_Option1(getData("Project originator")))) {
            error = "Failed to click the Project originator: " + getData("Project originator");
            return false;
        }
        narrator.stepPassedWithScreenShot("Project originator: " + getData("Project originator"));
        
        //Specify originator
        if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.SpecifyOriginator(), getData("Specify originator"))){
            error = "Failed to enter the Specify originator";
            return false;
        }
        narrator.stepPassedWithScreenShot("Specify originator: " + getData("Specify originator"));
        
        if(getData("Save to continue").equalsIgnoreCase("Yes")){
            //Save Button
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.SaveToContinue_saveBtn())) {
                error = "Failed to wait for 'Save to continue' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.SaveToContinue_saveBtn())) {
                error = "Failed to click on 'Save to continue' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the Save to continue button.");
        } else {
            //Save Button
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.saveBtn())) {
                error = "Failed to wait for 'Save' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.saveBtn())) {
                error = "Failed to click on 'Save' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the save button.");
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Project_Management_PageObject.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");
        
        //budget tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Budget_tab())){
            error = "Failed to wait for the Budget tab.";
            return false;
        }
        if(!SeleniumDriverInstance.scrollToElement(Project_Management_PageObject.Budget_tab())){
            error = "Failed to scroll to the Budget tab.";
            return false;
        }
        if(!SeleniumDriverInstance.ValidateByText(Project_Management_PageObject.Budget_tab(), "Budget")){
            error = "Failed to validate the Budget tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully validated the Budget tab.");
             
        //Strategic Objectives
        if(!SeleniumDriverInstance.ValidateByText(Project_Management_PageObject.StrategicObjectives_tab(), "Strategic Objectives")){
            error = "Failed to validate the Strategic Objectives tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully validated the Strategic Objectives tab.");
        
        //Linked Engagements
        if(!SeleniumDriverInstance.ValidateByText(Project_Management_PageObject.LinkedEngagements_tab(), "Linked Engagements")){
            error = "Failed to validate the Linked Engagements tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully validated the Linked Engagements tab.");
        
        //Project Actions
        if(!SeleniumDriverInstance.ValidateByText(Project_Management_PageObject.ProjectActions_tab(), "Project Actions")){
            error = "Failed to validate the Budget tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully validated the Project Actions tab.");
        
        return true;
    }
   
    public boolean Supporting_Documents() {
        //budget tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Budget_tab())){
            error = "Failed to wait for the Budget tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Budget_tab())){
            error = "Failed to click the Budget tab.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.TotalBudget())){
            error = "Failed to wait for the Total Budget.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.TotalBudget(), getData("Total Budget"))){
            error = "Failed to click the Total Budget.";
            return false;
        }
        
        //Supporting Documents
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Supporting_Doocuments_tab())) {
            error = "Failed to wait for the 'Supporting Documents' tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Supporting_Doocuments_tab())) {
            error = "Failed to click the 'Supporting Documents' tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the Supporting Documents tab");

        //Upload a link 
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.link_buttonxpath())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.link_buttonxpath())) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow()) {
            error = "Failed to switch tab.";
            return false;
        }

        //Link
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.LinkURL())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.LinkURL(), getData("Document Link"))) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.urlTitle())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.urlTitle(), getData("Title"))) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Entered Url: '" + getData("Document Link") + "' and 'Title: '" + getData("Title") + "'.");

        //url Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.urlAddButton())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.urlAddButton())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Project_Management_PageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }

        if (getData("Save supporting documents").equalsIgnoreCase("Yes")) {
            //Save to continue button        
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.SaveSupportingDocuments_Button())) {
                error = "Failed to wait for 'Save supporting documents' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.SaveSupportingDocuments_Button())) {
                error = "Failed to click on 'Save supporting documents' button.";
                return false;
            }
        } else {
            //Save button        
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.saveBtn())) {
                error = "Failed to wait for 'Save' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.saveBtn())) {
                error = "Failed to click on 'Save' button.";
                return false;
            }
        }

        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Project_Management_PageObject.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        return true;
    }
    

}
