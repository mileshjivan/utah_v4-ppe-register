/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_V5_2_PageObject.Project_Management_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR5-Capture Project Actions v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR5_Capture_Project_Actions_MainScenario extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR5_Capture_Project_Actions_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!Navigate_To_Actions()) {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Capture_Project_Actions()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Captured Project Actions");
    }

    public boolean Navigate_To_Actions() {
        //Navigate to Project Actions
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProjectActions_tab())) {
            error = "Failed to wait for 'Project Actions' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProjectActions_tab())) {
            error = "Failed to click on 'Project Actions' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Project Actions' tab.");

        //Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProjectActions_AddButton())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProjectActions_AddButton())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean Capture_Project_Actions() {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Actions_pf())) {
            error = "Failed to wait for 'Process Flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Actions_pf())) {
            error = "Failed to click on 'Process Flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process Flow' button.");

        //Description
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Actions_desc())) {
            error = "Failed to wait for 'Description' textarea.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.Actions_desc(), getData("Description"))) {
            error = "Failed to enter '" + getData("Description") + "' into Description textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Description : '" + getData("Description") + "'");

        //Department Responsible
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Actions_deptresp_dropdown())) {
            error = "Failed to wait for Department Responsible dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Actions_deptresp_dropdown())) {
            error = "Failed to click Department Responsible dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Actions_deptresp_select1(getData("Department Responsible 1")))) {
            error = "Failed to wait for '" + getData("Department Responsible 1") + "' in Department Responsible dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Actions_deptresp_select1(getData("Department Responsible 1")))) {
            error = "Failed to click '" + getData("Department Responsible 1") + "' from Department Responsible dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Actions_deptresp_select1(getData("Department Responsible 2")))) {
            error = "Failed to click '" + getData("Department Responsible 2") + "' from Department Responsible dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Actions_deptresp_select(getData("Department Responsible 3")))) {
            error = "Failed to click '" + getData("Department Responsible 3") + "' from Department Responsible dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Department Responsible : " + getData("Department Responsible 1") + " -> " + getData("Department Responsible 2") + " -> " + getData("Department Responsible 3") );

        //Responsible Person
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Actions_respper_dropdown())) {
            error = "Failed to wait for Responsible Person dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Actions_respper_dropdown())) {
            error = "Failed to click Responsible Person dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Actions_respper_select(getData("Responsible Person")))) {
            error = "Failed to wait for '" + getData("Responsible Person") + "' in Responsible Person dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Actions_respper_select(getData("Responsible Person")))) {
            error = "Failed to click '" + getData("Responsible Person") + "' from Responsible Person dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Responsible Person : '" + getData("Responsible Person") + "'.");

        //Due Date 
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Actions_ddate())) {
            error = "Failed to wait for Due Date textarea.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.Actions_ddate(), endDate)) {
            error = "Failed to enter '" + endDate + "' into due date textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Due Date : '" + endDate + "'.");

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Actions_save())) {
            error = "Failed to wait for Save button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Actions_save())) {
            error = "Failed to click Save button.";
            return false;
        }
       //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Project_Management_PageObject.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");
        
        //Close Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProjectActions_CloseButton())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.ProjectActions_CloseButton())){
                error = "Failed to wait for the Project Actions close button.";
                return false;
            }
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProjectActions_CloseButton())){
            error = "Failed to click the Project Actions close button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Project Actions.");
        pause(2000);
        //Updated record
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProjectActions_Record(getData("Description")))){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.ProjectActions_Record(getData("Description")))){
                error = "Failed to wait for the Project Actions Record.";
                return false;
            }
        }

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProcessFlow_Button())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProcessFlow_Button())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");        
        
        return true;
    }
}
