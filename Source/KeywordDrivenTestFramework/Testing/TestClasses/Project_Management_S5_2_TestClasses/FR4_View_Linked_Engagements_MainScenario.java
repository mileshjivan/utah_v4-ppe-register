/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_V5_2_PageObject.Project_Management_PageObject;
import KeywordDrivenTestFramework.Entities.TestResult;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR4-View Linked Engagements v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR4_View_Linked_Engagements_MainScenario extends BaseClass {
    String error = "";

    public FR4_View_Linked_Engagements_MainScenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Capture_Project_Actuals_MainScenario()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Captured Project Actuals Record");
    }

    public boolean Capture_Project_Actuals_MainScenario() {
        //Linked Engagements tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.LinkedEngagements_tab())){
            error = "Failed to wait for the Linked Engagements tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.LinkedEngagements_tab())){
            error = "Failed to click the Linked Engagements tab.";
            return false;
        }
        
        
        
        return true;
    }
   
}
