/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Inspection_Scheduler_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Planned_Task_Observations_V5_2_PageObjetcs.Inspection_Scheduler_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR3 View Inspections - Main Scenario",
        createNewBrowserInstance = false
)

public class FR3_View_Inspections_Main_Scenario extends BaseClass
{

    String error = "";

    public FR3_View_Inspections_Main_Scenario()
    {

        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Navigate_To_Inspection_Scheduler_Recurrence())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Viewed Inspections");
    }

    public boolean Navigate_To_Inspection_Scheduler_Recurrence()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.Inspection_Scheduler_Recurrence_Heading()))
        {
            error = "Failed to wait for Inspection Scheduler Recurrence tab.";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Inspection_Scheduler_PageObjects.Inspection_Scheduler_Recurrence_Heading()))
        {
            error = "Failed to scroll to Inspection Scheduler Recurrence tab.";
            return false;
        }
          if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.Inspection_Scheduler_Recurrence_Label()))
        {
            error = "Failed to click on Inspection Scheduler Recurrence tab.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to Inspection Scheduler Recurrence tab");

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.Inspection_Scheduler_Recurrence_Add_Button()))
        {
            error = "Failed to wait for add button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.Inspection_Scheduler_Recurrence_Add_Button()))
        {
            error = "Failed to click on add button";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully clicked aadd button");

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.Recurrence_ProcessFlow()))
        {
            error = "Failed to wait for process flow button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.Recurrence_ProcessFlow()))
        {
            error = "Failed to click on process flow button";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully clicked on process flow button");

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.SartDate()))
        {
            error = "Failed to wait for Sart Date";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Inspection_Scheduler_PageObjects.SartDate(), startDate))
        {
            error = "Failed enter start date :" + startDate;
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully enter sart date :" + startDate);

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Dropdown()))
        {
            error = "Failed to wait for Recurrence frequency drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Dropdown()))
        {
            error = "Failed to click Recurrence Frequency Dropdown";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully clicked Recurrence Frequency Dropdown ");

        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Dropdown_Option(getData("Recurrence Frequency"))))
        {
            error = "Failed to wait for Recurrence frequency drop down option";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_Dropdown_Option(getData("Recurrence Frequency"))))
        {
            error = "Failed to click Recurrence Frequency Dropdown option";
            return false;
        }

        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_SaveButton()))
        {
            error = "Failed to wait for Save Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Inspection_Scheduler_PageObjects.RecurrenceFrequency_SaveButton()))
        {
            error = "Failed to click on Save Button";
            return false;
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Inspection_Scheduler_PageObjects.saveWait2(), 400))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Inspection_Scheduler_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Inspection_Scheduler_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        return true;
    }
}
