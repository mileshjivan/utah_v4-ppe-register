/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Planned_Task_Observations_V5_2_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Bowtie_Risk_Assessment_S5_2_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.Bowtie_Risk_Assessment_PageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Permit_To_Work_V5_2_PageObjects.Permit_To_Work_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Planned_Task_Observations_V5_2_PageObjetcs.Planned_Task_Observations_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Planned Task Observations - Optional Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Planned_Task_Observations_OptionalScenario extends BaseClass
{

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Planned_Task_Observations_OptionalScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Navigate_TO_Planned_Task_Observations())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Capture_Planned_Task_Observations())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Permit To Work");
    }

    public boolean Navigate_TO_Planned_Task_Observations()
    {
        //Environmental Health and Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.EHS_Module()))
        {
            error = "Failed to wait for 'Environmental Health and Safety' module.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.EHS_Module()))
        {
            error = "Failed to click on 'Environmental Health and Safety' module.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental Health and Safety' module.");

        //Observations module
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Observations_Module()))
        {
            error = "Failed to wait for 'Observations' module.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Observations_Module()))
        {
            error = "Failed to click on 'Observations' module.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Observations' module.");

        //Planned Task Observations
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Planned_Task_Observations_Module()))
        {
            error = "Failed to wait for 'Planned Task Observations' module";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Planned_Task_Observations_Module()))
        {
            error = "Failed to click on 'Planned Task Observations' module";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Planned Task Observations' module search page.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.PlannedTaskObservations_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.PlannedTaskObservations_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean Capture_Planned_Task_Observations()
    {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.PlannedTaskObservations_ProcessFlow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.PlannedTaskObservations_ProcessFlow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //Business Unit 
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.PlannedTaskObservations_Dropdown()))
        {
            error = "Failed to wait for Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.PlannedTaskObservations_Dropdown()))
        {
            error = "Failed to click the Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.BusinessUnit_Option1(getData("Business Unit 1"))))
        {
            error = "Failed to wait for Business Unit Option: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.BusinessUnit_Option1(getData("Business Unit 1"))))
        {
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.BusinessUnit_Option1(getData("Business Unit 2"))))
        {
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.BusinessUnit_Option(getData("Business Unit 3"))))
        {
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 3");
            return false;
        }
        narrator.stepPassedWithScreenShot("Business Unit: " + getData("Business Unit 1") + " -> " + getData("Business Unit 2") + " -> " + getData("Business Unit 3"));

        //Project Link Checkbox
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.ProjectLink_Checkbox()))
        {
            error = "Failed to wait for Project Link Checkbox.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.ProjectLink_Checkbox()))
        {
            error = "Failed to click on Project Link Checkbox.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Project Link Checkbox.");

        //Project
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Dropdown()))
        {
            error = "Failed to wait for Project dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Dropdown()))
        {
            error = "Failed to click the Project dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Project dropdown.");

        //Project select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Project"))))
        {
            error = "Failed to wait for Project option: " + getData("Project");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Project"))))
        {
            error = "Failed to click the Project option: " + getData("Project");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Project option: " + getData("Project"));

        //Related stakeholder
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.RelatedStakeholder_Dropdown()))
        {
            error = "Failed to wait for Related stakeholder dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.RelatedStakeholder_Dropdown()))
        {
            error = "Failed to click the Related stakeholder dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Related stakeholder dropdown.");

        //Related stakeholder select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Related stakeholder"))))
        {
            error = "Failed to wait for Project option: " + getData("Related stakeholder");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Related stakeholder"))))
        {
            error = "Failed to click the Project option: " + getData("Related stakeholder");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Project option: " + getData("Related stakeholder"));

        //Task to be observed
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.TaskToBeObserved_Dropdown()))
        {
            error = "Failed to wait for Task to be observed dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.TaskToBeObserved_Dropdown()))
        {
            error = "Failed to click the Task to be observed dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Task to be observed dropdown.");

        //Related stakeholder select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Task to be observed"))))
        {
            error = "Failed to wait for Task to be observed option: " + getData("Task to be observed");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Task to be observed"))))
        {
            error = "Failed to click the Task to be observed option: " + getData("Task to be observed");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Task to be observed option: " + getData("Task to be observed"));

        //Task description
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.TaskDescription()))
        {
            error = "Failed to wait for Task description field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Planned_Task_Observations_PageObjects.TaskDescription(), getData("Task description")))
        {
            error = "Failed to enter '" + getData("Task description") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Task description") + "'.");

        //Person Executing Task
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.PersonExecutingTask_Dropdown()))
        {
            error = "Failed to wait for Person Executing Task dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.PersonExecutingTask_Dropdown()))
        {
            error = "Failed to click the Person Executing Task dropdown.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully click Person Executing Task dropdown.");
        
        //Person Executing Task select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Person Executing Task"))))
        {
            error = "Failed to wait for Person Executing Task option: " + getData("Person Executing Task");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Person Executing Task"))))
        {
            error = "Failed to click the Person Executing Task option: " + getData("Person Executing Task");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Person Executing TaskPerson Executing Task option: " + getData("Person Executing Task"));
        
        //Date
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Date()))
        {
            error = "Failed to wait for Date field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Planned_Task_Observations_PageObjects.Date(), startDate))
        {
            error = "Failed to enter '" + getData("Date") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + startDate + "'.");
        
        ////Notification in advance
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.NotificationInAdvance_Dropdown()))
        {
            error = "Failed to wait for Notification in advance dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.NotificationInAdvance_Dropdown()))
        {
            error = "Failed to click the Notification in advance dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Notification in advance dropdown.");

        //Notification in advance select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Notification in advance"))))
        {
            error = "Failed to wait for Notification in advance option: " + getData("Notification in advance");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Notification in advance"))))
        {
            error = "Failed to click the Notification in advance option: " + getData("Notification in advance");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Person applying option: " + getData("Notification in advance"));

        //Period in job
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.PeriodInJob_Dropdown()))
        {
            error = "Failed to wait for Period in job dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.PeriodInJob_Dropdown()))
        {
            error = "Failed to click the Period in job dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Period in job dropdown.");

        //Period in job select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Period in job"))))
        {
            error = "Failed to wait for Period in job option: " + getData("Period in job");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Period in job"))))
        {
            error = "Failed to click the Period in job option: " + getData("Period in job");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Period in job option: " + getData("Period in job"));
        
        //Relevant procedure
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.RelevantProcedure_Dropdown()))
        {
            error = "Failed to wait for Relevant procedure dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.RelevantProcedure_Dropdown()))
        {
            error = "Failed to click the Relevant procedure dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Relevant procedure dropdown.");

        //Relevant procedure select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Relevant procedure"))))
        {
            error = "Failed to wait for Relevant procedure option: " + getData("Relevant procedure");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Relevant procedure"))))
        {
            error = "Failed to click the Relevant procedure option: " + getData("Relevant procedure");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Relevant procedure option: " + getData("Relevant procedure"));
        
        //Reason for observation
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.ReasonForObservation_Dropdown()))
        {
            error = "Failed to wait for Reason for observation dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.ReasonForObservation_Dropdown()))
        {
            error = "Failed to click the Reason for observation dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Reason for observation dropdown.");

        //Relevant procedure select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Reason for observation"))))
        {
            error = "Failed to wait for Reason for observation option: " + getData("Reason for observation");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Reason for observation"))))
        {
            error = "Failed to click the Reason for observation option: " + getData("Reason for observation");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Reason for observation option: " + getData("Reason for observation"));

        //Save Button
        if (!getData("Save To Continue").equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.SaveButton()))
            {
                error = "Failed to wait for Save Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.SaveButton()))
            {
                error = "Failed to click on Save Button";
                return false;
            }
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.SaveToContinue_Button()))
            {
                error = "Failed to wait for Save to continue Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.SaveToContinue_Button()))
            {
                error = "Failed to click on Save to continue Button";
                return false;
            }
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Planned_Task_Observations_PageObjects.saveWait2(), 400))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Planned_Task_Observations_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Planned_Task_Observations_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");
        
        //Observation tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Observation_tab()))
        {
            error = "Failed to wait for Observation tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Observation_tab()))
        {
            error = "Failed to click Observation tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Observation Tab.");
        
        //Are company safety rules complied with?
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.SafetyRules_Dropdown()))
        {
            error = "Failed to wait for Are company safety rules complied with? dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.SafetyRules_Dropdown()))
        {
            error = "Failed to click the Are company safety rules complied with? dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Are company safety rules complied with? dropdown.");

        //Are company safety rules complied with? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Are company safety rules complied with?"))))
        {
            error = "Failed to wait for Are company safety rules complied with? option: " + getData("Are company safety rules complied with?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Are company safety rules complied with?"))))
        {
            error = "Failed to click the Are company safety rules complied with? option: " + getData("Are company safety rules complied with?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Are company safety rules complied with? option: " + getData("Are company safety rules complied with?"));
        
        //Are the standard procedures for the job followed?
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.StandardProcedures_Dropdown()))
        {
            error = "Failed to wait for Are the standard procedures for the job followed? dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.StandardProcedures_Dropdown()))
        {
            error = "Failed to click the Are the standard procedures for the job followed? dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Are the standard procedures for the job followed? dropdown.");

        //Are the standard procedures for the job followed? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Are the standard procedures for the job followed?"))))
        {
            error = "Failed to wait for Are the standard procedures for the job followed? option: " + getData("Are the standard procedures for the job followed?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Are the standard procedures for the job followed?"))))
        {
            error = "Failed to click the Are the standard procedures for the job followed? option: " + getData("Are the standard procedures for the job followed?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Are the standard procedures for the job followed? option: " + getData("Are the standard procedures for the job followed?"));
        
        //Is the correct personal protective clothing used?
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.ProtectiveClothing_Dropdown()))
        {
            error = "Failed to wait for Is the correct personal protective clothing used? dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.ProtectiveClothing_Dropdown()))
        {
            error = "Failed to click the Is the correct personal protective clothing used? dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Is the correct personal protective clothing used? dropdown.");

        //Is the correct personal protective clothing used? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Is the correct personal protective clothing used?"))))
        {
            error = "Failed to wait for Is the correct personal protective clothing used? option: " + getData("Is the correct personal protective clothing used?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Is the correct personal protective clothing used?"))))
        {
            error = "Failed to click the Is the correct personal protective clothing used? option: " + getData("Is the correct personal protective clothing used?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Is the correct personal protective clothing used? option: " + getData("Is the correct personal protective clothing used?"));
        
        //Is the person physically fit for the job?
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.PhysicallyFit_Dropdown()))
        {
            error = "Failed to wait for Is the person physically fit for the job? dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.PhysicallyFit_Dropdown()))
        {
            error = "Failed to click the Is the person physically fit for the job? dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Is the person physically fit for the job? dropdown.");

        //Is the person physically fit for the job? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Is the person physically fit for the job?"))))
        {
            error = "Failed to wait for Is the person physically fit for the job? option: " + getData("Is the person physically fit for the job?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Is the person physically fit for the job?"))))
        {
            error = "Failed to click the Is the person physically fit for the job? option: " + getData("Is the person physically fit for the job?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Is the person physically fit for the job? option: " + getData("Is the person physically fit for the job?"));
        
        //Environmental conditions acceptable?
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.EnvironmentalConditions_Dropdown()))
        {
            error = "Failed to wait for Environmental conditions acceptable? dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.EnvironmentalConditions_Dropdown()))
        {
            error = "Failed to click the Environmental conditions acceptable? dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Environmental conditions acceptable? dropdown.");

        //Environmental conditions acceptable? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Environmental conditions acceptable?"))))
        {
            error = "Failed to wait for Environmental conditions acceptable? option: " + getData("Environmental conditions acceptable?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Environmental conditions acceptable?"))))
        {
            error = "Failed to click the Environmental conditions acceptable? option: " + getData("Environmental conditions acceptable?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Environmental conditions acceptable? option: " + getData("Environmental conditions acceptable?"));
        
        //Expected quality achieved?
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.ExpectedQuality_Dropdown()))
        {
            error = "Failed to wait for Expected quality achieved? dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.ExpectedQuality_Dropdown()))
        {
            error = "Failed to click the Expected quality achieved? dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Expected quality achieved? dropdown.");

        //Expected quality achieved? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Expected quality achieved?"))))
        {
            error = "Failed to wait for Expected quality achieved? option: " + getData("Expected quality achieved?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Expected quality achieved?"))))
        {
            error = "Failed to click the Expected quality achieved? option: " + getData("Expected quality achieved?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Expected quality achieved? option: " + getData("Expected quality achieved?"));
        
        //Results achieved in expected period of time?
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.ResultsAchieved_Dropdown()))
        {
            error = "Failed to wait for Results achieved in expected period of time? dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.ResultsAchieved_Dropdown()))
        {
            error = "Failed to click the Results achieved in expected period of time? dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Results achieved in expected period of time? dropdown.");

        //Expected quality achieved? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Results achieved in expected period of time?"))))
        {
            error = "Failed to wait for Results achieved in expected period of time? option: " + getData("Results achieved in expected period of time?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Expected quality achieved?"))))
        {
            error = "Failed to click the Results achieved in expected period of time? option: " + getData("Results achieved in expected period of time?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Results achieved in expected period of time? option: " + getData("Results achieved in expected period of time?"));
        
        //Could acts observed lead to
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.ActsObserved_SelectAll()))
        {
            error = "Failed to wait for Could acts observed lead to Select all button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.ActsObserved_SelectAll()))
        {
            error = "Failed to click the Could acts observed lead to Select all button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Could acts observed lead to Select all button.");
        
        //Supporting Documents tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.supporting_tab()))
        {
            error = "Failed to wait for Supporting Documents tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.supporting_tab()))
        {
            error = "Failed to click Supporting Documents tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Supporting Documents Tab.");
        //Add support document - Hyperlink
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.linkbox()))
        {
            error = "Failed to wait for 'Link box' link.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.linkbox()))
        {
            error = "Failed to click on 'Link box' link.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");

        //switch to new window
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new window or tab.";
            return false;
        }

        //URL https
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.LinkURL()))
        {
            error = "Failed to wait for 'URL value' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Planned_Task_Observations_PageObjects.LinkURL(), getData("Document Link")))
        {
            error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.urlTitle()))
        {
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Planned_Task_Observations_PageObjects.urlTitle(), getData("URL Title")))
        {
            error = "Failed to enter '" + getData("URL Title") + "' into 'Url Title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("URL Title : '" + getData("URL Title") + "'.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.urlAddButton()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.urlAddButton()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Planned_Task_Observations_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        
        //Save Button
        if (!getData("Save To Continue").equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.SaveButton()))
            {
                error = "Failed to wait for Save Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.SaveButton()))
            {
                error = "Failed to click on Save Button";
                return false;
            }
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.SaveToContinue_Button()))
            {
                error = "Failed to wait for Save to continue Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.SaveToContinue_Button()))
            {
                error = "Failed to click on Save to continue Button";
                return false;
            }
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Planned_Task_Observations_PageObjects.saveWait2(), 400))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Save Button.");

        return true;
    }

}
