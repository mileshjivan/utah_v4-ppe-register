/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Planned_Task_Observations_V5_2_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Bowtie_Risk_Assessment_S5_2_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.Bowtie_Risk_Assessment_PageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Permit_To_Work_V5_2_PageObjects.Permit_To_Work_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Planned_Task_Observations_V5_2_PageObjetcs.Planned_Task_Observations_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Planned Task Observations - Alternate Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Planned_Task_Observations_AlternateScenario extends BaseClass
{

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Planned_Task_Observations_AlternateScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Navigate_TO_Planned_Task_Observations())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Capture_Planned_Task_Observations())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Permit To Work");
    }

    public boolean Navigate_TO_Planned_Task_Observations()
    {
        //Environmental Health and Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.EHS_Module()))
        {
            error = "Failed to wait for 'Environmental Health and Safety' module.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.EHS_Module()))
        {
            error = "Failed to click on 'Environmental Health and Safety' module.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental Health and Safety' module.");

        //Observations module
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Observations_Module()))
        {
            error = "Failed to wait for 'Observations' module.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Observations_Module()))
        {
            error = "Failed to click on 'Observations' module.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Observations' module.");

        //Planned Task Observations
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Planned_Task_Observations_Module()))
        {
            error = "Failed to wait for 'Planned Task Observations' module";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Planned_Task_Observations_Module()))
        {
            error = "Failed to click on 'Planned Task Observations' module";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Planned Task Observations' module search page.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.PlannedTaskObservations_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.PlannedTaskObservations_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean Capture_Planned_Task_Observations()
    {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.PlannedTaskObservations_ProcessFlow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.PlannedTaskObservations_ProcessFlow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //Business Unit 
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.PlannedTaskObservations_Dropdown()))
        {
            error = "Failed to wait for Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.PlannedTaskObservations_Dropdown()))
        {
            error = "Failed to click the Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.BusinessUnit_Option1(getData("Business Unit 1"))))
        {
            error = "Failed to wait for Business Unit Option: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.BusinessUnit_Option1(getData("Business Unit 1"))))
        {
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.BusinessUnit_Option1(getData("Business Unit 2"))))
        {
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.BusinessUnit_Option(getData("Business Unit 3"))))
        {
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 3");
            return false;
        }
        narrator.stepPassedWithScreenShot("Business Unit: " + getData("Business Unit 1") + " -> " + getData("Business Unit 2") + " -> " + getData("Business Unit 3"));

        //Task to be observed
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.TaskToBeObserved_Dropdown()))
        {
            error = "Failed to wait for Task to be observed dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.TaskToBeObserved_Dropdown()))
        {
            error = "Failed to click the Task to be observed dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Task to be observed dropdown.");

        //Related stakeholder select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Task to be observed"))))
        {
            error = "Failed to wait for Task to be observed option: " + getData("Task to be observed");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Task to be observed"))))
        {
            error = "Failed to click the Task to be observed option: " + getData("Task to be observed");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Task to be observed option: " + getData("Task to be observed"));

        //Task description
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.TaskDescription()))
        {
            error = "Failed to wait for Task description field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Planned_Task_Observations_PageObjects.TaskDescription(), getData("Task description")))
        {
            error = "Failed to enter '" + getData("Task description") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Task description") + "'.");

        //Person Executing Task
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.PersonExecutingTask_Dropdown()))
        {
            error = "Failed to wait for Person Executing Task dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.PersonExecutingTask_Dropdown()))
        {
            error = "Failed to click the Person Executing Task dropdown.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully click Person Executing Task dropdown.");
        
        //Person Executing Task select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Person Executing Task"))))
        {
            error = "Failed to wait for Person Executing Task option: " + getData("Person Executing Task");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Person Executing Task"))))
        {
            error = "Failed to click the Person Executing Task option: " + getData("Person Executing Task");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Person Executing TaskPerson Executing Task option: " + getData("Person Executing Task"));
        
        //Date
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Date()))
        {
            error = "Failed to wait for Date field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Planned_Task_Observations_PageObjects.Date(), startDate))
        {
            error = "Failed to enter '" + getData("Date") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + startDate + "'.");
        
        ////Notification in advance
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.NotificationInAdvance_Dropdown()))
        {
            error = "Failed to wait for Notification in advance dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.NotificationInAdvance_Dropdown()))
        {
            error = "Failed to click the Notification in advance dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Notification in advance dropdown.");

        //Notification in advance select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Notification in advance"))))
        {
            error = "Failed to wait for Notification in advance option: " + getData("Notification in advance");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Notification in advance"))))
        {
            error = "Failed to click the Notification in advance option: " + getData("Notification in advance");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Person applying option: " + getData("Notification in advance"));

        //Period in job
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.PeriodInJob_Dropdown()))
        {
            error = "Failed to wait for Period in job dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.PeriodInJob_Dropdown()))
        {
            error = "Failed to click the Period in job dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Period in job dropdown.");

        //Period in job select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Period in job"))))
        {
            error = "Failed to wait for Period in job option: " + getData("Period in job");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Period in job"))))
        {
            error = "Failed to click the Period in job option: " + getData("Period in job");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Period in job option: " + getData("Period in job"));
        
        //Relevant procedure
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.RelevantProcedure_Dropdown()))
        {
            error = "Failed to wait for Relevant procedure dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.RelevantProcedure_Dropdown()))
        {
            error = "Failed to click the Relevant procedure dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Relevant procedure dropdown.");

        //Relevant procedure select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Relevant procedure"))))
        {
            error = "Failed to wait for Relevant procedure option: " + getData("Relevant procedure");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Relevant procedure"))))
        {
            error = "Failed to click the Relevant procedure option: " + getData("Relevant procedure");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Relevant procedure option: " + getData("Relevant procedure"));
        
        //Reason for observation
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.ReasonForObservation_Dropdown()))
        {
            error = "Failed to wait for Reason for observation dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.ReasonForObservation_Dropdown()))
        {
            error = "Failed to click the Reason for observation dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Reason for observation dropdown.");

        //Relevant procedure select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Reason for observation"))))
        {
            error = "Failed to wait for Reason for observation option: " + getData("Reason for observation");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Reason for observation"))))
        {
            error = "Failed to click the Reason for observation option: " + getData("Reason for observation");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Reason for observation option: " + getData("Reason for observation"));

        //Save Button
        if (!getData("Save To Continue").equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.SaveButton()))
            {
                error = "Failed to wait for Save Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.SaveButton()))
            {
                error = "Failed to click on Save Button";
                return false;
            }
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.SaveToContinue_Button()))
            {
                error = "Failed to wait for Save to continue Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.SaveToContinue_Button()))
            {
                error = "Failed to click on Save to continue Button";
                return false;
            }
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Planned_Task_Observations_PageObjects.saveWait2(), 400))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Planned_Task_Observations_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Planned_Task_Observations_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        return true;
    }

}
