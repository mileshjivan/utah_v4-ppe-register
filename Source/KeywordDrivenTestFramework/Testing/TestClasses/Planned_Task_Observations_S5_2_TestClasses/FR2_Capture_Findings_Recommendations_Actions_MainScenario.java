/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Planned_Task_Observations_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Planned_Task_Observations_V5_2_PageObjetcs.Planned_Task_Observations_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR2-Capture Findings Recommendations Actions - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_Findings_Recommendations_Actions_MainScenario extends BaseClass
{

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_Capture_Findings_Recommendations_Actions_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Capture_Findings_Recommendations_Actions_Task_Observations())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Permit To Work");
    }

    public boolean Capture_Findings_Recommendations_Actions_Task_Observations()
    {
        //Observation tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Observation_tab()))
        {
            error = "Failed to wait for Observation tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Observation_tab()))
        {
            error = "Failed to click Observation tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Observation Tab.");
        
        //Are company safety rules complied with?
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.SafetyRules_Dropdown()))
        {
            error = "Failed to wait for Are company safety rules complied with? dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.SafetyRules_Dropdown()))
        {
            error = "Failed to click the Are company safety rules complied with? dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Are company safety rules complied with? dropdown.");

        //Are company safety rules complied with? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Are company safety rules complied with?"))))
        {
            error = "Failed to wait for Are company safety rules complied with? option: " + getData("Are company safety rules complied with?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Are company safety rules complied with?"))))
        {
            error = "Failed to click the Are company safety rules complied with? option: " + getData("Are company safety rules complied with?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Are company safety rules complied with? option: " + getData("Are company safety rules complied with?"));
        
        //Are the standard procedures for the job followed?
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.StandardProcedures_Dropdown()))
        {
            error = "Failed to wait for Are the standard procedures for the job followed? dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.StandardProcedures_Dropdown()))
        {
            error = "Failed to click the Are the standard procedures for the job followed? dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Are the standard procedures for the job followed? dropdown.");

        //Are the standard procedures for the job followed? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Are the standard procedures for the job followed?"))))
        {
            error = "Failed to wait for Are the standard procedures for the job followed? option: " + getData("Are the standard procedures for the job followed?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Are the standard procedures for the job followed?"))))
        {
            error = "Failed to click the Are the standard procedures for the job followed? option: " + getData("Are the standard procedures for the job followed?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Are the standard procedures for the job followed? option: " + getData("Are the standard procedures for the job followed?"));
        
        //Is the correct personal protective clothing used?
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.ProtectiveClothing_Dropdown()))
        {
            error = "Failed to wait for Is the correct personal protective clothing used? dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.ProtectiveClothing_Dropdown()))
        {
            error = "Failed to click the Is the correct personal protective clothing used? dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Is the correct personal protective clothing used? dropdown.");

        //Is the correct personal protective clothing used? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Is the correct personal protective clothing used?"))))
        {
            error = "Failed to wait for Is the correct personal protective clothing used? option: " + getData("Is the correct personal protective clothing used?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Is the correct personal protective clothing used?"))))
        {
            error = "Failed to click the Is the correct personal protective clothing used? option: " + getData("Is the correct personal protective clothing used?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Is the correct personal protective clothing used? option: " + getData("Is the correct personal protective clothing used?"));
        
        //Is the person physically fit for the job?
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.PhysicallyFit_Dropdown()))
        {
            error = "Failed to wait for Is the person physically fit for the job? dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.PhysicallyFit_Dropdown()))
        {
            error = "Failed to click the Is the person physically fit for the job? dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Is the person physically fit for the job? dropdown.");

        //Is the person physically fit for the job? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Is the person physically fit for the job?"))))
        {
            error = "Failed to wait for Is the person physically fit for the job? option: " + getData("Is the person physically fit for the job?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Is the person physically fit for the job?"))))
        {
            error = "Failed to click the Is the person physically fit for the job? option: " + getData("Is the person physically fit for the job?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Is the person physically fit for the job? option: " + getData("Is the person physically fit for the job?"));
        
        //Environmental conditions acceptable?
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.EnvironmentalConditions_Dropdown()))
        {
            error = "Failed to wait for Environmental conditions acceptable? dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.EnvironmentalConditions_Dropdown()))
        {
            error = "Failed to click the Environmental conditions acceptable? dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Environmental conditions acceptable? dropdown.");

        //Environmental conditions acceptable? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Environmental conditions acceptable?"))))
        {
            error = "Failed to wait for Environmental conditions acceptable? option: " + getData("Environmental conditions acceptable?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Environmental conditions acceptable?"))))
        {
            error = "Failed to click the Environmental conditions acceptable? option: " + getData("Environmental conditions acceptable?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Environmental conditions acceptable? option: " + getData("Environmental conditions acceptable?"));
        
        //Expected quality achieved?
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.ExpectedQuality_Dropdown()))
        {
            error = "Failed to wait for Expected quality achieved? dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.ExpectedQuality_Dropdown()))
        {
            error = "Failed to click the Expected quality achieved? dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Expected quality achieved? dropdown.");

        //Expected quality achieved? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Expected quality achieved?"))))
        {
            error = "Failed to wait for Expected quality achieved? option: " + getData("Expected quality achieved?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Expected quality achieved?"))))
        {
            error = "Failed to click the Expected quality achieved? option: " + getData("Expected quality achieved?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Expected quality achieved? option: " + getData("Expected quality achieved?"));
        
        //Results achieved in expected period of time?
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.ResultsAchieved_Dropdown()))
        {
            error = "Failed to wait for Results achieved in expected period of time? dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.ResultsAchieved_Dropdown()))
        {
            error = "Failed to click the Results achieved in expected period of time? dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Results achieved in expected period of time? dropdown.");

        //Expected quality achieved? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Results achieved in expected period of time?"))))
        {
            error = "Failed to wait for Results achieved in expected period of time? option: " + getData("Results achieved in expected period of time?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Expected quality achieved?"))))
        {
            error = "Failed to click the Results achieved in expected period of time? option: " + getData("Results achieved in expected period of time?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Results achieved in expected period of time? option: " + getData("Results achieved in expected period of time?"));
        
        //Could acts observed lead to
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.ActsObserved_SelectAll()))
        {
            error = "Failed to wait for Could acts observed lead to Select all button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.ActsObserved_SelectAll()))
        {
            error = "Failed to click the Could acts observed lead to Select all button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Could acts observed lead to Select all button.");
        
        //Findings/Recommendations/Actions
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.FindingsRecommendationsActiond_Panel()))
        {
            error = "Failed to wait for 'Findings/Recommendations/Actions' panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.FindingsRecommendationsActiond_Panel()))
        {
            error = "Failed to click on 'Findings/Recommendations/Actions' panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Findings/Recommendations/Actions' panel.");
        
        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Findings_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Findings_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Findings_ProcessFlow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Findings_ProcessFlow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //Finding Description
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Description_TextFieldxpath())) {
            error = "Failed to wait for Findings Description.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Planned_Task_Observations_PageObjects.Description_TextFieldxpath(), getData("Finding"))) {
            error = "Failed to Enter Text into Findings Description Textfield.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Findings Description Textfield: " + getData("Finding"));

        ///Finding Owner
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.FindingOwner_Selectxpath())) {
            error = "Failed to wait for Finding Owner Select field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.FindingOwner_Selectxpath())) {
            error = "Failed to click Finding Owner Select field.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("FindingOwner")))) {
            error = "Failed to wait for Finding Owner.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("FindingOwner")))) {
            error = "Failed to click Finding Owner.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Finding Owner: " + getData("FindingOwner"));

        //Incident Management Risk Source Select field.
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.RiskSource_Selectxpath())) {
            error = "Failed to wait for Risk Source Select field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.RiskSource_Selectxpath())) {
            error = "Failed to click Risk Source Select field.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.RiskSource_TickAllxpath())) {
            error = "Failed to wait for tick all Risk Source.";
            return false;
        }        
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.RiskSource_TickAllxpath())) {
            error = "Failed to click tick all Risk Source.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected all Risk Source.");

        //Finding Classification Select field
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Classification_Selectxpath())) {
            error = "Failed to wait for Finding Classification Select field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Classification_Selectxpath())) {
            error = "Failed to click Finding Classification Select field.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Classification")))) {
            error = "Failed to wait for Finding Classification.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Classification")))) {
            error = "Failed to click Finding Classification.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Finding Classification: " + getData("Classification"));
        
        //Incident Management Risk Select field
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Risk_Selectxpath())) {
            error = "Failed to wait for Risk Select field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Risk_Selectxpath())) {
            error = "Failed to click Risk Select field.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Risk")))) {
            error = "Failed to wait for Risk.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Risk")))) {
            error = "Failed to click Risk.";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully captured Finding Risk: " + getData("Risk"));

        //Incident Management Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Findings_Save_Selectxpath())) {
            error = "Failed to wait for Save button field.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Findings_Save_Selectxpath())) {
            error = "Failed to click Save button field.";
            return false;
        }
        
        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Planned_Task_Observations_PageObjects.saveWait2(), 400))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Save' button.");
        pause(2000);
        
        //Close button
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.CloseBtn())) {
            error = "Failed to wait for Close button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.CloseBtn())) {
            error = "Failed to click Close button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked close button");
        
         //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.PlannedTaskObservations_ProcessFlow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.PlannedTaskObservations_ProcessFlow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        
        return true;
    
    }

}
