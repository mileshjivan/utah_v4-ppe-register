/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Planned_Task_Observations_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Management_S5_2_PageObjects.Incident_Management_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Planned_Task_Observations_V5_2_PageObjetcs.Planned_Task_Observations_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR3-Complete Declaration to Close PTO - Main Scenario",
        createNewBrowserInstance = false
)

public class FR3_Complete_Declaration_To_Close_PTO_MainScenario extends BaseClass
{

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR3_Complete_Declaration_To_Close_PTO_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Capture_Findings_Recommendations_Actions_Task_Observations())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Permit To Work");
    }

    public boolean Capture_Findings_Recommendations_Actions_Task_Observations()
    {
        //Observation tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Observation_tab()))
        {
            error = "Failed to wait for Observation tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Observation_tab()))
        {
            error = "Failed to click Observation tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Observation Tab.");
        
        //Are company safety rules complied with?
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.SafetyRules_Dropdown()))
        {
            error = "Failed to wait for Are company safety rules complied with? dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.SafetyRules_Dropdown()))
        {
            error = "Failed to click the Are company safety rules complied with? dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Are company safety rules complied with? dropdown.");

        //Are company safety rules complied with? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Are company safety rules complied with?"))))
        {
            error = "Failed to wait for Are company safety rules complied with? option: " + getData("Are company safety rules complied with?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Are company safety rules complied with?"))))
        {
            error = "Failed to click the Are company safety rules complied with? option: " + getData("Are company safety rules complied with?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Are company safety rules complied with? option: " + getData("Are company safety rules complied with?"));
        
        //Are the standard procedures for the job followed?
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.StandardProcedures_Dropdown()))
        {
            error = "Failed to wait for Are the standard procedures for the job followed? dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.StandardProcedures_Dropdown()))
        {
            error = "Failed to click the Are the standard procedures for the job followed? dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Are the standard procedures for the job followed? dropdown.");

        //Are the standard procedures for the job followed? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Are the standard procedures for the job followed?"))))
        {
            error = "Failed to wait for Are the standard procedures for the job followed? option: " + getData("Are the standard procedures for the job followed?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Are the standard procedures for the job followed?"))))
        {
            error = "Failed to click the Are the standard procedures for the job followed? option: " + getData("Are the standard procedures for the job followed?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Are the standard procedures for the job followed? option: " + getData("Are the standard procedures for the job followed?"));
        
        //Is the correct personal protective clothing used?
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.ProtectiveClothing_Dropdown()))
        {
            error = "Failed to wait for Is the correct personal protective clothing used? dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.ProtectiveClothing_Dropdown()))
        {
            error = "Failed to click the Is the correct personal protective clothing used? dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Is the correct personal protective clothing used? dropdown.");

        //Is the correct personal protective clothing used? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Is the correct personal protective clothing used?"))))
        {
            error = "Failed to wait for Is the correct personal protective clothing used? option: " + getData("Is the correct personal protective clothing used?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Is the correct personal protective clothing used?"))))
        {
            error = "Failed to click the Is the correct personal protective clothing used? option: " + getData("Is the correct personal protective clothing used?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Is the correct personal protective clothing used? option: " + getData("Is the correct personal protective clothing used?"));
        
        //Is the person physically fit for the job?
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.PhysicallyFit_Dropdown()))
        {
            error = "Failed to wait for Is the person physically fit for the job? dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.PhysicallyFit_Dropdown()))
        {
            error = "Failed to click the Is the person physically fit for the job? dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Is the person physically fit for the job? dropdown.");

        //Is the person physically fit for the job? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Is the person physically fit for the job?"))))
        {
            error = "Failed to wait for Is the person physically fit for the job? option: " + getData("Is the person physically fit for the job?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Is the person physically fit for the job?"))))
        {
            error = "Failed to click the Is the person physically fit for the job? option: " + getData("Is the person physically fit for the job?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Is the person physically fit for the job? option: " + getData("Is the person physically fit for the job?"));
        
        //Environmental conditions acceptable?
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.EnvironmentalConditions_Dropdown()))
        {
            error = "Failed to wait for Environmental conditions acceptable? dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.EnvironmentalConditions_Dropdown()))
        {
            error = "Failed to click the Environmental conditions acceptable? dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Environmental conditions acceptable? dropdown.");

        //Environmental conditions acceptable? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Environmental conditions acceptable?"))))
        {
            error = "Failed to wait for Environmental conditions acceptable? option: " + getData("Environmental conditions acceptable?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Environmental conditions acceptable?"))))
        {
            error = "Failed to click the Environmental conditions acceptable? option: " + getData("Environmental conditions acceptable?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Environmental conditions acceptable? option: " + getData("Environmental conditions acceptable?"));
        
        //Expected quality achieved?
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.ExpectedQuality_Dropdown()))
        {
            error = "Failed to wait for Expected quality achieved? dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.ExpectedQuality_Dropdown()))
        {
            error = "Failed to click the Expected quality achieved? dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Expected quality achieved? dropdown.");

        //Expected quality achieved? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Expected quality achieved?"))))
        {
            error = "Failed to wait for Expected quality achieved? option: " + getData("Expected quality achieved?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Expected quality achieved?"))))
        {
            error = "Failed to click the Expected quality achieved? option: " + getData("Expected quality achieved?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Expected quality achieved? option: " + getData("Expected quality achieved?"));
        
        //Results achieved in expected period of time?
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.ResultsAchieved_Dropdown()))
        {
            error = "Failed to wait for Results achieved in expected period of time? dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.ResultsAchieved_Dropdown()))
        {
            error = "Failed to click the Results achieved in expected period of time? dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Results achieved in expected period of time? dropdown.");

        //Expected quality achieved? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Results achieved in expected period of time?"))))
        {
            error = "Failed to wait for Results achieved in expected period of time? option: " + getData("Results achieved in expected period of time?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Project_Option(getData("Expected quality achieved?"))))
        {
            error = "Failed to click the Results achieved in expected period of time? option: " + getData("Results achieved in expected period of time?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Results achieved in expected period of time? option: " + getData("Results achieved in expected period of time?"));
        
        //Could acts observed lead to
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.ActsObserved_SelectAll()))
        {
            error = "Failed to wait for Could acts observed lead to Select all button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.ActsObserved_SelectAll()))
        {
            error = "Failed to click the Could acts observed lead to Select all button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Could acts observed lead to Select all button.");
        
        //Findings/Recommendations/Actions
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.FindingsRecommendationsActiond_Panel()))
        {
            error = "Failed to wait for 'Findings/Recommendations/Actions' panel.";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Planned_Task_Observations_PageObjects.FindingsRecommendationsActiond_Panel()))
        {
            error = "Failed to Scroll to 'Findings/Recommendations/Actions' panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Scrolled to 'Findings/Recommendations/Actions' panel.");
        
        //I declare that I am authorised to conduct a PTO and that I have completed all relevant sections of this PTO. I have recorded my finding and discussed my findings with the person being observed.
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.PTO_CheckBox()))
        {
            error = "Failed to wait for 'PTO' checkbox.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.PTO_CheckBox()))
        {
            error = "Failed to click on 'PTO' checkbox.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'PTO' checkbox.");
        
        //Save Button
        if (!getData("Save To Continue").equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.SaveButton()))
            {
                error = "Failed to wait for Save Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.SaveButton()))
            {
                error = "Failed to click on Save Button";
                return false;
            }
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.SaveToContinue_Button()))
            {
                error = "Failed to wait for Save to continue Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.SaveToContinue_Button()))
            {
                error = "Failed to click on Save to continue Button";
                return false;
            }
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Planned_Task_Observations_PageObjects.saveWait2(), 400))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully click 'Save' button.");
       
        return true;
    
    }

}
