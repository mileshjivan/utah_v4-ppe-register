/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Planned_Task_Observations_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PPE_Type_Library_V5_2_PageObjects.PPE_Type_Library_PageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Planned_Task_Observations_V5_2_PageObjetcs.Planned_Task_Observations_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR5 View Reports - Main Scenario",
        createNewBrowserInstance = false
)
public class FR5_View_Reports_Main_Scenario extends BaseClass
{

    String parentWindow;
    String error = "";

    public FR5_View_Reports_Main_Scenario()
    {

        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!View_Reports())
        {
            return narrator.testFailed("View Reports Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Viewed Reports");
    }

    public boolean View_Reports()
    {
        //Navigate to Environmental Health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.navigate_EHS()))
        {
            error = "Failed to wait for 'Environmental, Health & Safety' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.navigate_EHS()))
        {
            error = "Failed to click on 'Environmental, Health & Safety' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' button.");

        //Navigate Planned Task
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Observations()))
        {
            error = "Failed to wait for Observations button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Observations()))
        {
            error = "Failed to click on Observations button.";
            return false;
        }
//        narrator.stepPassedWithScreenShot("Successfully navigated Observations Module.");

        //Navigate to Operate Maintenance
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Planned_Task_Observations()))
        {
            error = "Failed to wait for Planned Task Observations button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.Planned_Task_Observations()))
        {
            error = "Failed to click on Planned Task Observations button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked  Planned Task Observations button.");

        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.searchBtn()))
        {
            error = "Failed to wait for 'Serach' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.searchBtn()))
        {
            error = "Failed to click on 'Serach' button.";
            return false;
        }
        SeleniumDriverInstance.pause(5000);
        narrator.stepPassedWithScreenShot("Successfully click 'Serach' button.");

        //Reports button
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.reportsBtn()))
        {
            error = "Failed to wait for 'Reports' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.reportsBtn()))
        {
            error = "Failed to click on 'Reports' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Reports' button.");
        parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        //View reports icon
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.viewReportsIcon()))
        {
            error = "Failed to wait for 'View reports' icon.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.viewReportsIcon()))
        {
            error = "Failed to click on 'View reports' icon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'View reports' icon.");
        SeleniumDriverInstance.switchToTabOrWindow();
        //Continue button
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.continueBtn()))
        {
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.continueBtn()))
        {
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        pause(5000);
        SeleniumDriverInstance.switchToTabOrWindow();

        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.Observations_Label()))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Planned_Task_Observations_PageObjects.Observations_Label()))
            {
                error = "Failed to wait for Observations label.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Continue' button.");
        narrator.stepPassedWithScreenShot("Viewing Rreports.");

        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.iframeXpath()))
        {
            error = "Failed to wait for frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Planned_Task_Observations_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        //View full reports icon
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.viewFullReportsIcon()))
        {
            error = "Failed to wait for 'View full reports' icon.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.viewFullReportsIcon()))
        {
            error = "Failed to click on 'View full reports' icon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'View full reports' icon.");
        SeleniumDriverInstance.switchToTabOrWindow();
        //Continue button
        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.continueBtn()))
        {
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Planned_Task_Observations_PageObjects.continueBtn()))
        {
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        pause(5000);
        SeleniumDriverInstance.switchToTabOrWindow();
        narrator.stepPassedWithScreenShot("Successfully click 'Continue' button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Planned_Task_Observations_PageObjects.BusinessUnitHeader()))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Planned_Task_Observations_PageObjects.BusinessUnitHeader()))
            {
                error = "Failed to wait for Engagements header to be present.";
                return false;
            }
        }

        narrator.stepPassedWithScreenShot("Viewing Full Rreports.");
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

        pause(2000);
        return true;
    }

}
