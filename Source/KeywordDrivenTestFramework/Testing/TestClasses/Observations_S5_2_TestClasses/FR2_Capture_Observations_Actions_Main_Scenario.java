/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Observations_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import KeywordDrivenTestFramework.Testing.PageObjects.Observations_V5_2_PageObjects.Observations_V5_2_PageObjects;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR2-Capture Observations Actions v5.2 - Main Scenario",
        createNewBrowserInstance = false
)
public class FR2_Capture_Observations_Actions_Main_Scenario extends BaseClass {
    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_Capture_Observations_Actions_Main_Scenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Observations_Actions()) {
            return narrator.testFailed("Observations Actions Failed due to - " + error);
        }

        return narrator.finalizeTest("Successfully Capture Observations Actions");

    }

    public boolean Observations_Actions() {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Observations_ProcessFlow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        //Observations Actions

        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Observations_Add_2())) {
            error = "Failed to click Observations Actions add button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Observations Actions Add button.");

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.ObservationsActions_ProcessFlow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.ObservationsActions_ProcessFlow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        
        //Actions Description
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Action_description())) {
            error = "Failed to wait for Actions Description";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Observations_V5_2_PageObjects.Action_description(), getData("Action Description"))) {
            error = "Failed to enter Actions Description :" + getData("Action Description");
            return false;
        }
        narrator.stepPassedWithScreenShot("Actions Description: " + getData("Action Description"));


        //Department responsible
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Department_responsible())) {
            error = "Failed to wait Department responsible";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Department_responsible())) {
            error = "Failed to click  Department responsible ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.DepartmentResponsible_Option(getData("Department responsible 1")))) {
            error = "Failed to wait Department responsible option: " + getData("Department responsible 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.DepartmentResponsible_Option(getData("Department responsible 1")))) {
            error = "Failed to wait Department responsible option: " + getData("Department responsible 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.DepartmentResponsible_Option(getData("Department responsible 2")))) {
            error = "Failed to wait Department responsible option: " + getData("Department responsible 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.DepartmentResponsible_Option1(getData("Department responsible 3")))) {
            error = "Failed to wait Department responsible option: " + getData("Department responsible 3");
            return false;
        }
        narrator.stepPassedWithScreenShot("Department responsible option: " + getData("Department responsible 1") + " -> " + getData("Department responsible 2") + " -> " + getData("Department responsible 3"));

        //Responsible Person
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Responsible_Person())) {
            error = "Failed to wait Responsible Person";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Responsible_Person())) {
            error = "Failed to click Responsible Person";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Responsible_Person_option(getData("Responsible Person")))) {
            error = "Failed to wait Responsible Person option: " + getData("Responsible Person");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Responsible_Person_option(getData("Responsible Person")))) {
            error = "Failed to click Responsible Person option: " + getData("Responsible Person");
            return false;
        }
        narrator.stepPassedWithScreenShot("Responsible Person: " + getData("Responsible Person"));

        //start Date
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Action_due_date())) {
            error = "Failed to wait for Action due date";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Observations_V5_2_PageObjects.Action_due_date(), startDate)) {
            error = "Failed to enter for Action due date :" + startDate;
            return false;
        }
        narrator.stepPassedWithScreenShot("Action due date :" + startDate);

        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.saveBtn2())) {
            error = "Failed to wait for save button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.saveBtn2())) {
            error = "Failed to click  save button";
            return false;
        }

        //Saving mask
        if (SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Observations_V5_2_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        //Validation
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.inspection_Record_Saved_popup())) {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Observations_V5_2_PageObjects.inspection_Record_Saved_popup());
        } else {
            if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.inspection_Record_Saved_popup())) {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Successfully clicked save");
        } else {
            if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.failed())) {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Observations_V5_2_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved")) {
                error = "Failed to save record.";
                return false;
            }
        }
        
        String[] Actionid = SeleniumDriverInstance.retrieveTextByXpath(Observations_V5_2_PageObjects.incidentActionId()).split("#");
        setRecordId(Actionid[1]);

        narrator.stepPassed("Observations / Observation Actions Record Number: " + Actionid[1]);

        //Observation - Observation Actions - Close Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.ObservationActions_CloseButton())){
            error = "Failed to wait for Observation - Observation Actions - Close Button";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.ObservationActions_CloseButton())){
            error = "Failed to click Observation - Observation Actions - Close Button";
            return false;
        }
        pause(2000);
        
        //Observation - Observation Actions Gridview Record
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(Observations_V5_2_PageObjects.ObservationActions_Gridview(getData("Action Description")))){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Observations_V5_2_PageObjects.ObservationActions_Gridview(getData("Action Description")))){
                error = "Failed to wait for Incident Management - Incident Actions Gridview Record";
                return false;
            }
        }

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Observations_ProcessFlow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Observations_ProcessFlow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        pause(2000);
        
        return true;
    }

}
