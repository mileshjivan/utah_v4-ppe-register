/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Observations_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import KeywordDrivenTestFramework.Testing.PageObjects.Observations_V5_2_PageObjects.Observations_V5_2_PageObjects;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR3-View Reports v5.2 - Main Scenario",
        createNewBrowserInstance = false
)
public class FR3_View_Reports_Main_Scenario extends BaseClass {
    public WebDriver Driver;
    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR3_View_Reports_Main_Scenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Navigate_To_Observations()) {
            return narrator.testFailed("Navigate To Observations Failed due to - " + error);
        }

        return narrator.finalizeTest("Successfully Viewed Reports");
    }

    public boolean Navigate_To_Observations() {
        //Environment, Health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.navigate_EHS())) {
            error = "Failed to wait for Environment, Health & Safety";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.navigate_EHS())) {
            error = "Failed to click on Environment, Health & Safety";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Environment, Health & Safety.");

        //Observation Module
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Observation_tab())) {
            error = "Failed to wait for Observation module";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Observation_tab())) {
            error = "Failed to click on Observation module";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Observation module.");
        
        //Observation tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Observations_tab())) {
            error = "Failed to wait for Observation tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Observations_tab())) {
            error = "Failed to click on Observation tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Observation tab.");
        
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Search_Btn())) {
            error = "Failed to wait for 'Serach' button.";
            return false;
        }
        SeleniumDriverInstance.pause(5000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Search_Btn())) {
            error = "Failed to click on 'Serach' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Serach' button.");

        //Reports button
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Reports())) {
            error = "Failed to wait for 'Reports' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Reports())) {
            error = "Failed to click on 'Reports' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Reports' button.");
        parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        
        //View reports icon
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.View_report())) {
            error = "Failed to wait for 'View reports' icon.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.View_report())) {
            error = "Failed to click on 'View reports' icon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'View reports' icon.");
        SeleniumDriverInstance.switchToTabOrWindow();
        //Continue button
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Continue_Btn())) {
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Continue_Btn())) {
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        pause(5000);
        SeleniumDriverInstance.switchToTabOrWindow();
              
        if(!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.ObservationHeader())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Observations_V5_2_PageObjects.ObservationHeader())){
                error = "Failed to wait for Observation header to be present.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Continue' button.");
        narrator.stepPassedWithScreenShot("Viewing Rreports.");

        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.iframeXpath())) {
            error = "Failed to wait for frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Observations_V5_2_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        //View full reports icon
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Full_report())) {
            error = "Failed to wait for 'View full reports' icon.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Full_report())) {
            error = "Failed to click on 'View full reports' icon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'View full reports' icon.");
        SeleniumDriverInstance.switchToTabOrWindow();
        //Continue button
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Continue_Btn())) {
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Continue_Btn())) {
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        pause(5000);
        SeleniumDriverInstance.switchToTabOrWindow();
        narrator.stepPassedWithScreenShot("Successfully click 'Continue' button.");
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.BusinessUnitHeader())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Observations_V5_2_PageObjects.BusinessUnitHeader())){
                error = "Failed to wait for Engagements header to be present.";
                return false;
            }
        }
        
        narrator.stepPassedWithScreenShot("Viewing Full Rreports.");
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        
        pause(2000);

        return true;
    }

}//end of class 
