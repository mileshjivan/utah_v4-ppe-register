/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Observations_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Observations_V5_2_PageObjects.Observations_V5_2_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Observations v5.2 - Main Scenario",
        createNewBrowserInstance = false
)
public class FR1_Capture_Observations_Main_Scenario extends BaseClass {
    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Observations_Main_Scenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Navigate_To_Observations()) {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Capture_Observations_Details()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully");
    }

    public boolean Navigate_To_Observations() {
        //Environment, Health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.navigate_EHS())) {
            error = "Failed to wait for Environment, Health & Safety";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.navigate_EHS())) {
            error = "Failed to click on Environment, Health & Safety";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Environment, Health & Safety.");

        //Observation Module
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Observation_tab())) {
            error = "Failed to wait for Observation module";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Observation_tab())) {
            error = "Failed to click on Observation module";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Observation module.");
        
        //Observation tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Observations_tab())) {
            error = "Failed to wait for Observation tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Observations_tab())) {
            error = "Failed to click on Observation tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Observation tab.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Observations_Add())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Observations_Add())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean Capture_Observations_Details() {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Observations_ProcessFlow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Observations_ProcessFlow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //Business Unit 
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.BusinessUnit_Dropdown())) {
            error = "Failed to wait for Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.BusinessUnit_Dropdown())) {
            error = "Failed to click the Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.BusinessUnit_Option(getData("Business Unit 1")))) {
            error = "Failed to wait for Business Unit option: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.BusinessUnit_Option(getData("Business Unit 1")))) {
            error = "Failed to click the Business Unit option: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.BusinessUnit_Option(getData("Business Unit 2")))) {
            error = "Failed to click the Business Unit option: " + getData("Business Unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.BusinessUnit_Option1(getData("Business Unit 3")))) {
            error = "Failed to click the Business Unit option: " + getData("Business Unit 3");
            return false;
        }
        narrator.stepPassedWithScreenShot("Business Unit option: " + getData("Business Unit 1") + " -> " + getData("Business Unit 2") + " -> " + getData("Business Unit 3"));

        //Impact Type    
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.impactType())) {
            error = "Failed to click on Impact type";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.impactTypeSelect(getData("Impact Type")))) {
            error = "Failed to wait for Impact type option :" + getData("Impact Type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.impactTypeSelect(getData("Impact Type")))) {
            error = "Failed to click on Impact type option :" + getData("Impact Type");
            return false;
        }
        narrator.stepPassedWithScreenShot("Impact type option :" + getData("Impact Type"));

        //Date observed
        if (!SeleniumDriverInstance.enterTextByXpath(Observations_V5_2_PageObjects.Date_observed(), startDate)) {
            error = "Failed to enter start date: " + startDate;
            return false;
        }
        narrator.stepPassedWithScreenShot("start date: " + startDate);

        //Time observed
        if (!SeleniumDriverInstance.enterTextByXpath(Observations_V5_2_PageObjects.Time_observed(), endDate)) {
            error = "Failed to enter end date :" + endDate;
            return false;
        }
        narrator.stepPassedWithScreenShot("end date: " + endDate);

        //Observation
        if (!SeleniumDriverInstance.enterTextByXpath(Observations_V5_2_PageObjects.Observation(), getData("Observation"))) {
            error = "Failed to enter Observation :" + getData("Observation");
            return false;
        }
        narrator.stepPassedWithScreenShot("Observation: " + getData("Observation"));

        //Observation logged by
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Observation_logged_by())) {
            error = "Failed to click Observation logged by";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Observation_logged_by_Select(getData("Observation logged by")))) {
            error = "Failed to wait for IObservation logged by option :" + getData("Observation logged by");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Observation_logged_by_Select(getData("Observation logged by")))) {
            error = "Failed to click Observation logged by option :" + getData("Observation logged by");
            return false;
        }
        narrator.stepPassedWithScreenShot("Observation logged by: " + getData("Observation logged by"));

        //Type of observation
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Type_of_observation())) {
            error = "Failed to wait for Type of observation";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Type_of_observation())) {
            error = "Failed to click Type of observation";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.Type_of_observation(getData("Type of observation")))) {
            error = "Failed to wait for Type of observation";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Type_of_observation(getData("Type of observation")))) {
            error = "Failed to click for Type of observation :" + getData("Type of observation");
            return false;
        }
        narrator.stepPassedWithScreenShot("Type of observation: " + getData("Type of observation"));

        //Risk sources
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.Risk_sources())) {
            error = "Failed to click Select all";
            return false;
        }

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.saveBtn())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Observations_V5_2_PageObjects.saveBtn())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }

        //Saving mask
        if (SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Observations_V5_2_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }

        //Validation
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.inspection_Record_Saved_popup())) {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Observations_V5_2_PageObjects.inspection_Record_Saved_popup());
        } else {
            if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.inspection_Record_Saved_popup())) {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Successfully clicked save");
        } 
        else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Observations_V5_2_PageObjects.failed())) {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Observations_V5_2_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }
        
        return true;
    }//end og method 
}//end oF class
