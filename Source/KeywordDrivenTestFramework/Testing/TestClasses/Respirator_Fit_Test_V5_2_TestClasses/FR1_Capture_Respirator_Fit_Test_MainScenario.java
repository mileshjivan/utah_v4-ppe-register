/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Respirator_Fit_Test_V5_2_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Production_Register_V5_2_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Planned_Task_Observations_V5_2_PageObjetcs.Planned_Task_Observations_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Production_Register_V5_2_PageObjects.Production_Register_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Respirator_Fit_Test_V5_2_PageObjects.Respirator_Fit_Test_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Respirator Fit Test v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Respirator_Fit_Test_MainScenario extends BaseClass
{

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Respirator_Fit_Test_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("MM-dd-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("MM-dd-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Navigate_To_Production_Register())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Capture_Production_Register())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Production Register");
    }

    public boolean Navigate_To_Production_Register()
    {
        //Occupational Hygiene
        if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.OccupationalHygene_Module()))
        {
            error = "Failed to wait for 'Occupational Hygiene' module.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.OccupationalHygene_Module()))
        {
            error = "Failed to click on 'Occupational Hygiene' module.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Occupational Hygiene' module.");

        //Respirator Fit Test
        if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.RespiratorFitTest_Module()))
        {
            error = "Failed to wait for 'Respirator Fit Test' module.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.RespiratorFitTest_Module()))
        {
            error = "Failed to click on 'Respirator Fit Test' module.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Respirator Fit Test' module.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.RespiratorFitTest_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.RespiratorFitTest_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean Capture_Production_Register()
    {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.RespiratorFitTest_ProcessFlow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.RespiratorFitTest_ProcessFlow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //Business Unit 
        if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.BusinessUnitTab()))
        {
            error = "Failed to wait for Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.BusinessUnitTab()))
        {
            error = "Failed to click the Business Unit dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.BusinessUnit_Option(getData("Business Unit 1"))))
        {
            error = "Failed to wait for Business Unit 1 Option: " + getData("Business Unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.BusinessUnit_Option(getData("Business Unit 1"))))
        {
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 1");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business Unit: " + getData("Business Unit 1"));

        //Test date
        if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.TestDate()))
        {
            error = "Failed to wait for Test date field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Respirator_Fit_Test_PageObjects.TestDate(), endDate))
        {
            error = "Failed to enter '" + getData("Test date") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + endDate + "'.");

        if (getData("Test result").equalsIgnoreCase("Passed"))
        {
             //Test result 
            if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.TestResult()))
            {
                error = "Failed to wait for Test result dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.TestResult()))
            {
                error = "Failed to click the Test result dropdown.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.BusinessUnit_Option(getData("Test result"))))
            {
                error = "Failed to wait for Test result Option: " + getData("Test result");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.BusinessUnit_Option(getData("Test result"))))
            {
                error = "Failed to click the Test result Option: " + getData("Test result");
                return false;
            }

            narrator.stepPassedWithScreenShot("Test result: " + getData("Test result"));
            
            //Name 
            if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.Name()))
            {
                error = "Failed to wait for Name dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.Name()))
            {
                error = "Failed to click the Name dropdown.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.BusinessUnit_Option(getData("Name"))))
            {
                error = "Failed to wait for Name Option: " + getData("Name");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.BusinessUnit_Option(getData("Name"))))
            {
                error = "Failed to click the Name Option: " + getData("Name");
                return false;
            }

            narrator.stepPassedWithScreenShot("Name: " + getData("Name"));

            //SEG 
            if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.SEG()))
            {
                error = "Failed to wait for SEG dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.SEG()))
            {
                error = "Failed to click the SEG dropdown.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.BusinessUnit_Option(getData("SEG"))))
            {
                error = "Failed to wait for SEG Option: " + getData("SEG");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.BusinessUnit_Option(getData("SEG"))))
            {
                error = "Failed to click the SEG Option: " + getData("SEG");
                return false;
            }

            narrator.stepPassedWithScreenShot("SEG: " + getData("SEG"));

            //Respirator type 
            if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.RespiratorType()))
            {
                error = "Failed to wait for Respirator type dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.RespiratorType()))
            {
                error = "Failed to click the Respirator type dropdown.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.BusinessUnit_Option(getData("Respirator type"))))
            {
                error = "Failed to wait for Respirator type Option: " + getData("Respirator type");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.BusinessUnit_Option(getData("Respirator type"))))
            {
                error = "Failed to click the Year Option: " + getData("Respirator type");
                return false;
            }

            narrator.stepPassedWithScreenShot("Respirator type: " + getData("Respirator type"));

            //Brand/model
            if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.BrandModel()))
            {
                error = "Failed to wait for Brand/model field.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Respirator_Fit_Test_PageObjects.BrandModel(), getData("Brand/model")))
            {
                error = "Failed to enter '" + getData("Brand/model") + "'.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Brand/model") + "'.");
        } else
        {
            
             //Test result 
            if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.TestResult()))
            {
                error = "Failed to wait for Test result dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.TestResult()))
            {
                error = "Failed to click the Test result dropdown.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.BusinessUnit_Option(getData("Test result"))))
            {
                error = "Failed to wait for Test result Option: " + getData("Test result");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.BusinessUnit_Option(getData("Test result"))))
            {
                error = "Failed to click the Test result Option: " + getData("Test result");
                return false;
            }

            narrator.stepPassedWithScreenShot("Test result: " + getData("Test result"));
            
            //Name 
            if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.Name()))
            {
                error = "Failed to wait for Name dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.Name()))
            {
                error = "Failed to click the Name dropdown.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.BusinessUnit_Option(getData("Name"))))
            {
                error = "Failed to wait for Name Option: " + getData("Name");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.BusinessUnit_Option(getData("Name"))))
            {
                error = "Failed to click the Name Option: " + getData("Name");
                return false;
            }

            narrator.stepPassedWithScreenShot("Name: " + getData("Name"));

            //SEG 
            if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.SEG()))
            {
                error = "Failed to wait for SEG dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.SEG()))
            {
                error = "Failed to click the SEG dropdown.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.BusinessUnit_Option(getData("SEG"))))
            {
                error = "Failed to wait for SEG Option: " + getData("SEG");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.BusinessUnit_Option(getData("SEG"))))
            {
                error = "Failed to click the SEG Option: " + getData("SEG");
                return false;
            }

            narrator.stepPassedWithScreenShot("SEG: " + getData("SEG"));

            //Respirator type 
            if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.RespiratorType()))
            {
                error = "Failed to wait for Respirator type dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.RespiratorType()))
            {
                error = "Failed to click the Respirator type dropdown.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.BusinessUnit_Option(getData("Respirator type"))))
            {
                error = "Failed to wait for Respirator type Option: " + getData("Respirator type");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.BusinessUnit_Option(getData("Respirator type"))))
            {
                error = "Failed to click the Year Option: " + getData("Respirator type");
                return false;
            }

            narrator.stepPassedWithScreenShot("Respirator type: " + getData("Respirator type"));

            //Brand/model
            if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.BrandModel()))
            {
                error = "Failed to wait for Brand/model field.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Respirator_Fit_Test_PageObjects.BrandModel(), getData("Brand/model")))
            {
                error = "Failed to enter '" + getData("Brand/model") + "'.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Brand/model") + "'.");

            //Test result comments
            if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.TestResultComments()))
            {
                error = "Failed to wait for Test result comments field.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Respirator_Fit_Test_PageObjects.TestResultComments(), getData("Test result comments")))
            {
                error = "Failed to enter '" + getData("Test result comments") + "'.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Test result comments") + "'.");

        }

        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Respirator_Fit_Test_PageObjects.RespiratorFitTest_Save()))
        {
            error = "Failed to wait for Save Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Respirator_Fit_Test_PageObjects.RespiratorFitTest_Save()))
        {
            error = "Failed to click on Save Button";
            return false;
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Respirator_Fit_Test_PageObjects.saveWait2(), 400))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Respirator_Fit_Test_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Respirator_Fit_Test_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        return true;
    }

}
