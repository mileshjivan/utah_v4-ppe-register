/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Communication_Groups_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import KeywordDrivenTestFramework.Testing.PageObjects.Communication_Groups_V5_2_PageObjects.Communication_Groups_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */

@KeywordAnnotation(
        Keyword = "FR1-Capture Communication Groups - Main scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Communication_Groups_Main_Scenario extends BaseClass{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR1_Capture_Communication_Groups_Main_Scenario(){
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime()); 
    }

    public TestResult executeTest(){
        if (!Navigate_To_Communication_Groups()){
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Capture_Communication_Groups()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Successfully Captured Communication Groups");
    }
    
    public boolean Navigate_To_Communication_Groups(){
        //Navigate to Occupational Health
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Groups_PageObjects.Environmen_Health_Safety_Module())){
            error = "Failed to wait for 'Occupational Health and Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Groups_PageObjects.Environmen_Health_Safety_Module())){
            error = "Failed to click on 'Occupational Health and Safety' tab.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Occupational Health and Safety' tab.");
        
        //Support Maintenance
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Groups_PageObjects.SupportMaintenance_Module())){
            error = "Failed to wait for 'Support Maintenance' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Groups_PageObjects.SupportMaintenance_Module())){
            error = "Failed to click on 'Support Maintenance' tab.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Support Maintenance' tab.");
        
        //Navigate to Communication Manager
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Groups_PageObjects.CommunicationGroup_Module())){
            error = "Failed to wait for 'Communication Module' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Groups_PageObjects.CommunicationGroup_Module())){
            error = "Failed to click on 'Communication Module' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Communication Module' search page.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Groups_PageObjects.Communication_Groups_Add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Groups_PageObjects.Communication_Groups_Add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean Capture_Communication_Groups(){
        //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Groups_PageObjects.CommunicationGroup_ProcessFlow())){
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Groups_PageObjects.CommunicationGroup_ProcessFlow())){
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        
        //Group Name 
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Groups_PageObjects.GroupName())){
            error = "Failed to wait for Group Name field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Communication_Groups_PageObjects.GroupName(), getData("Group Name"))){
            error = "Failed to enter Group Name field: " + getData("Group Name");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Group Name: " + getData("Group Name"));
        
        //Job Profiles 
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Groups_PageObjects.JobProfiles_Dropdown())){
            error = "Failed to wait for Job Profiles dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Groups_PageObjects.JobProfiles_SelectAll())){
            error = "Failed to wait for the Job Profiles options.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Groups_PageObjects.JobProfiles_SelectAll())){
            error = "Failed to click the Job Profiles options.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected all Job Profiles...");
        
        //Custom emails
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Groups_PageObjects.CustomEmails())){
            error = "Failed to wait for Custom emails field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Communication_Groups_PageObjects.CustomEmails(), getData("Custom emails"))){
            error = "Failed to enter Custom emails field: " + getData("Custom emails");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Custom emails: " + getData("Custom emails"));
        
        //Save
        if(!SeleniumDriverInstance.waitForElementByXpath(Communication_Groups_PageObjects.CommunicationGroups_Save())){
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Communication_Groups_PageObjects.CommunicationGroups_Save())){
            error = "Failed to click on 'Save' button.";
            return false;
        }
        
        //Saving mask
        if (SeleniumDriverInstance.waitForElementByXpath(Communication_Groups_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Communication_Groups_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        
        //Validation
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Communication_Groups_PageObjects.inspection_Record_Saved_popup())){
            saved = SeleniumDriverInstance.retrieveTextByXpath(Communication_Groups_PageObjects.inspection_Record_Saved_popup());
        } else {
            if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Groups_PageObjects.inspection_Record_Saved_popup())){
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Successfully clicked save");
        } else {
            if (!SeleniumDriverInstance.waitForElementByXpath(Communication_Groups_PageObjects.failed())){
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Communication_Groups_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved")){
                error = "Failed to save record.";
                return false;
            }
        }
        
        return true;
    }

}
