/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Equipment_and_Tool_Category_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Equipment_and_Tool_Category_V5_2_PageObjects.Equipment_and_Tool_Category_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR3-Capture Asset Schedule v5.2 - Main Scenario",
        createNewBrowserInstance = false
)
public class FR3_Capture_Asset_Schedule_MainScenario extends BaseClass
{

    String error = "";

    public FR3_Capture_Asset_Schedule_MainScenario()
    {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Service_History())
        {
            return narrator.testFailed("Service History Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Capture Asset Schedule");
    }

    public boolean Service_History()
    {
        //Service History        
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Service_History_Tab()))
        {
            error = "Failed to wait for Service History Tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Service_History_Tab()))
        {
            error = "Failed to click on Service History Tab";
            return false;
        }
        //Asset Schedule
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Asset_Schedule_Add()))
        {
            error = "Failed to wait for Asset Schedule add button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Asset_Schedule_Add()))
        {
            error = "Failed to click on Asset Schedule add button";
            return false;
        }
        
        
            //Process flow
            if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.process_Asset()))
            {
                error = "Failed to wait for 'Process flow' button.";
                return false;
            }
            pause(2000);
            if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.process_Asset()))
            {
                error = "Failed to click on 'Process flow' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");

        //Schedule reference
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Schedule_reference()))
        {
            error = "Failed to wait for Schedule reference";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.Schedule_reference(), getData("Schedule reference")))
        {
            error = "Failed to enter Schedule reference";
            return false;
        }
        //Start date

        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.Schedule_Start_date(), startDate))
        {
            error = "Failed to enter Start date";
            return false;
        }
        //End date
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.Schedule_End_date(), endDate))
        {
            error = "Failed to enter End date";
            return false;
        }
        //Availability
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Availability_drop_down()))
        {
            error = "Failed to wait for Availability drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Availability_drop_down()))
        {
            error = "Failed to click on Availability drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.service_required_drop_down_option(getData("Availability"))))
        {
            error = "Failed to wait for Availability drop down option";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.service_required_drop_down_option(getData("Availability"))))
        {
            error = "Failed to click on Availability drop down";
            return false;
        }

        //Save button        
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Save_Button_Equipment()))
        {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Save_Button_Equipment()))
        {
            error = "Failed to click on 'Save' button.";
            return false;
        }

        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Equipment_and_Tool_Category_PageObjects.saveWait2()))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Equipment_and_Tool_Category_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Equipment_and_Tool_Category_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        //pause(5000);
        //close 
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.closeBtn()))
        {
            error = "Failed to wait for close button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.closeBtn()))
        {
            error = "Failed to click on close button";
            return false;
        }
        //validate record saved 
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Asset_Schedule_Add()))
        {
            error = "Failed to wait for Asset Schedule add button";
            return false;
        }

        String Schedule_reference = SeleniumDriverInstance.retrieveTextByXpath(Equipment_and_Tool_Category_PageObjects.Schedule_reference_name());
        if (Schedule_reference.equalsIgnoreCase(getData("Schedule reference")))
        {
            narrator.stepPassedWithScreenShot("Successfully Schedule Asset");

        } else
        {
            narrator.stepPassedWithScreenShot("Failed to Schedule Asset");
            error = "Failed to Schedule Asset";
            return false;

        }

        narrator.stepPassed("Schedule reference :" +getData("Schedule reference"));
        narrator.stepPassed("Availability :" + getData("Availability"));
        narrator.stepPassed("Start Date :" + startDate);
        narrator.stepPassed("End Date :" +endDate);
        return true;
     
    }

}
