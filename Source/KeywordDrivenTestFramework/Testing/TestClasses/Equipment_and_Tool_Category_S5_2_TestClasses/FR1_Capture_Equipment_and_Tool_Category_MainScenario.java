/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Equipment_and_Tool_Category_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Equipment_and_Tool_Category_V5_2_PageObjects.Equipment_and_Tool_Category_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Equipment and Tool Category v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Equipment_and_Tool_Category_MainScenario extends BaseClass {
    String error = "";

    public FR1_Capture_Equipment_and_Tool_Category_MainScenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Navigate_To_Equipment_and_Tool()) {
            return narrator.testFailed("Navigate To Equipment and Tool Failed due - " + error);
        }
        if (!Capture_Equipment_Tools()) {
            return narrator.testFailed("Capture Equipment Tools Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured Equipment and Tools.");
    }

    public boolean Navigate_To_Equipment_and_Tool() {
        //Navigate to Environmental Health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.navigate_EHS())) {
            error = "Failed to wait for 'Environmental, Health & Safety' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.navigate_EHS())) {
            error = "Failed to click on 'Environmental, Health & Safety' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' button.");

        //Navigate to plan maintenance
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Navigate_Plan_Maintenance())) {
            error = "Failed to wait for plan maintenance wheel";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Navigate_Plan_Maintenance())) {
            error = "Failed to click on plan maintenance wheel";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to  plan maintenance");

        //Navigate to Equipment & Tools
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Equipment_Tools_Tab())) {
            error = "Failed to wait for Equipment & Tools tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Equipment_Tools_Tab())) {
            error = "Failed to click on Equipment & Tools tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on Equipment & Tools tab.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.SE_add())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.SE_add())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean Capture_Equipment_Tools() {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.process_flow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.process_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");

        //Business unit
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Business_unit())) {
            error = "Failed to wait for business unit";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Business_unit())) {
            error = "Failed to wait for business unit";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully business unit.");

        //Process Activity
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Process_Activity())) {
            error = "Failed to wait for Process Activity drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Process_Activity())) {
            error = "Failed to click Process Activity drop down";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected Process Activity");

        //Impact type
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Impact_type_drop_down())) {
            error = "Failed to wait for Impact type dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Impact_type_drop_down())) {
            error = "Failed to click on Impact type dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Impact_type_drop_down_option(getData("Impact type")))) {
            error = "Failed to wait for Impact type option :" + getData("Impact type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Impact_type_drop_down_option(getData("Impact type")))) {
            error = "Failed to click on Impact type option :" + getData("Impact type");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on Impact type option :" + getData("Impact type"));

        //Category
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Category_drop_down())) {
            error = "Failed to wait for Category dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Category_drop_down())) {
            error = "Failed to click on Category dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Category_drop_down_option(getData("Category")))) {
            error = "Failed to wait for Category option :" + getData("Category");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Category_drop_down_option(getData("Category")))) {
            error = "Failed to click on Category option :" + getData("Category");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on Category option :" + getData("Category"));

        if(getData("Save to continue").equalsIgnoreCase("Yes")){
            //Save to continue button        
            if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.SaveToContinue_Btn())) {
                error = "Failed to wait for 'Save to continue' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.SaveToContinue_Btn())) {
                error = "Failed to click on 'Save to continue' button.";
                return false;
            }
        } else {
            //Save button        
            if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.SaveBtn())) {
                error = "Failed to wait for 'Save' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.SaveBtn())) {
                error = "Failed to click on 'Save' button.";
                return false;
            }
        }

        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Equipment_and_Tool_Category_PageObjects.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Equipment_and_Tool_Category_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Equipment_and_Tool_Category_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        pause(500);

        return true;
    }
}
