/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Equipment_and_Tool_Category_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Equipment_and_Tool_Category_V5_2_PageObjects.Equipment_and_Tool_Category_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR6-View Permitted Personnel v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR6_View_Permitted_Personnel_MainScenario extends BaseClass {
    String error = "";

    public FR6_View_Permitted_Personnel_MainScenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!View_Permitted_Personnel()) {
            return narrator.testFailed("Asset Details Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Viewed Permitted Personnel.");
    }

    public boolean View_Permitted_Personnel() {
        //Asset Details
        if(!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.AssestDetails())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Equipment_and_Tool_Category_PageObjects.AssestDetails())){
                error = "Failed to wait for the Asset Details.";
                return false;
            }
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.AssestDetails())){
            error = "Failed to wait for the Asset Details.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Asset details record ");
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.process_flow_Equipment())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.process_flow_Equipment())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");
        
        //Navigate to Required Permits
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.RequiredPermits_Tab())) {
            error = "Failed to wait for Required Permits tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.RequiredPermits_Tab())) {
            error = "Failed to click Required Permits tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Required Permits tab.");
        
        //Permitted Personnel
        if(!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.PermittedPersonnel_Panel())){
            error = "Failed to wait for the permmitted personnel panel.";
            return false;
        }
        
        //Permitted Personnel record
        if(!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.PermittedPersonnel_Record(getData("Impact type")))){
            error = "Failed to wait for the permmitted personnel record.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.PermittedPersonnel_Record(getData("Impact type")))){
            error = "Failed to click the permmitted personnel record.";
            return false;
        }
        
        //Mfr. serial No / VIN No
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.VIN(), getData("VIN Nr"))) {
            error = "Failed to enter Mfr. serial No / VIN No";
            return false;
        }
        //Rego/asset no
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.Reg(), getData("Rego/asset no"))) {
            error = "Failed to enter Date on-site";
            return false;
        }
        //License to operate
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.License(), getData("License"))) {
            error = "Failed to enter License";
            return false;
        }
        //Date on-site
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Date_on_site())) {
            error = "Failed to wait for Date on-site";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.Date_on_site(), startDate)) {
            error = "Failed to enter Date on-site";
            return false;
        }

        //Last routine maintenance
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Last_routine_maintenance())) {
            error = "Failed to wait for Last routine maintenance";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.Last_routine_maintenance(), startDate)) {
            error = "Failed to enter Last routine maintenance";
            return false;
        }

        //PHA date
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.PHA_date())) {
            error = "Failed to wait for PHA date";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.PHA_date(), startDate)) {
            error = "Failed to enter PHA date";
            return false;
        }

        //Date off-site
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Date_off_site())) {
            error = "Failed to wait for Date off-site";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.Date_off_site(), startDate)) {
            error = "Failed to enter Date off-site";
            return false;
        }

        //Next routine maintenance
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Next_routine_maintenance())) {
            error = "Failed to wait for Next routine maintenance";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.Next_routine_maintenance(), startDate)) {
            error = "Failed to enter Next routine maintenance";
            return false;
        }
        //Date check sheet completed
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Date_check_sheet_completed())) {
            error = "Failed to wait for Date check sheet completed";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.Date_check_sheet_completed(), startDate)) {
            error = "Failed to enter Date check sheet completed";
            return false;
        }

        if (getData("Execute").equalsIgnoreCase("True")) {
            boolean results = Supporting_Documents();
            return results;

        } else {
            //Save button        
            if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Save_Button())) {
                error = "Failed to wait for 'Save' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Save_Button())) {
                error = "Failed to click on 'Save' button.";
                return false;
            }

            //Saving mask
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Equipment_and_Tool_Category_PageObjects.saveWait2())) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }

            //Validate if the record has been saved or not.
            if (!SeleniumDriverInstance.waitForElementsByXpath(Equipment_and_Tool_Category_PageObjects.validateSave())) {
                error = "Failed to wait for Save validation.";
                return false;
            }
            String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Equipment_and_Tool_Category_PageObjects.validateSave());

            if (!SaveFloat.equals("Record saved")) {
                narrator.stepPassedWithScreenShot("Failed to save record.");
                return false;
            }
            narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

            pause(2000);
        }

        return true;
    }

    public boolean Supporting_Documents() {
        //Supporting Documents tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.supporting_tab())) {
            error = "Failed to wait for Supporting Documents tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.supporting_tab())) {
            error = "Failed to click Supporting Documents tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to supporting documents.");

        //Add support document - Hyperlink
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.linkbox())) {
            error = "Failed to wait for 'Link box' link.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.linkbox())) {
            error = "Failed to click on 'Link box' link.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");

        //switch to new window
        if (!SeleniumDriverInstance.switchToTabOrWindow()) {
            error = "Failed to switch to new window or tab.";
            return false;
        }

        //URL https
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.LinkURL())) {
            error = "Failed to wait for 'URL value' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.LinkURL(), getData("Document Link"))) {
            error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.urlTitle())) {
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.urlTitle(), getData("Title"))) {
            error = "Failed to enter Url Title :" + getData("Title");
            return false;
        }
        narrator.stepPassedWithScreenShot("Title : '" + getData("Title") + "'.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.urlAddButton())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.urlAddButton())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Equipment_and_Tool_Category_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");

        //Save supporting documents button        
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.SaveSupportingDocuments_SaveBtn())) {
            error = "Failed to wait for 'Save supporting documents' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.SaveSupportingDocuments_SaveBtn())) {
            error = "Failed to click on 'Save supporting documents' button.";
            return false;
        }

        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Equipment_and_Tool_Category_PageObjects.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Equipment_and_Tool_Category_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.processFlow_PersonalOperatingPermits())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");
       
        return true;
    }

}
