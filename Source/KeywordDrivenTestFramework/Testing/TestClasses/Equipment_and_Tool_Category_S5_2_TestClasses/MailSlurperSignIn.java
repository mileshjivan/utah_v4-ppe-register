/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Equipment_and_Tool_Category_S5_2_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Permit_To_Work_S5_2_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.MailSlurper_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Permit_To_Work_V5_2_PageObjects.Permit_To_Work_PageObjects;

/**
 *
 * @author MJivan
 */
@KeywordAnnotation(
        Keyword = "Sign In To MailSlurper v5.2 - Equipment and Tool",
        createNewBrowserInstance = true
)
public class MailSlurperSignIn extends BaseClass
{

    String parentWindow;
    String error = "";

    public MailSlurperSignIn()
    {

    }

    public TestResult executeTest()
    {
        if (!NavigateToMailSlurperSignInPage())
        {
            return narrator.testFailed("Failed to navigate to MailSlurper Sign In Page");
        }

        // This step will sign into the specified gmail account with the provided credentials
        if (!SignInToIsomoterics())
        {
            return narrator.testFailed("Failed to sign into the MailSlurper Home Page");
        }
        //This step will click the newly added record
        if (!clickEmailLink())
        {
            return narrator.testFailed(error + ": Failed to click record link");
        }

        return narrator.finalizeTest("Successfully Navigated through the Isometrix web page");
    }

    public boolean NavigateToMailSlurperSignInPage()
    {
//        if (!SeleniumDriverInstance.navigateTo(MailSlurper_PageObjects.mailSlurper())) {
//            error = "Failed to navigate to MailSlurper Home Page.";
//            return false;
//        }
        if (!SeleniumDriverInstance.navigateTo(getData("URL")))
        {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        return true;
    }

    public boolean SignInToIsomoterics()
    {
        if (!SeleniumDriverInstance.enterTextByXpath(MailSlurper_PageObjects.Username(), testData.getData("Username")))
        {
            error = "Failed to enter text into email text field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Added the Username: " + testData.getData("Username") + " to the Username Text Field");

        if (!SeleniumDriverInstance.enterTextByXpath(MailSlurper_PageObjects.Password(), testData.getData("Password")))
        {
            error = "Failed to enter text into email text field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Added the Password: " + testData.getData("Password") + " to the Password Text Field");

        if (!SeleniumDriverInstance.clickElementbyXpath(MailSlurper_PageObjects.LoginBtn()))
        {
            error = "Failed to click sign in button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Clicked The Sign In Button");
        pause(5000);

        return true;
    }

    public boolean clickEmailLink()
    {

        if (!getData("Status").equalsIgnoreCase("Expired"))
        {

            //Click the newly added record
            if (!SeleniumDriverInstance.waitForElementByXpath(MailSlurper_PageObjects.recordLink()))
            {
                error = "Failed to wait for the record link.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(MailSlurper_PageObjects.recordLink()))
            {
                error = "Failed to click on the record link.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully Clicked The Record Link: " + SeleniumDriverInstance.retrieveTextByXpath(MailSlurper_PageObjects.recordLink()));

            pause(1000);

        } else
        {
            //Search button
            if (!SeleniumDriverInstance.waitForElementByXpath(MailSlurper_PageObjects.searchBtn()))
            {
                error = "Failed to wait for Search button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(MailSlurper_PageObjects.searchBtn()))
            {
                error = "Failed to click on Search button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully Clicked Search button");

            SeleniumDriverInstance.switchToTabOrWindow();

            if (!SeleniumDriverInstance.enterTextByXpath(MailSlurper_PageObjects.recordNo(), testData.getData("Record number")))
            {
                error = "Failed to enter text into search text field.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully entered the Record number: " + testData.getData("Record number") + " to the search Text Field");

            //Search button
            if (!SeleniumDriverInstance.waitForElementByXpath(MailSlurper_PageObjects.search_Btn()))
            {
                error = "Failed to wait for Search button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(MailSlurper_PageObjects.search_Btn()))
            {
                error = "Failed to click on Search button.";
                return false;
            }

            pause(3000);

            narrator.stepPassedWithScreenShot("Successfully Clicked Search button");

            //SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
            //Click the newly added record
            if (!SeleniumDriverInstance.waitForElementByXpath(MailSlurper_PageObjects.recordLink()))
            {
                error = "Failed to wait for the record link.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(MailSlurper_PageObjects.recordLink()))
            {
                error = "Failed to click on the record link.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully Clicked The Record Link: " + SeleniumDriverInstance.retrieveTextByXpath(MailSlurper_PageObjects.recordLink()));
            //Link back to record
            if (!SeleniumDriverInstance.waitForElementByXpath(MailSlurper_PageObjects.linkBackToRecord()))
            {
                error = "Failed to wait Link back to record.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(MailSlurper_PageObjects.linkBackToRecord()))
            {
                error = "Failed to click on Link back to record.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully Clicked Link back to record");
            if (!SeleniumDriverInstance.enterTextByXpath(IsometricsPOCPageObjects.Username(), testData.getData("UserName")))
            {
                error = "Failed to enter text into email text field.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully Added the Username: " + testData.getData("UserName") + " to the Username Text Field");

            if (!SeleniumDriverInstance.enterTextByXpath(IsometricsPOCPageObjects.Password(), testData.getData("PassWord")))
            {
                error = "Failed to enter text into email text field.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully Added the Password: " + testData.getData("PassWord") + " to the Password Text Field");

            if (!SeleniumDriverInstance.clickElementbyXpath(IsometricsPOCPageObjects.LoginBtn()))
            {
                error = "Failed to click sign in button.";
                return false;
            }

            pause(3000);

            narrator.stepPassedWithScreenShot("Successfully Clicked Sign in button");

            //switch to the iframe
            if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.iframeXpath()))
            {
                error = "Failed to switch to frame.";
            }
            if (!SeleniumDriverInstance.switchToFrameByXpath(Permit_To_Work_PageObjects.iframeXpath()))
            {
                error = "Failed to switch to frame.";
            }
            narrator.stepPassedWithScreenShot("Successfully switched the iframe.");

            if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.PermitToWork_ProcessFlow()))
            {
                error = "Failed to wait for 'Process flow' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.PermitToWork_ProcessFlow()))
            {
                error = "Failed to click on 'Process flow' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        }

        return true;

    }

}
