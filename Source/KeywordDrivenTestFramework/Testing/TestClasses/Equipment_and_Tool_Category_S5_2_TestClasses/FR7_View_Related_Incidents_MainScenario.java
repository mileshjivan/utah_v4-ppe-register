/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Equipment_and_Tool_Category_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Equipment_and_Tool_Category_V5_2_PageObjects.Equipment_and_Tool_Category_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR7-View Related Incidents v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR7_View_Related_Incidents_MainScenario extends BaseClass {
    String error = "";

    public FR7_View_Related_Incidents_MainScenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!View_Related_Incidents()) {
            return narrator.testFailed("Asset Details Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully View Related Incidents.");
    }

    public boolean View_Related_Incidents() {
        //Asset Details
        if(!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.AssestDetails())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Equipment_and_Tool_Category_PageObjects.AssestDetails())){
                error = "Failed to wait for the Asset Details.";
                return false;
            }
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.AssestDetails())){
            error = "Failed to wait for the Asset Details.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Asset details record ");
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.process_flow_Equipment())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.process_flow_Equipment())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");
        
        //Navigate to Related Incidents, Risks, Inspections & Hygiene Monitoring
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.RelatedIncidentsRisksInspectionsHygieneMonitoring_Tab())) {
            error = "Failed to wait for Related Incidents, Risks, Inspections & Hygiene Monitoring tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.RelatedIncidentsRisksInspectionsHygieneMonitoring_Tab())) {
            error = "Failed to click Related Incidents, Risks, Inspections & Hygiene Monitoring tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Related Incidents, Risks, Inspections & Hygiene Monitoring tab.");
        
        //Related Incidents 
        if(!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.RelatedIncidents_Panel())){
            error = "Failed to wait for the Related Incidents panel.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.RelatedIncidents_Panel())){
            error = "Failed to click the Related Incidents panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Related Incidents panel.");
        
        //Search
        if(!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Search())){
            error = "Failed to wait for the Search.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Search())){
            error = "Failed to click the Search.";
            return false;
        }
        
        //Pin
        if(!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.pin())){
            error = "Failed to wait for the pin.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.pin())){
            error = "Failed to click the pin.";
            return false;
        }
        
        //Related Incidents record
        if(!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.PermittedPersonnel_Record(getData("Impact type")))){
            error = "Failed to wait for the Related Incidents record.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.PermittedPersonnel_Record(getData("Impact type")))){
            error = "Failed to click the Related Incidents record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Related Incidents record");
        
        //Personal Operating Permits Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.processFlow_PersonalOperatingPermits())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.processFlow_PersonalOperatingPermits())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");
       
        return true;
    }

}
