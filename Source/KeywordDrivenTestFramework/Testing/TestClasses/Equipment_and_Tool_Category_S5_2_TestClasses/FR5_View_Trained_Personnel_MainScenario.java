/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Equipment_and_Tool_Category_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Equipment_and_Tool_Category_V5_2_PageObjects.Equipment_and_Tool_Category_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR5-View Trained Personnel v5.2 - Main Scenario",
        createNewBrowserInstance = false
)
public class FR5_View_Trained_Personnel_MainScenario extends BaseClass
{

    String error = "";

    public FR5_View_Trained_Personnel_MainScenario()
    {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!View_Trained_Personnel())
        {
            return narrator.testFailed("View Trained Personnel Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Viewed Trained Personnel");
    }

    public boolean View_Trained_Personnel()
    {
        
         //Required Training         
            if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Trained_Personnel_Tab()))
            {
                error = "Failed to wait for Required Training Tab";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Trained_Personnel_Tab()))
            {
                error = "Failed to click on  Required Training Tab";
                return false;
            }
 
        
        
        
        
        
         //Save button        
            if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Save_Button()))
            {
                error = "Failed to wait for 'Save' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Save_Button()))
            {
                error = "Failed to click on 'Save' button.";
                return false;
            }

            //Saving mask
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Equipment_and_Tool_Category_PageObjects.saveWait2()))
            {
                error = "Webside too long to load wait reached the time out";
                return false;
            }

            //Validate if the record has been saved or not.
            if (!SeleniumDriverInstance.waitForElementsByXpath(Equipment_and_Tool_Category_PageObjects.validateSave()))
            {
                error = "Failed to wait for Save validation.";
                return false;
            }
            String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Equipment_and_Tool_Category_PageObjects.validateSave());

            if (!SaveFloat.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Failed to save record.");
                return false;
            }
            narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

            pause(5000);
            return true;
    }

}
