/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Equipment_and_Tool_Category_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Equipment_and_Tool_Category_V5_2_PageObjects.Equipment_and_Tool_Category_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR2-Capture Asset Details v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_Asset_Details_MainScenario extends BaseClass
{

    String error = "";

    public FR2_Capture_Asset_Details_MainScenario()
    {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Asset_Details())
        {
            return narrator.testFailed("Asset Details Failed due - " + error);
        }
        if (getData("Execute").equalsIgnoreCase("True"))
        {
            if (!Supporting_Documents())
            {
                return narrator.testFailed("Asset Details Failed due - " + error);
            }
        }

        return narrator.finalizeTest("Successfully captured Equipment and Tools.");
    }

    public boolean Asset_Details()
    {
        //Navigate to Asset Details
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Asset_Details_Add()))
        {
            error = "Failed to wait for Asset Details";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Equipment_and_Tool_Category_PageObjects.Asset_Details_Add()))
        {
            error = "Failed to scroll to Asset Details Add Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Asset_Details_Add()))
        {
            error = "Failed to click on Asset Details Add Button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Asset Details add button.");

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.process_flow_Equipment()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.process_flow_Equipment()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");

        //Asset
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Asset()))
        {
            error = "Failed to wait for Asset";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.Asset(), getData("Asset")))
        {
            error = "Failed to click on Asset";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Asset :" + getData("Asset"));

        if (getData("Service Required").equalsIgnoreCase("Yes"))
        {
            //Is a service required?
            if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.service_required()))
            {
                error = "Failed to wait for Is a service required ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.service_required()))
            {
                error = "Failed to wait for Is a service required ";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.service_required_drop_down_option(getData("Service Required"))))
            {
                error = "Failed to wait for Is a service required drop down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.service_required_drop_down_option(getData("Service Required"))))
            {
                error = "Failed to wait for Is a service required drop down";
                return false;
            }
            narrator.stepPassed("Service Required :" + getData("Service Required"));

            //Service frequency
            if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Service_frequency()))
            {
                error = "Failed to wait for Service frequency";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.Service_frequency(), getData("Service frequency")))
            {
                error = "Failed to enter Service frequency";
                return false;
            }
            narrator.stepPassedWithScreenShot("Service frequency option: " + getData("Service frequency"));

            // Last date of service
            if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.date_of_service()))
            {
                error = "Failed to wait for Last date of service";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.date_of_service(), startDate))
            {
                error = "Failed to enter Last date of service";
                return false;
            }
            narrator.stepPassedWithScreenShot("Last date of service: " + startDate);

            //Next date of service
            if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.next_date_of_service()))
            {
                error = "Failed to wait for Next date of service";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.next_date_of_service(), startDate))
            {
                error = "Failed to enter Next date of service";
                return false;
            }
            narrator.stepPassedWithScreenShot("Next date of service: " + startDate);
        } else
        {
            //Is a service required?
            if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.service_required()))
            {
                error = "Failed to wait for Is a service required ";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.service_required()))
            {
                error = "Failed to wait for Is a service required ";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.service_required_drop_down_option(getData("Service Required"))))
            {
                error = "Failed to wait for Is a service required drop down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.service_required_drop_down_option(getData("Service Required"))))
            {
                error = "Failed to wait for Is a service required drop down";
                return false;
            }
            narrator.stepPassedWithScreenShot("Service Required option: " + getData("Service Required"));
        }

        if (getData("Has a risk assessment been conducted").equalsIgnoreCase("Yes"))
        {
            //Has a risk assessment been conducted?
            if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.assessment_conducted()))
            {
                error = "Failed to wait for Has a risk assessment been conducted? drop down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.assessment_conducted()))
            {
                error = "Failed to click Has a risk assessment been conducted? drop down";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.service_required_drop_down_option(getData("Has a risk assessment been conducted"))))
            {
                error = "Failed to wait for  Has a risk assessment been conducted drop down option";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.service_required_drop_down_option(getData("Has a risk assessment been conducted"))))
            {
                error = "Failed to click  Has a risk assessment been conducted drop down option";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully selected  Has a risk assessment been conducted: " + getData("Has a risk assessment been conducted"));

            //Related risk assessment
            if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Related_risk_assessment()))
            {
                error = "Failed to wait for Related risk assessment drop down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Related_risk_assessment()))
            {
                error = "Failed to click Related risk assessment drop down";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Related_risk_assessment_drop_down_option(getData("Related risk assessment"))))
            {
                error = "Failed to wait for Related risk assessment drop down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Related_risk_assessment_drop_down_option(getData("Related risk assessment"))))
            {
                error = "Failed to click Related risk assessment drop down";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully selected Related risk assessment: " + getData("Related risk assessment"));
        } else
        {
            //Has a risk assessment been conducted?
            if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.assessment_conducted()))
            {
                error = "Failed to wait for Has a risk assessment been conducted? drop down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.assessment_conducted()))
            {
                error = "Failed to click Has a risk assessment been conducted? drop down";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.service_required_drop_down_option(getData("Has a risk assessment been conducted"))))
            {
                error = "Failed to wait for  Has a risk assessment been conducted drop down option";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.service_required_drop_down_option(getData("Has a risk assessment been conducted"))))
            {
                error = "Failed to click  Has a risk assessment been conducted drop down option";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully selected  Has a risk assessment been conducted: " + getData("Has a risk assessment been conducted"));

        }//end of if else statement 

        //Required training
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Required_training()))
        {
            error = "Failed to wait for Required training drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Required_training()))
        {
            error = "Failed to click Required training drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Required_training_drop_down_option(getData("Required training"))))
        {
            error = "Failed to wait for Required training drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Required_training_drop_down_select_all()))
        {
            error = "Failed to click Required training drop down";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected all Required training ");

        //Requred permit
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Required_permit()))
        {
            error = "Failed to wait for Required permit drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Required_permit()))
        {
            error = "Failed to click Required permit drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Required_permit_drop_down_option(getData("Required permits"))))
        {
            error = "Failed to wait for Required permit drop down option";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Required_permit_drop_down_select_all()))
        {
            error = "Failed to click Required permit drop down option";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected all Required permits");

        //Owner
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Owner()))
        {
            error = "Failed to wait for Owner drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Owner()))
        {
            error = "Failed to click Owner drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.visible_search_text_box()))
        {
            error = "Failed to wait for Owner search text box";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.visible_search_text_box(), getData("Owner")))
        {
            error = "Failed to enter  Owner in search text box :" + getData("Owner");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.owner_drop_down_option(getData("Owner"))))
        {
            error = "Failed to wait for Owner drop down option";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.owner_drop_down_option(getData("Owner"))))
        {
            error = "Failed to click Owner drop down option";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected owner :" + getData("Owner"));

        //Date on site
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.Date_on_site(), startDate))
        {
            error = "Failed to enter date on site: " + startDate;
            return false;
        }
        narrator.stepPassedWithScreenShot("Date on site: " + startDate);

        //Make
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.Make(), getData("Make")))
        {
            error = "Failed to enter Make: " + getData("Make");
            return false;
        }
        narrator.stepPassedWithScreenShot("Make: " + getData("Make"));

        //Model
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.Model(), getData("Model")))
        {
            error = "Failed to enter Model: " + getData("Model");
            return false;
        }
        narrator.stepPassedWithScreenShot("Model: " + getData("Model"));

        //Type
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.Type(), getData("Type")))
        {
            error = "Failed to enter Type: " + getData("Type");
            return false;
        }
        narrator.stepPassedWithScreenShot("Type: " + getData("Type"));

        //Mfr. serial No / VIN No
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.VIN(), getData("VIN Nr")))
        {
            error = "Failed to enter Mfr. serial No / VIN No: " + getData("VIN Nr");
            return false;
        }
        narrator.stepPassedWithScreenShot("VIN Nr: " + getData("VIN Nr"));

        //Rego/asset no
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.Reg(), getData("Rego/asset no")))
        {
            error = "Failed to enter Date on-site: " + getData("Rego/asset no");
            return false;
        }
        narrator.stepPassedWithScreenShot("Rego/asset no: " + getData("Rego/asset no"));

        //License to operate
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.License(), getData("License")))
        {
            error = "Failed to enter License: " + getData("License");
            return false;
        }
        narrator.stepPassedWithScreenShot("License: " + getData("License"));

        //Last routine maintenance
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Last_routine_maintenance()))
        {
            error = "Failed to wait for Last routine maintenance";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.Last_routine_maintenance(), startDate))
        {
            error = "Failed to enter Last routine maintenance";
            return false;
        }
        narrator.stepPassedWithScreenShot("Last routine maintenance: " + startDate);

        //PHA date
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.PHA_date()))
        {
            error = "Failed to wait for PHA date";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.PHA_date(), startDate))
        {
            error = "Failed to enter PHA date";
            return false;
        }
        narrator.stepPassedWithScreenShot("PHA date: " + startDate);

        //Date off-site
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Date_off_site()))
        {
            error = "Failed to wait for Date off-site";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.Date_off_site(), startDate))
        {
            error = "Failed to enter Date off-site";
            return false;
        }
        narrator.stepPassedWithScreenShot("Date off site: " + startDate);

        //Next routine maintenance
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Next_routine_maintenance()))
        {
            error = "Failed to wait for Next routine maintenance";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.Next_routine_maintenance(), startDate))
        {
            error = "Failed to enter Next routine maintenance";
            return false;
        }
        narrator.stepPassedWithScreenShot("Next routine maintenance: " + startDate);

        //Date check sheet completed
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Date_check_sheet_completed()))
        {
            error = "Failed to wait for Date check sheet completed";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.Date_check_sheet_completed(), startDate))
        {
            error = "Failed to enter Date check sheet completed";
            return false;
        }
        narrator.stepPassedWithScreenShot("Date check sheet completed: " + startDate);

        //Save button        
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Save_Button()))
        {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Save_Button()))
        {
            error = "Failed to click on 'Save' button.";
            return false;
        }

        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Equipment_and_Tool_Category_PageObjects.saveWait2()))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Equipment_and_Tool_Category_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Equipment_and_Tool_Category_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        if ((getData("Validate").equalsIgnoreCase("True")))
        {
            //Close button
            if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.closeButton()))
            {
                error = "Failed to wait for the close button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.closeButton()))
            {
                error = "Failed to click the close button.";
                return false;
            }

            //Asset Details
            if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.AssestDetails()))
            {
                if (!SeleniumDriverInstance.waitForElementPresentByXpath(Equipment_and_Tool_Category_PageObjects.AssestDetails()))
                {
                    error = "Failed to wait for the Asset Details.";
                    return false;
                }
            }

            //Process flow
            if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.process_flow()))
            {
                error = "Failed to wait for 'Process flow' button.";
                return false;
            }
            pause(2000);
            if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.process_flow()))
            {
                error = "Failed to click on 'Process flow' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");

        }

        return true;
    }

    public boolean Supporting_Documents()
    {
        //Asset Details
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.AssestDetails()))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Equipment_and_Tool_Category_PageObjects.AssestDetails()))
            {
                error = "Failed to wait for the Asset Details.";
                return false;
            }
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.AssestDetails()))
        {
            error = "Failed to wait for the Asset Details.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Asset details record ");

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.process_flow_Equipment()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.process_flow_Equipment()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");

        //Supporting Documents tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.supporting_tab()))
        {
            error = "Failed to wait for Supporting Documents tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.supporting_tab()))
        {
            error = "Failed to click Supporting Documents tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to supporting documents.");

        //Add support document - Hyperlink
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.linkbox()))
        {
            error = "Failed to wait for 'Link box' link.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.linkbox()))
        {
            error = "Failed to click on 'Link box' link.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");

        //switch to new window
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new window or tab.";
            return false;
        }

        //URL https
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.LinkURL()))
        {
            error = "Failed to wait for 'URL value' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.LinkURL(), getData("Document Link")))
        {
            error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.urlTitle()))
        {
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.urlTitle(), getData("Title")))
        {
            error = "Failed to enter Url Title :" + getData("Title");
            return false;
        }
        narrator.stepPassedWithScreenShot("Title : '" + getData("Title") + "'.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.urlAddButton()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.urlAddButton()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Equipment_and_Tool_Category_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");

        //Save supporting documents button        
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.SaveSupportingDocuments_SaveBtn()))
        {
            error = "Failed to wait for 'Save supporting documents' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.SaveSupportingDocuments_SaveBtn()))
        {
            error = "Failed to click on 'Save supporting documents' button.";
            return false;
        }

        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Equipment_and_Tool_Category_PageObjects.saveWait2()))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Equipment_and_Tool_Category_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Equipment_and_Tool_Category_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        pause(300);
        narrator.stepPassed("Document Link :" + getData("Document Link"));
        narrator.stepPassed("Title :" + getData("Title"));

        //Close button
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.closeButton()))
        {
            error = "Failed to wait for the close button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.closeButton()))
        {
            error = "Failed to click the close button.";
            return false;
        }

        //Asset Details
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.AssestDetails()))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Equipment_and_Tool_Category_PageObjects.AssestDetails()))
            {
                error = "Failed to wait for the Asset Details.";
                return false;
            }
        }

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.process_flow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.process_flow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");

        return true;
    }

}
