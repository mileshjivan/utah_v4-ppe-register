/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Classification_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Classification_V5_2_PageObjects.Incident_Classification;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Incident Classification v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Incident_Classification_MainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Incident_Classification_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!NavigateToEngagements()) {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!CaptureIncidentClassification()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured Incident Classification record.");
    }

    public boolean NavigateToEngagements() {
        //Navigate to Environmental Health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.navigate_EHS())) {
            error = "Failed to wait for 'Environmental, Health & Safety' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.navigate_EHS())) {
            error = "Failed to click on 'Environmental, Health & Safety' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' button.");

        //Navigate to Improve Maintenance
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.navigate_ImproveMaintenance())) {
            error = "Failed to wait for 'Improve Maintenance' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.navigate_ImproveMaintenance())) {
            error = "Failed to click on 'Improve Maintenance' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Improve Maintenance' button.");

        //Navigate to Incident Classification
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.navigate_IncidentClassification())) {
            error = "Failed to wait for 'Incident Classification' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.navigate_IncidentClassification())) {
            error = "Failed to click on 'Incident Classification' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Incident Classification' page.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.addButton())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.addButton())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean CaptureIncidentClassification() {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.process_flow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.process_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");

        //Business unit dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.BusinessUnit_Dropdown())) {
            error = "Failed to wait for Business Unit Dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.BusinessUnit_Dropdown())) {
            error = "Failed to click on Business Unit Dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.BusinessUnit_Option(getData("Business Unit 1")))) {
            error = "Failed to wait for the Business Unit Dropdown Option: '" + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.BusinessUnit_Option(getData("Business Unit 1")))) {
            error = "Failed to click on Business Unit Dropdown Option: '" + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.BusinessUnit_Option(getData("Business Unit 2")))) {
            error = "Failed to click on Business Unit Dropdown Option: '" + getData("Business Unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.BusinessUnit_Option1(getData("Business Unit 3")))) {
            error = "Failed to click on Business Unit Dropdown Option: '" + getData("Business Unit 3");
            return false;
        }        
        narrator.stepPassedWithScreenShot("Successfully selected business unit: " + getData("Business Unit 1") + " -> " + getData("Business Unit 2") + " -> " + getData("Business Unit 3"));

        //Impact type
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.ImpactType_Dropdown())) {
            error = "Failed to wait for Impact type Dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.ImpactType_Dropdown())) {
            error = "Failed to click on Impact type Dropdown";
            return false;
        }
        
        if(getData("Select all impact type").equalsIgnoreCase("Yes")){
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.BusinessUnit_SelectAll())) {
                error = "Failed to click on Impact type Select All";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked impact type select all...");
            
        } else {
            String[] Impact_Type = getData("Impact Type").split(",");

            for(int i = 0; i < Impact_Type.length; i++){
                if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.ImpactType_Option(Impact_Type[i]))) {
                    error = "Failed to wait for the Impact type Dropdown Option: " + Impact_Type[i];
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.ImpactType_Option(Impact_Type[i]))) {
                    error = "Failed to click on Impact type Dropdown Option: " + Impact_Type[i];
                    return false;
                }
                narrator.stepPassedWithScreenShot("Impact type option: " + Impact_Type[i]);
            }
        }
        
       //Incident classification
        if(!SeleniumDriverInstance.enterTextByXpath(Incident_Classification.IncidentClassification_Input(), getData("Incident classification"))){
           error = "Failed to enter Incident classification: " + getData("Incident classification");
           return false;
        }
        narrator.stepPassedWithScreenShot("Incident classification: " + getData("Incident classification"));
       
        if(getData("Save to continue").equalsIgnoreCase("Yes")){
            //Save to continue button        
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.SaveToContinue_SaveBtn())) {
                error = "Failed to wait for 'Save to continue' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.SaveToContinue_SaveBtn())) {
                error = "Failed to click on 'Save to continue' button.";
                return false;
            }
        }else{
            //Save button        
            if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.SaveButton())) {
                error = "Failed to wait for 'Save' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.SaveButton())) {
                error = "Failed to click on 'Save' button.";
                return false;
            }
        }

        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Classification.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Classification.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Incident_Classification.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        return true;
    }
}
