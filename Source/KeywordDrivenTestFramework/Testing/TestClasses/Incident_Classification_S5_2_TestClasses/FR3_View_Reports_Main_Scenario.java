/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Classification_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Classification_V5_2_PageObjects.Incident_Classification;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR3-View Reports v5.2 Incident Classification - Main Scenario",
        createNewBrowserInstance = false
)
public class FR3_View_Reports_Main_Scenario extends BaseClass {
    String parentWindow;
    String error = "";

    public FR3_View_Reports_Main_Scenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());

    }

    public TestResult executeTest() {
        if (!navigateToRelatedInitiatives()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully");
    }

    public boolean navigateToRelatedInitiatives() {
        //Navigate to Environmental Health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.navigate_EHS())) {
            error = "Failed to wait for 'Environmental, Health & Safety' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.navigate_EHS())) {
            error = "Failed to click on 'Environmental, Health & Safety' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' button.");

        //Navigate to Improve Maintenance
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.navigate_ImproveMaintenance())) {
            error = "Failed to wait for 'Improve Maintenance' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.navigate_ImproveMaintenance())) {
            error = "Failed to click on 'Improve Maintenance' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Improve Maintenance' button.");

        //Navigate to Incident Classification
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.navigate_IncidentClassification())) {
            error = "Failed to wait for 'Incident Classification' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.navigate_IncidentClassification())) {
            error = "Failed to click on 'Incident Classification' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Incident Classification' page.");
        
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.searchBtn())) {
            error = "Failed to wait for 'Serach' button.";
            return false;
        }        
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.searchBtn())) {
            error = "Failed to click on 'Serach' button.";
            return false;
        }
        SeleniumDriverInstance.pause(3000);
        narrator.stepPassedWithScreenShot("Successfully click 'Serach' button.");

        //Reports button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.reportsBtn())) {
            error = "Failed to wait for 'Reports' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.reportsBtn())) {
            error = "Failed to click on 'Reports' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Reports' button.");
        parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        //View reports icon
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.viewReportsIcon())) {
            error = "Failed to wait for 'View reports' icon.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.viewReportsIcon())) {
            error = "Failed to click on 'View reports' icon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'View reports' icon.");
        SeleniumDriverInstance.switchToTabOrWindow();
        //Continue button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.continueBtn())) {
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.continueBtn())) {
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        pause(5000);
        SeleniumDriverInstance.switchToTabOrWindow();
              
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.IncidentClassification_Header())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Classification.IncidentClassification_Header())){
                error = "Failed to wait for Incident Classification header to be present.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Continue' button.");
        narrator.stepPassedWithScreenShot("Viewing Rreports.");

        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.iframeXpath())) {
            error = "Failed to wait for frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Incident_Classification.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        //View full reports icon
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.viewFullReportsIcon())) {
            error = "Failed to wait for 'View full reports' icon.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.viewFullReportsIcon())) {
            error = "Failed to click on 'View full reports' icon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'View full reports' icon.");
        SeleniumDriverInstance.switchToTabOrWindow();
        //Continue button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.continueBtn())) {
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.continueBtn())) {
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        pause(5000);
        SeleniumDriverInstance.switchToTabOrWindow();
        narrator.stepPassedWithScreenShot("Successfully click 'Continue' button.");
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.BusinessUnitHeader())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Classification.BusinessUnitHeader())){
                error = "Failed to wait for Engagements header to be present.";
                return false;
            }
        }
        
        narrator.stepPassedWithScreenShot("Viewing Full Rreports.");
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        
        pause(2000);
        return true;
    }
}
