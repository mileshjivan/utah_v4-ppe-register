/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Incident_Classification_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Incident_Classification_V5_2_PageObjects.Incident_Classification;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR2-Capture Incident Sub Classification v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_Incident_Sub_Classification_MainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_Capture_Incident_Sub_Classification_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!CaptureIncidentSubClassification()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured Incident Sub Classification record.");
    }

    public boolean CaptureIncidentSubClassification() {
        //Incident Sub Classification panel
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.IncidentSubClassification_Panel())) {
            error = "Failed to wait for 'Incident Sub Classification' panel.";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Incident_Classification.IncidentSubClassification_Panel())) {
            error = "Failed to scroll to 'Incident Sub Classification' panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Incident Sub Classification' panel.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.ISC_addButton())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.ISC_addButton())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        //Incident sub classification
        if(!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.IncidentSubClassification())){
            error = "Failed to wait for Incident sub classification.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Incident_Classification.IncidentSubClassification(), getData("Incident sub classification"))){
            error = "Failed to enter the Incident sub classification: " + getData("Incident sub classification");
            return false;
        }
        narrator.stepPassedWithScreenShot("Incident sub classification: " + getData("Incident sub classification"));
        
        //Save button        
        if (!SeleniumDriverInstance.waitForElementByXpath(Incident_Classification.SaveButton())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Incident_Classification.SaveButton())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Incident_Classification.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Incident_Classification.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Incident_Classification.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        return true;
    }
}
