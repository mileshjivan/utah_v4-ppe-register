/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Charge_Code_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Charge_Code_V5_2_PageObject.Charge_Code_PageObject;
import KeywordDrivenTestFramework.Entities.TestResult;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Charge Code v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Charge_Code_MainScenario extends BaseClass {
    String error = "";

    public FR1_Capture_Charge_Code_MainScenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Navigate_To_Charge_Code()) {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Capture_Charge_Code()) {
            return narrator.testFailed("Failed due - " + error);
        }
        if(getData("Type of test").equalsIgnoreCase("Laboratory")){
            if (!Capture_Laboratory()) {
                return narrator.testFailed("Laboratory Failed due - " + error);
            }
        } else if(getData("Type of test").equalsIgnoreCase("Radiology")){
            if (!Capture_Radiology()) {
                return narrator.testFailed("Radiology Failed due - " + error);
            }
        }
        return narrator.finalizeTest("Successfully Captured Radiology Record");
    }

    public boolean Navigate_To_Charge_Code() {
        //Occupational Health
        if (!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.Occupational_Health())) {
            error = "Failed to wait for Occupational Health tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.Occupational_Health())) {
            error = "Failed to click Occupational Health tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Occupational Health tab");

        //Patient File Maintenance
        if (!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.Patient_File_Maintenance())) {
            error = "Failed to wait for Patient File Maintenance tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.Patient_File_Maintenance())) {
            error = "Failed to click Patient File Maintenance tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Patient File Maintenance tab");

        //Charge Code
        if (!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.Charge_Code())) {
            error = "Failed to wait for Charge Code tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.Charge_Code())) {
            error = "Failed to click Charge Code tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Job Safety Analysis tab");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.Add_Button())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.Add_Button())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        
        return true;
    }

    public boolean Capture_Charge_Code() {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.Process_Flow_Button())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.Process_Flow_Button())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //Type of test
        if (!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.Type_Of_Test_Dropdown())) {
            error = "Failed to wait for Type of test dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.Type_Of_Test_Dropdown())) {
            error = "Failed to click on Type of test dropdown";
            return false;
        }        
        if (!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.Type_Of_Test_Option(getData("Type of test")))) {
            error = "Failed to wait for Type of test option: " + getData("Type of test");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.Type_Of_Test_Option(getData("Type of test")))) {
            error = "Failed to click on Type of test option: " + getData("Type of test");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected Type of test option: " +  getData("Type of test"));

        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.saveBtn())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.saveBtn())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the save button.");
                
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Charge_Code_PageObject.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Charge_Code_PageObject.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Charge_Code_PageObject.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Charge_Code_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");
        
        if(getData("Type of test").equalsIgnoreCase("Laboratory")){
            if(!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.Laboratory_Panel())){
                error = "Failed to wait for Laboratory panel.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.Laboratory_Panel())){
                error = "Failed to click the Laboratory panel.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the Laboratory panel.");            
        } else if(getData("Type of test").equalsIgnoreCase("Radiology")){
            if(!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.Radiology_Panel())){
                error = "Failed to wait for Radiology panel.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.Radiology_Panel())){
                error = "Failed to click the Radiology panel.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the Radiology panel.");    
        }
                
        return true;
    }
    
    public boolean Capture_Laboratory(){
        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.MaintenanceLab_Add_Button())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.MaintenanceLab_Add_Button())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.MaintenanceLab_ProcessFlow())) {
            error = "Failed to wait for Maintenance Lab 'Process flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.MaintenanceLab_ProcessFlow())) {
            error = "Failed to click on Maintenance Lab 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Maintenance Lab 'Process flow' button.");
        
        //Lab test details
        if(!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.LabTestDetails())){
            error = "Failed to wait for the Lab test details.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Charge_Code_PageObject.LabTestDetails(), getData("Lab test details"))){
            error = "Failed to enter Lab test details: " + getData("Lab test details");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Lab test details: " + getData("Lab test details"));
        
        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.MaintenanceLab_SaveButton())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.MaintenanceLab_SaveButton())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the save button.");
                
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Charge_Code_PageObject.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Charge_Code_PageObject.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Charge_Code_PageObject.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Charge_Code_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");
            
        //Maintenance - Test Type
        if(!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.Maintenance_TestType())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Charge_Code_PageObject.Maintenance_TestType())){
                error = "Failed to wait for the Maintenance - Test type panel.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully displayed the Maintenance - Test type panel.");
        
        //------------------------------------------------------------------------------------------------------------
        //Maintenance - Test type Add Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.MaintenanceTestType_AddButton())){
            error = "Failed to wait for the Maintenance - Test Type Add Buttton.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.MaintenanceTestType_AddButton())){
            error = "Failed to wait for the Maintenance - Test Type Add Buttton.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Maintenance - Test Type Add Buttton.");
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.MaintenanceTestType_ProcessFlow())) {
            error = "Failed to wait for Maintenance - Charge code / Maintenance - Lab / Maintenance - Test Type 'Process flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.MaintenanceTestType_ProcessFlow())) {
            error = "Failed to click on Maintenance - Charge code / Maintenance - Lab / Maintenance - Test Type 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click  Maintenance - Charge code / Maintenance - Lab / Maintenance - Test Type 'Process flow' button.");
        
        //Lab test details
        if(!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.LabTestDetails_TestType())){
            error = "Failed to wait for the Lab test details.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Charge_Code_PageObject.LabTestDetails_TestType(), getData("Lab test details 2"))){
            error = "Failed to enter Lab test details: " + getData("Lab test details 2");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Lab test details: " + getData("Lab test details 2"));
        
        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.MaintenanceTestType_SaveButton())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.MaintenanceTestType_SaveButton())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the save button.");
                
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Charge_Code_PageObject.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Charge_Code_PageObject.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Charge_Code_PageObject.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }        
        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");
            
        //Maintenance - Result set
        if(!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.Maintenance_ResultSet())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Charge_Code_PageObject.Maintenance_ResultSet())){
                error = "Failed to wait for the Maintenance - Result set panel.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully displayed the Maintenance - Result set panel.");
        
        //-----------------------------------------------------------------------------------------------------------------
        //Maintenance - Result set Add Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.MaintenanceResultSet_AddButton())){
            error = "Failed to wait for the Maintenance - Result set Add Buttton.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.MaintenanceResultSet_AddButton())){
            error = "Failed to wait for the Maintenance - Result set Add Buttton.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Maintenance - Result Set Add Buttton.");
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.MaintenanceResultSet_ProcessFlow())) {
            error = "Failed to wait for Maintenance - Charge code / Maintenance - Lab / Maintenance - Test Type / Maintenance - Result Set 'Process flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.MaintenanceResultSet_ProcessFlow())) {
            error = "Failed to click on Maintenance - Charge code / Maintenance - Lab / Maintenance - Test Type / Maintenance - Result Set 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click  Maintenance - Charge code / Maintenance - Lab / Maintenance - Test Type / Maintenance - Result Set 'Process flow' button.");
        
        //Result set
        if(!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.ResultSet())){
            error = "Failed to wait for the Result set.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Charge_Code_PageObject.ResultSet(), getData("Result set"))){
            error = "Failed to enter Result set: " + getData("Result set");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Result set: " + getData("Result set"));
        
        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.MaintenanceResultSet_SaveButton())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.MaintenanceResultSet_SaveButton())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the save button.");
                
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Charge_Code_PageObject.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Charge_Code_PageObject.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Charge_Code_PageObject.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }        
        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");
            
        //--------------------------------------------------------------------------------------------------------------------
        //Close Maintenance - Result Set
        if(!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.MaintenanceResultSet_CloseButton())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Charge_Code_PageObject.MaintenanceResultSet_CloseButton())){
                error = "Failed to wait for the Maintenance - Result Set - Close Button.";
                return false;
            }
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.MaintenanceResultSet_CloseButton())){
            error = "Failed to click the Maintenance - Result Set - Close Button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Maintenance - Result Set - Close Button.");
        
        //Record Maintenance - Result Set
        if(!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.MaintenanceResultSet_Record())){
            error = "Failed to wait for Maintenance - Result Set record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Record Maintenance - Result set successfully created.");
        
        //----------------------------------------------------------------------------------------------------------------------
        //Close Maintenance - Test Type
        if(!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.MaintenanceTestType_CloseButton())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Charge_Code_PageObject.MaintenanceTestType_CloseButton())){
                error = "Failed to wait for the Maintenance - Result Set - Close Button.";
                return false;
            }
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.MaintenanceTestType_CloseButton())){
            error = "Failed to click the Maintenance - Test type - Close Button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Maintenance -  Test type - Close Button.");
        
        //Record Maintenance - Result Set
        if(!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.MaintenanceResultSet_Record())){
            error = "Failed to wait for Maintenance -  Test type record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Record Maintenance -  Test type successfully created.");
        
        //--------------------------------------------------------------------------------------------------------------------
        //Close
        if(!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.MaintenanceLab_CloseButton())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Charge_Code_PageObject.MaintenanceLab_CloseButton())){
                error = "Failed to wait for the Maintenance Lab - Close Button.";
                return false;
            }
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.MaintenanceLab_CloseButton())){
            error = "Failed to click the Close Button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Close Button.");
        
        //Laboratory panel.
        if(!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.Laboratory_Panel())){
            error = "Failed to wait for the Laboratory panel.";
            return false;
        }
        pause(3000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.Laboratory_Panel())){
            error = "Failed to click the Laboratory panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the Laboratory panel."); 
            
        //Maintenance - Lab
        if(!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.MaintenanceLab_Record())){
            error = "Failed to wait for Maintenance - Lab record.";
            return false;
        }
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.Process_Flow_Button())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.Process_Flow_Button())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        
        return true;
    }
    
    public boolean Capture_Radiology(){
        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.MaintenanceRadiology_Add_Button())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.MaintenanceRadiology_Add_Button())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.MaintenanceRadiology_ProcessFlow())) {
            error = "Failed to wait for Maintenance - Radiology 'Process flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.MaintenanceRadiology_ProcessFlow())) {
            error = "Failed to click on Maintenance - Radiology 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Maintenance - Radiology 'Process flow' button.");
        
        //Radiology Details
        if(!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.RadiologyDetails())){
            error = "Failed to wait for the Radiology Details.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Charge_Code_PageObject.RadiologyDetails(), getData("Radiology Details"))){
            error = "Failed to enter Radiology Details: " + getData("Radiology Details");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Radiology Details: " + getData("Radiology Details"));
        
        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.MaintenanceRadiology_SaveButton())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.MaintenanceRadiology_SaveButton())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the save button.");
                
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Charge_Code_PageObject.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Charge_Code_PageObject.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Charge_Code_PageObject.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Charge_Code_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");
          
        //Close Maintenance - Radiology
        if(!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.MaintenanceRadiology_CloseButton())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Charge_Code_PageObject.MaintenanceRadiology_CloseButton())){
                error = "Failed to wait for the Maintenance - Radiology - Close Button.";
                return false;
            }
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.MaintenanceRadiology_CloseButton())){
            error = "Failed to click the Maintenance - Radiology - Close Button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Maintenance - Radiology - Close Button.");
        
        //Record Maintenance - Result Set
        if(!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.MaintenanceRadiology_Record(getData("Radiology Details")))){
            error = "Failed to wait for Maintenance - Radiology record: " + getData("Radiology Details");
            return false;
        }
        narrator.stepPassedWithScreenShot("Record Maintenance - Radiology successfully created.");
            
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Charge_Code_PageObject.Process_Flow_Button())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Charge_Code_PageObject.Process_Flow_Button())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        //------------------------------------------------------------------------------------------------------------
        
        return true;
    }

}
