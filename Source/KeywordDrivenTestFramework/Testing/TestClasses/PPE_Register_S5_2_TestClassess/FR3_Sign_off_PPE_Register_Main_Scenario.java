/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.PPE_Register_S5_2_TestClassess;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PPE_Register_V5_2_PageObjects.Capture_PPE_Register_V5_2_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR3-Sign off PPE Register v5.2 - Main Scenario",
        createNewBrowserInstance = false
)
public class FR3_Sign_off_PPE_Register_Main_Scenario extends BaseClass {
    String error = "";

    public FR3_Sign_off_PPE_Register_Main_Scenario() {

        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Capture_SignOff_Register()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully");
    }

    public boolean Capture_SignOff_Register() {
        if(getData("Sign off Register Button").equalsIgnoreCase("Yes")){
            //Sign off Register Button
            if(!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.SignoffRegister_Button())){
                error = "Failed to wait for the Sign off Register Button.";
                return false;
            }
            if(!SeleniumDriverInstance.ValidateByText(Capture_PPE_Register_V5_2_PageObjects.SignoffRegister_Button(), "Sign off register")){
                error = "Failed to wait for the Sign off Register Button.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.SignoffRegister_Button())){
                error = "Failed to click the Sign off Register Button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the Sign off Register Button.");

            //SignOff tab
            if(!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.SignOff_tab())){
                error = "Failed to wait for the Sign Off tab.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.SignOff_tab())){
                error = "Failed to click the Sign Off tab.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the Sign Off tab.");

            //SignOff dropdown
            if(!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.SignOff_Dropdown())){
                error = "Failed to wait for the Sign Off dropdown.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.SignOff_Dropdown())){
                error = "Failed to click the Sign Off dropdown.";
                return false;
            }
            if(!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.SignOff_Option(getData("Sign off register")))){
                error = "Failed to wait the Sign Off register option: " + getData("Sign off register");
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.SignOff_Option(getData("Sign off register")))){
                error = "Failed to click the Sign Off register option: " + getData("Sign off register");
                return false;
            }
            narrator.stepPassedWithScreenShot("Sign off register option: " + getData("Sign off register"));

            if(getData("Sign off register").equalsIgnoreCase("Yes")){
                //Sign off Comments
                if(!SeleniumDriverInstance.enterTextByXpath(Capture_PPE_Register_V5_2_PageObjects.SignOffComments(), getData("Sign off comments"))){
                    error = "Failed to enter Sign off comments.";
                    return false;
                }
            }

            //Save 
            if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.Save_Button())) {
                error = "Failed to wait for 'Save' button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.Save_Button())) {
                error = "Failed to click the 'Save' button";
                return false;
            }
            narrator.stepPassed("Successfully clicked the 'Save' button");

            pause(5000);
            //Save mask
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Capture_PPE_Register_V5_2_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }

            //Validate if the record has been saved or not.
            if (!SeleniumDriverInstance.waitForElementsByXpath(Capture_PPE_Register_V5_2_PageObjects.validateSave())) {
                error = "Failed to wait for Save validation.";
                return false;
            }
            String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Capture_PPE_Register_V5_2_PageObjects.validateSave());

            if (!SaveFloat.equals("Record saved")) {
                narrator.stepPassedWithScreenShot("Failed to save record.");
                return false;
            }
            narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

            //1. Register tab
            if(!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.Register_tab())){
                error = "Failed to wait for the 1. Register tab.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.Register_tab())){
                error = "Failed to click the 1. Register tab.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the 1. Register tab.");
        }
        
        return true;
    }
}
