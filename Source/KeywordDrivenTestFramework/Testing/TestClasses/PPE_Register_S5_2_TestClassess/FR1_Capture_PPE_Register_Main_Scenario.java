/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.PPE_Register_S5_2_TestClassess;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PPE_Register_V5_2_PageObjects.Capture_PPE_Register_V5_2_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture PPE Register v5.2 - Main Scenario",
        createNewBrowserInstance = false
)
public class FR1_Capture_PPE_Register_Main_Scenario extends BaseClass {
    String error = "";

    public FR1_Capture_PPE_Register_Main_Scenario() {

        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());

    }

    public TestResult executeTest() {
        if (!Naviga_To_PPE_Registe()) {
            return narrator.testFailed("Naviga To PPE Registe Failed due - " + error);
        }
        if (!Capture_PPE_Register()) {
            return narrator.testFailed("Capture PPE Register Failed due - " + error);
        }
        if (!Supporting_Documents()) {
            return narrator.testFailed("Capture PPE Register Suppoting Documents Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully");
    }

    public boolean Naviga_To_PPE_Registe() {
        //Navigate to Environmental Health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.navigate_EHS())) {
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.navigate_EHS())) {
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");

        //Navigate To PPE Register
        if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.PPE_Register_Tab())) {
            error = "Failed to wait for PPE Register tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.PPE_Register_Tab())) {
            error = "Failed to click on PPE Register tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to PPE Registertab.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.Add_button(),10000)) {
            error = "Failed to wait for Add button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.Add_button())) {
            error = "Failed to click on Add button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Add button");

        return true;
    }

    public boolean Capture_PPE_Register() {
        //Process Flow
        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.PPERigister_ProcessFlow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.PPERigister_ProcessFlow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //Business unit
        if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.Register_Tab())) {
            error = "Failed to wait for  Register tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.Business_Unit())) {
            error = "Failed to click on Business Unit Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.Business_Unit_Drop_Down_Option_1(testData.getData("Business Unit 1")))) {
            error = "Failed to click on '" + testData.getData("Business Unit 1") + "' Business Unit Drop Down Option";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.Business_Unit_Drop_Down_Option_1(testData.getData("Business Unit 1")))) {
            error = "Failed to click on '" + testData.getData("Business Unit 2") + "' Business Unit Drop Down Option";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.Business_Unit_Drop_Down_Option_1(testData.getData("Business Unit 2")))) {
            error = "Failed to click on '" + testData.getData("Business Unit 3") + "' Business Unit Drop Down Option";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.Business_Unit_Drop_Down_Option_2(testData.getData("Business Unit 3")))) {
            error = "Failed to click '" + testData.getData("Business Unit 3") + "' on Business Unit Drop Down Option";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully selected '" + testData.getData("Business Unit 3") + "' as the Rigister Business unit.");

        //Issued by
        //Issued to
        if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.Issued_To_Drop_Down())) {
            error = "Failed to wait for 'Issued to' drop-down field";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.Issued_To_Drop_Down())) {
            error = "Failed to wait for 'Issued to' drop-down field";
            return false;
        }

        if (testData.getData("Issued To").equalsIgnoreCase("Employee")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.Issued_To_Option(testData.getData("Issued To")))) {
                error = "Failed to wait for '" + testData.getData("Issued To") + "' drop-down field";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.Issued_To_Option(testData.getData("Issued To")))) {
                error = "Failed to wait for '" + testData.getData("Issued To") + "' drop-down field";
                return false;
            }
            narrator.stepPassed("Successfully selected '" + testData.getData("Issued To") + "' fom the Issued To drop-down field");

            //employee
            if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.Employee_Drop_Down())) {
                error = "Failed to wait for the 'Employee' drop-down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.Employee_Drop_Down())) {
                error = "Failed to click the 'Employee' drop-down";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.Employee_Drop_Down_Option(testData.getData("Employee")))) {
                error = "Failed to wait for the '" + testData.getData("Employee") + "' from the Employee drop-down option";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.Employee_Drop_Down_Option(testData.getData("Employee")))) {
                error = "Failed to select '" + testData.getData("Employee") + "' from the Employee drop-down option";
                return false;
            }
            narrator.stepPassed("Successfully selected '" + testData.getData("Employee") + "' from the Employee drop-down option");
        } else if (testData.getData("Issued To").equalsIgnoreCase("Contractor")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.Issued_To_Option(testData.getData("Issued To")))) {
                error = "Failed to wait for '" + testData.getData("Issued To") + "' drop-down field";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.Issued_To_Option(testData.getData("Issued To")))) {
                error = "Failed to wait for '" + testData.getData("Issued To") + "' drop-down field";
                return false;
            }

            narrator.stepPassed("Successfully selected '" + testData.getData("Issued To") + "' fom the Issued To drop-down field");

            //Contractor
            if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.Contractor_Drop_Down())) {
                error = "Failed to wait for the 'Contractor' drop-down field";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.Contractor_Drop_Down())) {
                error = "Failed to click the 'Contractor' drop-down field";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.Contractor_Drop_Down_Option_1(testData.getData("Contractor")))) {
                error = "Failed to wait for '" + testData.getData("Contractor") + "' from the Contractor drop-down field";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.Contractor_Drop_Down_Option_1(testData.getData("Contractor")))) {
                error = "Failed to click on '" + testData.getData("Contractor") + "' from the Contractor drop-down field";
                return false;
            }

            narrator.stepPassed("Successfully selected '" + testData.getData("Contractor") + "' from the Contractor drop-down field");

            if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.Contractor_Drop_Down_Option_2(testData.getData("Contractor Sub")))) {
                error = "Failed to click on '" + testData.getData("Contractor Sub") + "' from the Contractor drop-down field";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.Contractor_Drop_Down_Option_2(testData.getData("Contractor Sub")))) {
                error = "Failed to click on '" + testData.getData("Contractor Sub") + "' from the Contractor drop-down field";
                return false;
            }

            narrator.stepPassed("Successfully selected '" + testData.getData("Contractor Sub") + "' from the Contractor drop-down field");

            //ID Number
            if (!this.Enter_ID_Number()) {
                return false;
            }
        } else if (testData.getData("Issued To").equalsIgnoreCase("Visitor")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.Issued_To_Option(testData.getData("Issued To")))) {
                error = "Failed to wait for '" + testData.getData("Issued To") + "' drop-down field";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.Issued_To_Option(testData.getData("Issued To")))) {
                error = "Failed to wait for '" + testData.getData("Issued To") + "' drop-down field";
                return false;
            }

            narrator.stepPassed("Successfully selected '" + testData.getData("Issued To") + "' fom the Issued To drop-down field");

            //Visitor
            if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.Visitor_Input())) {
                error = "Failed to wait for 'Visitor' input field";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Capture_PPE_Register_V5_2_PageObjects.Visitor_Input(), testData.getData("Visitor"))) {
                error = "Failed to enter '" + testData.getData("Visitor") + "' 'Visitor' input field";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully entered '" + testData.getData("Visitor") + "' in the Visitor input field");

            //ID Number
            if (!this.Enter_ID_Number()) {
                return false;
            }
        }

        //Date Issued     
        if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.Date_Issued())) {
            error = "Failed to wait for the  'Date Issued' input field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Capture_PPE_Register_V5_2_PageObjects.Date_Issued(), startDate)) {
            error = "Failed to enter '" + startDate + "' the  Date Issued input field";
            return false;
        }
        narrator.stepPassed("Successfully entered '" + startDate + "' in the Date Issued input field");

        //Notes
        if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.Register_Notes())) {
            error = "Failed to wait for Notes text area";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Capture_PPE_Register_V5_2_PageObjects.Register_Notes(), testData.getData("Notes"))) {
            error = "Failed to enter '" + testData.getData("Notes") + "' in the Notes text-area";
            return false;
        }

        //Save 
        if (!testData.getData("Save to continue").equalsIgnoreCase("Yes")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.Save_Button())) {
                error = "Failed to wait for 'Save' button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.Save_Button())) {
                error = "Failed to click the 'Save' button";
                return false;
            }
            narrator.stepPassed("Successfully clicked the 'Save' button");
        } else {
            //Save To Continue
            if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.Save_To_Continue())) {
                error = "Failed to wait for 'Save to continue' button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.Save_To_Continue())) {
                error = "Failed to click the 'Save to continue' button";
                return false;
            }
            narrator.stepPassed("Successfully clicked the 'Save to continue' button");
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Capture_PPE_Register_V5_2_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Capture_PPE_Register_V5_2_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Capture_PPE_Register_V5_2_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        return true;
    }

    public boolean Enter_ID_Number() {
        //ID Number
        if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.ID_Number_Input())) {
            error = "Failed to wait for the 'ID Number' input field";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.ID_Number_Input())) {
            error = "Failed to wait for the 'ID Number' input field";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Capture_PPE_Register_V5_2_PageObjects.ID_Number_Input(), testData.getData("ID Number"))) {
            error = "Failed to enter '" + testData.getData("ID Number") + "' in the 'ID Number' input field";
            return false;
        }
        narrator.stepPassed("Successfully entered '" + testData.getData("ID Number") + "' in the 'ID Number' input field");

        return true;
    }

    public boolean Supporting_Documents() {
        //Supporting Documents
        if (testData.getData("Supporting Documents").equalsIgnoreCase("Yes")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.Supporting_Doocuments_tab())) {
                error = "Failed to wait for the 'Supporting Documents' tab";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.Supporting_Doocuments_tab())) {
                error = "Failed to click the 'Supporting Documents' tab";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the Supporting Documents tab");

            //Upload a link 
            if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.link_buttonxpath())) {
                error = "Failed to wait for 'Link a document' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.link_buttonxpath())) {
                error = "Failed to click on 'Link a document' button.";
                return false;
            }
            pause(2000);
            narrator.stepPassedWithScreenShot("Successfully click 'Link a document' button.");

            if (!SeleniumDriverInstance.switchToTabOrWindow()) {
                error = "Failed to switch tab.";
                return false;
            }

            //Link
            if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.LinkURL())) {
                error = "Failed to wait for 'Link a document' button.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Capture_PPE_Register_V5_2_PageObjects.LinkURL(), getData("Document Link"))) {
                error = "Failed to click on 'Link a document' button.";
                return false;
            }

            //Title
            if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.urlTitle())) {
                error = "Failed to wait for 'Link a document' button.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Capture_PPE_Register_V5_2_PageObjects.urlTitle(), getData("Title"))) {
                error = "Failed to click on 'Link a document' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully Entered Url: '" + getData("Document Link") + "' and 'Title: '" + getData("Title") + "'.");

            //url Add Button
            if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.urlAddButton())) {
                error = "Failed to wait for 'Add' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.urlAddButton())) {
                error = "Failed to click on 'Add' button.";
                return false;
            }
            pause(2000);
            narrator.stepPassedWithScreenShot("Successfully uploaded a Supporting Document.");

            if (!SeleniumDriverInstance.switchToDefaultContent()) {
                error = "Failed to switch tab.";
                return false;
            }

            //iFrame
            if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.iframeXpath())) {
                error = "Failed to switch to frame.";
                return false;
            }
            if (!SeleniumDriverInstance.switchToFrameByXpath(Capture_PPE_Register_V5_2_PageObjects.iframeXpath())) {
                error = "Failed to switch to frame.";
                return false;
            }

            if (getData("Save supporting documents").equalsIgnoreCase("Yes")) {
                //Save to continue button        
                if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.SaveSupportingDocuments_Button())) {
                    error = "Failed to wait for 'Save supporting documents' button.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.SaveSupportingDocuments_Button())) {
                    error = "Failed to click on 'Save supporting documents' button.";
                    return false;
                }
            } else {
                //Save button        
                if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.EP_SaveBtn())) {
                    error = "Failed to wait for 'Save' button.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.EP_SaveBtn())) {
                    error = "Failed to click on 'Save' button.";
                    return false;
                }
            }

            pause(5000);
            //Saving mask
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Capture_PPE_Register_V5_2_PageObjects.saveWait2())) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }

            //Validate if the record has been saved or not.
            if (!SeleniumDriverInstance.waitForElementsByXpath(Capture_PPE_Register_V5_2_PageObjects.validateSave())) {
                error = "Failed to wait for Save validation.";
                return false;
            }

            String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Capture_PPE_Register_V5_2_PageObjects.validateSave());

            if (!SaveFloat.equals("Record saved")) {
                narrator.stepPassedWithScreenShot("Failed to save record.");
                return false;
            }
            narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
        }

        return true;
    }
}
