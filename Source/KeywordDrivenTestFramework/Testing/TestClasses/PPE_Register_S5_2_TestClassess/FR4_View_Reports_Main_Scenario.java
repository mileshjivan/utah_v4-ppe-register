/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.PPE_Register_S5_2_TestClassess;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PPE_Register_V5_2_PageObjects.Capture_PPE_Register_V5_2_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR4-View Reports v5.2 - Main Scenario",
        createNewBrowserInstance = false
)
public class FR4_View_Reports_Main_Scenario extends BaseClass {
    String parentWindow;
    String error = "";

    public FR4_View_Reports_Main_Scenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());

    }

    public TestResult executeTest() {
        if (!navigateToRelatedInitiatives()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully");
    }

    public boolean navigateToRelatedInitiatives() {
        //Navigate to Environmental Health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.navigate_EHS())) {
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.navigate_EHS())) {
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");

        //Navigate To PPE Register
        if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.PPE_Register_Tab())) {
            error = "Failed to wait for PPE Register tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.PPE_Register_Tab())) {
            error = "Failed to click on PPE Register tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to PPE Registertab.");
        
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.searchBtn())) {
            error = "Failed to wait for 'Serach' button.";
            return false;
        }        
        if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.searchBtn())) {
            error = "Failed to click on 'Serach' button.";
            return false;
        }
        SeleniumDriverInstance.pause(3000);
        narrator.stepPassedWithScreenShot("Successfully click 'Serach' button.");

        //Reports button
        if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.reportsBtn())) {
            error = "Failed to wait for 'Reports' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.reportsBtn())) {
            error = "Failed to click on 'Reports' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Reports' button.");
        parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        //View reports icon
        if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.viewReportsIcon())) {
            error = "Failed to wait for 'View reports' icon.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.viewReportsIcon())) {
            error = "Failed to click on 'View reports' icon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'View reports' icon.");
        SeleniumDriverInstance.switchToTabOrWindow();
        //Continue button
        if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.continueBtn())) {
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.continueBtn())) {
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        pause(5000);
        SeleniumDriverInstance.switchToTabOrWindow();
              
        if(!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.PPE_Register())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Capture_PPE_Register_V5_2_PageObjects.PPE_Register())){
                error = "Failed to wait for PPE Register header to be present.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Continue' button.");
        narrator.stepPassedWithScreenShot("Viewing Rreports.");

        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.iframeXpath())) {
            error = "Failed to wait for frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Capture_PPE_Register_V5_2_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        //View full reports icon
        if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.viewFullReportsIcon())) {
            error = "Failed to wait for 'View full reports' icon.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.viewFullReportsIcon())) {
            error = "Failed to click on 'View full reports' icon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'View full reports' icon.");
        SeleniumDriverInstance.switchToTabOrWindow();
        //Continue button
        if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.continueBtn())) {
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.continueBtn())) {
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        pause(5000);
        SeleniumDriverInstance.switchToTabOrWindow();
        narrator.stepPassedWithScreenShot("Successfully click 'Continue' button.");
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.BusinessUnitHeader())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Capture_PPE_Register_V5_2_PageObjects.BusinessUnitHeader())){
                error = "Failed to wait for Engagements header to be present.";
                return false;
            }
        }
        
        narrator.stepPassedWithScreenShot("Viewing Full Rreports.");
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        
        pause(2000);
        return true;
    }
}
