/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.PPE_Register_S5_2_TestClassess;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.PPE_Register_V5_2_PageObjects.Capture_PPE_Register_V5_2_PageObjects;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR2-Capture Items Issued v5.2 - Main Scenario",
        createNewBrowserInstance = false
)
public class FR2_Capture_Items_Issued_Main_Main_Scenario extends BaseClass {
    String error = "";

    public FR2_Capture_Items_Issued_Main_Main_Scenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!NavigateToItemsIssued()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured Items Issued");
    }

    public boolean NavigateToItemsIssued() {
        //Items Issued Panel
        if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.ItemsIssued_Panel())) {
            error = "Failed to wait for 'Items Issued' panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.ItemsIssued_Panel())) {
            error = "Failed to click on 'Items Issued' panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully viewed 'Items Issued' panel.");

        //items Issued Add Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.ItemsIssued_Add_Button())){
            error = "Failed to wait for the Itemns Issued Add Button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.ItemsIssued_Add_Button())){
            error = "Failed to click the Itemns Issued Add Button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the Add Button.");
        
        //PPE
        if(!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.PPE_Dropdown())){
            error = "Failed to wait for the PPE dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.PPE_Dropdown())){
            error = "Failed to click the PPE dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.PPE_Option(getData("PPE")))){
            error = "Failed to wait for the PPE option: " + getData("PPE");
            return false;
        }
        if(!SeleniumDriverInstance.doubleClickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.PPE_Option(getData("PPE")))){
            error = "Failed to click the PPE option: " + getData("PPE");
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.PPE_Option(getData("PPE 2")))){
            error = "Failed to click the PPE option: " + getData("PPE 2");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked PPE: " + getData("PPE") + " -> " + getData("PPE 2"));
        
        //Number of items issued
        if(!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.NumberOfItemsIssued())){
            error = "Failed to wait for the Number of items issued";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Capture_PPE_Register_V5_2_PageObjects.NumberOfItemsIssued(), getData("Number of items issued"))){
            error = "Failed to enter the Number of items issued: " + getData("Number of items issued");
            return false;
        }
        narrator.stepPassedWithScreenShot("Number of items issued: " + getData("Number of items issued"));
        
         //Save 
        if (!testData.getData("Items Issued Save Button").equalsIgnoreCase("Yes")) {
            if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.Save_Button())) {
                error = "Failed to wait for 'Save' button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.Save_Button())) {
                error = "Failed to click the 'Save' button";
                return false;
            }
            narrator.stepPassed("Successfully clicked the 'Save' button");
        } else {
            //Save To Continue
            if (!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.ItemsIssued_Save())) {
                error = "Failed to wait for 'Items Issued Save' button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Capture_PPE_Register_V5_2_PageObjects.ItemsIssued_Save())) {
                error = "Failed to click the 'Items Issued Save' button";
                return false;
            }
            narrator.stepPassed("Successfully clicked the 'Items Issued Save' button");
        }

        pause(5000);
        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Capture_PPE_Register_V5_2_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Capture_PPE_Register_V5_2_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Capture_PPE_Register_V5_2_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");
        
        //Sign off Register Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Capture_PPE_Register_V5_2_PageObjects.SignoffRegister_Button())){
            error = "Failed to wait for the Sign off Register Button.";
            return false;
        }
        if(!SeleniumDriverInstance.ValidateByText(Capture_PPE_Register_V5_2_PageObjects.SignoffRegister_Button(), "Sign off register")){
            error = "Failed to validate the Sign off Register Button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully validate the Sign off Register Button.");
        
        return true;
    }
}
