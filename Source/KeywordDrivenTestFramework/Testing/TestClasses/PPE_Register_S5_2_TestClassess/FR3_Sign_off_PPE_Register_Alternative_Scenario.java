/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.PPE_Register_S5_2_TestClassess;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Engagements_V5_2_PageObjects.Engagements_PageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.PPE_Register_V5_2_PageObjects.Capture_PPE_Register_V5_2_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "",
        createNewBrowserInstance = false
)
public class FR3_Sign_off_PPE_Register_Alternative_Scenario extends BaseClass
{

    String error = "";

    public FR3_Sign_off_PPE_Register_Alternative_Scenario()
    {

        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());

    }

    public TestResult executeTest()
    {

        if (!navigateToRelatedInitiatives())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully");
    }

    public boolean navigateToRelatedInitiatives()
    {
        //Create commitment panel
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.createCommitmentPanel()))
        {
            error = "Failed to wait for 'Create commitment' panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.createCommitmentPanel()))
        {
            error = "Failed to click on 'Create commitment' panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Create commitment' panel.");

        narrator.stepPassedWithScreenShot("Successfully viewed 'Stakeholder grievances' record.");

        return true;
    }
}
