/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Strategy_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Grievances_V5_2_PageObjects.Engagements_PageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Grievances_V5_2_PageObjects.Grievances_PageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Strategy_S5_2_PageObjects.Strategy_S5_2_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR1 Capture Strategy - Main Scenario",
        createNewBrowserInstance = false
)
public class FR1_Capture_Strategy_Main_Scenario extends BaseClass
{

    String error = "";

    public FR1_Capture_Strategy_Main_Scenario()
    {

        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Navigate_Strategy())
        {
            return narrator.testFailed("Navigate Strategy Failed due - " + error);
        }
        if (!Capture_Strategy())
        {
            return narrator.testFailed("Capture Strategy Failed due - " + error);
        }
        if (getData("Supporting Documents").equalsIgnoreCase("True"))
        {
            if (!Supporting_Documents())
            {
                return narrator.testFailed("Asset Details Failed due - " + error);
            }
            
        }
        return narrator.finalizeTest("Successfully captured Strategy");
    }

    public boolean Navigate_Strategy()
    {

        //Navigate to Environmental Health & Safety
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.navigate_EHS()))
        {
            error = "Failed to wait for 'Environmental, Health & Safety' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.navigate_EHS()))
        {
            error = "Failed to click on 'Environmental, Health & Safety' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' button.");

        //Navigate to strategic objectives
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.strategic_objectives()))
        {
            error = "Failed to wait for strategic objectives button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.strategic_objectives()))
        {
            error = "Failed to click on strategic objectives button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to strategic objectives.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.strategy_Add_Button()))
        {
            error = "Failed to wait for strategic add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.strategy_Add_Button()))
        {
            error = "Failed to click on strategic add button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked add button.");

        return true;

    }

    public boolean Capture_Strategy()
    {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.process_flow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.process_flow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");

        //Business unit dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.business_unit_drop_down()))
        {
            error = "Failed to wait for 'Business unit' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.business_unit_drop_down()))
        {
            error = "Failed to click on 'Business unit' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Global_Company_Drop_Down()))
        {
            error = "Failed to wait for Glocal Company";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.Global_Company_Drop_Down()))
        {
            error = "Failed click Glocal Company drop down";
            return false;
        }

        //Impact type selct all
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Impact_type_drop_down()))
        {
            error = "Failed to wait for Impact type  dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.Impact_type_drop_down()))
        {
            error = "Failed to click on Impact type  dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Impact_type_select_all()))
        {
            error = "Failed to wait for Impact type selct all.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.Impact_type_select_all()))
        {
            error = "Failed to click on Impact type selct all";
            return false;
        }

        narrator.stepPassedWithScreenShot("Impact type");

        //Strategy
        if (!SeleniumDriverInstance.enterTextByXpath(Strategy_S5_2_PageObjects.Strategy(), getData("Strategy")))
        {
            error = "Failed to enter Strategy :" + getData("Strategy");
            return false;
        }
        narrator.stepPassedWithScreenShot("Strategy : " + getData("Strategy"));

        //Purpose
        if (!SeleniumDriverInstance.enterTextByXpath(Strategy_S5_2_PageObjects.Purpose(), getData("Purpose")))
        {
            error = "Failed to enter Purpose :" + getData("Purpose");
            return false;
        }
        narrator.stepPassedWithScreenShot("Purpose : " + getData("Purpose"));

        //Mission statement    
        if (!SeleniumDriverInstance.enterTextByXpath(Strategy_S5_2_PageObjects.Mission_statement(), getData("Mission statement")))
        {
            error = "Failed to enter Mission statement :" + getData("Mission statement");
            return false;
        }
        narrator.stepPassedWithScreenShot("Mission statement : " + getData("Mission statement"));

        //Strategy owner
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Strategy_owner_drop_down()))
        {
            error = "Failed to wait for Strategy owner dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.Strategy_owner_drop_down()))
        {
            error = "Failed to click on Strategy owner dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Strategy_owner_option(getData("Strategy owner"))))
        {
            error = "Failed to wait for Strategy owner dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.Strategy_owner_option(getData("Strategy owner"))))
        {
            error = "Failed to click Strategy owner dropdown";
            return false;
        }

        if (getData("Check").equalsIgnoreCase("True"))
        {
            //Project link
            if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Project_link()))
            {
                error = "Failed to wait for Project link check box";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.Project_link()))
            {
                error = "Failed to click Project link check box";
                return false;
            }

            //Project
            if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Project_drop_down()))
            {
                error = "Failed to wait for Project dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.Project_drop_down()))
            {
                error = "Failed to click on Project dropdown.";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Strategy_owner_option(getData("Project"))))
            {
                error = "Failed to wait for Project dropdown";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.Strategy_owner_option(getData("Project"))))
            {
                error = "Failed to click Project dropdown";
                return false;
            }

        }

        //Save button        
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.SaveBtn()))
        {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.SaveBtn()))
        {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Strategy_S5_2_PageObjects.saveWait2()))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Strategy_S5_2_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Strategy_S5_2_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
        return true;
    }
    
    
    public boolean Supporting_Documents()
    {
        //Supporting Documents tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.supporting_tab()))
        {
            error = "Failed to wait for Supporting Documents tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.supporting_tab()))
        {
            error = "Failed to click Supporting Documents tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to supporting documents.");

        //Add support document - Hyperlink
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.linkbox()))
        {
            error = "Failed to wait for 'Link box' link.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.linkbox()))
        {
            error = "Failed to click on 'Link box' link.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");

        //switch to new window
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new window or tab.";
            return false;
        }

        //URL https
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.LinkURL()))
        {
            error = "Failed to wait for 'URL value' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Strategy_S5_2_PageObjects.LinkURL(), getData("Document Link")))
        {
            error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.urlTitle()))
        {
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Strategy_S5_2_PageObjects.urlTitle(), getData("Title")))
        {
            error = "Failed to enter Url Title :" + getData("Title");
            return false;
        }
        narrator.stepPassedWithScreenShot("Title : '" + getData("Title") + "'.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.urlAddButton()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.urlAddButton()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Strategy_S5_2_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");

        //Save supporting documents button        
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.SaveSupportingDocuments_SaveBtn()))
        {
            error = "Failed to wait for 'Save supporting documents' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.SaveSupportingDocuments_SaveBtn()))
        {
            error = "Failed to click on 'Save supporting documents' button.";
            return false;
        }

        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Strategy_S5_2_PageObjects.saveWait2()))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Strategy_S5_2_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Strategy_S5_2_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        pause(300);
        narrator.stepPassed("Document Link :" + getData("Document Link"));
        narrator.stepPassed("Title :" + getData("Title"));

        

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.process_flow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.process_flow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");

        return true;
    }

}
