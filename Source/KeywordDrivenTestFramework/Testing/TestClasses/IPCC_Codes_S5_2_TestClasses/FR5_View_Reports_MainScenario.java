/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.IPCC_Codes_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.IPCC_Codes_V5_2_PageObjects.IPCC_Codes_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR5-View Reports IPCC Codes v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR5_View_Reports_MainScenario extends BaseClass{
    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR5_View_Reports_MainScenario(){
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest(){
        if (!Navigate_To_IPCC_Codes_Reports()){
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully clicked IPCC Codes Reports.");
    }

    public boolean Navigate_To_IPCC_Codes_Reports(){
        //Navigate to Carbon Footprint
        if (!SeleniumDriverInstance.waitForElementByXpath(IPCC_Codes_PageObject.navigate_Carbon_Footprint())) {
            error = "Failed to wait for 'Carbon Footprint' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IPCC_Codes_PageObject.navigate_Carbon_Footprint())) {
            error = "Failed to click on 'Carbon Footprint' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Carbon Footprint' button.");

        //Navigate to Monitoring Maintenance
        if (!SeleniumDriverInstance.waitForElementByXpath(IPCC_Codes_PageObject.MonitoringMaintenance())) {
            error = "Failed to wait for 'Monitoring Maintenance' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IPCC_Codes_PageObject.MonitoringMaintenance())) {
            error = "Failed to click on 'Monitoring Maintenance' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Monitoring Maintenance' button.");

        //Navigate to IPCC Codes
        if (!SeleniumDriverInstance.waitForElementByXpath(IPCC_Codes_PageObject.IPCC_Codes())) {
            error = "Failed to wait for 'IPCC Codes' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IPCC_Codes_PageObject.IPCC_Codes())) {
            error = "Failed to click on 'IPCC Codes' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'IPCC Codes' search page.");
        
        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(IPCC_Codes_PageObject.searchBtn())) {
            error = "Failed to wait for 'Serach' button.";
            return false;
        }        
        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(IPCC_Codes_PageObject.searchBtn())) {
            error = "Failed to click on 'Serach' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Serach' button.");

        //Reports button
        if (!SeleniumDriverInstance.waitForElementByXpath(IPCC_Codes_PageObject.reportsBtn())) {
            error = "Failed to wait for 'Reports' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IPCC_Codes_PageObject.reportsBtn())) {
            error = "Failed to click on 'Reports' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Reports' button.");
        parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        
        //View reports icon
        if (!SeleniumDriverInstance.waitForElementByXpath(IPCC_Codes_PageObject.viewReportsIcon())) {
            error = "Failed to wait for 'View reports' icon.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IPCC_Codes_PageObject.viewReportsIcon())) {
            error = "Failed to click on 'View reports' icon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'View reports' icon.");
        SeleniumDriverInstance.switchToTabOrWindow();
        
        //Continue button
        if (!SeleniumDriverInstance.waitForElementByXpath(IPCC_Codes_PageObject.continueBtn())) {
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IPCC_Codes_PageObject.continueBtn())) {
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        pause(5000);
        SeleniumDriverInstance.switchToTabOrWindow();
              
        if(!SeleniumDriverInstance.waitForElementByXpath(IPCC_Codes_PageObject.IPCC_Codes_Header())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(IPCC_Codes_PageObject.IPCC_Codes_Header())){
                error = "Failed to wait for IPCC Codes header to be present.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Continue' button.");
        narrator.stepPassedWithScreenShot("Viewing Rreports.");

        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(IPCC_Codes_PageObject.iframeXpath())) {
            error = "Failed to wait for frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(IPCC_Codes_PageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        //View full reports icon
        if (!SeleniumDriverInstance.waitForElementByXpath(IPCC_Codes_PageObject.viewFullReportsIcon())) {
            error = "Failed to wait for 'View full reports' icon.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IPCC_Codes_PageObject.viewFullReportsIcon())) {
            error = "Failed to click on 'View full reports' icon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'View full reports' icon.");
        SeleniumDriverInstance.switchToTabOrWindow();
        
        //Continue button
        if (!SeleniumDriverInstance.waitForElementByXpath(IPCC_Codes_PageObject.continueBtn())) {
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IPCC_Codes_PageObject.continueBtn())) {
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        pause(4000);
        SeleniumDriverInstance.switchToTabOrWindow();
        narrator.stepPassedWithScreenShot("Successfully click 'Continue' button.");
        
        if(!SeleniumDriverInstance.waitForElementByXpath(IPCC_Codes_PageObject.ActivityReference_Header())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(IPCC_Codes_PageObject.ActivityReference_Header())){
                error = "Failed to wait for Activity Reference header to be present.";
                return false;
            }
        }        
        narrator.stepPassedWithScreenShot("Viewing Full Rreports.");
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        
        pause(2000);
        
        return true;
    }

}
