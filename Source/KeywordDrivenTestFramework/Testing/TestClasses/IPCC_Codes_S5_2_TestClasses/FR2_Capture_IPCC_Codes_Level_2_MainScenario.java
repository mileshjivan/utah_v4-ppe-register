/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.IPCC_Codes_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.IPCC_Codes_V5_2_PageObjects.IPCC_Codes_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR2-Capture IPCC Codes Level 2 v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_IPCC_Codes_Level_2_MainScenario extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_Capture_IPCC_Codes_Level_2_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Capture_IPCC_Codes_Level_2()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured IPCC Codes Level 2 record.");
    }

    public boolean Capture_IPCC_Codes_Level_2() {
        //IPCC Codes Level 2 Panel
        if (!SeleniumDriverInstance.waitForElementByXpath(IPCC_Codes_PageObject.IPCC_Codes_Level_2_Panel())) {
            error = "Failed to wait for 'IPCC Codes Level 2' panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IPCC_Codes_PageObject.IPCC_Codes_Level_2_Panel())) {
            error = "Failed to click on 'IPCC Codes Level 2' panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'IPCC Codes Level 2' panel.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(IPCC_Codes_PageObject.IPCC_Codes_Level_2_Add_Button())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IPCC_Codes_PageObject.IPCC_Codes_Level_2_Add_Button())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(IPCC_Codes_PageObject.Level2_process_flow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(IPCC_Codes_PageObject.Level2_process_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");

        //IPCC code
        if (!SeleniumDriverInstance.enterTextByXpath(IPCC_Codes_PageObject.IPCC_code2(), getData("IPCC code"))) {
            error = "Failed to '" + getData("IPCC code") + "' into IPCC code field";
            return false;
        }
        narrator.stepPassedWithScreenShot("IPCC code : '" + getData("IPCC code") + "'.");
        
        //Activity
        if (!SeleniumDriverInstance.enterTextByXpath(IPCC_Codes_PageObject.Activity2(), getData("Activity"))) {
            error = "Failed to '" + getData("Activity") + "' into Activity field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Activity : '" + getData("Activity") + "'.");
        
        //Unit of measure
        if(!SeleniumDriverInstance.waitForElementByXpath(IPCC_Codes_PageObject.UnitOfMeasure_Dropdown2())){
            error = "Failed to wait for the 'Unit of measure' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(IPCC_Codes_PageObject.UnitOfMeasure_Dropdown2())){
            error = "Failed to click the 'Unit of measure' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(IPCC_Codes_PageObject.UnitOfMeasure_Option(getData("Unit of measure")))) {
            error = "Failed to wait for 'Unit of measure': " + getData("Unit of measure");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IPCC_Codes_PageObject.UnitOfMeasure_Option(getData("Unit of measure")))) {
            error = "Failed to click on 'Unit of measure': " + getData("Unit of measure");
            return false;
        }
        narrator.stepPassedWithScreenShot("Unit of measure: " + getData("Unit of measure"));
        
        //Save button        
        if (!SeleniumDriverInstance.waitForElementByXpath(IPCC_Codes_PageObject.SaveButton_Level2())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(IPCC_Codes_PageObject.SaveButton_Level2())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(IPCC_Codes_PageObject.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(IPCC_Codes_PageObject.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(IPCC_Codes_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
        
        //Close Button
        if(!SeleniumDriverInstance.waitForElementByXpath(IPCC_Codes_PageObject.Level2_CloseButton())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(IPCC_Codes_PageObject.Level2_CloseButton())){
                error = "Failed to wait for the IPCC Code Level 2 close button.";
                return false;
            }
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(IPCC_Codes_PageObject.Level2_CloseButton())){
            error = "Failed to click the IPCC Code Level 2 close button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked IPCC Code Level 2.");
        
        pause(2000);
        //Updated record
        if(!SeleniumDriverInstance.waitForElementByXpath(IPCC_Codes_PageObject.Level2_Record(getData("IPCC code")))){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(IPCC_Codes_PageObject.Level2_Record(getData("IPCC code")))){
                error = "Failed to wait for the IPCC Codes Level 2 Record.";
                return false;
            }
        }
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(IPCC_Codes_PageObject.process_flow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(IPCC_Codes_PageObject.process_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");
        
        return true;
    }
}
