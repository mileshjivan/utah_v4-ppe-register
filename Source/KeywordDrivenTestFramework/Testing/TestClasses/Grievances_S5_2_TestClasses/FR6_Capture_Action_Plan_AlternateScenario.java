/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Grievances_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Grievances_V5_2_PageObjects.Grievances_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR6-Capture Action Plan - Alternate Scenario",
        createNewBrowserInstance = false
)

public class FR6_Capture_Action_Plan_AlternateScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String grievanceTitle;

    public FR6_Capture_Action_Plan_AlternateScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        grievanceTitle = getData("Grievance title") + "_" + SeleniumDriverInstance.generateDateTimeString();
        Grievances_PageObject.setGrievanceTitle(grievanceTitle);
    }

    public TestResult executeTest() {
        if (!captureActionPlan()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured 'Initial Response' record.");
    }

    public boolean captureActionPlan() {
        
        //STEP 4: Action Plan tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.actionPlanTab())) {
            error = "Failed to wait for 'STEP 4: Action Plan' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.actionPlanTab())) {
            error = "Failed to click on 'STEP 4: Action Plan' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'STEP 4: Action Plan' tab");
        
        //Scroll to Action description field
        if (!SeleniumDriverInstance.scrollToElement(Grievances_PageObject.receptionDate())) {
            error = "Failed to scroll to 'Action description' field.";
            return false;
        }
        
        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.actionPlanAdd())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.actionPlanAdd())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Add' button");
        
        //Process flow button
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.actionPlanProcessFlow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.actionPlanProcessFlow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Process flow' button");
        
        //Action description field
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.actionPlanDescription())) {
            error = "Failed to wait for 'Action description' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Grievances_PageObject.actionPlanDescription(), getData("Action description"))) {
            error = "Failed to enter '" + getData("Action description") + "' into Action description field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + getData("Action description") + "' into Action description field.");
        
        //Department responsible dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.departmentResponsible())) {
            error = "Failed to wait for 'Department responsible' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.departmentResponsible())) {
            error = "Failed to click on 'Department responsible' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Department responsible dropdown");
        
        //Department responsible select
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.BusinessUnit_Option1(getData("Business Unit 1"))))
        {
            error = "Failed to wait for Business Unit Option: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.BusinessUnit_Option1(getData("Business Unit 1"))))
        {
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.BusinessUnit_Option1(getData("Business Unit 2"))))
        {
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.BusinessUnit_Option(getData("Business Unit 3"))))
        {
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 3");
            return false;
        }
        narrator.stepPassedWithScreenShot("Business Unit: " + getData("Business Unit 1") + " -> " + getData("Business Unit 2") + " -> " + getData("Business Unit 3"));
        
        //Responsible person dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.responsiblePerson())) {
            error = "Failed to wait for 'Responsible person' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.responsiblePerson())) {
            error = "Failed to click on 'Responsible person' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Responsible person dropdown");
        
        //Responsible person select
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.actionPlanResponsiblePerson_Select(getData("Responsible person")))) {
            error = "Failed to wait for Responsible person option :" + getData("Responsible person");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.actionPlanResponsiblePerson_Select(getData("Responsible person")))) {
            error = "Failed to click on Responsible person option :" + getData("Responsible person");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Responsible person") + "' option");
        
        //Action due date field
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.actionDueDate())) {
            error = "Failed to wait for 'Action due date' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Grievances_PageObject.actionDueDate(), endDate)) {
            error = "Failed to enter '" + endDate + "' into Action due date field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + endDate + "' into Action due date field.");
        
        //Save
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Grievances_PageObject.actionPlanSaveBtn())) {
            error = "Failed to wait for Save button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.actionPlanSaveBtn())) {
            error = "Failed to wait for Save button";
            return false;
        }
        
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Grievances_PageObject.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Save button");

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Grievances_PageObject.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Grievances_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
        
        //Close action plan
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.closeActionPlan())) {
            error = "Failed to wait for 'Close action plan' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.closeActionPlan())) {
            error = "Failed to click on 'Close action plan' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Close action plan' button");
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.grievanceProcess_flow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.grievanceProcess_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        
        //Has the action plan been completed? dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.actionPlanCompeted())) {
            error = "Failed to wait for 'Has the action plan been completed?' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.actionPlanCompeted())) {
            error = "Failed to click on 'Has the action plan been completed?' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Action plan completed dropdown");
        
        //Has the initial response been completed? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.actionPlanCompeted_Select(getData("Action plan completed")))) {
            error = "Failed to wait for Has the action plan been completed? option :" + getData(" Action plan completed");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.actionPlanCompeted_Select(getData("Action plan completed")))) {
            error = "Failed to click on Has the action plan been completed? option :" + getData("Action plan completed");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Action plan completed") + "' option");
        
        //Save
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Grievances_PageObject.GrievanceSaveBtn())) {
            error = "Failed to wait for Save button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.GrievanceSaveBtn())) {
            error = "Failed to wait for Save button";
            return false;
        }
        
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Grievances_PageObject.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Save button");
        
        return true;
    }
}
