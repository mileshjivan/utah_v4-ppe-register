/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Grievances_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Grievances_V5_2_PageObjects.Grievances_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR8 Capture Resolve - Main Scenario",
        createNewBrowserInstance = false
)
public class FR8_Capture_Resolve_Main_Scenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String grievanceTitle;

    public FR8_Capture_Resolve_Main_Scenario()
    {

        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());

    }

    public TestResult executeTest()
    {
        if (!Capture_Resolve())
        {
            return narrator.testFailed("Capture Resolve Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured resolve");
    }

    public boolean Capture_Resolve()
    {
        //pause(3000);
        //right arrow 
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.right_arrow()))
        {
            error = "Failed to wait for scroll right arrow";
            return false;
        }
        pause(4000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.right_arrow()))
        {
            error = "Failed to click on scroll right arrow";
            return false;
        }

        //Navigate to STEP 6: Resolve tab.
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.Resolve_tab()))
        {
            error = "Failed to wait for STEP 6: Resolve tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.Resolve_tab()))
        {
            error = "Failed to click on STEP 6: Resolve tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked to Resolve tab.");
        pause(2000);

        // Process flow
//        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.grievanceProcess_flow()))
//        {
//            error = "Failed to wait for 'Process flow' button.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.grievanceProcess_flow()))
//        {
//            error = "Failed to click on 'Process flow' button.";
//            return false;
//        }
        //Grievance outcome
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.Grievance_outcome()))
        {
            error = "Failed to wait for Grievance outcome drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.Grievance_outcome()))
        {
            error = "Failed to click on Grievance outcome drop down";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected  Grievance outcome drop down.");
        pause(2000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.visible_drop_down_transition(getData("Grievance outcome"))))
        {
            error = "Failed to wait for Grievance outcome drop down option :" + getData("Grievance outcome");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.visible_drop_down_transition(getData("Grievance outcome"))))
        {
            error = "Failed to click on Grievance outcome drop down option :" + getData("Grievance outcome");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Grievance outcome :" + getData("Grievance outcome"));
        pause(2000);

        //Accept
        String Accept = getData("Grievance outcome");
        if (Accept.equalsIgnoreCase("Objection"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.Accept()))
            {
                error = "Failed to wait for Accept / reject the objection? drop down.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.Accept()))
            {
                error = "Failed to click on Accept / reject the objection? drop down";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked Accept / reject the objection? drop down.");
            pause(2000);

            if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.visible_drop_down_transition(getData("Accept"))))
            {
                error = "Failed to wait for Accept / reject the objection? drop down option :" + getData("Accept");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.visible_drop_down_transition(getData("Accept"))))
            {
                error = "Failed to click on Accept / reject the objection? drop down option :" + getData("Accept");
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked Accept / reject the objection? :" + getData("Accept"));
            pause(2000);
        }

        //Comments on outcome
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.Comments_on_outcome()))
        {
            error = "Failed to wait for Comments on outcome :" + getData("Comments on outcome");
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Grievances_PageObject.Comments_on_outcome(), getData("Comments on outcome")))
        {
            error = "Failed to enter Comments on outcome :" + getData("Comments on outcome");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Comments on outcome:" + getData("Comments on outcome"));

        //Outcome received by
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.Outcome_received_by()))
        {
            error = "Failed to wait for Outcome received by drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.Outcome_received_by()))
        {
            error = "Failed to click on Outcome received by drop down";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Outcome received by drop down.");
        pause(2000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.visible_drop_down_transition(getData("Outcome received by"))))
        {
            error = "Failed to wait for Outcome received by drop down option :" + getData("Outcome received by");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.visible_drop_down_transition(getData("Outcome received by"))))
        {
            error = "Failed to click on Outcome received by drop down option :" + getData("Grievance outcome");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Outcome received by :" + getData("Grievance outcome"));
        pause(2000);

        //Outcome received
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.Return()))
        {
            error = "Failed to wait for Outcome received drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.Return()))
        {
            error = "Failed to click on return drop down";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Outcome received drop down.");
        pause(2000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.visible_drop_down_transition(getData("return"))))
        {
            error = "Failed to wait for Outcome received drop down option :" + getData("return");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.visible_drop_down_transition(getData("return"))))
        {
            error = "Failed to click on Outcome received drop down option :" + getData("return");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked return :" + getData("return"));
        pause(2000);

        String outcome_data = SeleniumDriverInstance.retrieveTextByXpath(Grievances_PageObject.Outcome_data());
        //validate auto populated date
        //startDate(// need to trim this date than compare it to the auto populted one)
//        if (outcome_data.equalsIgnoreCase(user))
//        {
//            narrator.stepPassedWithScreenShot("Successfully  validated outcome date :"+outcome_data);
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.SaveAndContinue_STEP7()))
        {
            error = "Failed to wait for return drop down option :" + getData("return");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.SaveAndContinue_STEP7()))
        {
            error = "Failed to click on return drop down option :" + getData("return");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked return :" + getData("return"));
        pause(2000);

        //Save
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Grievances_PageObject.GrievanceSaveBtn()))
        {
            error = "Failed to wait for Save button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.GrievanceSaveBtn()))
        {
            error = "Failed to wait for Save button";
            return false;
        }

        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Grievances_PageObject.saveWait2()))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Save button");

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Grievances_PageObject.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Grievances_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
        return true;
    }

}
