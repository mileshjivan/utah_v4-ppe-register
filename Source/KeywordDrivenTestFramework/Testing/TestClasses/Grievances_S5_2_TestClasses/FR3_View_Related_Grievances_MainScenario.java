/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Grievances_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Grievances_V5_2_PageObjects.Engagements_PageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Grievances_V5_2_PageObjects.Grievances_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR3-View Related Grievances v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR3_View_Related_Grievances_MainScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;

    public FR3_View_Related_Grievances_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!NavigateToGrievances()) {
            return narrator.testFailed("Failed due - " + error);
        }
        if(!navigateToRelatedEngagement()){
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully viewed related grievances record.");
    }

    public boolean NavigateToGrievances() {
        String GRIEVANCE_TITLE = Engagements_PageObject.getGrievanceTitle();

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            error = "Failed to switch to default content.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.isoHome())) {
            error = "Failed to wait for 'ISOMETRIX' home button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.isoHome())) {
            error = "Failed to click on 'ISOMETRIX' home button.";
            return false;
        }

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Engagements_PageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
        }

        //Navigate to Social Sustainability
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.navigate_socialSustainability())) {
            error = "Failed to wait for 'Social Sustainability' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.navigate_socialSustainability())) {
            error = "Failed to click on 'Social Sustainability' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'navigate_socialSustainability' button.");
        pause(1000);

        //Navigate to Complaints & Grievances
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.navigate_complaintsGrievances())) {
            error = "Failed to wait for 'Complaints & Grievances' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.navigate_complaintsGrievances())) {
            error = "Failed to click on 'Stakeholders' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Grievances' serch page.");
        pause(2000);

        //Grievance title field
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.searchGrievanceTitle())) {
            error = "Failed to wait for 'Grievance title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Engagements_PageObject.searchGrievanceTitle(), GRIEVANCE_TITLE)) {
            error = "Failed to click enter '" + GRIEVANCE_TITLE + "' into 'Grievance title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + GRIEVANCE_TITLE + "' into 'Grievance title' field.");

        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.searchBtn())) {
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.searchBtn())) {
            error = "Failed to click on 'Search' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");

        //Click grievance record
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.clickGrievanceRecord())) {
            error = "Failed to wait for 'Grievance' record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.clickGrievanceRecord())) {
            error = "Failed to click on 'Grievance' record.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully click 'Grievance' record.");

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.grievance_Process_flow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.grievance_Process_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        return true;
    }

    public boolean navigateToRelatedEngagement() {
        //Grievance location 
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.GrievanceLocationTab())) {
            error = "Failed to wait for Grievance location dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.GrievanceLocationTab())) {
            error = "Failed to click the Grievance location dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.BusinessUnit_Option1(getData("Grievance location 1")))) {
            error = "Failed to wait for Grievance location Option: " + getData("Grievance location 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.BusinessUnit_Option1(getData("Grievance location 1")))) {
            error = "Failed to click the Grievance location Option: " + getData("Grievance location 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.BusinessUnit_Option1(getData("Grievance location 2")))) {
            error = "Failed to click the Grievance location Option: " + getData("Grievance location 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.BusinessUnit_Option1(getData("Grievance location 3")))) {
            error = "Failed to click the Grievance location Option: " + getData("Grievance location 3");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.BusinessUnit_Option(getData("Grievance location 4")))) {
            error = "Failed to click the Grievance location Option: " + getData("Grievance location 4");
            return false;
        }
        narrator.stepPassedWithScreenShot("Grievance location: " + getData("Grievance location 1") + " -> " + getData("Grievance location 2") + " -> " + getData("Grievance location 3") + " -> " + getData("Grievance location 4"));

        //Registration complete dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.RegistrationComplete())) {
            error = "Failed to wait for 'Registration complete' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.RegistrationComplete())) {
            error = "Failed to click on 'Registration complete' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.RegistrationComplete_Select(getData("Registration complete")))) {
            error = "Failed to wait for Registration complete option :" + getData("Registration complete");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.RegistrationComplete_Select(getData("Registration complete")))) {
            error = "Failed to click on Registration complete :" + getData("Registration complete");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Registration complete") + "' option");
        
        //Related Engagement
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.RelatedEngagement())) {
            error = "Failed to wait for Related Engagement tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.RelatedEngagement())) {
            error = "Failed to clicked the Related Engagement tab.";
            return false;
        }
        
        //Related Engagement Panel
        if(!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.RelateEngagement_Record(getData("Engagement Description")))){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Grievances_PageObject.RelateEngagement_Record(getData("Engagement Description")))){
                error = "Failed to wait for the Related Engagement Record.";
                return false;
            }
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.RelateEngagement_Record(getData("Engagement Description")))){
            error = "Failed to click the Related Engagement Record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Viewed Related Engagement Record - Engagement Description: " + getData("Engagement Description"));
        

        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Engagements_PageObject.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Engagements_PageObject.process_flow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Engagements_PageObject.process_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");
        
        
        return true;

    }
}
