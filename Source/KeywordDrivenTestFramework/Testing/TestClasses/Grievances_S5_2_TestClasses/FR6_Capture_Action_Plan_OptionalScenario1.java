/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Grievances_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Grievances_V5_2_PageObjects.Grievances_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR6-Capture Action Plan - Optional Scenario 1",
        createNewBrowserInstance = false
)
public class FR6_Capture_Action_Plan_OptionalScenario1 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String grievanceTitle;

    public FR6_Capture_Action_Plan_OptionalScenario1()
    {

        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());

    }

    public TestResult executeTest()
    {
        if (!navigateToGrievanceGistory())
        {
            return narrator.testFailed("Capture Resolve Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured resolve");
    }

    public boolean navigateToGrievanceGistory()
    {
        //Navigate to Grievance history tab.
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.grievanceHistory()))
        {
            error = "Failed to wait for Grievance history tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.grievanceHistory()))
        {
            error = "Failed to click on Grievance history tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Grievance history tab.");

        //Grievance history record
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.grievanceHistoryRecord()))
        {
            error = "Failed to wait for Grievance history record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.grievanceHistoryRecord()))
        {
            error = "Failed to click on Grievance history record";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully selected Grievance history record.");

        //Process flow
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Grievances_PageObject.grievanceHistory_ProcessFlow()))
        {
            error = "Failed to wait for Process flow button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.grievanceHistory_ProcessFlow()))
        {
            error = "Failed to click on Process flow button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked process flow button");

        return true;
    }

}
