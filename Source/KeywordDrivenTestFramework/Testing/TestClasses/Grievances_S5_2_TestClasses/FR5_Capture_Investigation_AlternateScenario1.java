/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Grievances_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Grievances_V5_2_PageObjects.Grievances_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR5-Capture Investigation - Alternate Scenario 1",
        createNewBrowserInstance = false
)

public class FR5_Capture_Investigation_AlternateScenario1 extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String grievanceTitle;

    public FR5_Capture_Investigation_AlternateScenario1() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        grievanceTitle = getData("Grievance title") + "_" + SeleniumDriverInstance.generateDateTimeString();
        Grievances_PageObject.setGrievanceTitle(grievanceTitle);
    }

    public TestResult executeTest() {
        if (!captureInvestigation()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured 'Initial Response' record.");
    }

    public boolean captureInvestigation() {
        
        //STEP 3 : Investigation tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.investigationTab())) {
            error = "Failed to wait for 'STEP 3 : Investigation' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.investigationTab())) {
            error = "Failed to click on 'STEP 3 : Investigation' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'STEP 3 : Investigation' tab");
        
        //Investigation results field
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.investigationResults())) {
            error = "Failed to wait for 'Investigation results' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Grievances_PageObject.investigationResults(), getData("Investigation results"))) {
            error = "Failed to enter '" + getData("Investigation results") + "' into Investigation results field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '" + getData("Investigation results") + "' into Investigation results field.");

        //Add support document - Hyperlink
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.linkbox2())) {
            error = "Failed to wait for 'Link box' link.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.linkbox2())) {
            error = "Failed to click on 'Link box' link.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");

        //switch to new window
        if (!SeleniumDriverInstance.switchToTabOrWindow()) {
            error = "Failed to switch to new window or tab.";
            return false;
        }

        //URL https
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.LinkURL())) {
            error = "Failed to wait for 'URL value' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Grievances_PageObject.LinkURL(), getData("Document Link"))) {
            error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.urlTitle())) {
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Grievances_PageObject.urlTitle(), getData("Title"))) {
            error = "Failed to enter '" + getData("Title") + "' into 'Url Title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Title : '" + getData("Title") + "'.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.urlAddButton())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.urlAddButton())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Grievances_PageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Uploaded supporting documents.");
        
        //Has the investigation been completed? dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.investigationCompleted())) {
            error = "Failed to wait for 'Has the investigation been completed?' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.investigationCompleted())) {
            error = "Failed to click on 'Has the investigation been completed?' dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Has the investigation been completed? dropdown");
        
        //Has the initial response been completed? select
        if (!SeleniumDriverInstance.waitForElementByXpath(Grievances_PageObject.RegistrationComplete_Select(getData("Investigation completed")))) {
            error = "Failed to wait for Has the initial response been completed? option :" + getData("Investigation completed");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.RegistrationComplete_Select(getData("Investigation completed")))) {
            error = "Failed to click on Has the initial response been completed? option :" + getData("Investigation completed");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click '" + getData("Investigation completed") + "' option");
        
        //Save
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Grievances_PageObject.GrievanceSaveBtn())) {
            error = "Failed to wait for Save button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Grievances_PageObject.GrievanceSaveBtn())) {
            error = "Failed to wait for Save button";
            return false;
        }
        
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Grievances_PageObject.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Save button");

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Grievances_PageObject.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Grievances_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        return true;
    }
}
