/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Permit_To_Work_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Permit_To_Work_V5_2_PageObjects.Permit_To_Work_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "UC PMW 0102 - Updating the Permit request – Permit Approval - Alternate Scenario 2",
        createNewBrowserInstance = false
)

public class UC_PMW_0102_Updating_The_Permit_Request_Permit_Approval_AlternateScenario2 extends BaseClass
{

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public UC_PMW_0102_Updating_The_Permit_Request_Permit_Approval_AlternateScenario2()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Capture_Permit_Approval())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Captured Permit Approval");
    }

    public boolean Capture_Permit_Approval()
    {
        //Permit approval
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.PermitApproval_Dropdown()))
        {
            error = "Failed to wait for Permit approval dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.PermitApproval_Dropdown()))
        {
            error = "Failed to click the Permit approval dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Permit approval dropdown.");

        //Permit approval select
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.Project_Option(getData("Permit approval"))))
        {
            error = "Failed to wait for Permit approval option: " + getData("Permit approval");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Project_Option(getData("Permit approval"))))
        {
            error = "Failed to click the Permit approval option: " + getData("Permit approval");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Permit approval option: " + getData("Permit approval"));

        //Permit rejection comments
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.PermitRejectionComments()))
        {
            error = "Failed to wait for Permit rejection comments field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Permit_To_Work_PageObjects.PermitRejectionComments(), getData("Permit rejection comments")))
        {
            error = "Failed to enter '" + getData("Permit rejection comments") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Permit rejection comments") + "'.");

        //Save Button
        if (!getData("Save To Continue").equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.CancelPermitRequestButton()))
            {
                error = "Failed to wait for Cancel Permit Request Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.CancelPermitRequestButton()))
            {
                error = "Failed to click on Cancel Permit Request Button";
                return false;
            }
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.SaveToContinue_Button()))
            {
                error = "Failed to wait for Save to continue Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.SaveToContinue_Button()))
            {
                error = "Failed to click on Save to continue Button";
                return false;
            }
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Cancel Permit Request Button.");
        pause(1000);
        
        //Search
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.SearchButton()))
        {
            error = "Failed to wait for Search Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.SearchButton()))
        {
            error = "Failed to click on Search Button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Search Button.");
        
        //Click Permit to work record
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.ClickPermitToWorkRecord()))
        {
            error = "Failed to wait for Permit to work record";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.ClickPermitToWorkRecord()))
        {
            error = "Failed to click on Permit to work record";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Permit to work record.");
        pause(1000);
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.PermitToWork_ProcessFlow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.PermitToWork_ProcessFlow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        return true;
    }

}
