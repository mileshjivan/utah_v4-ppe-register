/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Permit_To_Work_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Permit_To_Work_V5_2_PageObjects.Permit_To_Work_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR2 Capture Permit Acceptance - Alternate Scenario",
        createNewBrowserInstance = false
)
public class FR2_Capture_Permit_Acceptance_Alternate_Scenario extends BaseClass
{

    String error = "";

    public FR2_Capture_Permit_Acceptance_Alternate_Scenario()
    {

        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Alternate_Scenario())
        {
            return narrator.testFailed("Alternate Scenario Failed due to - " + error);
        }
         return narrator.finalizeTest("Permit Acceptanced Successfully");
    }

    public boolean Alternate_Scenario()
    {
        String Accept = getData("Accept");
        String Declare = getData("Declare");

        if (Accept.equalsIgnoreCase("True") && Declare.equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Accept()))
            {
                error = "Failed to click on the Accept check box";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Declare()))
            {
                error = "Failed to click on the Declare check box";
                return false;
            }
            
            narrator.stepPassedWithScreenShot("Clicked Declare check box");

        } else
        {

        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.SaveButton()))
        {
            error = "Failed to wait for Save Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.SaveButton()))
        {
            error = "Failed to click on Save Button";
            return false;
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Permit_To_Work_PageObjects.saveWait2(), 400))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Permit_To_Work_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Permit_To_Work_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        return true;
    }
}
