/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Permit_To_Work_S5_2_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Bowtie_Risk_Assessment_S5_2_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.Bowtie_Risk_Assessment_PageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Permit_To_Work_V5_2_PageObjects.Permit_To_Work_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR3- Permit to Work Automatically Expires - Main Scenario",
        createNewBrowserInstance = false
)

public class FR3_Permit_To_Work_Automatically_Expires_MainScenario extends BaseClass
{

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR3_Permit_To_Work_Automatically_Expires_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Navigate_TO_Permit_To_Work())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Check_Status())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Permit To Work");
    }

    public boolean Navigate_TO_Permit_To_Work()
    {
        //Tailings Management
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.TailingsManagement_Module()))
        {
            error = "Failed to wait for 'Tailings Management' module.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.TailingsManagement_Module()))
        {
            error = "Failed to click on 'Tailings Management' module.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Tailings Management' module.");

        //Risk Management module
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.RiskManagement_Module()))
        {
            error = "Failed to wait for 'Risk Management' module.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.RiskManagement_Module()))
        {
            error = "Failed to click on 'Risk Management' module.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Risk Management' module.");

        //Navigate to Permit To Work
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.Permit_To_Work_Module()))
        {
            error = "Failed to wait for 'Permit To Work' module";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Permit_To_Work_Module()))
        {
            error = "Failed to click on 'Permit To Work' module";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Permit To Work' module search page.");

        return true;
    }

    public boolean Check_Status()
    {
        //Status
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.Status_Dropdown()))
        {
            error = "Failed to wait for Status dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Status_Dropdown()))
        {
            error = "Failed to click the Status dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Status dropdown.");
        
        //Type of permit select
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.TypeOfPermit_Option(getData("Status"))))
        {
            error = "Failed to wait for Status option: " + getData("Status");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.TypeOfPermit_Option(getData("Status"))))
        {
            error = "Failed to click the Status option: " + getData("Type of permit");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Status option: " + getData("Status"));
        
        //Record number
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.RecordNo_Field()))
        {
            error = "Failed to wait for Record number field.";
            return false;
        }
         
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.RecordNo_Field()))
        {
            error = "Failed to click the Record number field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Permit_To_Work_PageObjects.RecordNo_Field(), getData("Record number")))
        {
            error = "Failed to enter '" + getData("Record number") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Record number") + "'.");
        
        //Search
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.SearchButton()))
        {
            error = "Failed to wait for Search Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.SearchButton()))
        {
            error = "Failed to click on Search Button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Search Button.");
        
        //Click Permit to work record
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.ClickPermitToWorkRecord()))
        {
            error = "Failed to wait for Permit to work record";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.ClickPermitToWorkRecord()))
        {
            error = "Failed to click on Permit to work record";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Permit to work record.");
        pause(2000);
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.PermitToWork_ProcessFlow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.PermitToWork_ProcessFlow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        return true;
    }

}
