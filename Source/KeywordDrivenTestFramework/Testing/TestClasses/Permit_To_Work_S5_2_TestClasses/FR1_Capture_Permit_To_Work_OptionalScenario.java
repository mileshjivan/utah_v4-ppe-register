/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Permit_To_Work_S5_2_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Bowtie_Risk_Assessment_S5_2_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.Bowtie_Risk_Assessment_PageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.Permit_To_Work_V5_2_PageObjects.Permit_To_Work_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Bowtie_Risk_Assessment_V5_2_PageObject.IsometricsPOCPageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Permit To Work - Optional Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Permit_To_Work_OptionalScenario extends BaseClass
{

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Permit_To_Work_OptionalScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Navigate_TO_Permit_To_Work())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Capture_Permit_To_Work())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Permit To Work");
    }

    public boolean Navigate_TO_Permit_To_Work()
    {
        //Tailings Management
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.TailingsManagement_Module()))
        {
            error = "Failed to wait for 'Tailings Management' module.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.TailingsManagement_Module()))
        {
            error = "Failed to click on 'Tailings Management' module.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Tailings Management' module.");

        //Risk Management module
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.RiskManagement_Module()))
        {
            error = "Failed to wait for 'Risk Management' module.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.RiskManagement_Module()))
        {
            error = "Failed to click on 'Risk Management' module.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Risk Management' module.");

        //Navigate to Permit To Work
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.Permit_To_Work_Module()))
        {
            error = "Failed to wait for 'Permit To Work' module";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Permit_To_Work_Module()))
        {
            error = "Failed to click on 'Permit To Work' module";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Permit To Work' module search page.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.PermitToWork_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.PermitToWork_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean Capture_Permit_To_Work()
    {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.PermitToWork_ProcessFlow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.PermitToWork_ProcessFlow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //Business Unit 
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.BusinessUnit_Dropdown()))
        {
            error = "Failed to wait for Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.BusinessUnit_Dropdown()))
        {
            error = "Failed to click the Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.BusinessUnit_Option1(getData("Business Unit 1"))))
        {
            error = "Failed to wait for Business Unit Option: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.BusinessUnit_Option1(getData("Business Unit 1"))))
        {
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.BusinessUnit_Option1(getData("Business Unit 2"))))
        {
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.BusinessUnit_Option(getData("Business Unit 3"))))
        {
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 3");
            return false;
        }
        narrator.stepPassedWithScreenShot("Business Unit: " + getData("Business Unit 1") + " -> " + getData("Business Unit 2") + " -> " + getData("Business Unit 3"));

        //Project Link Checkbox
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.ProjectLink_Checkbox()))
        {
            error = "Failed to wait for Project Link Checkbox.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.ProjectLink_Checkbox()))
        {
            error = "Failed to click on Project Link Checkbox.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Project Link Checkbox.");

        //Specific location
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.Specific_Locaion()))
        {
            error = "Failed to wait for Specific location field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Permit_To_Work_PageObjects.Specific_Locaion(), getData("Specific location")))
        {
            error = "Failed to enter '" + getData("Specific location") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Specific location") + "'.");

        //Project
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.Project_Dropdown()))
        {
            error = "Failed to wait for Project dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Project_Dropdown()))
        {
            error = "Failed to click the Project dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Project dropdown.");

        //Project select
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.Project_Select(getData("Project"))))
        {
            error = "Failed to wait for Project option: " + getData("Project");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Project_Select(getData("Project"))))
        {
            error = "Failed to click the Project option: " + getData("Project");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Project option: " + getData("Project"));

        //Related stakeholder
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.RelatedStakeholder_Dropdown()))
        {
            error = "Failed to wait for Related stakeholder dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.RelatedStakeholder_Dropdown()))
        {
            error = "Failed to click the Related stakeholder dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Related stakeholder dropdown.");

        //Related stakeholder select
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.Project_Option(getData("Related stakeholder"))))
        {
            error = "Failed to wait for Project option: " + getData("Related stakeholder");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Project_Option(getData("Related stakeholder"))))
        {
            error = "Failed to click the Project option: " + getData("Related stakeholder");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Project option: " + getData("Related stakeholder"));

        //Process/Activity
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.ProcessActivity_Dropdown()))
        {
            error = "Failed to wait for Process/Activity dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.ProcessActivity_Dropdown()))
        {
            error = "Failed to click the Process/Activity dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Process/Activity dropdown.");

        //Related stakeholder select
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.Project_Option(getData("Process/Activity"))))
        {
            error = "Failed to wait for Process/Activity option: " + getData("Process/Activity");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Project_Option(getData("Process/Activity"))))
        {
            error = "Failed to click the Process/Activity option: " + getData("Process/Activity");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Process/Activity option: " + getData("Process/Activity"));

        //Type of permit
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.TypeOfPermit_Dropdown_2()))
        {
            error = "Failed to wait for Type of permit dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementByJavascript(Permit_To_Work_PageObjects.TypeOfPermit_Dropdown_2()))
        {
            error = "Failed to click the Type of permit dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Type of permit dropdown.");

        //Type of permit select
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.TypeOfPermit_Option(getData("Type of permit"))))
        {
            error = "Failed to wait for Type of permit option: " + getData("Type of permit");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.TypeOfPermit_Option(getData("Type of permit"))))
        {
            error = "Failed to click the Type of permit option: " + getData("Type of permit");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.CloseTypeOfPermit_Dropdown()))
        {
            error = "Failed to click the Type of permit dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Type of permit requested option: " + getData("Type of permit"));

        //Description of work
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.DescriptionOfWork()))
        {
            error = "Failed to wait for Description of work field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Permit_To_Work_PageObjects.DescriptionOfWork(), getData("Description of work")))
        {
            error = "Failed to enter '" + getData("Description of work") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + getData("Description of work") + "'.");

        //Person applying
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.PersonApplying_Dropdown()))
        {
            error = "Failed to wait for Person applying dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.PersonApplying_Dropdown()))
        {
            error = "Failed to click the Person applying dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Person applying dropdown.");

        //Related stakeholder select
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.Project_Option(getData("Person applying"))))
        {
            error = "Failed to wait for Person applying option: " + getData("Person applying");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Project_Option(getData("Person applying"))))
        {
            error = "Failed to click the Person applying option: " + getData("Person applying");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Person applying option: " + getData("Person applying"));

        //Valid from date
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.ValidFromDate()))
        {
            error = "Failed to wait for Valid from date field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Permit_To_Work_PageObjects.ValidFromDate(), startDate))
        {
            error = "Failed to enter '" + getData("Valid from date") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + startDate + "'.");

        //Valid to date
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.ValidToDate()))
        {
            error = "Failed to wait for Valid to date field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Permit_To_Work_PageObjects.ValidToDate(), endDate))
        {
            error = "Failed to enter '" + getData("Valid to date") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered: '" + endDate + "'.");

        //Save Button
        if (!getData("Save To Continue").equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.SaveButton()))
            {
                error = "Failed to wait for Save Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.SaveButton()))
            {
                error = "Failed to click on Save Button";
                return false;
            }
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.SaveToContinue_Button()))
            {
                error = "Failed to wait for Save to continue Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.SaveToContinue_Button()))
            {
                error = "Failed to click on Save to continue Button";
                return false;
            }
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Permit_To_Work_PageObjects.saveWait2(), 400))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Permit_To_Work_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Permit_To_Work_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");
        
         //Permit approval
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.PermitApproval_Dropdown()))
        {
            error = "Failed to wait for Permit approval dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.PermitApproval_Dropdown()))
        {
            error = "Failed to click the Permit approval dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Permit approval dropdown.");

        //Permit approval select
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.Project_Option(getData("Permit approval"))))
        {
            error = "Failed to wait for Permit approval option: " + getData("Permit approval");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Project_Option(getData("Permit approval"))))
        {
            error = "Failed to click the Permit approval option: " + getData("Permit approval");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Permit approval option: " + getData("Permit approval"));

        //Supporting Documents tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.supporting_tab()))
        {
            error = "Failed to wait for Supporting Documents tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.supporting_tab()))
        {
            error = "Failed to click Supporting Documents tab.";
            return false;
        }
        //Add support document - Hyperlink
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.linkbox()))
        {
            error = "Failed to wait for 'Link box' link.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.linkbox()))
        {
            error = "Failed to click on 'Link box' link.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");

        //switch to new window
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new window or tab.";
            return false;
        }

        //URL https
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.LinkURL()))
        {
            error = "Failed to wait for 'URL value' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Permit_To_Work_PageObjects.LinkURL(), getData("Document Link")))
        {
            error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.urlTitle()))
        {
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Permit_To_Work_PageObjects.urlTitle(), getData("URL Title")))
        {
            error = "Failed to enter '" + getData("URL Title") + "' into 'Url Title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("URL Title : '" + getData("URL Title") + "'.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.urlAddButton()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.urlAddButton()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Permit_To_Work_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");

        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.SaveButton()))
        {
            error = "Failed to wait for Save Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.SaveButton()))
        {
            error = "Failed to click on Save Button";
            return false;
        }
        
        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Permit_To_Work_PageObjects.saveWait2(), 400))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully clicked Save Button.");
       
        return true;
    }

}
