/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Permit_To_Work_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Permit_To_Work_V5_2_PageObjects.Permit_To_Work_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR2 Capture Permit Acceptance - Main Scenario",
        createNewBrowserInstance = false
)
public class FR2_Capture_Permit_Acceptance_Main_Scenario extends BaseClass
{

    String error = "";

    public FR2_Capture_Permit_Acceptance_Main_Scenario()
    {

        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Permit_Acceptance())
        {
            return narrator.testFailed("Permit Acceptance Failed due to - " + error);
        }

        return narrator.finalizeTest("Permit Acceptanced Successfully");
    }

    public boolean Permit_Acceptance()
    {
        //Execution Tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.Execution_Tab()))
        {
            error = "Failed to wait for Execution Tab";
            return false;
        }

        //Execution Tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.Execution_Tab()))
        {
            error = "Failed to wait for Execution Tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Execution_Tab()))
        {
            error = "Failed to click the Execution Tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click Execution Tab");

        //Add
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.Execution_Tab_Add_Button()))
        {
            error = "Failed to wait for add button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Execution_Tab_Add_Button()))
        {
            error = "Failed to click the add button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked add button");
        //editable grid

        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.editable_grid()))
        {
            error = "Failed to wait editable grid";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Stakeholder_Type()))
        {
            error = "Failed to clickon the stakeholder type";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.Stakeholder_Type(getData("Stakeholder type"))))
        {
            error = "Failed to click on the stakeholder type";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Stakeholder_Type(getData("Stakeholder type"))))
        {
            error = "Failed to click on the stakeholder type";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Name_and_Surname()))
        {
            error = "Failed to click on the Name and Surname Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Name_and_Surname(getData("Name and Surname"))))
        {
            error = "Failed to click on the Name and Surname Drop Down name";
            return false;
        }

        String Accept = getData("Accept");
        String Declare = getData("Declare");

        if (Accept.equalsIgnoreCase("True") && Declare.equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Accept()))
            {
                error = "Failed to click on the Accept check box";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Declare()))
            {
                error = "Failed to click on the Declare check box";
                return false;
            }

        } else
        {

        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.SaveButton()))
        {
            error = "Failed to wait for Save Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.SaveButton()))
        {
            error = "Failed to click on Save Button";
            return false;
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Permit_To_Work_PageObjects.saveWait2(), 400))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Permit_To_Work_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Permit_To_Work_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        return true;
    }
}
