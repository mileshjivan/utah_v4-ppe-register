/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Permit_To_Work_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Permit_To_Work_V5_2_PageObjects.Permit_To_Work_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "UC PMW 02 02 Approve Reject Execution - Alternate Scenario 4",
        createNewBrowserInstance = false
)
public class UC_PMW_02_02_Approve_Reject_Execution_Alternate_Scenario_4 extends BaseClass
{
    String error = "";
    public UC_PMW_02_02_Approve_Reject_Execution_Alternate_Scenario_4()
    {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Execution_approval())
        {
            return narrator.testFailed("Execution Approval Failed Due - " + error);
        }
        return narrator.finalizeTest("Successfully Executed Approval");
    }

    public boolean Execution_approval()
    {
        //Execution Tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.Execution_Tab()))
        {
            error = "Failed to wait for Execution Tab";
            return false;
        }
        //Execution Tab
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Execution_Tab()))
        {
            error = "Failed to wait for Execution Tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Clicked Execution Tab");

        String approval = getData("Execution approval");
        if (!approval.equalsIgnoreCase(""))
        {
            //Execution approval
            if (!SeleniumDriverInstance.scrollToElement(Permit_To_Work_PageObjects.Execution_approval()))
            {
                error = "Failed to scroll Execution approval";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Execution_approval()))
            {
                error = "Failed to click Execution approval";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.Execution_approval_option(getData("Execution approval"))))
            {
                error = "Failed to wait Execution approval";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.Execution_approval_option(getData("Execution approval"))))
            {
                error = "Failed to click Execution approval";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Execution approval status :" + getData("Execution approval"));

        if (!SeleniumDriverInstance.waitForElementByXpath(Permit_To_Work_PageObjects.SaveButton()))
        {
            error = "Failed to wait for Save Button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Permit_To_Work_PageObjects.SaveButton()))
        {
            error = "Failed to click on Save Button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Save Button");

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Permit_To_Work_PageObjects.saveWait2(), 400))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Permit_To_Work_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Permit_To_Work_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            //return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        return true;
    }
}
