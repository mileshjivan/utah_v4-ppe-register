/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Production_Register_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Production_Register_V5_2_PageObjects.Production_Register_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR2-Update Production Measurements v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_Update_Production_Measurements_MainScenario extends BaseClass
{

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_Update_Production_Measurements_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Capture_Measurements())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully updated Production Measurements");
    }

    public boolean Capture_Measurements()
    {
        //Measurements
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.Measurements_Tab()))
        {
            error = "Failed to wait for 'Measurements' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.Measurements_Tab()))
        {
            error = "Failed to click on 'Measurements' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Measurements' button.");

        //Measure
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.Measure_Field())) {
            error = "Failed to wait for 'Measure' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Production_Register_PageObjects.Measure_Field(), getData("Measure"))) {
            error = "Failed to enter '" + getData("Measure") +"'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Measure: " + getData("Measure"));

        //Save Button
        if (!getData("Save supporting documents").equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.ProductionRegister_Save()))
            {
                error = "Failed to wait for Save Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.ProductionRegister_Save()))
            {
                error = "Failed to click on Save Button";
                return false;
            }
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.SaveSupDocs()))
            {
                error = "Failed to wait for Save supporting documents Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.SaveSupDocs()))
            {
                error = "Failed to click on Save supporting documents Button";
                return false;
            }
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Production_Register_PageObjects.saveWait2(), 400))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Production_Register_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Production_Register_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        return true;
    }

}
