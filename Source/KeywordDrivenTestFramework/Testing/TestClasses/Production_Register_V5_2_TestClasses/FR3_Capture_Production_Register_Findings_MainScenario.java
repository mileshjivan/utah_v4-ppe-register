/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Production_Register_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Production_Register_V5_2_PageObjects.Production_Register_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR3-Capture Production Register Findings v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR3_Capture_Production_Register_Findings_MainScenario extends BaseClass
{

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR3_Capture_Production_Register_Findings_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Capture_Production_Register_Findings())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured Production Register Findings");
    }

    public boolean Capture_Production_Register_Findings()
    {
        //Findings
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.Findings_Tab()))
        {
            error = "Failed to wait for 'Findings' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.Findings_Tab()))
        {
            error = "Failed to click on 'Findings' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Findings' button.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.ProductionRegisterFindings_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.ProductionRegisterFindings_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.ProductionRegisterFindings_ProcessFlow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.ProductionRegisterFindings_ProcessFlow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        
        //Finding description
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.FindingDescription_Field())) {
            error = "Failed to wait for 'Finding description' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Production_Register_PageObjects.FindingDescription_Field(), getData("Finding description"))) {
            error = "Failed to enter '" + getData("Finding description") +"'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Finding description: " + getData("Finding description"));
        
        //Finding owner 
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.FindingOwnerTab()))
        {
            error = "Failed to wait for Finding owner dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.FindingOwnerTab()))
        {
            error = "Failed to click the Finding owner dropdown.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Finding owner dropdown");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.BusinessUnit_Option(getData("Finding owner"))))
        {
            error = "Failed to wait for Finding owner Option: " + getData("Finding owner");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.BusinessUnit_Option(getData("Finding owner"))))
        {
            error = "Failed to click the Finding owner Option: " + getData("Finding owner");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Tailings facility: " + getData("Tailings facility"));
        
        //Risk source 
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.FindingSourceTab()))
        {
            error = "Failed to wait for Risk source dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.FindingSourceTab()))
        {
            error = "Failed to click the Risk source dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Finding source dropdown");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.FindingSource_Option(getData("Risk source"))))
        {
            error = "Failed to wait for Risk source Option: " + getData("Risk source");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.FindingSource_Option(getData("Risk source"))))
        {
            error = "Failed to click the Risk source Option: " + getData("Risk source");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Risk source: " + getData("Risk source"));
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.FindingSourceTab()))
        {
            error = "Failed to click the Risk source dropdown.";
            return false;
        }
        
        //Finding classification 
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.FindingClassificationTab()))
        {
            error = "Failed to wait for Finding classification dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.FindingClassificationTab()))
        {
            error = "Failed to click the Finding classification dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Finding classification dropdown");
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.BusinessUnit_Option(getData("Finding classification"))))
        {
            error = "Failed to wait for Finding classification Option: " + getData("Finding classification");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.BusinessUnit_Option(getData("Finding classification"))))
        {
            error = "Failed to click the Finding classification Option: " + getData("Finding classification");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Finding classification: " + getData("Finding classification"));

        //Save Button
        if (!getData("Save to continue").equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.ProductionRegisterFindings_Save()))
            {
                error = "Failed to wait for Save Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.ProductionRegisterFindings_Save()))
            {
                error = "Failed to click on Save Button";
                return false;
            }
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.ProductionRegisterFindings_SaveToContinue()))
            {
                error = "Failed to wait for Save to continue Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.ProductionRegisterFindings_SaveToContinue()))
            {
                error = "Failed to click on Save to continue Button";
                return false;
            }
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Production_Register_PageObjects.saveWait2(), 400))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Production_Register_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Production_Register_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");
        
        //Production Register Findings 'X'
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.close_ProductionRegisterFindings())) {
            error = "Failed to wait for 'X' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.close_ProductionRegisterFindings())) {
            error = "Failed to click on 'X' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'X' button");
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.ProductionRegister_ProcessFlow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.ProductionRegister_ProcessFlow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        return true;
    }

}
