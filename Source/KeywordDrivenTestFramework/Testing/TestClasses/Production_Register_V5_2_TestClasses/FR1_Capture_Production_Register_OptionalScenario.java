/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Production_Register_V5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Production_Register_V5_2_PageObjects.Production_Register_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Production Register v5.2 - Optional Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Production_Register_OptionalScenario extends BaseClass
{

    String parentWindow;
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Production_Register_OptionalScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Navigate_To_Production_Register())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Capture_Production_Register())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Captured Production Register");
    }

    public boolean Navigate_To_Production_Register()
    {
        //Tailings Management
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.TailingsManagement_Module()))
        {
            error = "Failed to wait for 'Tailings Management' module.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.TailingsManagement_Module()))
        {
            error = "Failed to click on 'Tailings Management' module.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Tailings Management' module.");

        //Production Register
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.ProductionRegister_Module()))
        {
            error = "Failed to wait for 'Production Register' module.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.ProductionRegister_Module()))
        {
            error = "Failed to click on 'Production Register' module.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Production Register' module.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.ProductionRegister_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.ProductionRegister_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    public boolean Capture_Production_Register()
    {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.ProductionRegister_ProcessFlow()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.ProductionRegister_ProcessFlow()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //Business Unit 
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.BusinessUnitTab()))
        {
            error = "Failed to wait for Business Unit dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.BusinessUnitTab()))
        {
            error = "Failed to click the Business Unit dropdown.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.BusinessUnit_Option(getData("Business Unit 1"))))
        {
            error = "Failed to wait for Business Unit 1 Option: " + getData("Business Unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.BusinessUnit_Option(getData("Business Unit 1"))))
        {
            error = "Failed to click the Business Unit Option: " + getData("Business Unit 1");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Business Unit: " + getData("Business Unit 1"));
        
        //Tailings facility 
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.TailingsFacilityTab()))
        {
            error = "Failed to wait for Tailings facility dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.TailingsFacilityTab()))
        {
            error = "Failed to click the Tailings facility dropdown.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.BusinessUnit_Option(getData("Tailings facility"))))
        {
            error = "Failed to wait for Tailings facility Option: " + getData("Tailings facility");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.BusinessUnit_Option(getData("Tailings facility"))))
        {
            error = "Failed to click the Tailings facility Option: " + getData("Tailings facility");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Tailings facility: " + getData("Tailings facility"));
        
        //Month 
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.MonthTab()))
        {
            error = "Failed to wait for Month dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.MonthTab()))
        {
            error = "Failed to click the Month dropdown.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.BusinessUnit_Option(getData("Month"))))
        {
            error = "Failed to wait for Month Option: " + getData("Month");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.BusinessUnit_Option(getData("Month"))))
        {
            error = "Failed to click the Month Option: " + getData("Month");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Month: " + getData("Month"));
        
        //Year 
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.YearTab()))
        {
            error = "Failed to wait for Year dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.YearTab()))
        {
            error = "Failed to click the Year dropdown.";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.BusinessUnit_Option(getData("Year"))))
        {
            error = "Failed to wait for Year Option: " + getData("Year");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.BusinessUnit_Option(getData("Year"))))
        {
            error = "Failed to click the Year Option: " + getData("Year");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Year: " + getData("Year"));
        
        //Upload a link
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.linkADoc_buttonxpath())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.linkADoc_buttonxpath())) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }
        narrator.stepPassed("Successfully click 'Link a document' button.");

        if (!SeleniumDriverInstance.switchToTabOrWindow()) {
            error = "Failed to switch tab.";
            return false;
        }

        //URL Input
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.urlInput_TextAreaxpath())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Production_Register_PageObjects.urlInput_TextAreaxpath(), getData("Document url"))) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document url: " + getData("Document url"));

        //Title Input
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.tile_TextAreaxpath())) {
            error = "Failed to wait for 'Link a document' button.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Production_Register_PageObjects.tile_TextAreaxpath(), getData("Title"))) {
            error = "Failed to click on 'Link a document' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Title: " + getData("Title"));

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.linkADoc_Add_buttonxpath())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.linkADoc_Add_buttonxpath())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        
        narrator.stepPassed("Successfully uploaded a Supporting Document.");

        if (!SeleniumDriverInstance.switchToDefaultContent()) {
            error = "Failed to switch tab.";
            return false;
        }

        //iFrame
        if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Production_Register_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully uploaded the document link.");

        //Save Button
        if (!getData("Save supporting documents").equalsIgnoreCase("Yes"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.ProductionRegister_Save()))
            {
                error = "Failed to wait for Save Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.ProductionRegister_Save()))
            {
                error = "Failed to click on Save Button";
                return false;
            }
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Production_Register_PageObjects.SaveSupDocs()))
            {
                error = "Failed to wait for Save supporting documents Button";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Production_Register_PageObjects.SaveSupDocs()))
            {
                error = "Failed to click on Save supporting documents Button";
                return false;
            }
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Production_Register_PageObjects.saveWait2(), 400))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Production_Register_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Production_Register_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        return true;
    }

}
